<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>特待生受賞者 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="在校生の方へ" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/support/">学費サポート</a></li>
				<li class="pankuzu_next">特待生受賞者</li>
			</ul>
		</div>
		<div id="mainarea" class="support_winner_content">


			<h2><img src="/common/images/support/winner/ttl_winner.png" alt="特待生受賞者" /></h2>
			<img src="/common/images/support/winner/ttl_happyou.jpg" alt="平成25年度　特待生発表" />
			<p class="txt_right"><a href="/support/exemption.php">→特待生制度についてはこちら</a></p>
			
			<img src="/common/images/support/winner/ttl_gakugyo.jpg" alt="学業優秀　賞受賞者のコメント" class="title_img" />
			<table>
			<colgroup span="1" class="sec_01"></colgroup>
			<colgroup span="1" class="sec_02"></colgroup>
			<colgroup span="1" class="sec_03"></colgroup>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_01.jpg" alt="太田 桂子さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>太田 桂子さん<span class="ttl_b">理容科2年</span></h3>
			<p>
			今回、学業優秀賞の認定をいただいたことを、とても嬉しく思います。11月に行われた全国学生技術大会に出場できたことも大きな励みとなっていました。<br>
			技術練習優先で過ごしてきたので、学科の方は授業中に弱点を見つけて覚えることと、課題を提出することで精一杯でした。<br>
			毎日、先生方のご指導をいただいて、周囲の応援もあったのに、全国大会で入賞が叶わなかったことは本当に残念です。<br>
			しかし、大会選手として、先生や理容科のみんな、クラスメイトからいろいろな形で支援をもらっていたことを思い出すと、とても幸せな学校生活を送れたと思います。<br>
			一人では練習にも身が入らない場合が多いですが、いつも一緒に練習する仲間がいて、勉強もお互いに相談する相手がいる環境でした。<br>
			今回の受賞も、周囲の力添えのおかげと感謝しています。次に控えている国家試験に向けても気を抜かずに、残り少ない学校生活を日々大切に過ごしていきたいと思います。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_02.jpg" alt="佐生 大さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>佐生 大さん<span>（富山県立滑川高等学校出身）</span><span class="ttl_b">理容科2年</span></span></h3>
			<p>
			今回の受賞は、とにかく驚きでした。今まで学業優秀賞に選ばれることが1度もなかったので、今年は今までで一番努力した年だったのかなと思いました。<br>
			自分の努力だけでなく、学校の先生方が、丁寧に勉強や技術を教えてくださったからだと思います。また、友達の支えがあったから、今まで頑張ることができたと思います。<br>
			感謝の気持ちでいっぱいです。今回の受賞で満足せずに、これから国家試験も控えているので、気を抜かず、最後の学生生活を過ごしたいと思います。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_03.jpg" alt="古家 汐里さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>古家 汐里さん<span>（富山県立富山西高等学校出身）</span><span class="ttl_b">理容科2年</span></span></h3>
			<p>
			今回、このような賞をいただけて、とても光栄に思います。学校生活ではくじけそうなこともたくさんありましたが、支えてくれる友人がいたからこそ辛いことを乗り越えてくることができました。<br>
			また、先生方のご指導もあり、自分の技術を高めることができました。厳しいご指導があったからこそ自分に厳しくすることができ、学校生活最後の大会では結果を残すことができてよかったです。先生方には感謝しています。<br>
			将来は、自分でお店を開いて理容師として頑張ります。ありがとうございました。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_04.jpg" alt="森田 佑奈さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>森田 佑奈さん<span>（岐阜県立飛騨高山高等学校出身）</span><span class="ttl_b">美容科2年</span></h3>
			<p>
			昨年に引き続き、学業優秀賞に選んでいただき、とても嬉しく思います。昨年いただいた時、そのことを両親に伝えたところ、とても喜んでくれて、がんばってよかったと思いました。<br>
			二年生になった時もワインディングを頑張って学生大会校内予選を突破し、信越北陸地区大会に出場できたり、レジーナフォトコンテストで準入選することができたりと、成果を出せたので達成感がありました。<br>
			今年、賞を頂いた時も両親はとても喜んでくれました。親元を離れて富山で暮らしていて、両親には大変な思いをさせたと思うので、親孝行ができたかなと嬉しく思います。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_05.jpg" alt="清水 理紗子さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>清水 理紗子さん<span>（富山第一高等学校出身）</span><span class="ttl_b">理容科1年</span></h3>
			<p>
			自分の名前を呼ばれたときは、とてもびっくりして少し叫んでしまいました。<br>
			しかし、それ以上にクラスのみんながびっくりしていて、まるで打ち合わせたかのように「えー！」「すごい、おめでとう！」と言ってくれました。<br>
			落ち着いてからは、嬉しさがこみ上げてきてコツコツ努力してきてよかったと思いました。<br>
			これで少しは親孝行ができたかなと思います。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_06.jpg" alt="菓子 茉穂さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>菓子 茉穂さん<span>（富山県立小杉高等学校出身）</span><span class="ttl_b">美容科1年</span></h3>
			<p>
			入学してから8ヶ月間、何をするにしても手際の悪かった私が、ワインディングコンクールで結果を残すことができ、学業優秀賞に選んでいただきました。<br>
			先生方や、放課後は先輩方が教えてくださったおかげだと思います。とても感謝しています。これからも学科100点、実習A成績を目指して頑張ります！</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_07.jpg" alt="中舘 沙也香さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>中舘 沙也香さん<span class="ttl_b">美容科1年</span></h3>
			<p>
			資格・就職に強いということで、何度もオープンキャンパスに通って、富山県理容美容専門学校に入学しました。学校内の雰囲気もとてもよく、熱心な先生方にも恵まれています。<br>
			自分から、意欲的に行動することを心がけてきました。特に実技では細かくノートをとり、わからないところや疑問に思ったことは、積極的に先生方に聞くようにしています。<br>
			今後は、学科全科目満点、実技はこれまで以上に毎日の努力を忘れず、確実に自分を伸ばしていきたいと思います。来年度のニューヨーク研修に行けるように頑張ります！！
			</p>
			</td>
			</tr>
			</table>
			
			<img src="/common/images/support/winner/ttl_zenko.jpg" alt="善行奨励賞　受賞者のコメント" class="title_img" />
			<table>
			<colgroup span="1" class="sec_01"></colgroup>
			<colgroup span="1" class="sec_02"></colgroup>
			<colgroup span="1" class="sec_03"></colgroup>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_08.jpg" alt="森田 航平さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>森田 航平さん<span>（岐阜県立飛騨高山高等学校出身）</span><span class="ttl_b">美容科2年</span></h3>
			<p>この度、善行奨励賞をいただきありがとうございます。二年生に入ってからは実技で覚える内容も多く、濃くなっていき、うまくできなかったり、戸惑うことも多くありました。<br>
			しかし、そんなときは友人がサポートや助言をして支えてくれたり、先生方の手厚い指導があったからこそ、今の自分の成長につながったと実感しています。<br>
			この二年間はたくさんの人に支えてもらったので、常に感謝の気持ちを大切にしていきたいです。卒業後は、学校生活で学んだことを十分に活かして、夢の独立を目指して頑張ります！</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_09.jpg" alt="狩野 千秋さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>狩野 千秋さん<span>（富山県立富山西高等学校出身）</span><span class="ttl_b">美容科2年</span></h3>
			<p>善行奨励賞表彰のとき、名前を呼ばれたことが不意でびっくりしました。善行奨励賞に選ばれるべく授業を真面目に受けていた一年生の頃がありました。<br>
			そのときは選ばれず残念な気分になったことを思い出します。<br>
			二年生になってからは、善行奨励賞制度のことを意識せず生活していました。コンテスト、大会で入選できたわけでないのに、なぜ選ばれたのかと疑問に思いましたが、一つ思ったのは『美容に興味を持っているから』ということです。<br>
			授業で勉強させられているとは感じず、ただ知りたいと思えることが賞につながったのではないかと思います。それが形に残ったようで、嬉しく思います。</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_10.jpg" alt="山本 琴乃さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>山本 琴乃さん<span>（富山県立雄山高等学校出身）</span><span class="ttl_b">美容科1年</span></h3>
			<p>まさか私が善行奨励賞に選ばれるとは思いもしなかったので、本当に驚きました。また、とても嬉しかったです。ありがとうございます。入学してから、無遅刻・無欠席・無早退を心がけてきました。<br>
			勉強もテストが近づくと、一生懸命覚えるよう努力してきました。しかし、実習になると私は、まだまだ力不足で頑張らなければならないところが山ほどあります。<br>
			社会に出ても教われることもあれば、今しか教われないこともあると思うので、この学校生活をもっと有意義に過ごすために先生や先輩、友達によいところを教わっていきたいです。あと一年間、どう過ごすかで世界がガラリと変わってくると思います。<br>
			ですから、私は『できない』や『無理』で切り捨てず、まずは挑戦して自分の出来ることを精一杯の力を出していきたいです。<br>
			また、善行奨励賞に選んでいただけるよう頑張りたいと思います。</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_11.jpg" alt="南 星来さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>南 星来さん<span>（富山県立富山商業高等学校出身）</span><span class="ttl_b">理容科1年</span></h3>
			<p>私は、入学してから今までの1年間のあいだで善行奨励賞に認定していただきました。正直いって、選ばれるなんて思ってもいなかったので、とても驚きました。<br>
			入学してから善行奨励賞制度があることは知っていましたが、あまり意識したことはありませんでした。授業では、次々と新しいことを学んでいき、毎回の授業でついていけるように、ノートに見やすく分かりやすくまとめたり、家で復習したりとしていました。<br>
			また、自分の都合が合うときには、学校のオープンキャンパスに参加したり、絶対に学校は休まないようにしました。そのためにも、自分の体調管理には気をつけるようにしました。<br>
			定期的にあるテストも気を抜かず、きちんと勉強をし、自分の満足のいく点数になるよう努力もしました。もちろん、善行奨励賞として選ばれたのも嬉しいですが、何よりもこれらを続けていくことが大切だと思います。<br>
			社会に出ても恥ずかしくない大人になれるように日々精進していきたいです。</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_12.jpg" alt="橘 桃加さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>橘 桃加さん<span>（富山県立上市高等学校出身）</span><span class="ttl_b">美容科1年</span></h3>
			<p>
			善行奨励賞という賞をいただき、とても嬉しい気持ちでいっぱいです。自分自身、賞をいただけると思っていなかったので、名前を呼ばれたときはびっくりしました。<br>
			この一年で、一番大変だったのがワインディングです。思うようにならず苦労しました。<br>
			また、オープンキャンパスでは日頃体験できないことが体験できたり、先輩たちから学んだことも多く、とても充実していました。これからも日々努力し、もっと上を目指し、二年次で賞が取れるように、今まで以上に何事も積極的に取り組んでいきたいです。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_13.jpg" alt="眞田 裕里さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>眞田 裕里さん<span>（富山県立富山商業高等学校出身）</span><span class="ttl_b">美容科1年</span></h3>
			<p>
			今回、このような賞をいただき、とても嬉しく思います。今年は、ワインディングをとても頑張りました。<br>
			私は、ワインディングを通して、努力することの大切さを学びました。ワインディングコンクールで優勝することができ、大変嬉しいです。<br>
			しかし、ゴールをした瞬間からスタートしているので、さらに完成度を高くし、全国大会に向けて頑張りたいです。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_14.jpg" alt="岩瀧 晴香さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>岩瀧 晴香さん<span>（富山県立南砺福光高等学校出身）</span><span class="ttl_b">美容科1年</span></h3>
			<p>
			今回、善行奨励賞になることができ、本当に嬉しいです。それに、私がこのような賞をもらえると思っていなかったので、驚いています。<br>
			この学校に入学してからは、毎日があっという間なので、いつも授業を大切に受けています。<br>
			私は、そのために学校を休まないようにしてきました。また、いつも私の夢を応援してくれる家族にすごく感謝しています。<br>
			その応援が私の励みになっているので、これからも頑張っていきたいです。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_15.jpg" alt="砂 ひとみさん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>砂 ひとみさん<span>（富山県立伏木高等学校出身）</span><span class="ttl_b">エステティック科1年</span></h3>
			<p>
			このような素晴らしい賞を受賞することができ、本当に嬉しいです。<br>
			この一年間は、無遅刻・無欠席と授業を真面目に受けることを目標に、オープンキャンパスや生徒会活動にも積極的に参加しました。<br>
			頑張ったことを認めていただいて、本当に良かったです。そして、先生や家族、クラスメイトにおめでとうと言われて嬉しかったです。
			</p>
			</td>
			</tr>
			<tr>
			<td>
			<img src="/common/images/support/winner/photo_16.jpg" alt="中舘 沙也香さん" />
			</td>
			<td>&nbsp;</td>
			<td>
			<h3>鍋山 梨菜さん<span class="ttl_b">エステティック科1年</span></h3>
			<p>
			このような素晴らしい賞をいただき、大変光栄に思います。今回の受賞は、熱心にご指導していただいた先生方や、クラスのみんなが支えてくれたおかげです。<br>
			就職した際にも、一層努力していきたいです。本当にありがとうございました。
			</p>
			</td>
			</tr>
			</table>



		</div><!-- /display_none end -->

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>