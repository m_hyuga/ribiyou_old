<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学費支援制度 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="学費サポート" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/support">学費サポート</a></li>
				<li class="pankuzu_next">学費支援制度</li>
			</ul>
		</div>
		<div id="mainarea" class="support_content">
			<div class="support_sec_03">
				<h2><img src="/common/images/support/h2_s_system_01.gif" alt="学費支援制度" /></h2>
				<p>
				進学時のまとまった出費や学費について、国の奨学金制度「日本学生支援機構奨学制度」を利用しながら、<br>
				学費を分割払いしていただける学費支援制度です。<br>
				高校在学中からお申込いただくことで、入学金・教育実習費のみ納入で入学が可能です。
				</p>
			</div><!--support_sec_03-->
			<div class="support_sec_03">
				<h3><img src="/common/images/support/h3_s_system_01.gif" alt="対象者"></h3>
				<p>
				理容科、美容科、トータルビューティ科<br>
				日本学生支援機構の奨学生採用候補者（第二種奨学金の月額「10万円以上」）に決定している方<br>
				（もしくは現在申請中の方）
				</p>
			</div><!--support_sec_03-->
			<div class="support_sec_03">
				<h3><img src="/common/images/support/h3_s_system_02.gif" alt="手続きの流れ (高校在学中にお申し込みください。)"></h3>
				<ul id="system_flow">
					<li class="f01">
						<h4><img src="/common/images/support/flow_s_system_01.gif" alt="STEP1 申し込み"></h4>
						<p>日本学生支援機構の予約奨学金へ。在学されている高校から奨学金担当の先生にご相談の上、お申し込み下さい。</p>
					</li>
					<li class="f01">
						<h4><img src="/common/images/support/flow_s_system_02.gif" alt="STEP2 本校へ出願"></h4>
						<p>「富山理美容専門学校学費特別応援制度」の申請書類と日本学生支援機構の「採用候補者決定通知」のコピーを願書に同封して下さい。</p>
					</li>
					<li class="f02">
						<h4><img src="/common/images/support/flow_s_system_03.gif" alt="STEP3 本校受験"></h4>
						<p style="text-align:center;">合格</p>
					</li>
					<li class="f01">
						<h4><img src="/common/images/support/flow_s_system_04.gif" alt="STEP4 入学手続き"></h4>
						<p>入学金と実習器具費を納入いただき、その他の学納金についての分割納入手続きを行います。</p>
					</li>
					<li class="f03">
						<h4><img src="/common/images/support/flow_s_system_05.gif" alt="STEP5 分割払い開始"></h4>
						<p>奨学金の振込後、毎月授業料を分割していきます。</p>
					</li>
				</ul>
			</div><!--support_sec_03-->


		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>