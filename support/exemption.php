<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>授業料免除制度 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="学費サポート" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/support">学費サポート</a></li>
				<li class="pankuzu_next">授業料免除制度</li>
			</ul>
		</div>
		<div id="mainarea" class="support_content">
			<div class="support_sec_03">
				<h2><img src="/common/images/support/h2_s_exemption_01.gif" alt="授業料免除制度" /></h2>
				<p>意欲ある優秀な生徒の授業料の免除や、経済的な理由で学生生活に支障をきたす生徒の皆さんの学費の一部を免除する制度です。</p>
			</div><!--support_sec_03-->
			<div class="support_sec_03">
				<h3><img src="/common/images/support/h3_s_exemption_01.gif" alt="特待生制度"></h3>
				<p>本校では、学業成績ならびに日々の行いが特に優秀な学生を表彰する特待生制度を設け、意欲的に学ぼうとする学生を支援しています。特待生に選ばれることは学生として栄誉なことです。努力次第で誰にでもチャンスがあります。</p>
				<ul id="exemption_01">
					<li class="exem01">
						<p class="point">・学業優秀賞：免除額20万円</p>
						<p>　特に成績が優秀な生徒に学費の一部を免除する。</p>
					</li>
					<li class="exem02">
						<p class="point">・善行奨励賞：免除額10万円</p>
						<p>　学業優秀で模範となる善行生徒に学費の一部を免除する。</p>
					</li>
				</ul>
				<table>
					<tr>
						<th class="h01" rowspan="2">対象者</th>
						<th class="h01" colspan="2">学業優秀賞</th>
						<th class="h01" colspan="2">善行奨励賞</th>
					</tr>
					<tr>
						<th class="h01" width="90">免除額</th>
						<th class="h01" width="90">人数</th>
						<th class="h01" width="90">免除額</th>
						<th class="h01" width="90">人数</th>
					</tr>
					<tr>
						<th class="h02">理容科</th>
						<td class="d01" rowspan="2">20万円</td>
						<td class="d01" rowspan="2">1年次/4名程度<br>2年次/5名程度</td>
						<td class="d01" rowspan="2">10万円</td>
						<td class="d01" rowspan="2">1年次/10名程度<br>2年次/13名程度</td>
					</tr>
					<tr>
						<th class="h02">美容科</th>
					</tr>
					<tr>
						<th class="h02">エステティック科</th>
						<td class="d01" rowspan="2">20万円</td>
						<td class="d01" rowspan="2">若干名</td>
						<td class="d01" rowspan="2">10万円</td>
						<td class="d01" rowspan="2">若干名</td>
					</tr>
					<tr>
						<th class="h02">トータルビューティ科</th>
					</tr>
				</table>
			</div><!--support_sec_03-->
			<div class="support_sec_03">
				<h3><img src="/common/images/support/h3_s_exemption_02.gif" alt="緊急支援型奨学金制度"></h3>
				<p>人物、学業成績ともに優秀でありながら、経済的理由で学生生活に支障をきたす恐れのある学生を支援する制度です。</p>
				<table>
					<tr>
						<th class="h01">奨学金の種類</th>
						<td>緊急支援型奨学金</td>
					</tr>
					<tr>
						<th class="h01">対象資格</th>
						<td>特待生または準特待生であり、何らかの理由で授業料等の納入が困難と判断された方</td>
					</tr>
					<tr>
						<th class="h01">対象者数</th>
						<td>若干名</td>
					</tr>
					<tr>
						<th class="h01">返還の有無</th>
						<td>返還義務はありません</td>
					</tr>
					<tr>
						<th class="h01">奨学金額</th>
						<td>1年分の学費を限度(授業料減免)</td>
					</tr>
				</table>
			</div><!--support_sec_03-->

		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>