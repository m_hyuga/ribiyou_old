<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学費サポート | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="学費サポート" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">学費サポート</li>
			</ul>
		</div>
		<div id="mainarea" class="support_content">
			<h2><img src="/common/images/support/h2_support_01.gif" alt="奨学金で対応可能に!入学時の学費0円サポート！!" /></h2>
			<div class="support_sec_01">
				<h3><img src="/common/images/support/h3_support_01.gif" alt="通常の入学時の学費納入　合格>入学時納入金43万円>入学"></h3>
				<h3><img src="/common/images/support/h3_support_02.gif" alt="入学時0円サポートを利用した場合　合格>入学時納入金0円>入学>6月末、奨学金支給>分割納入43万円"></h3>
				<p class="img01"><img src="/common/images/support/h3_support_02_1.png" alt="「日本学生支援機構」の予約採用申し込み者対象！"></p>
				<p class="img02"><img src="/common/images/support/h3_support_02_2.png" alt="奨学金で対応可能に!入学時の学費0円サポート！!の画像"></p>
			</div><!--support_sec_01-->
			<div class="support_sec_02">
				<h3><img src="/common/images/support/h3_support_03.gif" alt="対象者の条件 ※原則として下記の1〜3の条件を満たす者（3は①〜③のいずれか）"></h3>
				<ol>
					<li>高等学校在学中に「日本学生支援機構の予約採用」を申込している者。</li>
					<li>第1回および第2回の申込期間で予約採用を申込している者（第1回4月〜6月・第2回10月）</li>
					<li>①高校在学中に『第二種奨学金の月10万円以上（第一種との併用可）との予約採用を申込している者。<br>②高校在学中に『第一種奨学金もしくは第二種奨学金の月8万円以下』と<br>『入学時特別増額』の予約申込している者</li>
				</ol>
				<p class="point"><span class="tbb_red">※</span> 学費サポートについて詳しくはお問い合わせ下さい</p>
				<ul>
					<li>
						<h4><a href="system.php"><img src="/common/images/support/btn_support_system.gif" alt="学費支援制度"></a></h4>
						<p>入学時の経済的負担を軽減する富山県理美容専門学校オリジナルのサポート制度です。</p>						
					</li>
					<li>
						<h4><a href="exemption.php"><img src="/common/images/support/btn_support_exemption.gif" alt="授業料免除制度"></a></h4>
						<p>意欲ある優秀な学生に授業料を免除、また、経済的な理由で学生生活に支障をきたす生徒に対する学費の一部免除制度です。</p>						
					</li>
					<li>
						<h4><a href="loan.php"><img src="/common/images/support/btn_support_loan.gif" alt="教育ローン"></a></h4>
						<p>進学の際のまとまった出費や学納金について、各種学資サポートをご利用いただけます。</p>						
					</li>
					<li>
						<h4><a href="winner.php"><img src="/common/images/support/btn_support_winner.gif" alt="特待生受賞者"></a></h4>
						<p>本校では、学業成績ならびに日々の行いが特に優秀な学生を表彰する特待生制度を設け、意欲的に学ぼうとする学生を支援しています。</p>						
					</li>
					<li>
						<h4><a href="hojo.php"><img src="/common/images/support/btn_support_hojo.gif" alt="家賃補助制度"></a></h4>
						<p>平成27年度以降の入学生の方で、県外または県内の通学困難な方の家賃を補助する制度です。</p>						
					</li>
					<li>
					&nbsp;				
					</li>
				</ul>
			</div><!--support_sec_02-->
		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>