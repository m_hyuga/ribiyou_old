<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>家賃補助制度 | 学費サポート | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="学費サポート" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/support">学費サポート</a></li>
				<li class="pankuzu_next">家賃補助制度</li>
			</ul>
		</div>
		<div id="mainarea" class="support_hojo_content">
			<div>
			<img src="/common/images/support/img_hojo.jpg" alt="家賃補助制度スタート"/>
			</div>
		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>