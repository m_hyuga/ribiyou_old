<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>教育ローン | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/support.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/support/img_main.jpg" alt="学費サポート" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
					<dt><a href="/support/"><img src="/common/images/common/sidenav/support/nav_support.png" alt="学費サポート" /></a></dt>
					<dd><a href="/support/system.php"><img src="/common/images/common/sidenav/support/nav_support_system.png" alt="学費支援制度" /></a></dd>
					<dd><a href="/support/exemption.php"><img src="/common/images/common/sidenav/support/nav_support_exemption.png" alt="授業料免除制度" /></a></dd>
					<dd><a href="/support/loan.php"><img src="/common/images/common/sidenav/support/nav_support_loan.png" alt="教育ローン" /></a></dd>
					<dd><a href="/support/winner.php"><img src="/common/images/common/sidenav/support/nav_support_winner.png" alt="特待生受賞者" /></a></dd>
					<dd><a href="/support/hojo.php"><img src="/common/images/common/sidenav/support/nav_support_hojo.png" alt="家賃補助制度" /></a></dd>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/support">学費サポート</a></li>
				<li class="pankuzu_next">教育ローン</li>
			</ul>
		</div>
		<div id="mainarea" class="support_content">
			<div class="support_sec_03">
				<h2><img src="/common/images/support/h2_s_loan_01.gif" alt="教育ローン" /></h2>
				<p>進学時のまとまった出費や学費についての各種学資サポートです。<br>入学前からお申込み出来るものもあります。</p>
				<table>
					<tr>
						<th class="h01">&nbsp;</th>
						<th class="h03" width="215"><span class="tit_head">国の奨学金制度</span><br>日本学生支援機構奨学制度</th>
						<th class="h03" width="205"><span class="tit_head">日本学生支援機構奨学制度</span><br>日本政策金融公庫</th>
						<th class="h03" width="225"><span class="tit_head">学校提携教育ローン</span><br>オリエントコーポレーション</th>
					</tr>
					<tr>
						<th class="h01">特　徴</th>
						<td>
							<ul>
								<li>学生本人の申込なので、ご家族の負担が減らせる！</li>
								<li>在学期間は返済が免除</li>
								<li>保証人がない場合も可能<br>（機関保証）</li>
								<li>採用枠が限定されていますが、入学前から申込ができるので気になる方は高校に問い合わせを！</li>
							</ul>
						</td>
						<td>
							<ul>
								<li>原則親が申込</li>
								<li>まとまった金額が必要な時に！</li>
								<li>低金利で安心の教育ローン</li>
							</ul>
						</td>
						<td>
							<ul>
								<li>20歳以上であれば、本人も申込可能</li>
								<li>さまざまな返済方法が選べる！</li>
								<li>入学前から利用可能</li>
								<li>金融機関窓口に行かなくても申込可能</li>
								<li>何度でも利用可能</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th class="h01">対　象</th>
						<td>理容科、美容科、<br>トータルビューティ科<br>本校への入学予定者<br>(予約採用制度※1)、<br>及び在校生年齢制限なし</td>
						<td>本校への入学予定者、及び在校生<br>年齢制限
							<ul>
								<li>申込者が学生の場合、20歳以上</li>
								<li>申込者が保護者の場合、制限あり</li>
							</ul>
						</td>
						<td>本校への入学予定者、及び在校生<br>年齢制限
							<ul>
								<li>申込者が学生の場合、20歳以上</li>
								<li>申込者が保護者の場合、制限あり</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th class="h01">融資金額</th>
						<td>
							<span class="tbb_red">第一種</span><br>
							自宅通学…<br>
							月額 30,000円または 53,000円<br>
							自宅外通学…<br>
							月額 30,000円または 60,000円<br>
							<span class="tbb_red">第二種</span><br>
							選択制(融資は、在学期間)<br>
							月額<br>
							30,000円／50,000円／80,000円／<br>
							100,000円／120,000円
						</td>
						<td>学生1名につき 3,000,000円以内<br>（世帯年収条件あり）</td>
						<td>10万円〜</td>
					</tr>
					<tr>
						<th class="h01">利　息</th>
						<td>
							<span class="tbb_red">第一種</span>‥無利子<br>
							<span class="tbb_red">第二種</span>‥上限3.0%
						</td>
						<td>固定(一般)　2.65%</td>
						<td>固定(定型ローン)　4.5%</td>
					</tr>
					<tr>
						<th class="h01">返済期間</th>
						<td>
							在学2年間の場合<br>
							9、12、13、15年
						</td>
						<td>
							15年以内<br>
							（交通遺児家庭または母子家庭の方は18年以内に延長）
						</td>
						<td>お借入金額により異なる<br>（追加のご利用も可能）</td>
					</tr>
					<tr>
						<th class="h01">お問い合わせ先</th>
						<td>
							日本学生支援機構<br>
							奨学事業相談センター<br>
							【TEL】0570-03-7240<br>
							月～金<br>
							(土日、祝日、年末年始を除く)<br><br>
							<span class="tbb_red"><a href="http://www.jasso.go.jp/" target="_blank">日本学生支援機構ホームページ</a></span><br>
						</td>
						<td>
							日本政策金融公庫<br>
							教育ローンコールセンター<br>
							【TEL】0570-00-8656<br>
							TEL0570-00-8656(ﾅﾋﾞﾀﾞｲﾔﾙ)<br>
							受付時間:月～金曜日9:00～21:00<br>
							土曜日9:00～17:00<br>
							(日、祝日、年末年始<br>
							(12月31日～1月3日)を除く)<br>
							お客様が加入されている電話で<br>
							ご利用いただけない場合は、<br>
							03(5321)8656におかけください。<br><br>
							<span class="tbb_red"><a href="https://www.jfc.go.jp/n/finance/search/ippan.html" target="_blank">日本政策金融公庫ホームページ</a></span>
						</td>
						<td>
							オリエントコーポレーション<br>
							オリコ学費サポートデスク<br>
							【TEL】0120-517-325<br>
							受付時間:9:30～17:30<br>
							（土・日・祝日も受付）<br><br>
							<span class="tbb_red"><a href="http://www.orico.tv/gakuhi/" target="_blank">オリコホームページ</a></span>
						</td>
					</tr>
				</table>
				<p class="point">※上記以外にも、国や県、各市町村団体が行っている奨学金制度もあります。</p>
				<p><span class="point">※1</span>.予約採用制度（日本学生支援機構）本校へ入学後の採用枠(在学採用制度)には限りがありますので、<br><span class="point">　　</span>高校3年生の4月から5月にかけて高等学校で受付をお願いします。</p>
			</div><!--support_sec_03-->

		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>