<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>採用担当の方へ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/placement/img_main.jpg" alt="採用担当の方へ" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href=""><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">採用担当の方へ</li>
			</ul>
		</div>
		<div id="mainarea" class="placement_content">
				<p>
				4月1日より就職活動を開始いたします。<br>
				平成28年卒業生を対象に求人票を受付いたします。<br>
				多くの企業からの求人お待ちしております。</p>
				<br>
				<br>
				<h2><img src="/common/images/placement/ttl_download.png" alt="スケジュール" /></h2>
				<p>
				下記のボタンから求人票をダウンロードいただけます。<br>
				必要事項をご記入の上、郵送またはFAX（<span class="txt_pink">076-432-3046</span>）にてご返信ください。
				</p>
				<ul class="placement_list cf">
					<li class="left"><a href="/common/images/placement/placement_barber.pdf" target="_blank"><img src="/common/images/placement/btn_pdf_barber.png" alt="pdfダウンロード" class="over" /></a></li>
					<li class="right"><a href="/common/images/placement/placement_barber.xls" target="_blank"><img src="/common/images/placement/btn_excel_barber.png" alt="Excelダウンロード" class="over" /></a></li>
					<li class="left"><a href="/common/images/placement/placement_beauty.pdf" target="_blank"><img src="/common/images/placement/btn_pdf_beauty.png" alt="pdfダウンロード" class="over" /></a></li>
					<li class="right"><a href="/common/images/placement/placement_beauty.xls" target="_blank"><img src="/common/images/placement/btn_excel_beauty.png" alt="Excelダウンロード" class="over" /></a></li>
					<li class="left"><a href="/common/images/placement/placement_totalbeauty.pdf" target="_blank"><img src="/common/images/placement/btn_pdf_tb.png" alt="pdfダウンロード" class="over" /></a></li>
					<li class="right"><a href="/common/images/placement/placement_totalbeauty.xls" target="_blank"><img src="/common/images/placement/btn_excel_tb.png" alt="Excelダウンロード" class="over" /></a></li>
					<li class="left"><a href="/common/images/placement/placement_esthetic.pdf" target="_blank"><img src="/common/images/placement/btn_pdf_esthetic.png" alt="pdfダウンロード" class="over" /></a></li>
					<li class="right"><a href="/common/images/placement/placement_esthetic.xls" target="_blank"><img src="/common/images/placement/btn_excel_esthetic.png" alt="Excelダウンロード" class="over" /></a></li>
				</ul>
				
				<div class="placement_box">
				<h3><img src="/common/images/placement/ttl_to.png" alt="送付先・お問い合わせ"></h3>
					<dl class="cf">
						<dt>
							<img src="/common/images/placement/ttl_ribiyo.png" alt="">
						</dt>
						<dd>
							<p>
							<span class="btn_tel">076-432-3037</span><span class="btn_call">0120-190-135</span><br>
							<span class="btn_fax">076-432-3046</span><br>
							<a href="/inquiry/"><img src="/common/images/placement/btn_inquiry.png" alt="お問い合わせ・資料請求フォームはこちら" class="over" /></a>
							</p>
						</dd>
					</dl>
				</div>

			</div><!-- /display_none end -->

		</div>
	</div>
		<?php
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>