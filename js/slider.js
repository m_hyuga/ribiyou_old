$(document).ready(function(){
  var slider = $('#slide #inner').bxSlider({
    controls: true,
    displaySlideQty: 3,
	moveSlideQty: 1,
	auto: true,
	speed: 1000,
	pause: 5000,
	nextSelector: '#slide-next',
	prevSelector: '#slide-prev',
	nextText: '<img src="/images/top/slide_arrow_r.png" alt="次へ">',
	prevText: '<img src="/images/top/slide_arrow_l.png" alt="前へ">',
	pagerCustom: '#thumbnail ul',
	easing: 'easeOutQuart',
	slideMargin: 4
  });
});