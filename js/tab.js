// JavaScript Document

function tabChange(tabNo) {
    var i;
    var tabCount = 4;
    
    /*
    * すべてのタブ名とタブ内部を一度非選択にする
    */
    for (i = 1; i <= tabCount; i++) {
        document.getElementById("tabsel" + i).className = "tab_selector";
        document.getElementById("tab" + i).className = "noshow";
    }
    
    /*
    * 選択されたタブ名とタブ内部を選択状態にする。
    */
    //選択されたタブ名のclass属性をtab_selector selectedに変える。
    document.getElementById("tabsel" + tabNo).className="tab_selector1";
    
    //選択されたタブ内部のclass属性をtabに変える。
    document.getElementById("tab" + tabNo).className="tab";
}