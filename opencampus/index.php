<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/opencampus.css" />
<link rel="stylesheet" type="text/css" href="/common/css/colorbox.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.colorbox-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".opencampus_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/opencampus/img_main.jpg" alt="オープンキャンパス" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">オープンキャンパス</li>
			</ul>
		</div>
		<div id="mainarea" class="opencampus_content">
			<div class="opencampus_sec_01">
				<h2><img src="/common/images/opencampus/ttl_open2014.png" alt="Open Campus 2014 申し込み受付中" /></h2>
				<p class="top_comment">富山県理容美容専門学校のオープンキャンパスは、セミナー、スペシャル体験など‥多彩なメューをご用意しています！是非、実際に見て・触れて・体験して、あなたの夢をもっと近くに感じてください！</p>
			</div>

			<div class="opencampus_sec_02">
				<h2><img src="/common/images/opencampus/ttl_schedule.png" alt="スケジュール" /></h2>
				<p>
				サロン現場体験、理容師・美容師免許徹底解説、ヘアメイクアーティストを目指せ！　などなど…<br>
				県理美のオープンキャンパスは盛り沢山！詳細は都度公開します！
				</p>
				<div>
				<table>
					<tr>
						<th>1月</th>
						<td>
							<dl class="cfi">
								<dt class="shinro">24日(土)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>2月</th>
						<td>
							<dl class="cfi">
								<dt class="shinro">14日(土)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>3月</th>
						<td>
							<dl class="cfi">
								<dt>14日(土)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
								<dt>26日(木)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>4月</th>
						<td>
							<dl class="cfi">
								<dt>26日(日)</dt>
								<dd><a href=".deta0426"><img src="/common/images/opencampus/ico_shosai_01.png" alt="詳細" class="over" /></a></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>5月</th>
						<td>
							<dl class="cfi">
								<dt>23日(土)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>6月</th>
						<td>
							<dl class="cfi">
								<dt>6日(日)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
								<dt>21日(日)</dt>
								<dd><img src="/common/images/opencampus/ico_shosai_02.png" alt="詳細" /></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th>7月</th>
						<td>&nbsp;
							
						</td>
					</tr>
					<tr>
						<th>8月</th>
						<td>&nbsp;
							
						</td>
					</tr>
					<tr>
						<th>9月</th>
						<td>&nbsp;
							
						</td>
					</tr>
					<tr>
						<th>10月</th>
						<td>&nbsp;
							
						</td>
					</tr>
					<tr>
						<th>11月</th>
						<td>&nbsp;
							
						</td>
					</tr>
					<tr>
						<th>12月</th>
						<td>&nbsp;
							
						</td>
					</tr>
				</table>
				<p class="img_01"><img src="/common/images/opencampus/img_suke_01.jpg" alt="" /></p>
				<p class="img_02"><img src="/common/images/opencampus/img_suke_02.png" alt="" /></p>
				</div>
			</div>

			<div class="opencampus_sec_03">
				<h2><img src="/common/images/opencampus/ttl_menu.png" alt="当日メニュー" /></h2>
				<img src="/common/images/opencampus/img_menu.png" alt="" />
			</div>

			<div class="display_none">




				<div class="shosai deta0426"><!-- ここから詳細4月26日 -->
					<div class="detail">
						<div class="content">
							<h2>2015年4月26日(木)開催概要</h2>
							<p>県理美の特色や実習・授業がよくわかる！<br>美容の仕事、理容の仕事、エステやネイル・メイクの仕事に<br>少しでも興味があったら、お気軽にご参加ください。</p>
							<div class="dotted_line"></div>
							<dl>
								<dt>受付場所</dt>
								<dd>富山県理容美容専門学校　正面玄関</dd>
								<dt>持ち物</dt>
								<dd>上履き</dd>
								<dt>受付時間</dt>
								<dd>9：30</dd>
								<dt>開始時間</dt>
								<dd>10：00</dd>
								<dt>終了時間</dt>
								<dd>12：30</dd>
							</dl>
							<a href="http://www.toyama-bb.ac.jp/opencampus/contact_20150426_o.php" class="btn_entry" target="_blank"><img src="/common/images/opencampus/btn_entry.png" alt="お申し込みはこちらから" class="over" /></a>
						</div>
					</div>
				</div><!-- ここまで詳細4月26日 -->








			</div><!-- /display_none end -->

		</div>
	</div>
		<?php
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>