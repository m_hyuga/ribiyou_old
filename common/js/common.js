$(function () {
	//------------------------ナビの背景
	$('header nav .navigation>li').hover(function(){
		$(this).stop().animate( { backgroundColor: "#ee66a5" }, 300 );},
		function(){$(this).stop().animate( { backgroundColor: "#f28cbb" }, 250 );
	});
	$('#sidenavi .side_navigation dt a').hover(function(){
		$('#sidenavi .side_navigation dt').stop().animate( { backgroundColor: "#ee5299" }, 100 );},
		function(){$('#sidenavi .side_navigation dt').stop().animate( { backgroundColor: "#f27ab2" }, 80 );
	});
	$('#sidenavi .side_navigation dd').hover(function(){
		$(this).stop().animate( { backgroundColor: "#ffddec" }, 100 );},
		function(){$(this).stop().animate( { backgroundColor: "#ffffff" }, 80 );
	});
	//------------------------ナビのドロップダウン
	$("header nav .navidown").hide();
	$("header nav .navigation>li").hover(function(){
	$("ul:not(:animated)", this).slideDown(0);},
	function(){$("header .navidown").slideUp(0);});

	//------------------------.over を透過
	$(".over") .hover(function(){
			 $(this).stop().animate({'opacity' : '0.6'}, 240); // マウスオーバーで透明度を30%にする
		},function(){
			 $(this).stop().animate({'opacity' : '1'}, 200); // マウスアウトで透明度を100%に戻す
	});

	//------------------------トップへスクロール
	$('a[href^=#]').click(function(){
	var speed = 300;
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top;
	$("html, body").animate({scrollTop:position}, speed, "swing");
	return false;
	});
	
  $('.bxslider').bxSlider({
    auto:true,
    speed:500,
    captions:false,
    responsive:true,
		controls:true,
		nextSelector:'.next-slider',
		nextText:'<img src="/common/images/top/btn_slider_right_off.png" alt="" />',
		prevSelector:'.prev-slider',
		prevText:'<img src="/common/images/top/btn_slider_left_off.png" alt="" />'
  });


	$(".open").click(function(){
		$(this).parent().children(".slide_area").slideToggle("slow");
	});

});   

