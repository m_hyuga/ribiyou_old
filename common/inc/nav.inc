<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40732470-1', 'toyama-bb.ac.jp');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
$(function() {
	$("header nav ul li").hover(function() {
		$(this).children('ul').show();
	}, function() {
		$(this).children('ul').hide();
	});
});
</script>
<header>
	<section>
		<p class="btn_belong"><a href="/student/"><img src="/common/images/common/btn_belong.png" alt="在校生の皆様へ" /></a></p>
		<h1><a href="/"><img src="/common/images/common/logo.png" alt="富山県理容美容専門学校" /></a></h1>
		<div class="btn_datail">
			<ul class="cf">
				<li><img src="/common/images/common/img_phone.png" alt="入学案内専用ダイヤル" /></li>
				<li><a href="/inquiry/"><img src="/common/images/common/btn_inquiry.png" alt="資料請求・お問い合わせはこちら" class="over" /></a></li>
			</ul>
		</div>
	</section>
	<nav>
		<ul class="cf navigation">
			<li class="nav_home<?php if($pageID=="home") echo " navigationhover"; ?>"><a href="/"><img src="/common/images/common/nav/nav_home.png" alt="HOME" /></a></li>
			<li class="nav_school<?php if($pageID=="school") echo " navigationhover"; ?>">
				<a href="/school/"><img src="/common/images/common/nav/nav_school.png" alt="学校案内" /></a>
				<ul class="navidown">
					<li><a href="/school/history.php"><img src="/common/images/common/nav/nav_school_history.png" alt="学校の歴史" /></a></li>
					<li><a href="/school/facility.php"><img src="/common/images/common/nav/nav_school_facility.png" alt="施設・設備" /></a></li>
				</ul>
			</li>
			<li class="nav_course<?php if($pageID=="course") echo " navigationhover"; ?>">
				<a href="/course/"><img src="/common/images/common/nav/nav_course.png" alt="学科紹介" /></a>
				<ul class="navidown">
					<li><a href="/course/barber.php"><img src="/common/images/common/nav/nav_course_barber.png" alt="理容科" /></a></li>
					<li><a href="/course/beauty.php"><img src="/common/images/common/nav/nav_course_beauty.png" alt="美容科" /></a></li>
					<li><a href="/course/esthetic.php"><img src="/common/images/common/nav/nav_course_esthetic.png" alt="エステティック科" /></a></li>
					<li><a href="/course/total.php"><img src="/common/images/common/nav/nav_course_total.png" alt="トータルビューティ科" /></a></li>
					<li><a href="/course/correspondence.php"><img src="/common/images/common/nav/nav_course_correspondence.png" alt="通信課程" /></a></li>
				</ul>
			</li>
			<li class="nav_campuslife<?php if($pageID=="campuslife") echo " navigationhover"; ?>">
				<a href="/campuslife/"><img src="/common/images/common/nav/nav_campuslife.png" alt="キャンパスライフ" /></a>
			</li>
			<li class="nav_work<?php if($pageID=="work") echo " navigationhover"; ?>"><a href="/employment/"><img src="/common/images/common/nav/nav_work.png" alt="就職支援" /></a></li>
			<li class="nav_admission<?php if($pageID=="admission") echo " navigationhover"; ?>">
				<a href="/admission/"><img src="/common/images/common/nav/nav_admission.png" alt="入学案内" /></a>
				<ul class="navidown">
					<li><a href="/admission/daytime.php"><img src="/common/images/common/nav/nav_admission_daytime.png" alt="昼間課" /></a></li>
					<li><a href="/admission/correspondence.php"><img src="/common/images/common/nav/nav_admission_correspondence.png" alt="通信課" /></a></li>
					<li><a href="/admission/tuition.php"><img src="/common/images/common/nav/nav_admission_tuition.png" alt="学費" /></a></li>
				</ul>
			</li>
			<li class="nav_access<?php if($pageID=="access") echo " navigationhover"; ?>"><a href="/access/"><img src="/common/images/common/nav/nav_access.png" alt="アクセス" /></a></li>
		</ul>
	</nav>
</header>
