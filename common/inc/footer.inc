	<footer>
		<section>
			<div class="footer_wrap">
				<address><img src="/common/images/common/img_datail.png" alt="" /></address>
				<p class="footer_totop"><a href="#pagetop"><img src="/common/images/common/btn_totop_off.png" alt="" /></a></p>
					<dl class="cf">
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/school/">学校案内</a></li>
								<li><a href="/school/history.php">学校の歴史</a></li>
								<li><a href="/school/facility.php">施設・設備</a></li>
							</ul>
						</dd>
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/course/">学校紹介</a></li>
								<li><a href="/course/barber.php">理容科</a></li>
								<li><a href="/course/beauty.php">美容科</a></li>
								<li><a href="/course/esthetic.php">エステティック科</a></li>
								<li><a href="/course/total.php">トータルビューティ科</a></li>
								<li><a href="/course/correspondence.php">通信課程</a></li>
							</ul>
						</dd>
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/admission/">入学案内</a></li>
								<li><a href="/admission/daytime.php">昼間課</a></li>
								<li><a href="/admission/correspondence.php">通信課程</a></li>
								<li><a href="/admission/tuition.php">学費</a></li>
							</ul>
						</dd>
					</dl>
					<dl class="cf">
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/support/">学費サポート</a></li>
								<li><a href="/support/system.php">学費支援制度</a></li>
								<li><a href="/support/exemption.php">授業料免除制度</a></li>
								<li><a href="/support/loan.php">教育ローン</a></li>
								<li><a href="/support/winner.php">特待生受賞者</a></li>
								<li><a href="/support/hojo.php">家賃補助制度</a></li>
							</ul>
						</dd>
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/campuslife/">キャンパスライフ</a></li>
							</ul>
							<ul>
								<li class="sitemap_cap"><a href="/opencampus/">オープンキャンパス</a></li>
							</ul>
						</dd>
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/employment/">就職支援</a></li>
								<li class="sitemap_cap"><a href="/access/">アクセス</a></li>
								<li class="sitemap_cap"><a href="/mailmagazine/">メールマガジン</a></li>
							</ul>
						</dd>
					</dl>
					<dl class="cf">
						<dd>
							<ul>
								<li class="sitemap_cap"><a href="/inquiry/">お問い合せ・資料請求</a></li>
								<li class="sitemap_cap"><a href="/sitemap/">サイトマップ</a></li>
							</ul>
						</dd>
						<dd class="sitemap_any">
							<ul>
								<li><a href="/placement/">・採用担当の方へ</a></li>
								<li><a href="/parents/">・保護者の方へ</a></li>
								<li><a href="/student/">・在校生の方へ</a></li>
								<!--<li><a href="/recruit/">・教員募集</a></li>-->
							</ul>
						</dd>
					</dl>
				</div>
		</section>
		<p class="footer_txt">Copyright © TOYAMA Barber & Beauty School. All Rights Reserved.</p>
	</footer>
