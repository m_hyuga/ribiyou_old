<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>就職支援 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="employment";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/employment/img_main.jpg" alt="就職支援" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href=""><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">就職支援</li>
			</ul>
		</div>
		<div id="mainarea" class="employment_content">
			<img src="/common/images/employment/img.png" alt="就職率１００％" />
			<h2><img src="/common/images/employment/ttl_main.png" alt="万全の就職サポートも、県理美ならでは！" /></h2>
			<p>
			個人面談や就職ガイダンス、サロン見学、個人指導、そして、国家試験合格から就職まで、<br>
			あらゆる場面で、あなたの希望をもとに丁寧にサポート。就職率は100%を誇ります。
			</p>
			
			<div class="box01 cf">
				<table class="box_blue left">
				<tr>
					<th><span>就職実績 / 理容科</span></th>
				</tr>
				<tr>
					<td>
					<ul>
						<li>hamada</li>
						<li>ビーハウス バイ カメオカ</li>
						<li>ヘアーサロンふらっと</li>
						<li>HAIR PRODUCE Reve.N</li>
						<li>Hair Visualist AXE</li>
						<li>エステスタジオ AXE</li>
						<li>マインドサイトウ・ヘア＆スパ</li>
						<li>hair shop 440</li>
						<li>ワイビズ ヘア</li>
						<li>カットハウス ぐりんぴいす</li>
					</ul>
					<ul class="ul_last">
						<li>S.hiroz</li>
						<li>ワイト</li>
						<li>グレイス(石川県)</li>
						<li>Smile C.C.(石川県)</li>
						<li>hair salon WEED(東京都)</li>
						<li>Hair C2(東京都)</li>
						<li>Glanz Esse(京都府)</li>
						<li>株式会社スカイ(東京都) </li>
						<li>他多数</li>
						<li>&nbsp;</li>
					</ul>
					</td>
				</tr>
				</table>
				<p class="right"><img src="/common/images/employment/photo.png" alt="" /></p>
			</div>
			
			<table class="box_orange">
			<tr>
				<th><span>就職実績 / 美容科</span></th>
			</tr>
			<tr>
				<td>
				<ul>
					<li><h3>県内</h3></li>
					<li>adan hair and make</li>
					<li>(株)アクアフィールド/LA ROUGE</li>
					<li>axis hair</li>
					<li>(株)アトリエNoel</li>
					<li>(有)ア・ラ・モード/Un nouvelle</li>
					<li>アリーナヘア</li>
					<li>an ami</li>
					<li>ambition</li>
					<li>イヴ・ピアジュ</li>
					<li>(有)ヴィルアップ/GiGi-HAIR</li>
					<li>　　　　　　　  /Arranger</li>
					<li>(有)ウナミ企画</li>
					<li>Air Lapoa</li>
					<li>(株)きくら美容室</li>
					<li>(有)キャノン美容室</li>
					<li>キレイコム</li>
					<li>Grand Beaute</li>
					<li>(有)サニー美容室</li>
					<li>(株)ZIPPY</li>
					<li>Ships</li>
					<li>Ships EEL</li>
					<li>Ships LAGOON</li>
					<li>JUNQUE</li>
				</ul>
				
				<ul>
					<li>SPELL</li>
					<li>(有)ZENITH</li>
					<li>Zero+</li>
					<li>Defy's HAIR FACTORY</li>
					<li>VANILLA</li>
					<li>(株)はまざき</li>
					<li>美容室a.p.t</li>
					<li>美容室キュービック</li>
					<li>(有)美容室せきぐち</li>
					<li>美容室Soeur</li>
					<li>美容室TALISE</li>
					<li>(有)ブーベンコップ</li>
					<li>(有)プリーズ髪風</li>
					<li>BLOW UP</li>
					<li>hair assist+esthetic nicott</li>
					<li>Hair＆Bridal みたに美容室 Hair</li>
					<li>Salon Rigolette</li>
					<li>Hair studio 髪神</li>
					<li>HAIR STUDIO DoDo</li>
					<li>ヘアースタジオ La.lien Hair</li>
					<li>Studio Ludwig</li>
					<li>HAIR DESIN MIX</li>
					<li>HAIR DESIGN feLICE</li>
					<li>hair feel</li>
				</ul>
				
				<ul class="ul_last">
					<li>HAIR BOUTIQUE ONE</li>
					<li>HAIR PRODUCE Reve.N</li>
					<li>hair room e's</li>
					<li>マサ美容室</li>
					<li>モードサロンTopsRapunzel Hair Studio</li>
					<li>(有)LEGEND OF K</li>
					<li>Y's garden for hair</li>
					<li>和楽グループ</li>
					<li>Ciel haïr Salon</li>
					<li>&nbsp;</li>
					<li><h3>県外</h3></li>
					<li>(株)イエロープライド/HAIR IZM</li>
					<li>カミオ(株)</li>
					<li>カラーズジャパン(株)</li>
					<li>SICグループ/ラ・ブランシェ</li>
					<li>(株)チャンピオン美容室</li>
					<li>(株)トゥルース</li>
					<li>(株)フォルテ</li>
					<li>Hair＆Make "Putti"</li>
					<li>(株)ヘアー・ディレクション ナンプ</li>
					<li>(有)ヘアーメッセージ/U2C</li>
					<li>Euphoriaグループ</li>
					<li>LAZ DRJGDEUX</li>
				</ul>
				</td>
			</tr>
			</table>
			<p class="txt_btn">
			65年以上の歴史と伝統を誇り、卒業生が全国に13,000人以上!<br>
			富山県理容組合と富山県美容組合が完全バックアップしています。
			</p>
			
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>