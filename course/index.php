<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学科紹介 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/course.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/img_main.jpg" alt="学科紹介" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティー科" /></a></dd>
					<dd><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">学科紹介</li>
			</ul>
		</div>
		<div id="mainarea" class="course_content">
		
		<h2><img src="/common/images/course/ttl_course.png" alt="学科紹介" /></h2>
		<p>
		ここから始まる、美のプロとしての第一歩。<br>
		65年の歴史がある富山県理容美容専門学校では、つねに美容業界の最先端の情報や、指導者も一流のプロが集結！<br>
		北陸No.1の環境と、全国に先駆けた先進的な取り組みで注目を集めています。<br>
		国家資格も、多彩な資格や職業も、選び方次第で進路は無限。<br>
		トータルな美のスキルを、より実践的に学ぶことで、あなただから、かなえられる夢が、しっかり見えてきます。
		</p>
		
		<table>
				<colgroup span="1" class="sec_01"></colgroup>
				<colgroup span="1" class="sec_02"></colgroup>
				<colgroup span="1" class="sec_01"></colgroup>
			<tr>
				<td>
				<a href="/course/barber.php"><img src="/common/images/course/btn_barber.png" alt="理容科" class="over" /></a>
				<p>
				トータルスキルを楽しく磨きながら、<br>
				国家試験合格率100％へ丁寧にサポート。
				</p>
				</td>
				<td>&nbsp;</td>
				<td>
				<a href="/course/beauty.php"><img src="/common/images/course/btn_beauty.png" alt="美容科" class="over" /></a>
				<p>
				各分野の一流のプロが協力し合い、<br>
				業界のトップとなれる人材を育成しています。
				</p>
				</td>
			</tr>
			<tr>
				<td>
				<a href="/course/esthetic.php"><img src="/common/images/course/btn_esthetic.png" alt="エステティック科" class="over" /></a>
				<p>
				学生のうちにTEAの資格が受験可能。<br>
				学内サロンで実践技術とおもてなしの心を学ぼう。
				</p>
				</td>
				<td>&nbsp;</td>
				<td>
				<a href="/course/total.php"><img src="/common/images/course/btn_total.png" alt="トータルビューティー科" class="over" /></a>
				<p>
				ネイル・メイク・エステ・ブライダルなどの基礎を学び、<br>
				自分のやってみたいを追求します。
				</p>
				</td>
			</tr>
			<tr>
				<td>
				<a href="/course/correspondence.php"><img src="/common/images/course/btn_correspondence.png" alt="通信課程" class="over" /></a>
				<p>
				働きながら、あるいは大学などに在学しながら、理容師、<br>
				美容師の国家資格を取得できます。
				</p>
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>