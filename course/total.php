<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>トータルビューティ科 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/c_total.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/total/img_main.jpg" alt="トータルビューティ科" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd class="navigationhover"><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティ科" /></a></dd>
					<dd><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/course/">学科紹介</a></li>
				<li class="pankuzu_next">トータルビューティ科</li>
			</ul>
		</div>
		<div id="mainarea" class="total_content">
			<div class="course_sec_01">
				<h2><img src="/common/images/course/total/h2_01.gif" alt="ネイル・メイク・エステ・ブライダルなどの基礎を学び自分のやってみたいを追求します。" /></h2>
				<p class="top_comment">
				トータルビューティ科は、ネイル、メイク、エステ、ブライダルの現場で必要な全ての要素を2年間で学べる充実したプログラムが特徴です。あなたの「やってみたい」を応援し実践するワークショップ（サロン）を展開します。
				学生の間に、より実践的チャレンジができるのが大きなメリット。<br>
				学びながら新たな課題を発見していくことで、自然と自分らしい進路が見つかります。<br>
				やりたいことを追求し、実践していけば、将来、美容界でそして社会で大きく飛躍することができるでしょう。
				</p>

				<ul id="tokucho">
					<h3><img src="/common/images/course/total/h3_tokucho.jpg" alt="ここがすごい！トータルビューティ科の特色" /></h3>
					<li>
						<h4 class="point">ポイント1</h4>
						<h4 class="tit_tokucho1">ワークショップが充実！</h4>
						<p>自分が挑戦したいこと、興味のある分野などをもとに、グループごとにワークショップを企画立案、運営。まずは、企画立案・計画をし、ワークショップオープンに向けて準備を。チラシなど広告活動の計画も、実践しながら学びます。</p>
					</li>
					<li>
						<h4 class="point">ポイント2</h4>
						<h4 class="tit_tokucho2">1年目で基礎を、<br>2年目でコース別進路へ</h4>
						<p>1年目でメイク、ネイル、ヘア、ブライダル、エステの基礎を学び、2年目でそれぞれコースに別れ、自分のやりたい分野へ。技術力アップのためのより専門的な学びやワークショップを実践し、将来の進路に、より近づいていきます。</p>
					</li>
					<li>
						<h4 class="point">ポイント3</h4>
						<h4 class="tit_tokucho2">社会人として<br>求められるスキルを習得</h4>
						<p>社会で特に求められる、接客に大切なおもてなしの心や、ビジネスマナーなどのほか、パソコンなどの情報処理能力など、一般教養や総合科目を学びます。社会人として、サービススタッフとして必要なスキルを身につけます。</p>
					</li>
				</ul><!--tokucho-->
			</div><!--course_sec_01-->


			<div class="course_sec_02">
				<h2><img src="/common/images/course/total/h2_curriculum.gif" alt="カリキュラム Curriculum" /></h2>
				<p class="top_comment">
				<img src="/common/images/course/total/img_curri01.jpg" alt="">
				ネイル、メイク、エステ、ブライダルの現場で必要な全ての要素を2年間で学ぼう！おもてなしの心や接客、ビジネスマナーを身につけたプロを育成します。メイクの様々なテクニックや、ネイルは基礎からジェルネイルまでエステはフェイシャルを、着付けの知識やウェディングプランナーの知識まで幅広く学ぶことで就職の幅が広がります。
				</p>
				<ul id="curriCnts01">
					<li>
						<h3>ネイル</h3>
						<img src="/common/images/course/total/img_curri02.jpg" alt="フェイシャルエステ">
						<p>ネイリスト技能検定取得を目指して、検定に必要な知識、技術、衛生管理を身につける授業です。3級はケアとカラーリング、アート、2級はチップ＆ラップ、1級はミックスメディアアート、スカルプチュア等の技術の習得を目指します。</p>
					</li>
					<li>
						<h3>メイク</h3>
						<img src="/common/images/course/total/img_curri03.jpg" alt="ボディエステ">
						<p>ブライダルメイクやモードメイクなど、ワンランク上のテクニックを身につけます。お客様の信頼を得るための応用技術や、カウンセリングのテクニックを習得します。</p>
					</li>
					<li>
						<h3>ブライダル</h3>
						<img src="/common/images/course/total/img_curri04.jpg" alt="ネイル">
						<p>主役である花嫁の美しさを最大限に引き出すブライダルエステ・ネイル・メイク。そして着付けの知識やプランナー知識も身に付きます。流行を取り入れながら、プロとしての知識と技術をしっかりと学びます。</p>
					</li>
					<li>
						<h3>ヘア</h3>
						<img src="/common/images/course/total/img_curri05.jpg" alt="メイク">
						<p>ブライダルの現場で役立つ基礎知識、技術を身につけます。アイロンテクニック、スタイリングテクニックなど美容師のアシスタントが出来る技を身につけます。</p>
					</li>
				</ul><!--curriCnts01-->
				<ul id="curriCnts02">
					<li>メイク</li>
					<li>ヘア</li>
					<li>ネイル</li>
					<li>エステ</li>
					<li class="clear">ブライダル</li>
					<li>一般教養</li>
					<li>ビジネス学</li>
					<li>総合</li>
				</ul><!--curriCnts02-->

				<div id="shikaku01">
					<h3><img src="/common/images/course/total/h3_shikaku01.png" alt="取得可能資格"></h3>
					<p>
					ネイル、メイク、エステ、ブライダルの就職に有利な、多様な資格を持つことで、就職後の様々な場面での活躍が可能になります。少人数クラスのため、キメ細やかな指導を行っています。<br>
					1級までの試験対策を行いますので、メイク検定1級、ネイル検定1級の取得も可能となります。
					</p>
					<ul id="shikakuCnts">
						<li>
							<h4>日本メイクアップ技術検定協会　日本メイクアップ技術検定試験　4・3・2・1級</h4>
							<p>プロのメイクアップアーティストとしての技術、知識を有しているかを判断し、国際的に通用するメイクアップアーティストの育成を目的とした試験制度です。リクエストに応じたメイク技術と、それに適した化粧品の使用及び説明や、カウンセリングが適切にできているかなどが判断のポイントの1つといえます。</p>
						</li>
						<li>
							<h4>日本ネイリスト協会　◎ネイリスト技能検定試験 3・2・1級<br>◎ジェルネイル技能検定　◎ネイルサロン衛生管理士資格</h4>
							<p>国際的に通用するネイリストの育成を目指す日本ネイリスト協会が主催する、正しい技術と知識の向上を目的として実践に役立つ検定試験です。ケア、アート、イクステンション、ジェルの知識と技術を総合的に習得し、第一線で活躍できます。</p>
						</li>
						<li>
							<h4>日本エステティック協会　認定エステティシャン</h4>
							<p>理論・技術を理解、習得し、エステ業界の中核を担うエステティシャンの資格です。（協会正会員に登録が必要）</p>
						</li>
						<li>
							<h4>社団法人全日本きもの振興会　きもの文化検定5・4級</h4>
							<p>きものやその歴史、文化の基礎的な知識を身につけることを目標とします。</p>
						</li>
						<li>
							<h4>日本ウエディングプランナーネットワーク協会　アシスタントウエディングプランナー検定</h4>
							<p>最大イベントである結婚式、披露宴をいかに成功させるかを習得するブライダルのプロを目指すための検定。</p>
						</li>
						<li>
							<h4>サービス接遇検定 </h4>
							<p>基本的なサービスを行うのに必要な知識、技能を持つ検定です。</p>
						</li>
					</ul>
				</div><!--shikaku01-->

				<div id="shikaku02">
					<h3><img src="/common/images/course/total/h3_shikaku02.png" alt="2年間で自分らしさを追求しながら、トータルな技術を磨いていこう！"></h3>
					<dl>
						<dt><img src="/common/images/course/total/dt_shikaku02_1.gif" alt="1年次"></dt>
						<dd>
							<h4>美容の基礎をトータルに学ぼう</h4>
							<p>ネイル、メイク、エステ、ヘアスタイル、ブライダルなどの基礎知識と技術を学び、各種検定試験で資格も取得。これからの美容界で活きる基礎を、確実に丁寧に学びます。</p>
                        </dd>
						<dt><img src="/common/images/course/total/dt_shikaku02_2.gif" alt="2年次"></dt>
						<dd>
							<h4>ワークショップで実践しながら自分らしさを追求しよう。</h4>
							<p>自分がさらに追求したいジャンルで、より実践的な力と感覚を磨きます。学内で開催するワークショップなどで実際のお客様に対応、施術しながら、あなたらしい新たな課題を見つけ、将来の進路へと着実につなげていきます。</p>
                        </dd>
					</dl>
					<p class="shikaku_img01"><img src="/common/images/course/total/img_shikaku01.png" alt="2年間で自分らしさを追求しながら、トータルな技術を磨いていこう！の画像"></p>
				</div><!--shikaku02-->

			</div><!--course_sec_02-->

			<div class="course_sec_03">
				<div id="comment03_1">
					<h2><img src="/common/images/course/total/h2_03.gif" alt="社会ですぐに役立ち、進路がぐんと広がる、最強のワークショップがスタート！ Work Shop ワークショップ"></h2>
					<h3><img src="/common/images/course/total/h3_03_1.gif" alt="夢に向ってリアルに学べる！"></h3>
					<p>
					あなたが挑戦したいテーマを自ら企画立案して、<br>
					実際のお客様に施術するワークショップ。<br>
					抜群の環境を誇る学内サロンだけでなく、学外の企業や地域などで、<br>
					あなたの夢に向かって実践しながら学ぶことができます。<br>
					先生方の確かなバックアップを受けながら安心して、<br>
					企画から運営まで、社会で役立つスキル全般を学べるのが大きな特長です。
					</p>
					<p class="img03_1"><img src="/common/images/course/total/img_03_1.png" alt="ワークショップ画像1"></p>
					<p class="img03_2"><img src="/common/images/course/total/img_03_2.gif" alt="ワークショップ画像2"></p>
				</div><!--comment03_1-->


				<ul id="comment03_2">
					<li id="c01">
						<h3>企画立案</h3>
						<p>自分が挑戦したいこと、興味ある分野などをもとに、グループごとにワークショップを企画立案します。グループごとにどんなサロンを運営するか計画していきます。</p>
						<p class="point">チラシやPOP、名刺づくりにもチャレンジ！</p>
					</li>
					<li id="c02">
						<h3>実践(サロン運営)</h3>
						<p>計画に基づいて、ワークショップを展開します。一般のお客様を相手にしながら、自らのサロンを運営していきます。お客様とのやりとりを通じて、技術や接客も、実際に合ったものにレベルアップすることができます。</p>
						<p class="point">学内サロンのほか、企業や地域で実践</p>
					</li>
					<li id="c03">
						<h3>検証</h3>
						<p>サロン運営後、問題点や改善点を検証します。どこがうまくいかなかったのか、どうすればうまくいくのかなどをグループごとに検討し、次に向けて改善していきます。　</p>
						<p class="point">失敗は次の成功への糧になる！</p>
					</li>
					<li id="c04">
						<h3>NEXT CHALLENGE</h3>
						<p>次回のワークショップへ向けての計画を立てていきます。同じ分野をさらに追求することも、全く別の分野に挑戦することもできます。自分の興味関心を追求していき、卒業後の進路選択に活かしましょう。</p>
					</li>
				</ul><!--comment03_2-->

				<div id="comment03_3">
					<h3><img src="/common/images/course/total/h3_comment03_3.png" alt="多彩な進路を可能性にするのも、県理美のワークショップならでは！"></h3>
					<p id="txt03_3">パソコンスキル、ビジネスマナー、プレゼンテーション、おもてなしなど、ワークショップを運営しながら、社会で即戦力となる大切なスキルも身につけられます。だから、卒業後の進路も、可能性もさらに広がっていきます。何より、あなたの本当になりたい将来像が、さまざまな実践から、見えてきます！</p>
					<p><img src="/common/images/course/total/txt_comment03_3.png" alt="■ブライダルアドバイザー、■ブライダルコーディネーター、■きものアドバイザー、■企画制作、■クリエーター、■プランナー、■ビューティーアドバイザー、■メイクアップアーティスト、■ネイリスト、■美容部員、■ブライダルスタイリスト、■ショッププロデューサー、■アロマセラピスト"></p>
					<p class="img03_3"><img src="/common/images/course/total/img_comment03_3.png" alt="将来の夢は、無限大！"></p>
				</div><!--comment03_3-->

			</div><!--course_sec_03-->
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>