<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>エステティック科 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/c_esthetic.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/esthetic/img_main.jpg" alt="エステティック科" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd class="navigationhover"><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティ科" /></a></dd>
					<dd><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/course/">学科紹介</a></li>
				<li class="pankuzu_next">エステティック科</li>
			</ul>
		</div>
		<div id="mainarea" class="esthe_content">
			<div class="course_sec_01">
				<h2><img src="/common/images/course/esthetic/h2_01.gif" alt="学生のうちにTEAの資格が受験可能。学内サロンで実践技術とおもてなしの心を学ぼう。" /></h2>
				<p class="top_comment">
				本校では、エステティック業界の最高位とされている資格、日本エステティック協会認定トータルエステティックアドバイザー（TEA）資格取得のためのカリキュラムを2年で集中して学びます。<br>
				一般的にはサロン勤務者しか取得できない資格も、学生のうちに受験可能。また、学内サロン「tea forêt（ティアフォーレ）」では、お客様に施術しながら、実践的な感覚を身につけます。<br>
				全国的にも珍しい学内サロンは本校の大きな特長です。技術に加え、おもてなしの心＝ホスピタリティや、ソシオエステティックを備えたプロを育成します。
				</p>

				<ul id="tokucho">
					<h3><img src="/common/images/course/esthetic/h3_tokucho.gif" alt="ここがすごい！エステティック科の特色" /></h3>
					<li>
						<h4 class="point">ポイント1</h4>
						<h4 class="tit_tokucho">全国的にもめずらしい！！在学中にTEA取得可能！</h4>
						<p>協会認定トータルエステティックアドバイザー（TEA）の1次・2次試験は本校で受験できます。しかも、1年の実務経験は学内サロンで可能。つまり、2年でTEAの取得が目指せます。3次試験の受験の準備も可能です。</p>
					</li>
					<li>
						<h4 class="point">ポイント2</h4>
						<h4 class="tit_tokucho">高齢化社会で役立つソシオエステが学べる。</h4>
						<p>高齢化社会において、必要とされるソシオエステ。ソシオエステとは、精神的・肉体的・社会的な困難を抱えている人に対し、医療や福祉の知識に基づいて行う、総合的なエステティックのことです。</p>
					</li>
					<li>
						<h4 class="point">ポイント3</h4>
						<h4 class="tit_tokucho">学内サロンでサロンワークを実践。</h4>
						<p>学内エステティックサロン、tea forêt（ティアフォーレ）で本物のサロンワークを学びます！サロン運営に必要な接客、集客の技能、知識を実践のなかで習得。お客様一人ひとりに合った、質の高いサービスをプロのエステティシャンのもと学ぶことができます。</p>
					</li>
				</ul><!--tokucho-->
			</div><!--course_sec_01-->


			<div class="course_sec_02">
				<h2><img src="/common/images/course/esthetic/h2_curriculum.gif" alt="カリキュラム Curriculum" /></h2>
				<p class="top_comment">
				<img src="/common/images/course/esthetic/img_curri01.jpg" alt="">
				学内サロンで実践的な実習を行いながら、<br>
                最高級のテクニックと質の高いおもてなしの、<br>
                「プレミアムエステティシャン」を育成します。<br>
				フェイシャル、ボディ、脱毛、アロマテラピーなど、<br>
                美をトータルで学び、美しい所作で女性としても<br>成長！
				</p>
				<ul id="curriCnts01">
					<li>
						<h3>フェイシャルエステ</h3>
						<img src="/common/images/course/esthetic/img_curri02.jpg" alt="フェイシャルエステ">
						<p>ハンドケアだけではなく、機器による本格的なトリートメントで、お客様一人ひとりの肌状態に合ったトリートメント方法を学びます。</p>
					</li>
					<li>
						<h3>ボディエステ</h3>
						<img src="/common/images/course/esthetic/img_curri03.jpg" alt="ボディエステ">
						<p>振動作用によって、脂肪を分解させたり、セルライトの予防や改善を行うための機器の効果的使用方法を学びます。筋肉や神経もリラックスさせることができ、お手入れ後は、体が軽くなるのが実感できます。</p>
					</li>
					<li>
						<h3>ネイル</h3>
						<img src="/common/images/course/esthetic/img_curri04.jpg" alt="ネイル">
						<p>スキンケアのプロフェッショナルは、爪についても正しい知識が必要。マニキュアの正しい知識と技術を学びます。</p>
					</li>
					<li>
						<h3>メイク</h3>
						<img src="/common/images/course/esthetic/img_curri05.jpg" alt="メイク">
						<p>エステの後のメイクアップをサービスの一環として行うことができるよう、ナチュラルメイクの知識と技術を身につけます。</p>
					</li>
				</ul><!--curriCnts01-->
				<ul id="curriCnts02">
					<li>エステティック概論</li>
					<li>生理解剖学</li>
					<li>化粧品学</li>
					<li>エステティックカウンセリング</li>
					<li class="clear">フェイシャル</li>
					<li>ワックス脱毛</li>
					<li>サロン実習</li>
					<li>皮膚科学</li>
					<li>栄養学</li>
					<li>サロン経営学</li>
					<li class="clear">アロマテラピー</li>
					<li>ボディ</li>
					<li>マニキュア　　など</li>
				</ul><!--curriCnts02-->

				<div id="shikaku01">
					<h3><img src="/common/images/course/esthetic/h3_shikaku01.png" alt="取得可能資格"></h3>
					<p>
					卒業後、すぐにサロンで働くことができる、<br>
					エステティックに関する資格・検定試験を受けることができます。<br>
					クラスは少人数制。各生徒に適した合格までのサポートを行なっています。
					</p>
					<ul id="shikakuCnts">
						<li>
							<h4>日本エステティック協会　認定トータルエステティックアドバイザー（TEA）受験資格</h4>
							<p>これからのエステ業界の指導的、社会的立場を担うエステティシャンの国内基準として、最高位の資格です。認定上級エステティシャン取得者は、1次、2次試験が免除になります。</p>
						</li>
						<li>
							<h4>日本エステティック協会　認定上級エステティシャン</h4>
							<p>エステティックに関する理論、技術のすべてを理解し、実践する能力を有しているエステティシャンの資格です。</p>
						</li>
						<li>
							<h4>日本エステティック協会　認定エステティシャン</h4>
							<p>理論・技術を理解、習得し、エステ業界の中核を担うエステティシャンの資格です。（協会正会員に登録が必要）</p>
						</li>
						<li>
							<h4>日本メイクアップ技術検定協会　メイクアップ技術検定　4級・3級</h4>
							<p>国際的に通用するメイクアップアーティストの育成を目的とした試験制度です。</p>
						</li>
						<li>
							<h4>日本アロマ環境協会　アロマテラピー検定　2級・1級</h4>
							<p>アロマテラピーの楽しみ方や健康維持のための活用方法、アロマオイル(精油)の基礎知識の習得。</p>
						</li>
					</ul>
				</div><!--shikaku01-->

				<div id="shikaku02">
					<h3><img src="/common/images/course/esthetic/h3_shikaku02.png" alt="2年間で即戦力をつけるカリキュラム"></h3>
					<p>認定トータルエステティックアドバイザー［TEA］の資格を、学生のうちに受験できます！</p>
					<dl>
						<dt><img src="/common/images/course/esthetic/dt_shikaku02_1.gif" alt="1年"></dt>
						<dd>
							<h4>きめ細かい授業で認定上級エステティシャン取得を目指します。</h4>
							<p>認定上級エステティシャン試験に必要な1000時間の授業を集中して<br>学習します。基礎からわかりやすく、丁寧な指導を行います。</p>
                        </dd>
						<dt><img src="/common/images/course/esthetic/dt_shikaku02_2.gif" alt="2年専科"></dt>
						<dd>
							<h4>学内サロン「tea forêt(ティアフォーレ)」で実務経験を！</h4>
							<p>3次試験に必要な、お客様の診断と施術提案を、実践を通して身につけます。サロン実習時はアルバイト収入を得ることもできます。</p>
                        </dd>
					</dl>
					<ul id="senka01">
						<img src="/common/images/course/esthetic/txt_shikaku02_senka.gif" class="▼ 専科ではこんなことも学べます">
						<li>ソシオエステティック　　→</li>
						<li>アロマテラピー</li>
						<li>ビジネススキルなど</li>
						<li>ブライダルエステ</li>
					</ul><!--senka01-->
					<div id="senka02">
						<p>
							<img src="/common/images/course/esthetic/img_senka02.jpg" alt="ソシオエステティックとは">
							<span class="txt_senka02">●ソシオエステティックとは</span><br>
							ソシオエステティックとは、エステティックの技術によって人を癒し、励まし、QOL（生活の質）の向上に寄与し、社会生活を送ることが困難な方々が、本来の自分を取り戻すための支援をすることです。
						</p>
					</div><!--senka02-->
					<p class="shikaku_img01"><img src="/common/images/course/esthetic/img_shikaku01.png" alt="2年間で即戦力をつけるカリキュラムの画像"></p>
				</div><!--shikaku02-->

			</div><!--course_sec_02-->

			<div class="course_sec_03">
				<h2><img src="/common/images/course/esthetic/h2_03.gif" alt="最新の施設・設備、独自のカリキュラムも県理美ならでは！ Practice in the salon サロン実習"></h2>
				<div id="comment03">
					<h3><img src="/common/images/course/esthetic/h3_03_1.gif" alt="学内サロン、tea forêt（ティアフォーレ）で実践力を身につけよう。"></h3>
					<p>
					学内エステティックサロン、tea forêt（ティアフォーレ）は、<br>
					富山県理容美容専門学校が運営するエステティックサロンです。<br>
					地域に開かれたエステティックサロンとして、実際にお客様にご来店いただき、比較的低価格でプロのエステティシャンがサービスを提供しています。サロン実習では、学生達はサロン運営に必要な接客や集客の方法、知識などを実際の仕事の流れのなかで学びます。
					</p>
				</div><!--comment03-->
					<img class="main03" src="/common/images/course/esthetic/img_03_main.jpg" alt="学内サロン、tea foret（ティアフォーレ）で実践力を身につけよう。">
					<div style="clear:both;"></div>

				<div id="comment03_2">
					<p class="txt01">
					実際のサロン内では、学生同士で学んだだけでは得られない、様々な年齢や職業のお客様と触れ合う、本物の体験がたくさんできます！お肌の状態の見極めなど、肌は同じ方でも体調や環境等で変化します。一人ひとりに合ったトリートメントや施術メニューのために、まずはカウンセリングからスタートです！
					</p>
					<p class="name">エステティック科　砂ひとみさん</p>
					<p class="img01"><img src="/common/images/course/esthetic/img_03_02.png" alt="砂ひとみさん"></p>
				</div><!--comment03_2-->

				<ul id="flow03">
					<li>
						<div class="txt_left">
							<h4>カルテに基づき、まずは、カウンセリング</h4>
							<p>
							エステを始める前には、お肌の状態を見極め、お客様の要望と合わせて、最適な施術内容をお話ししながら確認していきます。
							</p>
						</div>
						<img class="right" src="/common/images/course/esthetic/img_03_flow01.jpg" alt="カルテに基づき、まずは、カウンセリング">
					</li>
					<li>
						<img class="left" src="/common/images/course/esthetic/img_03_flow02.jpg" alt="お部屋へご案内">
						<div class="txt_right">
							<h4>お部屋へご案内</h4>
							<p>
							実際の施術に入るために、各お部屋にご案内します。<br>本格的なサロン機能を備えた、学内サロンとなっています。
							</p>
						</div>
					</li>
					<li>
						<div class="txt_left">
							<h4>先生の施術から学ぶ</h4>
							<p>
							まずは講師の先生による施術を見学します。<br>この日はフェイシャルコースでご来店のお客様に、お肌に合ったマッサージとトリートメントを。　
							</p>
						</div>
						<img class="right" src="/common/images/course/esthetic/img_03_flow03.jpg" alt="先生の施術から学ぶ">
					</li>
					<li>
						<img class="left" src="/common/images/course/esthetic/img_03_flow04.jpg" alt="今度は、実際に施術しながら">
						<div class="txt_right">
							<h4>今度は、実際に施術しながら</h4>
							<p>
							今度は、資格を持った学生が、実際のトリートメントを行います。丁寧な指導で、お客様にもご負担のかからない、気持ちのいいサービスを目指します。
							</p>
						</div>
					</li>
					<li>
						<div class="txt_left">
							<h4>受付業務も大事な仕事です</h4>
							<p>
							お客様の予約受付や、ご来店から施術後のお見送りまで、一連の業務の流れをトータルに学びます。実際のお店じゃないと、できない経験がいっぱいです！
							</p>
						</div>
						<img class="right" src="/common/images/course/esthetic/img_03_flow05.jpg" alt="受付業務も大事な仕事です">
					</li>
					<li>
						<img class="left" src="/common/images/course/esthetic/img_03_flow06.jpg" alt="商品知識や販売方法を習得しよう！">
						<div class="txt_right">
							<h4>商品知識や販売方法を習得しよう！</h4>
							<p>
							お客様のお肌はそれぞれみんな違うため、その方に合ったスキンケア商品を選ぶことが大事。商品知識を学ぶこともサロン運営の重要なポイントです。
							</p>
						</div>
                   </li>
				</ul><!--flow03-->

				<div id="comment_last">
					<div id="txt_left">
						<h3><img src="/common/images/course/esthetic/h3_txt03_2.gif" alt="お客様の笑顔がうれしい！"></h3>
						<p>
						何より、本物の接客を学べるのがサロン実習。普通の授業では得られない、貴重な経験ばかり。緊張はしますが、お客様の笑顔がみられるとうれしいもの。実際に就職や資格試験にも、このサロンワークが大きな自信になるはず。信頼されるエステティシャンを目指して、楽しみながら学んでいきます！
						</p>
						<p id="notes"><img class="left" src="/common/images/course/esthetic/txt_03_02.gif" alt="卒業後すぐに役立つ実践力が身につけば、就職活動も、就職後も断然有利に！"></p>
					</div><!--txt_left-->
					<img class="right" src="/common/images/course/esthetic/img_03_03.jpg" alt="お客様の笑顔がうれしい！">
				</div><!--comment_last-->
			</div><!--course_sec_03-->

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>