<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>通信課程 理容科／美容科 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/c_correspondence.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/correspondence/img_main.gif" alt="Distance Learning Course 通信課程 理容科／美容科" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティ科" /></a></dd>
					<dd class="navigationhover"><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/course/">学科紹介</a></li>
				<li class="pankuzu_next">通信課程 理容科／美容科</li>
			</ul>
		</div>
		<div id="mainarea" class="correspondence_content">
			<div class="course_sec_01">
				<h2><img src="/common/images/course/correspondence/h2_dlc.gif" alt="あなたらしいスタイルで理容師・美容師の国家資格の取得をめざそう！" /></h2>
				<div class="dlc_cnts">
					<h3 class="h3_txt"><span class="dlc_green">■</span>通信課程も全面的にバックアップ<span class="dlc_green">■</span></h3>
					<ul class="dlc_li_2">
						<li>
							<h4>通信授業+スクーリング</h4>
							<p>通信教材での学習と面接授業＝スクーリングの受講により、ステップアップしていきます！</p>
						</li>
						<li>
							<h4>自宅で学習+課題提出</h4>
							<p>教科書や補習教材、報告課題を配布します。<br>自宅で学習しながら定期的に課題を提出していただきます。</p>
						</li>
					</ul><!--dlc_li_2-->
				</div><!--dlc_cnts-->

				<div class="dlc_cnts">
					<h3 class="h3_txt"><span class="dlc_green">■</span>いろいろなスタイルで国家資格を<span class="dlc_green">■</span></h3>
					<ul class="dlc_li_3">
						<li>
							<h4>働きながら、国家資格取得</h4>
							<p>働きながら、あるいは大学などに在学しながら、理容師・美容師の国家資格取得をめざします。エステティック科、トータルビューティ科とのダブルスクールも可能です。</p>
							<p id="jump_ippan"><a href="#ippan"><img src="/common/images/course/correspondence/btn_style_ippan.gif" alt="一般コース"></a></p>
						</li>
						<li>
							<h4>サロンに勤務しながら</h4>
							<p>理容・美容サロンに勤務しながら国家資格取得をめざします。一般コースよりスクーリング時間が短縮できます。</p>
							<p id="jump_salon"><a href="#salon"><img src="/common/images/course/correspondence/btn_style_salon.gif" alt="サロン従事者コース"></a></p>
						</li>
						<li>
							<h4>年2回の入学時期</h4>
							<p>通信課程には4月、10月と2回の入学のタイミングがあるため、ご希望のタイミングでスクーリングが可能となります。</p>
						</li>
					</ul><!--dlc_li_3-->
				</div><!--dlc_cnts-->

				<div class="dlc_cnts" id="ippan">
					<h3 class="h3_img"><img src="/common/images/course/correspondence/h3_ippan_sche.gif" alt="［一般コース受講スケジュール］"></h3>
						<p class="dlc_comment">スクーリングでは国家試験対策+サロンで必要な技術（シャンプーなど）も学びます<br>対象：理容・美容サロンに勤務していない方</p>
						<h4 class="ippan">4月入学</h4>
							<p class="dlc_sche"><img src="/common/images/course/correspondence/img_ippan_april_table.gif" alt="一般コース受講スケジュール 4月入学"></p>
						<h4 class="ippan">10月入学</h4>
							<p class="dlc_sche"><img src="/common/images/course/correspondence/img_ippan_october_table.gif" alt="一般コース受講スケジュール 10月入学"></p>
				</div><!--dlc_cnts-->
				<div class="dlc_cnts" id="salon">
					<h3 class="h3_img"><img src="/common/images/course/correspondence/h3_salon_sche.gif" alt="［サロン従事者コース受講スケジュール］"></h3>
						<p class="dlc_comment">スクーリングでは国家試験対策+サロンで必要な技術（シャンプーなど）も学びます<br>対象：理容・美容サロンに勤務していない方</p>
						<h4 class="salon">4月入学</h4>
							<p class="dlc_sche"><img src="/common/images/course/correspondence/img_salon_april_table.gif" alt="サロン従事者コース受講スケジュール 4月入学"></p>
						<h4 class="salon">10月入学</h4>
							<p class="dlc_sche"><img src="/common/images/course/correspondence/img_salon_october_table.gif" alt="サロン従事者コース受講スケジュール 10月入学"></p>
				</div><!--dlc_cnts-->
				<div id="dlc_qa">
					<h3 class="h3_img"><img src="/common/images/course/correspondence/h3_dlc_qa.gif" alt="Q&A"></h3>
					<dl>
						<dt>お店はどうやって決めればいいの？</dt>
						<dd>みなさんの希望の条件を整理すると、<br>きっと理想のお店が見つかります。<br>お気軽にご相談ください。</dd>
					</dl>
					<dl>
						<dt>学費はどのくらい必要ですか？</dt>
						<dd>3年間で約125万円となります。<br>昼間課程より、 修業年限は１年長くなりますが、必要費用は抑えた設定になっています。</dd>
					</dl>
					<dl>
						<dt>面接授業（スクーリング）は必ず<br>出席しなければいけないの？</dt>
						<dd>面接授業（スクーリング）の時間数と通信授業（レポート提出）の成績を満たさないと国家試験の受験はできません。面接授業に関するご質問は本校までご相談下さい。</dd>
					</dl>
					<dl>
						<dt>学校に来れる日が少なくて、<br>国家試験に合格するか心配です。</dt>
						<dd>3年次のスクーリングでは、国家試験課題を徹底的に習い、不安を取り除く授業を行っています。 本番さながらの実技試験のシミュレーションを繰り返して練習できるので、国家試験対策もバッチりです。</dd>
					</dl>
				</div>
			</div><!--course_sec_01-->
		</div><!--correspondence_content-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>