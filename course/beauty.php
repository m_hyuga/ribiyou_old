<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>美容科 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/c_beauty.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/beauty/img_main.jpg" alt="美容科" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd class="navigationhover"><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティ科" /></a></dd>
					<dd><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/course/">学科紹介</a></li>
				<li class="pankuzu_next">美容科</li>
			</ul>
		</div>
		<div id="mainarea" class="beauty_content">
			<div class="course_sec_01">
				<h2><img src="/common/images/course/beauty/h2_01.gif" alt="各分野の一流のプロが協力し合い、業界のトップとなれる人材を育成しています。" /></h2>
				<p class="top_comment">
				65年の歴史を誇る本校では、各分野の一流のプロフェッショナルが協力し合い、<br>
				業界のトップとなれる人材を育成しています。<br>
				美容科では、実践的でトータルな美容のスキルを身につけることができます。カット、カラー、スタイリング、着付などの基礎から応用までを、対話しながら楽しく学びます。人としての心遣い、マナーも身につけましょう。<br>
				どの授業も最新設備のなかで展開されますが、本番さながらの授業で国家試験の対策も万全です。
				</p>

				<ul id="tokucho">
					<h3><img src="/common/images/course/beauty/h3_tokucho.jpg" alt="ここがすごい！美容科の特色" /></h3>
					<li>
						<h4 class="point">ポイント1</h4>
						<h4 class="tit_tokucho">国家試験合格率100％（実技）就職率100％！</h4>
						<p>本校では全教員が資格取得のプロ。学生の個性に応じた「合格プログラム」を作成し、丁寧な指導で国家試験合格率は100％！国家試験本番のシミュレーションも万全です。幅広いネットワークで就職率も100％を誇ります。</p>
					</li>
					<li>
						<h4 class="point">ポイント2</h4>
						<h4 class="tit_tokucho">サロンで役立つ実践力が<br>身につきます</h4>
						<p>レディースカットや高い技術が必要なメンズカットも習得。レザーを使ったカットアレンジ、ブロー、アップススタイルからアレンジヘアまで、トータルな技術やコミュニケーション力を磨き、サロンで役立つ実践力を身につけます。</p>
					</li>
					<li>
						<h4 class="point">ポイント3</h4>
						<h4 class="tit_tokucho">ネイル・メイク・エステの<br>SBS資格を取得！</h4>
						<p>本校は、美容師しか取得できないSBS検定の認定校。しかも、学生のうちに受験できるのは北陸で唯一、本校だけです！そのほか、メイク前に必要なレディースシェービングや、美容師しかできない「まつげエクステ」も学習します。</p>
					</li>
				</ul><!--tokucho-->
			</div><!--course_sec_01-->


			<div class="course_sec_02">
				<h2><img src="/common/images/course/beauty/h2_curriculum.gif" alt="カリキュラム Curriculum" /></h2>
				<p class="top_comment">
				<img src="/common/images/course/beauty/img_curri01.jpg" alt="">
				お客様を笑顔にし、「ありがとう」の言葉をもらう、<br>
				やりがいに満ちた美容師という仕事。<br>
				プロとしての実践的技術を丁寧に学びながら、<br>
				接客の基本やマナー、お客様のニーズをつかみご提案するコミュニケーション力を磨きましょう。
				</p>
				<ul id="curriCnts01">
					<li>
						<h3>美容実習：スタイリング</h3>
						<img src="/common/images/course/beauty/img_curri02.jpg" alt="フェイシャルエステ">
						<p>コーム、ブラシ、ピンを使って様々なスタイルに。ボリュームを出し、流れをつくる、結い上げなどのテクニックを身につけ、オリジナル作品を完成。着物やドレスに似合うアップヘアのマスターで、ブライダルシーンでの活躍も。</p>
					</li>
					<li>
						<h3>カラー</h3>
						<img src="/common/images/course/beauty/img_curri03.jpg" alt="ボディエステ">
						<p>ヘアブリーチ剤やヘアカラー剤の取扱いを学び、ムラなく染めるために、スピードと塗布の正確さを身につけます。また、サロンでも多くとり入れられているホイルワークも学びます。</p>
					</li>
					<li>
						<h3>シャンプー</h3>
						<img src="/common/images/course/beauty/img_curri04.jpg" alt="ネイル">
						<p>お客様の横に立って行う基本のサイドシャンプーから、最新の設備でのヘッドスパやマッサージ効果を意識したシャンプーも学びます。</p>
					</li>
					<li>
						<h3>国試対策</h3>
						<img src="/common/images/course/beauty/img_curri05.jpg" alt="メイク">
						<p>美容科教員全員が、国家試験対策や資格取得のプロフェッショナル。個々の特徴や弱点に合わせて、丁寧に指導を行います。</p>
					</li>
				</ul><!--curriCnts01-->
				<ul id="curriCnts02">
					<li>美容実習…ワインディング・カット・カラーリング・スタイリング・シャンプー・ヘアセッティング・着付</li>
					<li>関係法規・制度</li>
					<li>衛生管理</li>
					<li>保健</li>
					<li>物理・化学</li>
					<li>美容文化論</li>
					<li>運営管理</li>
					<li class="clear">美容技術理論</li>
					<li>総合</li>
					<li>色彩</li>
					<li>ビジネス学</li>
					<li>エステティック技術</li>
					<li class="clear">ネイル</li>
					<li>メイク</li>
					<li>まつ毛エクステ</li>
					<li>実務実習</li>
					<li class="clear">レディースシェービング（メイク前）</li>
				</ul><!--curriCnts02-->

				<div id="shikaku01">
					<h3><img src="/common/images/course/beauty/h3_shikaku01.png" alt="取得可能資格"></h3>
					<p>
					美容科教員全員が国家試験対策や資格取得のプロ。<br>
					丁寧に、個々の特徴や弱点に合わせて指導を行います。<br>
					また、施設も充実しているので、国家試験や資格取得にも、とても良い環境で学べます。
					</p>
					<ul id="shikakuCnts">
						<li>
							<h4>美容師国家試験受験資格</h4>
							<p>美容師への第一歩として、国家試験受験のために、厚生労働省指定養成施設である本校で所定の単位を習得。</p>
						</li>
						<li>
							<h4>社団法人全国服飾教育者連合会 色彩検定3級</h4>
							<p>色に関する幅広い知識を問う検定試験です。 美容師にもヘアカラーリングや、メイク、ネイルなど色に関する知識が求められています。</p>
						</li>
						<li>
							<h4>全国美容業生活衛生同業組合連合会評価認定制度 SBSディレクター</h4>
								<h5>●SBSネイルディレクター</h5>
									<p>ネイルケアに関する基本的な技術、および、知識などに関する試験です。</p>
								<h5>●SBSメイクディレクター</h5>
									<p>ベーシックなメイクアップ料の使い方や日常生活に相応しいメイク技術に関する試験です。</p>
                                <h5>●SBSメイクディレクター</h5>
									<p>衛生・安全面およびスキンケアの基本知識とフェイシャルの基礎技術に関する試験です。</p>
						</li>
					</ul>
				</div><!--shikaku01-->
			</div><!--course_sec_02-->

			<div class="course_sec_03">
				<h3><img src="/common/images/course/beauty/h3_st.gif" alt="在校生に聞きました。Current Student"></h3>
				<ul class="comment03">
					<li>
						<h4>ショーやドラマのヘアメイクを目標に、トータルに高いレベルで学べる学校へ。</h4>
						<p>
							<img class="left" src="/common/images/course/beauty/img_st01_1.jpg" alt="菓子茉穂さん 平成25年入学  小杉高等学校出身">
							ヘアはもちろん、ネイル・メイク・エステと様々な勉強ができて、国家資格合格率も就職率も100％。<br>
							安心して学べる環境にすごく魅力を感じて入学しました。<br>
							将来は、ショーやドラマなどのヘアメイクを担当するのが目標。<br>
							だから、美容師しか取れない資格SBSが取れて、<br>トータルで学べるこの学校を選んだんです。<br>先生方の指導は丁寧で、優しいです。もっと高い<br>レベルを目指して、頑張ろうと思えるんです。
						</p>
						<p class="img01_st"><img src="/common/images/course/beauty/img_st01_2.jpg" alt="菓子茉穂さん 平成25年入学  小杉高等学校出身"></p>
					</li>
					<li>
						<h4>ヘアメイクアップアーティストを目指して、技術も心も磨いています。</h4>
						<p>
							<img class="left" src="/common/images/course/beauty/img_st02_1.jpg" alt="小林憲之亮さん　平成25年入学  入善高等学校出身">
							この学校はレベルが高いですし、永年の伝統や実績、最新の設備が揃うなか。トータルで美容を学べるすばらしい環境は、他にはあまりと思います。日々、学びながら技術が確実に身に付いているのを感じまねす。将来は東京で、ヘアメイクアップアーティストになるのが夢です。<br>
							先生は熱心ですし、やる気次第で、どんどん<br>ステップアップが可能です。技術をさらに<br>磨いて、全国大会出場も目指します。
						</p>
						<p class="img02_st"><img src="/common/images/course/beauty/img_st02_2.jpg" alt="小林憲之亮さん　平成25年入学  入善高等学校出身"></p>
					</li>
				</ul><!--comment03-->
				<h3><img src="/common/images/course/beauty/h3_gd.gif" alt="卒業生に聞きました。Graduate"></h3>
				<ul class="comment03">
					<li>
						<h4>ネイル・メイクも美容師免許も、トータルな技術がこれからの力になる。</h4>
						<p><img src="/common/images/course/beauty/img_gd01.jpg" alt="下條真子さん　平成25年卒業　美容師　美容室a.p.t勤務"></p>
						<p>最初はネイル・メイク科で学び、その後、美容科で美容師免許を取得。ネイリスト志望だったのですが、やはり、美容師免許を持っている方が仕事の幅もぐんと広がりますね。お店でも、ネイルやメイク、フェイシャルエステも担当しますし、学校で学んできたことが生かされています。今後、トータルでサービスを提供するお店は増えていくはず。県理美で真面目に学べば、必ず将来の力になるはずです。</p>
					</li>
					<li>
						<h4>授業内容は、より実践的になり、学校はどんどん進化していますね。</h4>
						<p><img src="/common/images/course/beauty/img_gd02.jpg" alt="中谷成勝さん　平成20年卒業　スタイリスト　アランジェ勤務"></p>
						<p>学生時代にインターンシップでこのお店で学び、現在はスタイリストに。先輩たちの活き活きとした姿やファッション、お客様がきれいになり喜んでおられる様子をみて入社した理想の職場です。学校では授業がより実践的になり、授業内容も設備もさらに進化していて、本当にすばらしいですね。自分も勉強を忘れず、トータルな美しさをご提案し、質の高いおもてなしを、大切にしていきたいです。</p>
					</li>
				</ul><!--comment03-->
			</div><!--course_sec_03-->

			<div class="course_sec_04">
				<h3><img src="/common/images/course/beauty/h3_04.png" alt="人気のまつ毛エクステは、美容師だけの技術です！"></h3>
				<p class="txt04">まつ毛エクステンションは、まつ毛が長く濃くなり、目を大きく見せてくれる効果があり、需要が高まっています。そして、美容師免許を持っている人だけができる技術です。目元という、デリケートな部分に施術するため、美容師の国家資格を持ち、技術や衛生面でもしっかりとしたカリキュラムで学んだ人材がいま求められています。本校では、ヘアはもちろん、人気のまつ毛エクステについても、最新の技術をしっかり学ぶことができます。確かな資格のある、美のスペシャリストになりましょう！</p>
				<h4>まつ毛エクステとは</h4>
				<p class="txt04">まつ毛1本1本に専用のグルーで加工されたまつ毛をつけていきます。とても自然で、顔を洗っても取れず、海やプールもOK！いつでもまつ毛が長く、目元パッチリな状態をキープ♡メイク時間も短縮できる、最近話題の技術です！</p>
				<p class="img04"><img src="/common/images/course/beauty/img_04.png" alt="人気のまつ毛エクステは、美容師だけの技術です！の画像"></p>
			</div><!--course_sec_04-->
<!--
			<div class="course_sec_03">
				<div id="comment03_1">
					<h2><img src="/common/images/course/total/h2_03.gif" alt="社会ですぐに役立ち、進路がぐんと広がる、最強のワークショップがスタート！ Work Shop ワークショップ"></h2>
					<h3><img src="/common/images/course/total/h3_03_1.gif" alt="夢に向ってリアルに学べる！"></h3>
					<p>
					あなたが挑戦したいテーマを自ら企画立案して、<br>
					実際のお客様に施術するワークショップ。<br>
					抜群の環境を誇る学内サロンだけでなく、学外の企業や地域などで、<br>
					あなたの夢に向かって実践しながら学ぶことができます。<br>
					先生方の確かなバックアップを受けながら安心して、<br>
					企画から運営まで、社会で役立つスキル全般を学べるのが大きな特長です。
					</p>
					<p class="img03_1"><img src="/common/images/course/total/img_03_1.png" alt="ワークショップ画像1"></p>
					<p class="img03_2"><img src="/common/images/course/total/img_03_2.gif" alt="ワークショップ画像2"></p>
				</div><!--comment03_1-->

<!--
				<ul id="comment03_2">
					<li id="c01">
						<h3>企画立案</h3>
						<p>自分が挑戦したいこと、興味ある分野などをもとに、グループごとにワークショップを企画立案します。グループごとにどんなサロンを運営するか計画していきます。</p>
						<p class="point">チラシやPOP、名刺づくりにもチャレンジ！</p>
					</li>
					<li id="c02">
						<h3>実践(サロン運営)</h3>
						<p>計画に基づいて、ワークショップを展開します。一般のお客様を相手にしながら、自らのサロンを運営していきます。お客様とのやりとりを通じて、技術や接客も、実際に合ったものにレベルアップすることができます。</p>
						<p class="point">チラシやPOP、名刺づくりにもチャレンジ！</p>
					</li>
					<li id="c03">
						<h3>検証</h3>
						<p>サロン運営後、問題点や改善点を検証します。どこがうまくいかなかったのか、どうすればうまくいくのかなどをグループごとに検討し、次に向けて改善していきます。　</p>
						<p class="point">失敗は次の成功への糧になる！</p>
					</li>
					<li id="c04">
						<h3>NEXT CHALLENGE</h3>
						<p>次回のワークショップへ向けての計画を立てていきます。同じ分野をさらに追求することも、全く別の分野に挑戦することもできます。自分の興味関心を追求していき、卒業後の進路選択に活かしましょう。</p>
					</li>
				</ul><!--comment03_2-->

<!--
				<div id="comment03_3">
					<h3><img src="/common/images/course/total/h3_comment03_3.png" alt="多彩な進路を可能性にするのも、県理美のワークショップならでは！"></h3>
					<p id="txt03_3">パソコンスキル、ビジネスマナー、プレゼンテーション、おもてなしなど、ワークショップを運営しながら、社会で即戦力となる大切なスキルも身につけられます。だから、卒業後の進路も、可能性もさらに広がっていきます。何より、あなたの本当になりたい将来像が、さまざまな実践から、見えてきます！</p>
					<p><img src="/common/images/course/total/txt_comment03_3.png" alt="■ブライダルアドバイザー、■ブライダルコーディネーター、■きものアドバイザー、■企画制作、■クリエーター、■プランナー、■ビューティーアドバイザー、■メイクアップアーティスト、■ネイリスト、■美容部員、■ブライダルスタイリスト、■ショッププロデューサー、■アロマセラピスト"></p>
					<p class="img03_3"><img src="/common/images/course/total/img_comment03_3.png" alt="将来の夢は、無限大！"></p>
				</div>

			</div><!--course_sec_03-->
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>