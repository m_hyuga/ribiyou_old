<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>理容科 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/c_barber.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="course";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/course/barber/img_main.jpg" alt="理容科" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/course/"><img src="/common/images/common/sidenav/course/nav_course.png" alt="学科紹介" /></a></dt>
					<dd class="navigationhover"><a href="/course/barber.php"><img src="/common/images/common/sidenav/course/nav_course_barber.png" alt="理容科" /></a></dd>
					<dd><a href="/course/beauty.php"><img src="/common/images/common/sidenav/course/nav_course_beauty.png" alt="美容科" /></a></dd>
					<dd><a href="/course/esthetic.php"><img src="/common/images/common/sidenav/course/nav_course_esthetic.png" alt="エステティック科" /></a></dd>
					<dd><a href="/course/total.php"><img src="/common/images/common/sidenav/course/nav_course_total.png" alt="トータルビューティ科" /></a></dd>
					<dd><a href="/course/correspondence.php"><img src="/common/images/common/sidenav/course/nav_course_correspondence.png" alt="通信課程" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/course/">学科紹介</a></li>
				<li class="pankuzu_next">理容科</li>
			</ul>
		</div>
		<div id="mainarea" class="barber_content">
			<div class="course_sec_01">
				<h2><img src="/common/images/course/barber/h2_01.gif" alt="トータルスキルを楽しく磨きながら、国家試験合格率100％へ丁寧にサポート。" /></h2>
				<p class="top_comment">
				理容業界では、高い技術だけでなく、より幅広い分野で、トータルな美しさや、あたらしいサービスを<br>
				自らつくり出せる人材が求められています。<br>
				「エステ」「ネイル」「レディースシェービング」とメニューも増え、「自分がやりたいお店づくりができる」のが<br>
				新しい理容のカタチ。生涯の仕事として、おしゃれなマイスターを目指しませんか。<br>
				理容師の国家試験合格率は100％。一人ひとりを親身に、丁寧にサポートできるのも強みです。
				</p>

				<ul id="tokucho">
					<h3><img src="/common/images/course/barber/h3_tokucho.jpg" alt="ここがすごい！理容科の特色" /></h3>
					<li>
						<h4 class="point">ポイント1</h4>
						<h4 class="tit_tokucho2">国家試験の合格率100％<br>就職率100％</h4>
						<p>国家試験は100%の合格率。試験本番を想定した実習が充実しているから、いつも通りの力を発揮することができます。そして、就職率も100％と、確実に社会で活躍できる充実したバックアップ体制が整っています。</p>
					</li>
					<li>
						<h4 class="point">ポイント2</h4>
						<h4 class="tit_tokucho2">注目の<br>レディースシェービング。</h4>
						<p>理容師しかできない仕事の一つとして、いま「シェービング（顔そり）」が大注目！ブライダル・エステの分野でもレディースシェービングが、注目を集めるなど、多彩な分野で「シェービング技術」を持った理容師が求められています。</p>
					</li>
					<li>
						<h4 class="point">ポイント3</h4>
						<h4 class="tit_tokucho1">一流の講師陣が指導。</h4>
						<p>サロン経営や実務経験のある一流の講師陣が指導しますから、サロンで役立つ高い技術を身につけることができます。カット、シェービング技術も超一流！全国学生技術大会で優勝するなど、大きな舞台で活躍する先輩達がいます。</p>
					</li>
				</ul><!--tokucho-->
			</div><!--course_sec_01-->


			<div class="course_sec_02">
				<h2><img src="/common/images/course/barber/h2_curriculum.gif" alt="カリキュラム Curriculum" /></h2>
				<p class="top_comment">
				<img src="/common/images/course/total/img_curri01.jpg" alt="">
				ショートからロングまでスタイルを自在につくる、ハイレベルなカットの技術を習得します！<br>
				いま注目のレディースシェービングやエステ、<br>
				ネイルやメイクまで、トータルに学ぶことができます。
				</p>
				<ul id="curriCnts01">
					<li>
						<h3>理容実習：シェービング</h3>
						<img src="/common/images/course/barber/img_curri02.jpg" alt="フェイシャルエステ">
						<p>学生同士の相モデルで行い、実践感覚と基礎を学びます。フェイシャルエステは、レディースシェービングに応用可能で、まさにサロンで必要とされる人材へ。シェービング技術のすべてを学び、実践的な人材を育てます。</p>
						<p class="center"><a href="/shaving/" class="over"><img src="/common/images/course/barber/btn_shaving.jpg" alt="詳しくはこちら"></a></p>
					</li>
					<li>
						<h3>理容実習：カット</h3>
						<img src="/common/images/course/barber/img_curri03.jpg" alt="ボディエステ">
						<p>国家試験課題のミディアムカットは、刈り上げ中心のヘアスタイル。サロンで求められている刈り上げが確実に身に付きます。ショートからロングまで、男性も女性もいろいろな髪型に対応できるプロフェッショナルを目指します。</p>
					</li>
					<li>
						<h3>ネイル</h3>
						<img src="/common/images/course/barber/img_curri04.jpg" alt="ネイル">
						<p>より高度なサービスを提供できる知識と技術を磨くために、ネイルの基礎であるケア、カラーリングを中心に学びます。</p>
					</li>
					<li>
						<h3>フリースタイル</h3>
						<img src="/common/images/course/barber/img_curri05.jpg" alt="メイク">
						<p>メンズフリースタイルでは、自分の個性を発揮。基礎をしっかり学ぶことで、応用力も確実に身につけることができます。</p>
					</li>
				</ul><!--curriCnts01-->
				<ul id="curriCnts02">
					<li>理容実習…ワインディング・カット・シェービング・シャンプー</li>
					<li class="clear">関係法規・制度</li>
					<li>衛生管理</li>
					<li>保健</li>
					<li>物理・化学</li>
					<li>理容文化論</li>
					<li>運営管理</li>
					<li class="clear">理容技術理論</li>
					<li>総合</li>
					<li>色彩</li>
					<li>ビジネス学</li>
					<li>エステティック</li>
					<li>シャンプー</li>
					<li class="clear">実務実習</li>
				</ul><!--curriCnts02-->

				<div id="shikaku01">
					<h3><img src="/common/images/course/barber/h3_shikaku01.png" alt="取得可能資格"></h3>
					<p>
					理容師国家試験はもちろんのことエステ・ネイルなど、幅広い資格・検定試験を受けることができます。<br>
					授業では、個々の性格や技術力に合わせて合格プログラムを作成。<br>
					国家試験のプロである教員が、そのノウハウを余すことなく伝授します。
					</p>
					<ul id="shikakuCnts">
						<li>
							<h4>理容師国家試験受験資格</h4>
							<p>理容師への第一歩として、国家試験受験のために、厚生労働省指定養成施設である当校で所定の単位を取得。</p>
						</li>
						<li>
							<h4>公益社団法人日本理容美容教育センター認定制度<br>ABE　エステティックアシスタントディレクター</h4>
							<p>衛生・安全面およびスキンケアの基本知識とフェイシャルの基礎技術に関する試験です。</p>
						</li>
						<li>
							<h4>公益社団法人日本理容美容教育センター認定制度<br>ABE　ネイルアシスタントディレクター</h4>
							<p>ネイルケアに関する基本的な技術、および、知識などに関する試験です。</p>
						</li>
						<li>
							<h4>日本ネイリスト協会 ネイリスト技能検定試験3級</h4>
							<p>JNA日本ネイリスト協会が主催する、ネイルケアに関する基本的な技術及び知識などに関する試験です。</p>
						</li>
						<li>
							<h4>社団法人全国服飾教育者連合会 色彩検定3級</h4>
							<p>色に関する幅広い知識を問う検定試験です。理容師にもヘアカラーリングや、レディースシェービング後のメイクなど色に関する知識が求められています。</p>
						</li>
					</ul>
				</div><!--shikaku01-->
			</div><!--course_sec_02-->

			<div class="course_sec_03">
				<h3><img src="/common/images/course/barber/h3_st.gif" alt="在校生に聞きました。Current Student"></h3>
				<ul class="comment03">
					<li>
						<h4>先生の熱意と高い指導力のおかげで、北陸3県で１位になりました。</h4>
						<p>
							<img class="left" src="/common/images/course/barber/img_st01_1.jpg" alt="木本貴子さん　平成25年入学  高岡商業高等学校出身">
							母も祖父母も理容師で、自宅で理容店を営んでいます。
							様々なお客様に柔軟に対応している母を見て、親ですが憧れています。母の母校であることや、設備面もとても整っているこの学校へ入学。先生方の熱意と高い指導力のおかげで、北陸3県<br>の大会ではワインディング部門で1位になりました。<br>
							今後は、エステやレディースシェービングの分野で<br>女性も活躍できますし、お客様から慕われる<br>理容師になりたいです。
						</p>
						<p class="img01_st"><img src="/common/images/course/barber/img_st01_2.jpg" alt="木本貴子さん　平成25年入学  高岡商業高等学校出身"></p>
					</li>
					<li>
						<h4>一流の技術を持つ先生のもと、全国のトップを目指せます。</h4>
						<p>
							<img class="left" src="/common/images/course/barber/img_st02_1.jpg" alt="堀田慎一郎さん 平成25年入学  富山第一高等学校出身">
							地元の理容店のオーナーに憧れて、理容師の道へ。<br>
							この学校では一流の技術を持った先生方から、楽しく技術を教えてもらえます。熱い想いを持った方ばかりで、自然とやる気もわいてきますね。実習でも、日々、仲間と練習しているうちに、確実に力がつくのを実感。<br>
							同じ夢を持つ仲間や先輩との学校生活は、本当に<br>楽しいもの。全国大会で優勝した先輩を目標に、<br>より高いレベルの理容師を目指していきたいです。
						</p>
						<p class="img02_st"><img src="/common/images/course/barber/img_st02_2.jpg" alt="堀田慎一郎さん 平成25年入学  富山第一高等学校出身"></p>
					</li>
				</ul><!--comment03-->
				<h3><img src="/common/images/course/barber/h3_gd.gif" alt="卒業生に聞きました。Graduate"></h3>
				<ul class="comment03">
					<li>
						<h4>全国に誇れる最新の設備と環境、自分の理想の仕事がきっと見つかる。</h4>
						<p><img src="/common/images/course/barber/img_gd01.jpg" alt="松本涼さん　平成24年卒業　理容師　Hair Visualist AXE勤務"></p>
						<p>授業は、実習も学科もとてもわかりやすく、仕事に活かせる実践的な勉強ができました。先生方の熱い指導のもと、仲間たちと競い合い、助け合って学んだことは、とてもいい思い出です。全国に誇れる最新の設備はもちろん、富山駅から近く、そばには環水公園、スターバックスもあり、抜群の環境が整っています。この学校でしっかり学べば、自分に合った理想の仕事が、きっと見つけられるはずです。</p>
					</li>
					<li>
						<h4>最高の環境のなかで、レディースシェービングを学べます。</h4>
						<p><img src="/common/images/course/barber/img_gd02.jpg" alt="板坂眞登香さん　平成24年卒業　理容師 エステスタジオAXE勤務"></p>
						<p>学校では、サロンを経営する先生方から実践的に教えていただけるので、国家試験も安心です。授業では、エステやレディースシェービング、色彩など、幅広く、楽しく学びました。先生方がとても親身で、気軽に質問できる雰囲気が良かった。レディースシェービングやエステの仕事をしていますが、最高の環境で学んだおかげで、理想のお店に就職できました。将来は専門店を開くのが夢です。</p>
					</li>
				</ul><!--comment03-->
			</div><!--course_sec_03-->

			<div class="course_sec_04">
				<h3><img src="/common/images/course/barber/h3_04.png" alt="全国大会で有償の快挙を達成!!"></h3>
				<p class="txt04">2013年に兵庫県神戸市で開催された、全国理容美容学生技術大会で理容部門ミディアムカットで本校理容科の佐生大さんが優勝するなど、本校の在校生、卒業生は、全国大会で数々のトップの成績を収めています。<br>
				本校では一流の技術をもった先生方が、つねに実践的なサロンワークを、一人ひとりに丁寧に指導しています。国家試験の合格率100％であることはもちろん、より高いレベルの技術力を身につけることが可能な、優れた学びの環境が整っています。</p>
				<p class="img04"><img src="/common/images/course/barber/img_04.png" alt="全国大会で有償の快挙を達成!!の画像"></p>
			</div><!--course_sec_04-->

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>