<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>保護者の方へ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/parents/img_main.jpg" alt="保護者の方へ" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href=""><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">保護者の方へ</li>
			</ul>
		</div>
		<div id="mainarea" class="parents_content">
				<p>
				理容・美容の専門技術者の養成機関として、昭和22年に開校して以来、沢山の優秀な人材を輩出し続けている本校。常に時代を先行くトップクラスの教育水準を維持し続けています。最新の機器を導入した実践的な教育環境と実力のある指導者の元で全国でも屈指のスタイリスト技術と接客術を学ぶことができます！
				</p>
				
				<table>
				<colgroup span="1" class="sec_01"></colgroup>
				<colgroup span="1" class="sec_02"></colgroup>
				<colgroup span="1" class="sec_01"></colgroup>
					<tr>
						<td>
								<a href="/school/facility.php"><img src="/common/images/parents/btn_facility.jpg" alt="最先端の施設・設備" class="over" /></a>
								<p>本校自慢の施設・設備は、北陸トップクラス水準です。実践的な教育環境によって、現場ですぐに活躍できるプロを養成します。</p>
						</td>
						<td>&nbsp;</td>
						<td>
								<a href="/employment/"><img src="/common/images/parents/btn_employment.jpg" alt="就職支援" class="over" /></a>
								<p>富山県内で約1,800店が加盟する富山県理容組合・富山県美容組合の強力なサポートで、入学から資格取得、就職まで本校の学生をしっかりサポートし続けています。</p>
						</td>
					</tr>
					<tr>
						<td>
								<a href="/admission/tuition.php"><img src="/common/images/parents/btn_tuition.jpg" alt="学費について" class="over" /></a>
								<p>卒業までに必要な授業料や行事費、教材費等を詳しく説明します。</p>
						</td>
						<td>&nbsp;</td>
						<td>
								<a href="/support/"><img src="/common/images/parents/btn_support.jpg" alt="学費サポート" class="over" /></a>
								<p>本校では、夢を追いかける生徒さんの力になりたい思いで、数種類の学費サポートをご用意しております。</p>
						</td>
					</tr>
				</table>
				<div class="placement_box">
				<h3><img src="/common/images/placement/ttl_to.png" alt="送付先・お問い合わせ"></h3>
					<dl class="cf">
						<dt>
							<img src="/common/images/placement/ttl_ribiyo.png" alt="">
						</dt>
						<dd>
							<p>
							<span class="btn_tel">076-432-3037</span><span class="btn_call">0120-190-135</span><br>
							<span class="btn_fax">076-432-3046</span><br>
							<a href="/inquiry/"><img src="/common/images/placement/btn_inquiry.png" alt="お問い合わせ・資料請求フォームはこちら" class="over" /></a>
							</p>
						</dd>
					</dl>
				</div>

			</div><!-- /display_none end -->

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>