#!/usr/bin/perl

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#\\  Creation company : WEB service DIC (ウェブサービス ディック)
#\\  produce by Manabu_Kimura
#\\  http://www.d-ic.com/
#\\  DIC-Studio. Mail_v26 Version:1.5 (2007/05/09)
#\\  Copyright (C) DIC All Rights Reserved. このスクリプトの再配布などを禁止します.
#\\  バグ報告は studio@d-ic.com 宛にお願いします。
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

##*****<< 設置方法 >>******************************************************************************
##
## ※ＣＧＩファイルの初期設定をお使いの環境に合わせてカスタマイズしてください。
## ※お使いのサーバによっては下のファイル構成では動作しない場合があります。
##   その際はサーバ管理者にお問い合わせください。
## ※"[]"内の数字はパーミッションです。
## ※"form.html"は、同梱の"sample.html"を参考に作成してください。
##
## public_html/
##  |         form.html
##  |
##  +-- cgi-bin/
##       |
##       +-- mail_v26/
##              |    mail.cgi	[755]
##              |    jcode.pl
##              |    stdio.pl
##              |    check.html
##              |    title.gif
##              |    remail.txt
##              +-- tmp/	[777]
##
##*****<< バージョンアップ情報 >>******************************************************************
##
## 2007/05/09 .....Ver1.5
##   ・セキュリティ対策
##
## 2007/05/08 .....Ver1.4
##   ・送信メールアドレス、メール件名、必須項目、送信完了ページのURL、をmail.cgiにて設定するように修正。
##
## 2005/05/12 .....Ver1.3
##   ・添付ファイルを最大5つまで可能なように改良。
##
## 2004/12/05 .....Ver1.2
##   ・同じname属性が複数あった場合に重複してしまうバグを修正。
##   ・添付するファイル名を重複しないユニーク値に変更。
##
## 2003/07/11 .....Ver1.1
##   ・入力フォームにてEmailを省略した場合エラーを起こすバグを修正。
##
## 2003/06/03 .....Ver1.0
##   ・配布開始
##
##*************************************************************************************************


##=====================================
## サブルーチン                       =
##=====================================
require './jcode.pl';
require './stdio.pl';


##=====================================
##           初期設定部分             =
##=====================================

# 送信メールアドレス
$mailaddress = 'nojiri@mdm-web.jp';

# 件名
$subject = '富山県理容美容専門学校へのお問い合わせ';

# 入力必須項目
@necessary = ();

# 送信完了画面のURL
$thank_page = './inquiry_finish.php';

# sendmailのパス
#$sendmail = '/usr/sbin/sendmail';
$sendmail = 'c:\sendm\sendmane.exe'; 

# 自動返信機能 ON/OFF（1=ON  2=OFF）
$auto_remail = '1';

# テンプレート（自動返信メールの本文）
$remail_txt = './remail.txt';

# テンプレートＨＴＭＬ（入力確認用）
$template1 = './inquiry_check.php';

# 確認画面のテーブルデザイン
$tablewidth = '100%';		# テーブルの横幅
$bgcolor1 = '#f08888';		# 項目の色
$bgcolor2 = '#FFFFFF';		# 内容の色
$border = '0';				# 枠線の太さ（なし=0）
$bordercolor = '#999999';	# 枠線の色
$cellpadding = '0';			# cellpadding

# 一時ファイル生成ディレクトリ
$tmp = './tmp/';

# 添付を許可するファイル拡張子（"'"で囲み、","で区切る）
@impossible = ('jpg','JPG','gif','GIF','png','PNG','txt','TXT','xls','csv','doc','zip','lzh');

# 添付ファイルの最大容量（単位はバイト）
# 1KB = 1024*1 = 1024バイト
# 1MB = 1024*1024 = 1048576バイト
# 1048576バイト以上は指定できません。
$maxsize = '512000';




#□□□□□□□□□□□□ ここから下を修正した場合にはサポート対象外になります。ご注意ください。 □□□□□□□□□□□

##=====================================
## データを受け取る                   =
##=====================================
%param = ();
@key_list = stdio::getMultipartFormData(\%param,0,sjis,1," \/ ",);





#□□□□□□□□□□□□□□□□□□□ モード指定 "なし" □□□□□□□□□□□□□□□□□□□□□□□
if(!$param{'mode'}){

##=====================================
## 必須項目の入力チェック             =
##=====================================
foreach(@necessary){
	if(!$param{$_}){
		$hissu_check .= "・ ".$_."<br>";
	}
}
if($hissu_check){ &error('入力エラー',"以下の必須項目に入力がありませんでした。<br><br>$hissu_check"); }


##=====================================
## Eメール入力のチェック              =
##=====================================
if($param{'Email'}){
	if($param{'Email'} =~ /^\S+@\S+\.\S+/){ ; }
	else{ &error('入力エラー',"Eメールアドレスの入力を間違えています。"); }
	if($param{'Email'} =~ /Ａ|Ｂ|Ｃ|Ｄ|Ｅ|Ｆ|Ｇ|Ｈ|Ｉ|Ｊ|Ｋ|Ｌ|Ｍ|Ｎ|Ｏ|Ｐ|Ｑ|Ｒ|Ｓ|Ｔ|Ｕ|Ｖ|Ｗ|Ｘ|Ｙ|Ｚ/){ &error('入力エラー',"Eメールアドレスの入力が間違えています。半角英数字を使用してください。"); }
	if($param{'Email'} =~ /ａ|ｂ|ｃ|ｄ|ｅ|ｆ|ｇ|ｈ|ｉ|ｊ|ｋ|ｌ|ｍ|ｎ|ｏ|ｐ|ｑ|ｒ|ｓ|ｔ|ｕ|ｖ|ｗ|ｘ|ｙ|ｚ/){ &error('入力エラー',"Eメールアドレスの入力が間違えています。半角英数字を使用してください。"); }
	if($param{'Email'} =~ /[^\-\.\@\_0-9a-zA-Z]/){ &error('エラー','入力を間違えています。'); }
}


##=====================================
## 添付ファイルのチェック
##=====================================
&file_check($param{'file1->name'}, $param{'file1->size'});
&file_check($param{'file2->name'}, $param{'file2->size'});
&file_check($param{'file3->name'}, $param{'file3->size'});
&file_check($param{'file4->name'}, $param{'file4->size'});
&file_check($param{'file5->name'}, $param{'file5->size'});

sub file_check
{
	local($filename, $file_size) = @_;
	local($impossible_flag, @upfiile);
	
	if($filename){
		$impossible_flag = 0;
		@upfile = split(/\./,$filename);
		foreach(@impossible){
			if($upfile[-1] eq $_){
				$impossible_flag = 1;
			}
		}
		if(!$impossible_flag){ &error('エラー','ご指定のファイル拡張子は、添付ファイルとして許可されておりません。'); }
		if($file_size > $maxsize){ &error('エラー','添付ファイルのサイズ容量が大きすぎます。'); }
	}
}


##=====================================
## 特殊記号を変換する                 =
##=====================================
foreach (@key_list) {
	if(/file/){ next; }
	$param{$_} =~ s/&/＆/g;
	$param{$_} =~ s/"/”/g;
	$param{$_} =~ s/</＜/g;
	$param{$_} =~ s/>/＞/g;
	$param{$_} =~ s/,/，/g;
	$param{$_} =~ s/'/’/g;
	$param{$_} =~ s/\n/ /g;
}


##=====================================
## 添付ファイルのファイル名           =
##=====================================
if($param{'file1->name'}){ $unique1 = &fileName($param{'file1->name'}); }
if($param{'file2->name'}){ $unique2 = &fileName($param{'file2->name'}); }
if($param{'file3->name'}){ $unique3 = &fileName($param{'file3->name'}); }
if($param{'file4->name'}){ $unique4 = &fileName($param{'file4->name'}); }
if($param{'file5->name'}){ $unique5 = &fileName($param{'file5->name'}); }

sub fileName 
{
	local($tmp_name) = @_;
	local($cnt, $unique, @tmp_name);
	
	@tmp_name = split(/\./, $tmp_name);
	
	$cnt = 1;
	foreach(@tmp_name){
		$unique .= $_;
		if($#tmp_name == $cnt){ last; }
		$cnt++;
	}
	
	$unique .= "_";
	$unique .= &getRandomString(3, "0123456789");
	$unique .= ".".$tmp_name[-1];
	return $unique;
}


##=====================================
## 入力確認画面の一部                 =
##=====================================
@key_list = grep(!$seen{$_}++, @key_list);

foreach(@key_list){
	if($_ eq 'file1'){ if($unique1){ $mail_table .= qq|<tr><td bgcolor="$bgcolor1">file1</td><td bgcolor="$bgcolor2">$unique1 ($param{'file1->size'}バイト)<input type="hidden" name="file1" value="$unique1"></td></tr>\n|; next; } }
	elsif($_ eq 'file2'){ if($unique2){ $mail_table .= qq|<tr><td bgcolor="$bgcolor1">file2</td><td bgcolor="$bgcolor2">$unique2 ($param{'file2->size'}バイト)<input type="hidden" name="file2" value="$unique2"></td></tr>\n|; next; } }
	elsif($_ eq 'file3'){ if($unique3){ $mail_table .= qq|<tr><td bgcolor="$bgcolor1">file3</td><td bgcolor="$bgcolor2">$unique3 ($param{'file3->size'}バイト)<input type="hidden" name="file3" value="$unique3"></td></tr>\n|; next; } }
	elsif($_ eq 'file4'){ if($unique4){ $mail_table .= qq|<tr><td bgcolor="$bgcolor1">file4</td><td bgcolor="$bgcolor2">$unique4 ($param{'file4->size'}バイト)<input type="hidden" name="file4" value="$unique4"></td></tr>\n|; next; } }
	elsif($_ eq 'file5'){ if($unique5){ $mail_table .= qq|<tr><td bgcolor="$bgcolor1">file5</td><td bgcolor="$bgcolor2">$unique5 ($param{'file5->size'}バイト)<input type="hidden" name="file5" value="$unique5"></td></tr>\n|; next; } }
	elsif($_ eq 'submit'){ next; }
	
	if($param{$_}){ $param = $param{$_}; }
	else{ $param = ''; }
	$mail_table .= "\n<tr><td bgcolor=\"$bgcolor1\" nowrap>$_</td><td bgcolor=\"$bgcolor2\">$param<input type=\"hidden\" name=\"$_\" value=\"$param{$_}\"></td></tr>";
}

$mail = qq|
<input type="hidden" name="mode" value="mailsend">
<table border="0" cellpadding="$cellpadding" cellspacing="$border" width="100%">
$mail_table
</table>
|;


##=====================================
## 添付ファイルを一時保存
##=====================================
&file_up($unique1, $param{'file1'});
&file_up($unique2, $param{'file2'});
&file_up($unique3, $param{'file3'});
&file_up($unique4, $param{'file4'});
&file_up($unique5, $param{'file5'});

sub file_up
{
	local($file_name, $file) = @_;
	local($filename);
	
	if($file_name ne ""){
		$filename = $tmp.$file_name;
		if(open(OUT, ">$filename")){
			binmode(OUT);
			print OUT $file;
			close(OUT);
		} else {
			&error('エラー','ファイルのアップロードに失敗しました。');
		}
	}
}


##=====================================
## 確認テンプレートファイルをオープン =
##=====================================
if(!open(HTML,$template1)){ &error('システムエラー',"テンプレートファイル ( $template1 ) がオープンできません。");}
@html = <HTML>;
close(HTML);


##=====================================
## 特殊文字を置き換え                 =
##=====================================
$dic = qq|<div align="right" class="copyright">- <a href="http://www.d-ic.com/" target="_top">メール送信プログラム： DIC-Studio</a> -</div>|;
foreach(@html){
	if($_ =~ /_%copyright%_/){ $flag = '1'; }
	s/_%mail%_/$mail/g;
	s/_%Email%_/$mailaddress/g;
	s/_%copyright%_/$dic/g;
}
if(!$flag){ &error('システムエラー','著作権表示が削除されています。'); }


##=====================================
## 確認画面表示                       =
##=====================================
print <<"EOF";
Content-type: text/html

@html
EOF
exit;
}	# モード指定 なし ここまで





#□□□□□□□□□□□□□□□□□□□ モード指定 "mailsend" □□□□□□□□□□□□□□□□□□□□□□□
if($param{'mode'} eq 'mailsend'){

##=====================================
## 特殊記号を変換する                 =
##=====================================
foreach (@key_list) {
	if(/file/){ next; }
	$param{$_} =~ s/\n/ /g;
	$param{$_} =~ s/&/＆/g;
	$param{$_} =~ s/"/”/g;
	$param{$_} =~ s/</＜/g;
	$param{$_} =~ s/>/＞/g;
	$param{$_} =~ s/,/，/g;
	$param{$_} =~ s/'/’/g;
}


##=====================================
## メール本文                         =
##=====================================
$mailbody = "$subject\n\n";

foreach(@key_list){
	if($_ eq 'mode'){ next; }
	elsif($_ eq 'remail'){ next;}
	
	$mailbody .= "■$_\n$param{$_}\n\n";
	$remailbody .= "\n■$_\n$param{$_}\n";
}


##=====================================
## メール送信                         =
##=====================================
if($param{'Email'}){ $mailfrom = $param{'Email'}; }
else{ $mailfrom = 'xxxxx@xxxx.xxx'; }

foreach(1 .. 5){
	if($param{'file'.$_}){
		$file_flag = 1;
	}
}

$file1 = $tmp.$param{'file1'};
$file2 = $tmp.$param{'file2'};
$file3 = $tmp.$param{'file3'};
$file4 = $tmp.$param{'file4'};
$file5 = $tmp.$param{'file5'};

if($param{'file1'}){ push(@at_file, $file1); }
if($param{'file2'}){ push(@at_file, $file2); }
if($param{'file3'}){ push(@at_file, $file3); }
if($param{'file4'}){ push(@at_file, $file4); }
if($param{'file5'}){ push(@at_file, $file5); }

%header = (
    'To'      => $mailaddress,
    'From'    => $mailfrom,
    'Subject' => $subject
);
if($file_flag){ $result = stdio::sendmail($sendmail, \%header, $mailbody, 0, 0,@at_file); }
else{ $result = stdio::sendmail($sendmail, \%header, $mailbody); }

if(!$result){ &error('システムエラー',"メールの送信に失敗しました。"); }


##=====================================
## 自動返信メールのテンプレートを開く =
##=====================================
if($auto_remail eq 1){	# 自動返信 ON/OFF
if(!open(TXT,$remail_txt)){ &error('システムエラー',"ファイル ( $remail_txt ) がオープンできません。"); }
@txt = <TXT>;
close(TXT);

foreach(@txt){
	s/_%remail_body%_/$remailbody/g;
	$re_mailbody .= $_;
}


##=====================================
## 自動返信メールの送信               =
##=====================================
if($param{'Email'}){

$re_subject = "Re: ".$subject;
%header = (
    'To'      => $mailfrom,
    'From'    => $mailaddress,
    'Subject' => $re_subject
);
$result = stdio::sendmail($sendmail, \%header, $re_mailbody, 0, 0,);
if(!$result){ &error('システムエラー',"メールの送信に失敗しました。"); }
}
}	# 自動返信 ON/OFF


##=====================================
## 一時ファイルを削除
##=====================================
if($param{'file1'}){ unlink $tmp.$param{'file1'}; }
if($param{'file2'}){ unlink $tmp.$param{'file2'}; }
if($param{'file3'}){ unlink $tmp.$param{'file3'}; }
if($param{'file4'}){ unlink $tmp.$param{'file4'}; }
if($param{'file5'}){ unlink $tmp.$param{'file5'}; }

@tmpfile = glob($tmp."*");
foreach(@tmpfile){
	@stat = stat $_;
	if((time - $stat[9]) > 86400){ unlink $_; }
}


##=====================================
## メール送信完了表示                 =
##=====================================
#print <<"EOF";
#Content-type: text/plain
#
#$mailbody
#
#
#$re_mailbody
#EOF
#exit;
#
#
if($thank_page){
	print "Location: $thank_page\n\n";
}else{
	print <<"EOF";
Content-type: text/html

<html>
<head>
<title>送信完了</title>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
</head>
<body>
<h2>送信完了</h2>
ご記入されたものは<a href="mailto:$mailaddress">$mailaddress</a>宛てに電子メールされました。<br>
<br>
<br>
<a href="javascript:history.go(-2)">戻る</a>
</body>
</html>
EOF
exit;
}
}	# モード指定 mailsend ここまで





##=====================================
## エラー表示                         =
##=====================================
sub error {
print <<"END";
Content-type: text/html

<html><head><title>$_[0]</title>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<style type="text/css">
<!--
td {  font-size: 14px}
a {  color: #333}
a:hover {  color: #FF3366}
-->
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000">
<table width="500" border="2" bordercolor="#2CB8FF" cellpadding="3" cellspacing="0" align="center">
 <tr>
  <td bgcolor="#2CB8FF"><font color="#FFFFFF"><b>$_[0]</b></font></td>
 </tr>
 <tr>
  <td>
   <br>
   <blockquote>
    <b>$_[1]</b>
    <p><a href="JavaScript:history.back()">こちらをクリックして前の画面に移動してください。</a>
   </blockquote>
  </td>
 </tr>
</table>
</body></html>
END
exit;
}



# ============================
# Make Random String.
# ============================
# stdio.pl から引用 <<http://www.webpower.jp/>>
sub getRandomString #($len, $str)
{
  local($len, $str) = @_;
  local(@str) = $str ? split //, $str : ('A'..'Z','a'..'z','0'..'9');

  undef $str;
  $len = 8 if (!$len);
  for (1 .. $len) {
    $str .= $str[int rand($#str+1)];
  }
  return $str;
}
