<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<link rel="stylesheet" href="/hyuga.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-40732470-1', 'toyama-bb.ac.jp');
      ga('send', 'pageview');
    
    </script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article>
			<section>
				<div id="magazine">
					<h2><img src="/images/inquiry/mailmagazine/title01.png" alt="メールマガジン" /></h2>
					<div class="magazine_box">
						<div class="magazine_list">
							<p class="magazine_a">登録完了</p>
							<p>メールマガジンへ登録しました。</p>
						</div>
					</div>
				</div>
			</section>
		</article>
		<aside>
			<?php require_once($_SERVER['DOCUMENT_ROOT']."/inc/sidebnr.inc");?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>メールマガジン</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/inc/footer.inc");?>
	</footer>
</body>
</html>