	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-40732470-1', 'toyama-bb.ac.jp');
      ga('send', 'pageview');
    
    </script>
	<header class="cfi">
		<h1><a href="/sp/"><img src="/sp/common/images/common/logo.png" alt="富山県理容美容専門学校" width="100%" /></a></h1>
		<div class="cfi">
			<ul>
				<li>
					<a href="javascript:void(0);" id="menuOpen"><img src="/sp/common/images/common/btn_menu.png" alt="MENU" width="100%" /></a>
					<div id="toggleMenu">
						<div class="b-b cfi">
							<h3><img src="/sp/common/images/common/nav/ttl_menu.png" alt="menu"></h3>
							<div id="btn_close"><a href="javascript:void(0);" id="trg_close"><img src="/sp/common/images/common/nav/btn_close.png" alt="close"></a></div>
						</div>
						<ul id="menuList">
							<li><a href="/sp/" class="arrow">トップページ</a></li>
							<li><a href="/sp/course/" class="arrow">学科紹介</a></li>
							<li class="circle cfi"><a href="/sp/course/barber.html" class="arrow">理容科</a></li>
							<li class="circle cfi"><a href="/sp/course/beauty.html" class="arrow">美容科</a></li>
							<li class="circle cfi"><a href="/sp/course/esthetic.html" class="arrow">エステティック科</a></li>
							<li class="circle cfi"><a href="/sp/course/total.html" class="arrow">トータルビューティ科</a></li>
							<li><a href="/sp/admission/" class="arrow">入学案内</a></li>
							<li><a href="/sp/access/" class="arrow">アクセス</a></li>
							<li><a href="/sp/tuition/" class="arrow">学費一覧</a></li>
							<li class="circle cfi"><a href="/sp/tuition/support.html" class="arrow">学費サポート</a></li>
							<li><a href="/sp/opencampus/" class="arrow">オープンキャンパス</a></li>
							<li><a href="/sp/mailmagazine/" class="arrow">メールマガジン</a></li>
							<li><a href="/sp/inquiry/" class="arrow">お問い合わせ</a></li>
						</ul>
						<ul id="menuBtn">
							<li><a href="https://www.facebook.com/pages/%E5%AF%8C%E5%B1%B1%E7%9C%8C%E7%90%86%E5%AE%B9%E7%BE%8E%E5%AE%B9%E5%B0%82%E9%96%80%E5%AD%A6%E6%A0%A1/591984787519515" target="_blank"><img src="/sp/common/images/common/nav/btn_facebook.png" alt="Facebookページ"></a></li>
							<li><a href="/" target="_blank"><img src="/sp/common/images/common/nav/btn_pcsite.png" alt="PCサイトへ"></a></li>
						</ul>
					</div>
				</li>
				<li><a href="https://www.facebook.com/pages/%E5%AF%8C%E5%B1%B1%E7%9C%8C%E7%90%86%E5%AE%B9%E7%BE%8E%E5%AE%B9%E5%B0%82%E9%96%80%E5%AD%A6%E6%A0%A1/591984787519515" target="_blank"><img src="/sp/common/images/common/btn_facebook.png" alt="FaceBook" width="100%" /></a></li>
			</ul>
		</div>
	</header>