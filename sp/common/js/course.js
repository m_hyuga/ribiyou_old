/*********************************************************************
	 tabpanel
*********************************************************************/
(function($) {
	$(function(){
/* $("ul#courseTab").hide(); */
		$("ul#coursePanel>li:not("+$("ul#courseTab>li.selected>a").attr("href")+")").hide();
		$("ul#courseTab>li>a").click(function(){
			$("ul#courseTab>li").removeClass("selected");
			$(this).parent("li").addClass("selected");
			$("ul#coursePanel>li").hide();
			$($(this).attr("href")).show();
			return false;
		});
/*
		$("ul#coursePanel>li:not("+$("ul#courseTab>li>a.selected").attr("href")+")").hide();
		$("ul#courseTab>li>a").click(function(){
			$("ul#courseTab>li>a").removeClass("selected");
			$(this).addClass("selected");
			$("ul#coursePanel>li").hide();
			$($(this).attr("href")).show();
			return false;
		});
*/
	});
})(jQuery);


