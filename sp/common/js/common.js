$(function () {
	//グローバルナビゲーション箇所
	function MenuObj(){
		//変数定義
		var $toggleTrg = $('#menuOpen'); //Aタグ イベント発火用ID
		var $toggleObj = $('#toggleMenu'); //toggle対象のID
		$toggleObj.hide(); // toggle対象を隠す
		//実行処理
		$('#toggleMenu')
		$toggleTrg.on('click', function(event){
			$toggleObj.fadeIn(500);
			$('body').append('<div id="submenu_wrap"></div>');
		});
		$toggleObj.on('click','#trg_close', function(event){
			$toggleObj.fadeOut(500);
			$('#submenu_wrap').remove();
		});
	}
	MenuObj();


	//------------------------トップへスクロール
	$('a[href^=#]').click(function(){
	var speed = 300;
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top;
	$("html, body").animate({scrollTop:position}, speed, "swing");
	return false;
	});
	
  $('.bxslider').bxSlider({
    auto:true,
    speed:500,
    captions:false,
    responsive:true,
		controls:false
  });
	setTimeout("scrollTo(0,1)", 100);

	//入学案内ページタブ
	$('.admission_tab>li').click(function() {

		var index = $('.admission_tab>li').index(this);
		$('.content>li').css('display','none');
		$('.content>li').eq(index).css('display','block');
		$('.admission_tab>li').removeClass('select');
		$(this).addClass('select')
	});
	
	$(".open").click(function(){
		$(this).parent().parent().parent().parent().parent().children(".slideBox").slideToggle("slow");
	});

});