<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>和装コレクション | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<link rel="stylesheet" type="text/css" href="/common/css/colorbox.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.colorbox-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".opencampus_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/wasocollection/img_main.jpg" alt="和装コレクション" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">和装コレクション</li>
			</ul>
		</div>
		<div id="mainarea" class="wasocollection_content">
		<div>
			<img src="/common/images/wasocollection/ttl_img.jpg" alt="和装コレクション" />
			<h2><img src="/common/images/wasocollection/ttl_01.jpg" alt="Ⅰ部 日本髪髪結いショー" /></h2>
			<p>京都から舞妓はん・芸妓はんが県理美にやってきます！<br>
			一流の日本髪髪結いショーをお楽しみください♪</p>
			<table>
				<tr>
				<th><img src="/common/images/wasocollection/ico_nitiji.jpg" alt="日時" /></th>
				<td>10：00 ～ 11：30</td>
				<th><img src="/common/images/wasocollection/ico_basho.jpg" alt="場所" /></th>
				<td>富山県理容美容専門学校</td>
				</tr>
			</table>
			<p>13：00からは場所を移して<span class="btm_line">、富山市グランドプラザにて写真撮影会を実施予定</span></p>
<br><br>
			<h2><img src="/common/images/wasocollection/ttl_02.jpg" alt="Ⅱ部 和装コレクション" /></h2>
			<p>県理美学生が現代モダンの和装美へチャレンジ！<br>
			アイドルユニット「P.IDOL」をプロデュースします。<br>
			生徒たちの真剣な熱い想い、芸術的なヘアアレンジをお楽しみください♪</p>
			<table>
				<tr>
				<th><img src="/common/images/wasocollection/ico_nitiji.jpg" alt="日時" /></th>
				<td>16：00 ～ 17：00</td>
				<th><img src="/common/images/wasocollection/ico_basho.jpg" alt="場所" /></th>
				<td><a href="http://www.grandplaza.jp/" target="_blank">富山市グランドプラザ</a></td>
				</tr>
			</table>

		</div>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>