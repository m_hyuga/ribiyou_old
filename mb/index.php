<?php echo '<?xml version="1.0" encoding="Shift_JIS"?>' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head><title>富山県理容美容専門学校</title>

<style type="text/css"><![CDATA[ a.view { font-size: 13px; color: #000000; background-color: #FFFFFF }]]></style>

<style type="text/css"><![CDATA[ a:link{color:#0000FF}a:visited{color:#800080}]]></style>

<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=Shift_JIS">

<meta name="viewport" content="width=300">

<meta name="keywords" content="">

<meta name="description" content="">

</head>

<body style="background-color:#FFFFFF;">

<a id="pagetop" name="pagetop"></a>

<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#000000">

<tr>

<td align="center">

<span style="color:#FFFFFF"><span style="font-size:xx-small">富山県理容美容専門学校</span></span>

</td>

</tr>

</table>

<table border="0" cellpadding="0" cellspacing="0" width="240" align="center">

<tr>

<td width="240" align="center">

<img src="img/01q.gif" alt="理容・美容・エステ・ネイルメイクの県理美にいこう！" width="240" height="78" border="0" style="vertical-align:bottom;" />

<a href="/mb/barbar.html" accesskey="1"><img src="img/01_2q.jpg" alt="理容" width="240" height="30" border="0" style="vertical-align:bottom;" /></a>

<a href="/mb/beauty.html" accesskey="2"><img src="img/01_3q.jpg" alt="美容" width="240" height="30" border="0" style="vertical-align:bottom;" /></a>

<a href="/mb/esthe.html" accesskey="3"><img src="img/01_4q.jpg" alt="エステ" width="240" height="30" border="0" style="vertical-align:bottom;" /></a>

<a href="/mb/nail.html" accesskey="4"><img src="img/01_5q.jpg" alt="ネイル・メイク" width="240" height="30" border="0" style="vertical-align:bottom;" /></a></td>

</tr>

</table>

<br />

<table width="240" border="0" align="center" cellpadding="0" cellspacing="0">

	<tr>

		<td height="20" align="center"><a href="/mb/annai.html" accesskey="5"><img src="img/01_6q.jpg" alt="学校案内" width="115" height="20" border="0" /></a><a href="/mb/boshu.html" accesskey="6"><img src="img/01_7q.jpg" alt="募集要項" width="115" height="20" border="0" /></a></td>

	</tr>

	<tr>

		<td height="20" align="center" valign="top"><a href="/mb/seikyu.html" accesskey="5"><img src="img/01_8q.jpg" alt="資料請求" width="115" height="20" border="0" style="vertical-align:top;" /></a><a href="/mb/shien.html" accesskey="6"><img src="img/01_8-2q.jpg" alt="学費支援制度" width="115" height="20" border="0" style="vertical-align:top;" /></a></td>

	</tr>

</table>

<br />

<table width="227" border="0" align="center" cellpadding="0" cellspacing="0">

	<tr>

		<td><a href="/mb/student/mt4i.cgi"><img src="/mb/img/top_btn_student.jpg" alt="在学生の皆さんへ" width="240" height="20" border="0" /></a></td><!-- 元の画像先 "img/top_..." -->

	</tr>

</table>

<br />

<table width="240" border="0" align="center" cellpadding="0" cellspacing="0">

	<tr>

		<td><img src="/mb/img/t_new.jpg" width="240" height="30" alt="オープンキャンパス" /></td>

	</tr>

	<tr>
	<td><?php include('blognews.html') ?></td>
	
</tr>

</table>

<table border="0" cellpadding="0" cellspacing="0" width="240" align="center">

	<tr>

<td align="right">

<hr />

<span style="font-size:xx-small"><a href="#pagetop">▲ﾍﾟｰｼﾞﾄｯﾌﾟへ</a></span></td>

</tr>

<tr>

<td bgcolor="#000000" align="left"><font color="#FFFFFF"><span style="font-size:xx-small">学校法人 富山県理容美容学校</span></font></td></tr>

<tr><td align="left" bgcolor="#000000"><font color="#FFFFFF">富山県理容美容専門学校<br />

<span style="font-size:xx-small">  〒930-0804 富山県富山市下新町32-26</span><br />

<span style="font-size:xx-small">TEL <a href="tel:0764323037">076-432-3037</a> ﾌﾘｰﾀﾞｲﾔﾙ <a href="tel:0120190135">0120-190-135</a></span><br />

<span style="font-size:xx-small">FAX 076-432-3046</span><br />

<span style="font-size:xx-small">  Mail <a href="mailto:info@toyama-bb.ac.jp">info@toyama-bb.ac.jp</a></span></font><br /><br />

</td>

</tr>

<tr>

<td bgcolor="#333333" align="center">

<span style="color:#FFFFFF"><span style="font-size:xx-small">&copy;TOYAMA Barber & Beauty School.</span></span>

</td>

</tr>

</table>

</body>

</html>