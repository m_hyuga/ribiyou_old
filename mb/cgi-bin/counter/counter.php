<?php
#==========================================================================
#
# counter.php : phpカウンター
#
##  記述方法  　#===========================================================
#
#
#	http:// 〜　counter.php?cdata=counter&ipr=1&print=0&count=1
#
#
##  パラメータ  #-----------------------------------------------------------
# 
#	■ cdata = カウントデータ（ファイル）
#	デフォルト)
#				couter
#	例）
#			×××××××.txt データを使用する場合
#			http://　〜  counter.php?cdata=×××××××		※ .txtは省略
#
#	■ ipr =  IPによるリカウント
#　デフォルト)
#			0			※ ON
#	例)
#			ipr=0	※ OFF
#			ipr=1	※ ON
#
#	■ print =  表示
#	デフォルト)
#			1			※ ON
#	例)
#			print=0	※ OFF
#			print=1	※ ON
#
#  ■ count =  カウント
#	デフォルト)
#			1			※ ON
#	例)
#			count=0	※ OFF
#			count=1	※ ON
#
#  ■ digit =  桁数指定
#	デフォルト)
#			1			※ ON
#	例)
#			digit=0	※ OFF
#			digit=1	※ ON
#
#  ■ imgf =  カウントイメージ
#	デフォルト)
#			0
#	例)
#			img× フォルダのイメージを使用する場合
#			http://　〜  counter.php?imgf=×		※ .imgは省略
#
##  イメージの追加　#-----------------------------------------------------------
#
#	■ カウンタのイメージデータ追加方法
#		イメージフォルダの作成、フォルダ名指定
#	例)
#			img××	※××のところに半角英数を入れる
#
#		作成したフォルダにイメージファイルの追加
#
#			0.gif 〜　9.gif
#===============================================================================



#GD
	include("gifcat.php");
#---------------------
# 設定値受け取り
# 
#---------------------

$Pm = $_REQUEST;

#---------------------
# 省略時設定
# 
#---------------------
# カウントデータ
	if(!isset($Pm["cdata"]) || !strlen($Pm["cdata"])){
		$Pm["cdata"] = "counter";
	}

# IPによるリカウント
	if(!isset($Pm["ipr"]) || !strlen($Pm["ipr"])){
		$Pm["ipr"] = 0;
	}

# 表示
	if(!isset($Pm["print"]) || !strlen($Pm["print"])){
		$Pm["print"] = 1;
	}

# カウント
	if(!isset($Pm["count"]) || !strlen($Pm["count"])){
		$Pm["count"] = 1;
	}

# 桁数指定
	if(!isset($Pm["digit"]) || !strlen($Pm["digit"])){
		$Pm["digit"] = 6;
	}

# カウントイメージ
	if(!isset($Pm["imgf"]) || !strlen($Pm["imgf"])){
		$Pm["imgf"] = 0;
	}


	#---------------------
	# 処理開始（ファイルロック）
	# 
	#====================================================================

		$fillocker = fopen("lock/lock.txt","w");
		flock($fillocker,LOCK_EX);				//ロック(ファイルロック)
		
#		sleep(10);

	#---------------------
	# ファイルオープン　読み込み専用
	# 
	#---------------------

		$filepointer = fopen("data/".$Pm["cdata"].".txt","r");
		$fileline = fgets($filepointer);
		fclose($filepointer);
		$a = split(" ",$fileline);
		$fileline_k 	= $a[0];
		$MIP			= $a[1];
		$time			= $a[2];
		$mycount_h		= 0;

	#---------------------
	# IP
	# 
	#---------------------

		$IP = $_SERVER['REMOTE_ADDR'];
		$mycount_kr = (int)$fileline_k;

		if($Pm["count"] == 1 && ($_SERVER['REMOTE_ADDR'] != $MIP || $time < time() - 3600)){
			$mycount_kr += 1;
		}

	#---------------------
	# カウント
	# 
	#---------------------
	
	$mycount_k = (int)$fileline_k;

	if($Pm["count"] == 1){
		$mycount_k +=  1;
	}


	#---------------------
	# ファイルオープン　書き込み専用　
	# ファイルが存在しなかったら追加
	#
	#---------------------

		$filepointer = fopen("data/".$Pm["cdata"].".txt","w");
		stream_set_write_buffer($filepointer,0);

	#---------------------
	# カウンタ表示
	# 
	#---------------------

		if($Pm["ipr"] == 1){
			$mycount_h = $mycount_kr;
		}else{
			$mycount_h = $mycount_k;
		}

		$mycount_h2 = sprintf("%0".$Pm["digit"]."d",$mycount_h);
		$array = preg_split("//",$mycount_h2,-1,PREG_SPLIT_NO_EMPTY);

		$img_gif = array();

		if(1 == $Pm["print"]){
			foreach($array as $val){
				array_push($img_gif,"img".$Pm["imgf"]."/".$val.".gif");
			}
		}else if(0 == $Pm["print"]){
			foreach($array as $val){
				array_push($img_gif,"img/".$val.".gif");
			}
		}

		$gifcat = new gifcat;
		header("Content-type: image/gif");
		echo $gifcat->output($img_gif);
	#---------------------
	# カウンタ書き込み
	# 
	#---------------------

		flock($filepointer,LOCK_EX);				//ロック

		rewind($filepointer);
		fwrite($filepointer,"".$mycount_h." ".$IP." ".time());				//書き込み

		flock($filepointer,LOCK_UN);				//アンロック
		fclose($filepointer);						//クロウズ


		flock($fillocker,LOCK_UN);			   //アンロック(ファイルロック)
		fclose($fillocker);							//クローズ(ファイルロック)

	#=====================================================================
	# 処理終了
	# 
	#---------------------


?>
