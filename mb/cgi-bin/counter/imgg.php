<?php
#==========================================================================
#
# imgg.php : GIファイル連結
#
#
# 指定された画像フォルダのgif画像のひとつを選び画像サイズを取得し
# 表示サイズを設定。
# 設定した画像に結合させてカウンタ画像データをcounter.phpに送る
#
#===========================================================================

class img_class{

	function imgg($img_gif){
		$ketasu = count($img_gif);
		list($w,$h,$t,$a) = getimagesize("".$img_gif[0]."");

		$image = ImageCreate($w * $ketasu,$h);
		$image2 = imagecreatefromgif("".$img_gif[0]."");

		$black = ImageColorAllocate($image,255,165,16);

		for($i = 0;$i < $ketasu;$i++){
			$image2 = imagecreatefromgif("".$img_gif[$i]."");
			imagecopy($image,$image2,$w * $i,0,0,0,$w,$h);
		}
		
		return	$image;
	}
}

?>
