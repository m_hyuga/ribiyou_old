#==========================================================================
#
# counter.php : phpカウンター
#
##  記述方法  　#===========================================================
#
#
#	http:// 〜　counter.php?cdata=counter&ipr=1&print=0&count=1
#
#
##  パラメータ  #-----------------------------------------------------------
# 
#	■ cdata = カウントデータ（ファイル）
#	デフォルト)
#				couter
#	例）
#			×××××××.txt データを使用する場合
#			http://　〜  counter.php?cdata=×××××××		※ .txtは省略
#
#	■ ipr =  IPによるリカウント
#　デフォルト)
#			1			※ ON
#	例)
#			ipr=0	※ OFF
#			ipr=1	※ ON
#
#	■ print =  表示
#	デフォルト)
#			1			※ ON
#	例)
#			print=0	※ OFF
#			print=1	※ ON
#
#  ■ count =  カウント
#	デフォルト)
#			1			※ ON
#	例)
#			count=0	※ OFF
#			count=1	※ ON
#
#  ■ digit =  桁数指定
#	デフォルト)
#			1			※ ON
#	例)
#			digit=0	※ OFF
#			digit=1	※ ON
#
#  ■ imgf =  カウントイメージ
#	デフォルト)
#			0
#	例)
#			img× フォルダのイメージを使用する場合
#			http://　〜  counter.php?imgf=×		※ .imgは省略
#
##  イメージの追加　#-----------------------------------------------------------
#
#	■ カウンタのイメージデータ追加方法
#		イメージフォルダの作成、フォルダ名指定
#	例)
#			img××	※××のところに半角英数を入れる
#
#		作成したフォルダにイメージファイルの追加
#
#			0.gif 〜　9.gif
#===============================================================================


#==========================================================================
#
# imgg.php : GIファイル連結
#
#
# 指定された画像フォルダのgif画像のひとつを選び画像サイズを取得し
# 表示サイズを設定。
# 設定した画像に結合させてカウンタ画像データをcounter.phpに送る
#
#===========================================================================

