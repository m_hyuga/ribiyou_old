<?php
/*
 *********************************************************************
 * メールシステムhttp://www.procreo.jp/tutorial03.html
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 *********************************************************************
*/

define('FORMTPL_DIE'     , '' );                    //フォームテンプレート
define('FORMCOMPILE_DIR' , '_php/templates_c' );    //フォームテンプレート
define('MAILTPL_DIE'     , '' );                    //メールテンプレート
define('MAILCOMPILE_DIR' , '_php/templates_c' );    //メールテンプレート

define('UPLOAD_DIR'      , '_php/upload/' );        //添付ディレクトリ
define('ENCOD'           , 'SJIS-win' );            //文字コード [UTF-8] [SJIS-win] [EUC-JP]

define('FINOK'           , 'fin_ok.html' );  //完了画面
define('FINERR'          , 'fin_err.html' ); //完了エラー画面


ini_set("default_charset"             , "SJIS");
ini_set("mbstring.internal_encoding"  , "SJIS");
ini_set("mbstring.http_output"        , "SJIS");

ini_set("session.use_trans_sid","1");
ini_set("display_errors","off");


//----------------------------
//クォート部分を取り除く
//----------------------------
if(get_magic_quotes_gpc()){
    function strip_magic_quotes_slashes($arr){
        return is_array($arr) ?
        array_map('strip_magic_quotes_slashes', $arr) :
        stripslashes($arr);
    }

    $_GET     = strip_magic_quotes_slashes($_GET);
    $_POST    = strip_magic_quotes_slashes($_POST);
    $_REQUEST = strip_magic_quotes_slashes($_REQUEST);
    $_COOKIE  = strip_magic_quotes_slashes($_COOKIE);
}

//------------------------------------------------------------
//セッションスタート
//------------------------------------------------------------
if(!isset($_SESSION)) {
    session_start();
}
require_once('_php/phpmailer/class.phpmailer.php');
require_once('_php/Smarty/libs/Smarty.class.php');

//------------------------------------------------------------
//F2M_IDチェック
//------------------------------------------------------------

$txt       = "mail_conf.txt";
$F2M_ID    = isset($_POST["F2M_ID"])? $_POST["F2M_ID"] : "";

if(!$F2M_ID){
    header("Location: ".FINERR);
    exit();
}

$F2M       = F2M($txt);
$mail_conf = isset($F2M[$F2M_ID])? F2M_PARTS($F2M[$F2M_ID]) : array();

if(!$mail_conf){
    header("Location: ".FINERR);
    exit();
}


//------------------------------------------------------------
//モード
//------------------------------------------------------------

$mode = (isset($_GET["mode"]))?  $_GET["mode"]  : "";

//************************************************************
//送信処理 (mode)
//************************************************************
if($mode == "send"){


    //エラーカウント
    $errcount = 0;

    if($errcount == 0){
        //入力チェック
        $check_ms = InputDataCheck($mail_conf);
        if(count($check_ms) != 0){ $errcount++; }
    }


    //------------------------------------------------------------
    //エラー画面
    //------------------------------------------------------------
    if($errcount > 0){

        $Smarty = GetSmarty(FORMTPL_DIE, FORMCOMPILE_DIR);
        $Smarty->assign("F2M_ID", $F2M_ID);
        $Smarty->assign("err"   , $check_ms);
        $Smarty->assign("myphp" , $_SERVER["PHP_SELF"]);

        echo $Smarty->fetch( $mail_conf["TPL"]["CHECKERR"] );
//        $html = $Smarty->fetch( $mail_conf["TPL"]["CHECKERR"] );
//        echo Form($html, $mail_conf);
        exit();

    }
    //------------------------------------------------------------
    //エラー無
    //------------------------------------------------------------
    else{

        //タイムチェック
        $err = true;
        $key = (isset($_POST["key"]))? $_POST["key"] : 0;
        if(date("YmdHis",time() - 3600) <= $key){

            $form_data = FormMail($mail_conf);

            //記録メール送信
            $err_sv_mail = true;
            if(isset($mail_conf["SAVE"]["TO"])){
                $err_sv_mail = AdminSendmail($mail_conf, $form_data);
            }
            //返信メール
            $err_mail = true;
            if(isset($mail_conf["SEND"]["TO"])){
                $err_mail = UserSendmail($mail_conf, $form_data);
            }
            //CSV記録
            if(isset($mail_conf["CSV"]["FILE"])){
                Csv($mail_conf, $form_data, $err_sv_mail, $err_mail);
            }
            //送信したファイルと一定時間経過したファイルを削除
            FileDelete($form_data);

            //セッション削除
            $_SESSION["form"] = array();

            if(!$err_sv_mail){
                $err = false;
            }
        }else{
            $err = false;
        }

        if($err){
            header("Location: ".FINOK);
            exit();
        }else{
            header("Location: ".FINERR);
            exit();
        }
    }

}

//************************************************************
//入力フォーム表示
//************************************************************

else if($mode == "form"){

    $Smarty = GetSmarty(FORMTPL_DIE, FORMCOMPILE_DIR);

    $html = $Smarty->fetch( $mail_conf["TPL"]["FORM"] );
    echo    Form($html, $mail_conf);

}

//************************************************************
//入力チェック (mode)
//************************************************************

else{

    //エラーカウント
    $errcount = 0;

    //セッションセーブ
    Save($mail_conf);

    if($errcount == 0){
        //入力チェック
        $check_ms = InputDataCheck($mail_conf);
        if(count($check_ms) != 0){ $errcount++; }
    }

    //------------------------------------------------------------
    //エラー画面
    //------------------------------------------------------------
    if($errcount > 0){

        $Smarty = GetSmarty(FORMTPL_DIE, FORMCOMPILE_DIR);
        $Smarty->assign("F2M_ID", $F2M_ID);
        $Smarty->assign("err"   , $check_ms);
        $Smarty->assign("myphp" , $_SERVER["PHP_SELF"]);

//        echo $Smarty->fetch( $mail_conf["TPL"]["CHECKERR"] );
        $html = $Smarty->fetch( $mail_conf["TPL"]["CHECKERR"] );
        echo Form($html, $mail_conf);
        exit();
    	
    }
    //------------------------------------------------------------
    //確認画面
    //------------------------------------------------------------
    else{

        $Smarty = GetSmarty(FORMTPL_DIE, FORMCOMPILE_DIR);
        $Smarty->assign("F2M_ID"  , $F2M_ID);
        $Smarty->assign("form"    , FormText($mail_conf));
        $Smarty->assign("send_key", date("YmdHis"));
        $Smarty->assign("myphp"   , $_SERVER["PHP_SELF"]);

        echo $Smarty->fetch( $mail_conf["TPL"]["CHECK"] );
        exit();

    }
}


/*
*********************************************************************
* 表示
* Original-00 2009-09-01 shingo.Takeuchi
* Revision-01
* (IN)
*   $file_name  -- (txt) テンプレートファイル名
*   $data       -- (arr) システム値
* (OUT)
*********************************************************************
*/
function Form($html,$conf){

	$data = FormText($conf);

    preg_match_all('/<(select)[^>]*>(.*?)<\/\1>/ims', $html, $match_select );
    preg_match_all('/<(textarea)[^>]*>(.*?)<\/\1>/ims', $html, $match_textarea );

	foreach($data as $key => $value){
		
		//------------------------------------------------------------
		//inputタグ
		//------------------------------------------------------------
		if(preg_match_all('/<input.*?name=[\'"]'.$key.'(\[\])*[\'"]("[^"]*"|\'[^\']*\'|[^\'">])*>/i' , $html , $match)){

			foreach($match[0] as $tag){

				//---------------
				//type text
				//---------------
				if(preg_match('/type=[\'"]text[\'"]/i', $tag)){
					if(preg_match_all('/value=[\'"].*?[\'"]/ms', $tag, $match_v)){
						$tag2  = str_replace($match_v[0][0],"value=\"".htmlspecialchars($value["value"], ENT_QUOTES)."\"", $tag);
						$html  = str_replace($tag          ,$tag2                                                    , $html);
					}else{
						$tag2  = str_replace(array("/>",">") ," value=\"".htmlspecialchars($value["value"], ENT_QUOTES)."\" ", $tag);
						$html  = str_replace($tag             ,$tag2."/>"                                                , $html);
					}
				}

				//---------------
				//type radio
				//---------------
				else if(preg_match('/type=[\'"]radio[\'"]/', $tag)){
					if(preg_match('/value=[\'"]'.str_replace('/' ,'\/' , $value["value"]).'[\'"]/', $tag)){
						$tag2  = str_replace(">"  ," checked=\"checked\" ", $tag);
						$html  = str_replace($tag ,$tag2.">"              , $html);

					}else{
						$tag2  = str_replace(array("checked='checked'","checked=\"checked\"","checked") ,"", $tag);
						$html  = str_replace($tag             ,$tag2                   , $html);
					}
				}

				//---------------
				//type checkbox
				//---------------
				else if(preg_match('/type=[\'"]checkbox[\'"]/', $tag)){
					$checs      = explode(",",$value["value"]);
					$checs_flag = false;
					foreach($checs as $value_checks){
						if(preg_match('/value=[\'"]'.str_replace('/' ,'\/' , $value_checks).'[\'"]/', $tag)){
							$checs_flag =true;
							break;
						}
					}
					if($checs_flag){
						$tag2  = str_replace(">" ," checked=\"checked\" ", $tag);
						$html  = str_replace($tag ,$tag2.">"             , $html);

					}else{
						$tag2  = str_replace(array("checked='checked'","checked=\"checked\"","checked") ,"", $tag);
						$html  = str_replace($tag             ,$tag2                   , $html);
					}
				}
			}
		}

		//------------------------------------------------------------
		//textareaタグ
		//------------------------------------------------------------
		foreach($match_textarea[0] as $value_textarea){
			if(preg_match_all('/<textarea.*?name=[\'"]'.$key.'[\'"]("[^"]*"|\'[^\']*\'|[^\'>"])*>/i' , $value_textarea, $match)){

				$tag = str_replace(">",">".htmlspecialchars($value["value"], ENT_QUOTES)."</textarea>", $match[0][0]);
				$html  = str_replace($value_textarea ,$tag , $html);

				break;
			}
		}

		//------------------------------------------------------------
		//selectタグ
		//------------------------------------------------------------
		foreach($match_select[0] as $value_select){
			if(preg_match('/<select.*?name=[\'"]'.$key.'(\[\])*[\'"]("[^"]*"|\'[^\']*\'|[^\'>"])*>/i' , $value_select)){
				$org_select = $value_select;
				
				$select_data = explode(",",$value["value"]);
				
				foreach($select_data as $value_select_data){
					if(preg_match_all( '/<option[^>].*?value=[\'"]'.str_replace('/' ,'\/' , $value_select_data).'[\'"].*?>/i' , $value_select , $match_v)){
						$match_v2     = str_replace(">" ," selected=\"selected\" " , $match_v[0][0]);
						$value_select = str_replace($match_v[0][0] ,$match_v2.">"  , $value_select);
					}
				}
				$html = str_replace($org_select ,$value_select , $html);
				break;
			}
		}
	}

//	exit();
    return $html;
}

/*
*********************************************************************
* 表示
* Original-00 2009-09-01 shingo.Takeuchi
* Revision-01
* (IN)
*   $file_name  -- (txt) テンプレートファイル名
*   $data       -- (arr) システム値
* (OUT)
*********************************************************************
*/

function FormText($conf){


    $form    = (isset($_SESSION["form"]))?   $_SESSION["form"]   : array();
    $return  = array();
	
	foreach( $conf["PARTS"] as  $key => $value ){
		//アップファイル
        if($value["FILE"] == 'file'){

            $file_up_name   = "";

            $file = isset( $form[ $key ] )? explode(";",$form[ $key ]) : array();

            foreach($file as $value_file){
                $file2 = explode(":",$value_file);

                if($file2[0] == "file_up_name"  ){ $file_up_name   = $file2[1]; }
            }
        	$return[$key] = array( "name" => $value["NAME"] , "value" => $file_up_name );
        }
		//入力フォーム
		else{
        	$return[$key] = array( "name" => $value["NAME"] , "value" => isset( $form[ $key ] ) ? $form[ $key ] : "" );
	    }
	}

    return $return;
}

/*
*********************************************************************
* 表示
* Original-00 2009-09-01 shingo.Takeuchi
* Revision-01
* (IN)
*   $file_name  -- (txt) テンプレートファイル名
*   $data       -- (arr) システム値
* (OUT)
*********************************************************************
*/

function FormMail($conf){


    $form              = (isset($_SESSION["form"]))?   $_SESSION["form"]   : array();

    $return["mail_to"] = "";
    $return["form"]    = $return["file"] = array();

    foreach( $conf["PARTS"] as  $key => $value ){
        //アップファイル
        if($value["FILE"] == 'file'){

            $file_save_name = "";
            $file_up_name   = "";
            $file_up_err    = "";

            $file = isset( $form[ $key ] )? explode(";",$form[ $key ]) : array();

            foreach($file as $value_file){
                $file2 = explode(":",$value_file);

                if($file2[0]      == "file_save_name"){ $file_save_name = $file2[1]; }
                else if($file2[0] == "file_up_name"  ){ $file_up_name   = $file2[1]; }
                else if($file2[0] == "file_up_err"   ){ $file_up_err    = $file2[1]; }

            }
            $return["form"][$key] = array( "name" => $value["NAME"] , "value" => ($file_up_name)? $file_up_name." => ".$file_save_name : "" );

            if($file_save_name){ $return["file"][$key] = array( "name" => $file_save_name ); }
        }
        //入力フォーム
        else{
            $return["form"][$key] = array( "name" => $value["NAME"] , "value" => isset( $form[ $key ] ) ? $form[ $key ] : "" );

            if($conf["SEND"]["TO"] == $key){ $return["mail_to"] = isset( $form[ $key ] ) ? $form[ $key ] : ""; }
        }
    }

    return $return;
}


/*
 *********************************************************************
 * 入力データ
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *   $return      -- (arr) 
 *********************************************************************
 */

function Save($conf){

	$_SESSION["form"] = array();
    foreach($conf["PARTS"] as $key => $value){

        //ファイル =====================================
        if($value["FILE"] == 'file'){

            $format_check      = (isset($value["FILETYPE"]))?      $value["FILETYPE"]       : '*';
            $input_check       = (isset($value["INPUTCHECK"]))?    $value["INPUTCHECK"]     : 'x';
            $byte              = (isset($value["FILEBYTE"]))?      $value["FILEBYTE"]       : 0  ;

            $file_save_name    = '';
            $file_up_name      = '';
            $file_up_err       = '';

            //アップロードチェック
            if((is_uploaded_file($_FILES[$key]['tmp_name'])) && ($_FILES[$key]['size'] > 0)){
                //拡張子エラー
                if(!FileCheck($format_check,$_FILES[$key]['name'])){
                    $file_up_err = 'FileTypeErrar';

                //バイトサイズエラー
                }else if($_FILES[$key]['size'] > (1048576 * $byte)){
                    $file_up_err = 'ByteErrar';

                }
            }else{
                //パラメーターエラー
                if($_FILES[$key]['error'] == 0){
                    $file_up_err = 'ParameterErrar';

                }
            }
            if(!$file_up_err){
                //必須エラー
                if($_FILES[$key]['error'] == 4 && $input_check == 'o'){
                   $file_up_err = 'NotFileErrar';

                }
            }
        	//エラーが無ければアップロード
            if(!$file_up_err){

                if($_FILES[$key]['error'] != 4){

                    $filename_array  = explode(".",$_FILES[$key]['name']);
                    $save_type       = $filename_array[(count($filename_array)) - 1];

                    $new_file_dir  = UPLOAD_DIR;
                    $new_file      = date("YmdHis")."_".RandomKye(5);
                    $new_file     .= ($save_type)? '.'.$save_type : '';

                    //アップロード
                    $answer   = @copy($_FILES[$key]['tmp_name'], $new_file_dir.$new_file);
                    // 権限
                    chmod($new_file_dir.$new_file , 0666);

                    $file_save_name = $new_file;
                    $file_up_name   = $_FILES[$key]['name'];
                }
            }

            //--------------------
            $_SESSION["form"][$key] = "file_save_name:".$file_save_name
                                    .";file_up_name:"  .$file_up_name
                                    .";file_up_err:"   .$file_up_err;
            //--------------------

        //通常のフォーム ===============================
        }else{
            $_SESSION["form"][$key] =  isset($_POST[$key]) ? (is_array($_POST[$key]) ? join("," , $_POST[$key]) : $_POST[$key]) : ""; 
        }
        //==============================================

    }
}

/*
 *********************************************************************
 * 入力チェック
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *   $return      -- (arr) 入力エラーデータ
 *********************************************************************
 */

function InputDataCheck($conf){

    $form    = (isset($_SESSION["form"]))?   $_SESSION["form"]   : array();
    $return  = array();

    foreach($conf["PARTS"] as $key => $value){
        $chk_flag = true;

        //チェックデータ取得
        $chk_form_data = (isset($form[$key]))? $form[$key] : '';

        //ファイル =====================================
        if($value["FILE"] == 'file'){

            $file = explode(";",$chk_form_data);

            $file_save_name = "";
            $file_up_name   = "";
            $file_up_err    = "";

            foreach($file as $value_file){
                $file2 = explode(":",$value_file);

                if($file2[0]      == "file_save_name"){ $file_save_name = $file2[1]; }
                else if($file2[0] == "file_up_name"  ){ $file_up_name   = $file2[1]; }
                else if($file2[0] == "file_up_err"   ){ $file_up_err    = $file2[1]; }
            }

            //必須チェック--------------------------------------
            if($value["INPUTCHECK"] == "o"){
                if(!is_file(UPLOAD_DIR.$file_save_name)){
                    $return[$key]["name"] = $value["NAME"];
                    $return[$key]["ms"]   = "入力されていません";
                    $chk_flag = false;
                }
            }
            //--------------------------------------------------

            //セッション保存時に発生したエラー------------------
        	if($chk_flag && $file_up_err != ''){

                if($file_up_err == 'FileTypeErrar'){
            		$err_ms = $value["FILETYPE"]."をアップしてください";
                }
        		else if($file_up_err == 'ByteErrar'){
            		$err_ms = $value["FILEBYTE"]."Mを超えるデータはアップ出来ません";
                }
        		else if($file_up_err == 'ParameterErrar'){
            		$err_ms = "パラメーターエラーです";
                }
        		else if($file_up_err == 'NotFileErrar'){
            		$err_ms = "入力されていません";
        		}

                $return[$key]["name"] = $value["NAME"];
                $return[$key]["ms"]   = $err_ms;

        	}
            //--------------------------------------------------

        //通常のフォーム ===============================
        }else{
            $chk_form_data_array = explode(",",$chk_form_data);

            //必須チェック--------------------------------------
            if($value["INPUTCHECK"] == "o"){
                if(!$chk_form_data){
                    $return[$key]["name"] = $value["NAME"];
                    $return[$key]["ms"]   = $value["ERRMS"];
                    $chk_flag = false;
                }
            }
            //--------------------------------------------------

            //イコールチェック------------------------------
        	if($value["EQUALCHECK"] && $chk_flag){
                $chk_equal     = (isset($form[$value["EQUALCHECK"]]))? $form[$value["EQUALCHECK"]] : "";
        		$koumok_name   = isset($conf["PARTS"][$value["EQUALCHECK"]]["NAME"])?  $conf["PARTS"][$value["EQUALCHECK"]]["NAME"] : "";
        		
    			if($chk_form_data  != $chk_equal){
    				$return[$key]["name"] = $value["NAME"];
    				$return[$key]["ms"]   = $koumok_name."と一致しません";
                    $chk_flag = false;
    			}
        	}
            //フォーマットチェック------------------------------
        	if($value["FORMATCHECK"] && $chk_flag){
        		foreach($chk_form_data_array as $value_chk){
        			if($value_chk != ""){
            			if(!preg_match($value["FORMATCHECK"] ,$value_chk)){
            				$return[$key]["name"] = $value["NAME"];
            				$return[$key]["ms"]   = $value["ERRMS"];
                            $chk_flag = false;
            			}
        			}
        		}
        	}
            //emailチェック------------------------------
        	if($value["EMAILCHECK"]  == "o" && $chk_flag && $chk_form_data != ""){
    			if(!preg_match('/^([a-zA-Z0-9"])+([a-zA-Z0-9"\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/' ,$chk_form_data)){
    				$return[$key]["name"] = $value["NAME"];
    				$return[$key]["ms"]   = "正しく入力してください";
                    $chk_flag = false;
    			}
        	}
            //--------------------------------------------------
        }
    }

    return $return;
}
/*
 *********************************************************************
 * メール送信
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *  
 *********************************************************************
 */

function AdminSendmail($conf,$form){

    $Smarty = GetSmarty(MAILTPL_DIE, MAILCOMPILE_DIR);
    $Smarty->assign("form"    , $form["form"]);

    $Smarty->assign("ip"      , $_SERVER["REMOTE_ADDR"]);
    $Smarty->assign("time"    , date("Y/m/d/  H:i:s"));

    $body = $Smarty->fetch( $conf["SAVE"]["MAILTPL"] );

    //添付ファイル
    $attachfile = array();
    foreach($form["file"] as $file){
        $attachfile[]   = UPLOAD_DIR.$file["name"];
    }
	
    $err = SendmailPhpmailer(
        ENCOD
        ,$conf["SAVE"]["TO"]
        ,$conf["SAVE"]["SUBJECT"]
        ,$body
        ,$conf["FROM"]["MAIL"]
        ,$conf["FROM"]["NAME"]
        ,$attachfile
    );

    return $err;
}


/*
 *********************************************************************
 * メール送信
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *  
 *********************************************************************
 */

function UserSendmail($conf, $form){


	//Email取得
	$mail_to = $form["mail_to"];

	//フォーマットエラー
    if(!preg_match('/^([a-zA-Z0-9"])+([a-zA-Z0-9"\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/' ,$mail_to)){
        return false;
    }
	//送信処理
	else{

        $Smarty = GetSmarty(MAILTPL_DIE, MAILCOMPILE_DIR);
        $Smarty->assign("form"    , $form["form"]);

        $Smarty->assign("ip"      , $_SERVER["REMOTE_ADDR"]);
        $Smarty->assign("time"    , date("Y/m/d/  H:i:s"));

        $body = $Smarty->fetch( $conf["SEND"]["MAILTPL"] );

    	//添付ファイル
        $attachfile = array();
    	foreach($form["file"] as $file){
            $attachfile[]   = UPLOAD_DIR.$file["name"];
    	}

        $mail_to = EmailDoubleQuotation($mail_to);

        $err = SendmailPhpmailer(
            ENCOD
            ,$mail_to
            ,$conf["SEND"]["SUBJECT"]
            ,$body
            ,$conf["FROM"]["MAIL"]
            ,$conf["FROM"]["NAME"]
            ,$attachfile
        );
        return $err;
    }
}

/*
 *********************************************************************
 * CSV登録
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *  
 *********************************************************************
 */

function Csv($conf, $form,$err_sv_mail,$err_mail){

	$title_array = array();
	$data_array  = array();

    $title_array[] = '"TIME"';       $data_array[] = '"'.str_replace('"', '""', date("Y-m-d H:i:s")    ).'"';
    $title_array[] = '"IP"';         $data_array[] = '"'.str_replace('"', '""', $_SERVER["REMOTE_ADDR"]).'"';
    $title_array[] = '"SAVE_MAIL"';  $data_array[] = (!$err_sv_mail)? '"err"' : '""';
    $title_array[] = '"USER_MAIL"';  $data_array[] = (!$err_mail)?    '"err"' : '""';

    foreach($conf["PARTS"] as $key_parts => $value_parts){
        if($value_parts["SAVE"] == 'o'){

            //コード変換
            $form_title = (ENCOD == "SJIS-win")? $form["form"][$key_parts]["name"]  : mb_convert_encoding($form["form"][$key_parts]["name"]  , "SJIS-win", ENCOD);
            $form_data  = (ENCOD == "SJIS-win")? $form["form"][$key_parts]["value"] : mb_convert_encoding($form["form"][$key_parts]["value"] , "SJIS-win", ENCOD);

            $title_array[] = '"'.str_replace('"', '""', $form_title).'"';
            $data_array[]  = '"'.str_replace('"', '""', $form_data ).'"';

        }
    }

    //新規書き出し
    if(!is_file($conf["CSV"]["FILE"])){

        $title = join(",", $title_array);
        $data  = join(",", $data_array);

        $file_dat=fopen($conf["CSV"]["FILE"],"w+");
        flock($file_dat, LOCK_EX);
        fputs($file_dat, $title."\n".$data);
        flock($file_dat, LOCK_UN);
        chmod($conf["CSV"]["FILE"],0666);

	//更新
    }else{
        $data  = join(",", $data_array);

        $file_dat=fopen($conf["CSV"]["FILE"],"a+");
        flock($file_dat, LOCK_EX);
        fputs($file_dat, "\n".$data);
        flock($file_dat, LOCK_UN);
    }

}
/*
 *********************************************************************
 * いらないファイル削除
 * Original-00 2009-09-01 shingo.Takeuchi
 * Revision-01
 * (IN)
 * (OUT)
 *  
 *********************************************************************
 */

function FileDelete($form){

    //添付ファイル
    foreach($form["file"] as $file){
        if(is_file(UPLOAD_DIR.$file["name"])){
            unlink(UPLOAD_DIR.$file["name"]);
        }
    }

    //時間が経過したファイル
    $file_name = array();
    $file_dir  = opendir(UPLOAD_DIR);

    while($filenames = readdir($file_dir)) {
        $delet = false;

        if(is_file(UPLOAD_DIR.$filenames)) {
            //ファイル名で登録
            $filenames_arr   = explode(".", $filenames);
            $file_name_split = explode("_",   $filenames_arr[0]);

            if(preg_match("/^\d{14}$/",$file_name_split[0])){

                //一定時間すぎたファイルは削除
                $file_time = $file_name_split[0] * 1;
                if(date("YmdHis",time() - 3600) > $file_time){
                    $delet = true;
                }

            //想定外のファイルは削除
            }else{
                $delet = true;
            }
            if($delet){
                    unlink(UPLOAD_DIR.$filenames);
            }
        }
    }
}


/*
 *********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【拡張子チェック】
  (IN)
    format_check -- (txt)  jpg:gif:png など 
    filename     -- (txt)  test.jpg    など

  (OUT)
 *******************************************************************
 */


function FileCheck($format_check,$filename){

    $filename_array  = explode(".",$filename);
    $filename_kaku   = $filename_array[(count($filename_array)) - 1];

    $format_check_array = explode(".",$format_check);

    foreach($format_check_array as $value){
        if(preg_match("/".$value."/i", $filename_kaku) || ($filename == "") || ($value == "*")){
            return true;
        }
    }
    return false;
}

/*
 *********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【テンプレート】
  (IN)
    tpl_folder     -- (txt)  テンプレートファイルの置き場所
    compile_folder -- (txt)  コンパイルされたテンプレートの置き場所
    DATA           -- (arr) テンプレートに表示されるデータ
  (OUT)
 *******************************************************************
 */


function GetSmarty($tpl_folder,$compile_folder){

    $smarty = new Smarty();

    $smarty->template_dir    = $tpl_folder;
    $smarty->compile_dir     = $compile_folder;

    $smarty->left_delimiter  = '<!--{{';
    $smarty->right_delimiter = '}}-->';

    return $smarty;

}

/*
 *********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【メーラー】
  (IN)
       encod              -- (txt)  コード
       to                 -- (txt)  宛先
       subject            -- (txt)  題名
       body               -- (txt)  本文
       from               -- (txt)  差出人
       fromname           -- (txt)  差し出し人名
       attachfile         -- (aryy) ファイル
       attachfile_pas     -- (txt)  添付ファイルパス

  (OUT)
 *******************************************************************
 */

function SendmailPhpmailer($encod, $to, $subject, $body, $from, $fromname, $attachfile = array(), $attachfile_pas = ''){

    //言語設定、内部エンコーディングを指定する
    mb_language("ja");
    mb_internal_encoding($encod);

    $mail = new PHPMailer();
    $mail->CharSet = "iso-2022-jp";
    $mail->Encoding = "7bit";
 
    $mail->AddAddress($to);
    $mail->From     = $from;
//    $mail->FromName = mb_encode_mimeheader($fromname,"JIS",$encod);
//    $mail->Subject  = mb_encode_mimeheader($subject,"JIS",$encod);
//    $mail->Body     = mb_convert_encoding($body,"JIS",$encod);

    $mail->FromName = mb_encode_mimeheader($fromname,"JIS");
    $mail->Subject  = mb_encode_mimeheader($subject,"JIS");
    $mail->Body     = mb_convert_encoding($body,"JIS",$encod);


    if(is_array($attachfile)){
        foreach($attachfile as $value){
            //添付ファイル追加
            $mail->AddAttachment($attachfile_pas.$value);
        }
    }

    return  $mail->Send();

}

/*
*********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【emailの@マークの前をダブルコーテーションで囲む処理】
 (IN)
    email -- (txt)  変換するEmail
 (OUT)
    変換されたEmail
*******************************************************************
*/
function EmailDoubleQuotation($email) {

    if(preg_match("/@(docomo|ezweb).ne.jp/i", $email)){

        $email_arr = explode("@",$email);
        if(count($email_arr) == 2){
            if(mb_strpos($email_arr[0], "..") || mb_strpos($email, ".@")){
                $email = '"'.$email_arr[0].'"@'.$email_arr[1];
            }
        }
    }
    return $email;
}

/*
 *********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【ランダムキー】
  (IN)
    suu    -- (int)  生成する識別子の桁数
  (OUT)
    return   -- (text) 生成したランダムな識別子
 ********************************************************************
 */
function RandomKye($suu) {
  // 識別子を構成するコード群
  $key_data = "1Aa2Bb3Cc4Dd5Ee6Ff7Gg8Hh9IiJjKkLlMmNnOoPpQqRr0SsTtUuVvWwXxYyZz";

  $key = "";
  for($i = 0; $i < $suu; $i++) {
    $key .= substr($key_data, (rand() % (strlen($key_data))), 1);
  }
  return($key);
}

/*
 *********************************************************************
 * Original-00 2009-08-11 Shingo.Takeuchi
 * Revision-01
 【メールフォーム設定取得】
  (IN)
    suu    -- (int)  生成する識別子の桁数
  (OUT)
    return   -- (text) 生成したランダムな識別子
 ********************************************************************
 */

function F2M($txt){

    $file   = (is_file($txt))? file($txt) : array();
    $return = array();
    $F2M_ID = "";


	
    foreach($file as $value){

        if(preg_match("/^#/",$value)) continue; 

        preg_match( '/^([^\t:]+)\t*:\t*([^\t\r\n]+)/' , $value , $match );

        if(!$match)  continue;

        if($match[1] == "F2M_ID"){
            $F2M_ID          = $match[2];
            $return[$F2M_ID] = array();
        }
        else{
            $koumoku = explode("_",$match[1]);

            if($koumoku[0] != "PARTS"){
                $return[$F2M_ID][$koumoku[0]][$koumoku[1]] = $match[2];
            }else{
                $F2M[$F2M_ID]["PARTS"] = array();
            }
        }
    }

    return $return;
}



function F2M_PARTS($F2M){

    $FORM = isset($F2M["FORM"])? $F2M["FORM"] : array();
    $CSV  = isset($F2M["CSV"])?  $F2M["CSV"]  : array();

    $FORM_Array = array();
    $CSV_Array  = array();

    foreach($FORM as $key => $value){
        $FORM_Array[$key] = F2M_SPLIT($key, $value);
    }
    foreach($CSV as $key => $value){
        $CSV_Array[$key]  = F2M_SPLIT($key, $value);
    }

	$PARTS = array();
	foreach($FORM_Array["NAME"] as $form_key => $form_name){

		$PARTS[$form_key]["NAME"]        = $form_name;
		$PARTS[$form_key]["INPUTCHECK"]  = isset($FORM_Array["INPUTCHECK"][$form_key]    )?  "o" : "x";
		$PARTS[$form_key]["EMAILCHECK"]  = isset($FORM_Array["EMAILCHECK"][$form_key]    )?  "o" : "x";
		$PARTS[$form_key]["EQUALCHECK"]  = isset($FORM_Array["EQUALCHECK"][$form_key]    )?  $FORM_Array["EQUALCHECK"][$form_key] : "";
		$PARTS[$form_key]["FORMATCHECK"] = isset($FORM_Array["FORMATCHECK"][$form_key]   )?  $FORM_Array["FORMATCHECK"][$form_key] : "";
		$PARTS[$form_key]["ERRMS"]       = isset($FORM_Array["ERRMS"][$form_key]         )?  $FORM_Array["ERRMS"][$form_key] : "入力してください";
		$PARTS[$form_key]["FILE"]        = isset($FORM_Array["FILE"][$form_key]          )?  "file" : "text";
		$PARTS[$form_key]["FILEBYTE"]    = isset($FORM_Array["FILEBYTE"][$form_key]      )?  $FORM_Array["FILEBYTE"][$form_key] : "";
		$PARTS[$form_key]["FILETYPE"]    = isset($FORM_Array["FILETYPE"][$form_key]      )?  $FORM_Array["FILETYPE"][$form_key] : "";
		
		$PARTS[$form_key]["SAVE"]        = !isset($CSV_Array["SAVE"]                     )?   "o" : (isset($CSV_Array["SAVE"][$form_key])? "o" : "x");
		
	}
	$F2M["PARTS"] = $PARTS;

	return $F2M;
	
}


function F2M_SPLIT($name ,$data){
	$return = array();


	
	if($name =="FORMATCHECK"){
		preg_match_all( '/\[##.*?##\]/' , $data , $match );
        $data2 = mb_split("\[##.*?##\]", $data);
		
		foreach($match[0] as $key => $value){
			 $data_key = str_replace(array(",",":"),"", $data2[$key]);

			
			$return[$data_key] = str_replace(array("[##","##]"),"", $value);
		}

	}
	else if($name =="INPUTCHECK" || $name == "EMAILCHECK" || $name =="FILE" || $name =="SAVE"){
		$data_array = explode(",", $data);
		foreach($data_array as $value){
			$return[$value] = $value;
		}
	}
	else{
		$data_array = explode(",", $data);
		foreach($data_array as $value){
			$data_array2             = explode(":", $value);
			$return[$data_array2[0]] = $data_array2[1];
		}
	}
	return $return;
}



?>