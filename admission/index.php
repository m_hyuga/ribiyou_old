<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>入学案内 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/admission.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="admission";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/admission/img_main.jpg" alt="入学案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/admission/"><img src="/common/images/common/sidenav/admission/nav_admission.png" alt="入学案内" /></a></dt>
					<dd><a href="/admission/daytime.php"><img src="/common/images/common/sidenav/admission/nav_admission_daytime.png" alt="昼間課" /></a></dd>
					<dd><a href="/admission/correspondence.php"><img src="/common/images/common/sidenav/admission/nav_admission_correspondence.png" alt="通信課" /></a></dd>
					<dd><a href="/admission/tuition.php"><img src="/common/images/common/sidenav/admission/nav_admission_tuition.png" alt="学費" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">入学案内</li>
			</ul>
		</div>
		<div id="mainarea" class="admission_content">
			<table class="admission_table_01">
			<colgroup span="1" class="sec_01"></colgroup>
			<colgroup span="1" class="sec_02"></colgroup>
			<colgroup span="3" class="sec_03"></colgroup>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>学科</th>
					<th>入学時期</th>
					<th>性別</th>
					<th>募集定員</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th rowspan="4"><p>昼間課<br>(2年)</p><a href="/admission/daytime.php"><img src="/common/images/admission/btn_shosai.jpg" alt="詳細" class="over" /></a></th>
					<td class="td_left">理容科</td>
					<td rowspan="4">4月</td>
					<td rowspan="2">男女</td>
					<td>40</td>
				</tr>
				<tr>
					<td class="td_left">美容科</td>
					<td>80</td>
				</tr>
				<tr>
					<td class="td_left">エステティック科</td>
					<td>女性のみ</td>
					<td>20</td>
				</tr>
				<tr>
					<td class="td_left">トータルビューティ科</td>
					<td>男女</td>
					<td>40</td>
				</tr>
			</tbody>
			</table>
			<table class="admission_table_01">
			<colgroup span="1" class="sec_01"></colgroup>
			<colgroup span="1" class="sec_04"></colgroup>
			<colgroup span="1" class="sec_05"></colgroup>
			<colgroup span="3" class="sec_03"></colgroup>
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>学科</th>
					<th>入学時期</th>
					<th>性別</th>
					<th>募集定員</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th rowspan="4"><p>通信課程<br>(3年)</p><a href="/admission/correspondence.php"><img src="/common/images/admission/btn_shosai.jpg" alt="詳細" class="over" /></a></th>
					<td rowspan="2">理容科</td>
					<td class="td_left">サロン従事者コース</td>
					<td rowspan="4">4月･10月</td>
					<td rowspan="4">男女</td>
					<td>20</td>
				</tr>
				<tr>
					<td class="td_left">一般コース</td>
					<td>20</td>
				</tr>
				<tr>
					<td rowspan="2">美容科</td>
					<td class="td_left">サロン従事者コース</td>
					<td>40</td>
				</tr>
				<tr>
					<td class="td_left">一般コース</td>
					<td>40</td>
				</tr>
			</tbody>
			</table>
			<br>
			
			<h2><img src="/common/images/admission/ttl_nite.png" alt="出願日程" /></h2>
			<ul class="admission_list_01">
				<li class="a_li_01"><a href="daytime.php#ao"><img src="/common/images/admission/btn_nitte_01.png" alt="AO入試エントリー" class="over" /></a></li>
				<li class="a_li_02"><a href="daytime.php#ao"><img src="/common/images/admission/btn_nitte_02.png" alt="AO入試出願" class="over" /></a></li>
				<li class="a_li_03"><a href="daytime.php#si"><img src="/common/images/admission/btn_nitte_03.png" alt="指定校推薦入試出願" class="over" /></a></li>
				<li class="a_li_04"><a href="daytime.php#su"><img src="/common/images/admission/btn_nitte_04.png" alt="推薦入試出願" class="over" /></a></li>
				<li class="a_li_05"><a href="daytime.php#ji"><img src="/common/images/admission/btn_nitte_05.png" alt="自己推薦入試出願" class="over" /></a></li>
				<li class="a_li_06"><a href="daytime.php#i"><img src="/common/images/admission/btn_nitte_06.png" alt="一般入試1" class="over" /></a></li>
				<li class="a_li_07"><a href="daytime.php#i"><img src="/common/images/admission/btn_nitte_07.png" alt="一般入試2" class="over" /></a></li>
				<li class="a_li_08"><a href="daytime.php#i"><img src="/common/images/admission/btn_nitte_08.png" alt="一般入試3" class="over" /></a></li>
			</ul>
			<br>
			
			<h2><img src="/common/images/admission/ttl_nite.png" alt="出願日程" /></h2>
			<dl class="admission_dl_01 cf">
				<dt>窓口受付</dt>
				<dd class="a_dl_01">出願書類一式と選考料を受付期間内に、本校の正面玄関先の窓口に提出して下さい。<br>土曜・日曜・祝祭日を除く毎日／午前9時〜午後5時まで</dd>
			</dl>
			<dl class="admission_dl_01 cf">
				<dt>郵送受付</dt>
				<dd class="a_dl_02">選考料を郵便為替証書とする場合は、出願書類一式を所定の封筒に入れ、書留郵便で送付してください。選考料を現金とする場合は、出願書類一式を添付し、現金書留で送付してください。</dd>
			</dl>
			<br>
			<br>
			
			<ul class="admission_btn_01 cf">
				<li class="left"><a href="/admission/daytime.php"><img src="/common/images/admission/btn_hiruma.jpg" alt="昼間科（募集要項）" class="over" /></a></li>
				<li class="left"><a href="/admission/correspondence.php"><img src="/common/images/admission/btn_tsusin.jpg" alt="通信課程（募集要項）" class="over" /></a></li>
			</ul>
			
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>