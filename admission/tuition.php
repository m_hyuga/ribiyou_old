<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学費 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/admission.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="admission";
				require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/admission/img_main.jpg" alt="入学案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/admission/"><img src="/common/images/common/sidenav/admission/nav_admission.png" alt="入学案内" /></a></dt>
					<dd><a href="/admission/daytime.php"><img src="/common/images/common/sidenav/admission/nav_admission_daytime.png" alt="昼間課" /></a></dd>
					<dd><a href="/admission/correspondence.php"><img src="/common/images/common/sidenav/admission/nav_admission_correspondence.png" alt="通信課" /></a></dd>
					<dd class="navigationhover"><a href="/admission/tuition.php"><img src="/common/images/common/sidenav/admission/nav_admission_tuition.png" alt="学費" /></a></dd>
				</dl>
			</div>
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/admission/">入学案内</a></li>
				<li class="pankuzu_next">学費</li>
			</ul>
		</div>
		<div id="tuition" class="admission_content">
			<h2><img src="/common/images/admission/ttl_h2_tuition01.png" alt="学費／昼間課"></h2>
			<h3><img src="/common/images/admission/ttl_h3_ribyou.png" alt="理容科／美容科"></h3>
			<table class="general_table">
				<tr>
					<th class="bor_tp">&nbsp;</th>
					<td colspan="5">1年目</td>
					<td colspan="3">2年目</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<th>納入時期</th>
					<td>合格後<br>約2週間</td>
					<td>合格後<br>約8週間</td>
					<td>1期<br>4月</td>
					<td>2期<br>7月</td>
					<td>3期<br>11月</td>
					<td>4期<br>3月</td>
					<td>5期<br>7月</td>
					<td>6期<br>11月</td>
					<td>合計</td>
				</tr>
				<tr>
					<th>入学金</th>
					<td>100,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>100,000</td>
				</tr>
				<tr>
					<th>施設設備維持・拡充費</th>
					<td>&nbsp;</td>
					<td>290,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>165,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>455,000</td>
				</tr>
				<tr>
					<th>授業料</th>
					<td>&nbsp;</td>
					<td>70,600</td>
					<td>70,600</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>847,200</td>
				</tr>
				<tr>
					<th>実習費</th>
					<td>&nbsp;</td>
					<td>49,000</td>
					<td>49,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>588,000</td>
				</tr>
				<tr>
					<th>諸経費</th>
					<td>25,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>25,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>50,000</td>
				</tr>
				<tr>
					<th class="bor_bp">合計</th>
					<td>125,000</td>
					<td>409,600</td>
					<td>119,600</td>
					<td>239,200</td>
					<td>239,200</td>
					<td>429,200</td>
					<td>239,200</td>
					<td>239,200</td>
					<td>2,040,200</td>
				</tr>
			</table>
			<ul>
				<li>[その他の費用]</li>
				<li>①学生生徒諸費：別途80,000円必要となります。(生徒会活動費、研修旅行費、卒業諸経費)</li>
				<li>②実習器具　　：別途約20万円必要になります。(平成26年度実績)</li>
				<li class="list_ind">モデルウィッグ、カット用具、メイクセット、ネイルセット、実習衣等</li>
				<li class="list_ind">・この他の消耗品は個人で購入していただきます。</li>
				<li class="list_ind">・科や男女によって金額は変動します。</li>
				<li>納入時期は、入試種別により若干異なりますので、詳しくは事務局までお問い合わせください</li>
			</ul>
			<h3><img src="/common/images/admission/ttl_h3_esthe.png" alt="エステティック科"></h3>
			<table class="general_table">
				<tr>
					<th class="bor_tp">&nbsp;</th>
					<td colspan="5">1年目</td>
					<td colspan="3">2年目</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<th>納入時期</th>
					<td>合格後<br>約2週間</td>
					<td>合格後<br>約8週間</td>
					<td>1期<br>4月</td>
					<td>2期<br>7月</td>
					<td>3期<br>11月</td>
					<td>4期<br>3月</td>
					<td>5期<br>7月</td>
					<td>6期<br>11月</td>
					<td>合計</td>
				</tr>
				<tr>
					<th>入学金</th>
					<td>100,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>免除</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>100,000</td>
				</tr>
				<tr>
					<th>施設設備維持・拡充費</th>
					<td>&nbsp;</td>
					<td>290,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>290,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>580,000</td>
				</tr>
				<tr>
					<th>授業料</th>
					<td>&nbsp;</td>
					<td>70,600</td>
					<td>70,600</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>160,000</td>
					<td>160,000</td>
					<td>160,000</td>
					<td>903,600</td>
				</tr>
				<tr>
					<th>実習費</th>
					<td>&nbsp;</td>
					<td>49,000</td>
					<td>49,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>160,000</td>
					<td>160,000</td>
					<td>160,000</td>
					<td>774,000</td>
				</tr>
				<tr>
					<th>諸経費</th>
					<td>25,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>30,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>55,000</td>
				</tr>
				<tr>
					<th class="bor_bp">合計</th>
					<td>125,000</td>
					<td>409,600</td>
					<td>119,600</td>
					<td>239,200</td>
					<td>239,200</td>
					<td>640,000</td>
					<td>320,000</td>
					<td>320,000</td>
					<td>2,412,600</td>
				</tr>
			</table>
			<ul>
				<li>[その他の費用]</li>
				<li>①学生生徒諸費：別途80,000円必要となります。(生徒会活動費、研修旅行費、卒業諸経費)</li>
				<li>②実習器具　　：別途約25万円必要になります。(平成26年度実績)</li>
				<li class="list_ind">エステ実習器具一式、ネイル、メイク用具一式、各種検定科、実習衣</li>
				<li class="list_ind">・この他の消耗品は個人で購入していただきます。</li>
				<li>納入時期は、入試種別により若干異なりますので、詳しくは事務局までお問い合わせください</li>
			</ul>
			<h3><img src="/common/images/admission/ttl_h3_beauty.png" alt="トータルビューティー科"></h3>
			<table class="general_table">
				<tr>
					<th class="bor_tp">&nbsp;</th>
					<td colspan="5">1年目</td>
					<td colspan="3">2年目</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<th>納入時期</th>
					<td>合格後<br>約2週間</td>
					<td>合格後<br>約8週間</td>
					<td>1期<br>4月</td>
					<td>2期<br>7月</td>
					<td>3期<br>11月</td>
					<td>4期<br>3月</td>
					<td>5期<br>7月</td>
					<td>6期<br>11月</td>
					<td>合計</td>
				</tr>
				<tr>
					<th>入学金</th>
					<td>100,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>100,000</td>
				</tr>
				<tr>
					<th>施設設備維持・拡充費</th>
					<td>&nbsp;</td>
					<td>290,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>165,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>455,000</td>
				</tr>
				<tr>
					<th>授業料</th>
					<td>&nbsp;</td>
					<td>70,600</td>
					<td>70,600</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>141,200</td>
					<td>847,200</td>
				</tr>
				<tr>
					<th>実習費</th>
					<td>&nbsp;</td>
					<td>49,000</td>
					<td>49,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>98,000</td>
					<td>588,000</td>
				</tr>
				<tr>
					<th>諸経費</th>
					<td>25,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>25,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>50,000</td>
				</tr>
				<tr>
					<th class="bor_bp">合計</th>
					<td>125,000</td>
					<td>409,600</td>
					<td>119,600</td>
					<td>239,200</td>
					<td>239,200</td>
					<td>429,200</td>
					<td>239,200</td>
					<td>239,200</td>
					<td>2,040,200</td>
				</tr>
			</table>
			<ul>
				<li>[その他の費用]</li>
				<li>①学生生徒諸費：別途80,000円必要となります。(生徒会活動費、研修旅行費、卒業諸経費)</li>
				<li>②実習器具　　：別途約20万円必要になります。(平成26年度実績)</li>
				<li class="list_ind">エステ実習器具一式、ネイル、メイク用具一式、各種検定科、実習衣</li>
				<li class="list_ind">・この他の消耗品は個人で購入していただきます。</li>
				<li>納入時期は、入試種別により若干異なりますので、詳しくは事務局までお問い合わせください</li>
			</ul>
			<h2><img src="/common/images/admission/ttl_h2_tuition02.png" alt="学費／通信課"></h2>
			<h3><img src="/common/images/admission/ttl_h3_saloon.png" alt="サロン従事者コース"></h3>
			<table class="cor_table">
				<tr>
					<th rowspan="3" class="bor_tp">納入時期</th>
					<th class="bor_tp th_s">&nbsp;</th>
					<td>入学前</td>
					<td>1年次</td>
					<td>2年次</td>
					<td>3年次</td>
					<td rowspan="3">合計</td>
				</tr>
				<tr>
					<th class="th_s">春期生</th>
					<td rowspan="2">合格後<br>約20日後</td>
					<td>9月下旬</td>
					<td>3月下旬</td>
					<td>3月下旬</td>
				</tr>
				<tr>
					<th class="th_s">秋期生</th>
					<td>4月下旬</td>
					<td>9月下旬</td>
					<td>9月下旬</td>
				</tr>
				<tr>
					<th colspan="2">入学金</th>
					<td>108,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>108,000</td>
				</tr>
				<tr>
					<th colspan="2">施設設備維持・拡充費</th>
					<td>260,280</td>
					<td>130,140</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>390,420</td>
				</tr>
				<tr>
					<th colspan="2">授業料</th>
					<td>176,580</td>
					<td>&nbsp;</td>
					<td>176,580</td>
					<td>176,580</td>
					<td>529,740</td>
				</tr>
				<tr>
					<th colspan="2">面接授業料</th>
					<td>&nbsp;</td>
					<td>64,800</td>
					<td>64,800</td>
					<td>129,600</td>
					<td>259,200</td>
				</tr>
				<tr>
					<th colspan="2">諸経費</th>
					<td>5,400</td>
					<td>&nbsp;</td>
					<td>5,400</td>
					<td>5,400</td>
					<td>16,200</td>
				</tr>
				<tr>
					<th colspan="2" class="bor_bp">合計</th>
					<td>550,260</td>
					<td>194,940</td>
					<td>246,780</td>
					<td>311,580</td>
					<td>1,303,560</td>
				</tr>
			</table>
			<h3 class="ttl_course"><img src="/common/images/admission/ttl_h3_genecourse.png" alt="一般コース"></h3>
			<table class="cor_table">
				<tr>
					<th rowspan="3" class="bor_tp">納入時期</th>
					<th class="bor_tp th_s">&nbsp;</th>
					<td>入学前</td>
					<td>1年次</td>
					<td>2年次</td>
					<td>3年次</td>
					<td rowspan="3">合計</td>
				</tr>
				<tr>
					<th class="th_s">春期生</th>
					<td rowspan="2">合格後<br>約20日後</td>
					<td>9月下旬</td>
					<td>3月下旬</td>
					<td>3月下旬</td>
				</tr>
				<tr>
					<th class="th_s">秋期生</th>
					<td>4月下旬</td>
					<td>9月下旬</td>
					<td>9月下旬</td>
				</tr>
				<tr>
					<th colspan="2">入学金</th>
					<td>108,000</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>108,000</td>
				</tr>
				<tr>
					<th colspan="2">施設設備維持・拡充費</th>
					<td>260,280</td>
					<td>130,140</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>390,420</td>
				</tr>
				<tr>
					<th colspan="2">授業料</th>
					<td>176,580</td>
					<td>&nbsp;</td>
					<td>176,580</td>
					<td>176,580</td>
					<td>529,740</td>
				</tr>
				<tr>
					<th colspan="2">面接授業料</th>
					<td>&nbsp;</td>
					<td>86,940</td>
					<td>86,940</td>
					<td>173,880</td>
					<td>347,760</td>
				</tr>
				<tr>
					<th colspan="2">諸経費</th>
					<td>5,400</td>
					<td>&nbsp;</td>
					<td>5,400</td>
					<td>5,400</td>
					<td>16,200</td>
				</tr>
				<tr>
					<th colspan="2" class="bor_bp">合計</th>
					<td>550,260</td>
					<td>217,080</td>
					<td>268,920</td>
					<td>355,860</td>
					<td>1,392,120</td>
				</tr>
			</table>
			<ul>
				<li>※上記の他、実習時に使用する材料費(カット用具セット、モデルウイッグ、実習費等)が必要になります。</li>
				<li>※最終学歴が中学卒業者の方は、別途16,200円必要になります。</li>
				<li>※納入時期は、受験時期により異なりますので、詳しくは事務局までお問い合わせください。</li>
			</ul>
		</div><!-- tuition end -->
	</div><!-- content end -->
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");?>
</div>
</body>
</html>