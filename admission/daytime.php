<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>昼間課 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/admission.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="admission";
				require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/admission/img_main.jpg" alt="入学案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/admission/"><img src="/common/images/common/sidenav/admission/nav_admission.png" alt="入学案内" /></a></dt>
					<dd class="navigationhover"><a href="/admission/daytime.php"><img src="/common/images/common/sidenav/admission/nav_admission_daytime.png" alt="昼間課" /></a></dd>
					<dd><a href="/admission/correspondence.php"><img src="/common/images/common/sidenav/admission/nav_admission_correspondence.png" alt="通信課" /></a></dd>
					<dd><a href="/admission/tuition.php"><img src="/common/images/common/sidenav/admission/nav_admission_tuition.png" alt="学費" /></a></dd>
				</dl>
			</div>
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/admission/">入学案内</a></li>
				<li class="pankuzu_next">昼間課</li>
			</ul>
		</div>
		<div id="daytime" class="admission_content">
			<h2><img src="/common/images/admission/ttl_h2_bosyu.png" alt="昼間課/募集要項"></h2>
			<h3 id="ao"><img src="/common/images/admission/ttl_h3_ao.png" alt="AO入試"></h3>
			<table>
					<tr>
						<th colspan="2">入試種別</th>
						<td>専願／AO入試</td>
					</tr>
					<tr>
						<th colspan="2">特典</th>
						<td>選考料<span class="bold">18,000円免除</span></td>
					</tr>
					<tr>
						<th rowspan="2" class="row2"><p>エントリ｜</p></th>
						<td class="th_w140">資格</td>
						<td>＊本校オープンキャンパス等のイベントに参加<br>＊AO入試エントリー面談を受ける</td>
					</tr>
					<tr>
						<td class="th_w140">面談実施日</td>
						<td>エントリー面談は、下記のオープンキャンパス・進学相談会の日に行います。<br>
							<dl>
								<dd class="dd_left">【6月】7日、22日</dd>
								<dd>【7月】13日、19日、26日</dd>
								<dd class="dd_left">【8月】2日、6日、24日、30日</dd>
								<dd>【9月】6日、13日、20日、28日</dd>
							</dl>
						</td>
					</tr>
					<tr>
						<th rowspan="3" class="row2"><p>出願</p></th>
						<td class="th_w140">出願資格</td>
						<td><span class="td_ttl">＊AO入試入学前プログラムを修了し、出願許可を受けた方で、下記のいずれかに該当する方</span><br>
							<dl class="ao_left">
								<dt>(1)</dt>
								<dd>高等学校を卒業した方、または平成27年3月に全日制、定時制、単位制のいずれかの制度の高等学校を卒業見込みの方(平成26年4月以降の卒業を含む)。</dd>
								<dt>(2)</dt>
								<dd>最終学歴が中学校卒業で平成27年3月31日までに、高等学校卒業程度認定試験(大検含む)に合格した方または合格見込みの方。</dd>
							</dl>
						</td>
					</tr>
					<tr>
						<td class="th_w140">選考方法</td>
						<td>書類審査</td>
					</tr>
					<tr>
						<td class="th_w140">出願書類</td>
						<td>
							<ul>
								<li>＊入学願書</li>
								<li>＊出願許可書</li>
								<li>＊調査書または最終学校の成績証明書<span class="notes">&nbsp;※</span></li>
								<li>＊最終学校の卒業証明書または卒業見込証明書<span class="small">(卒業見込証明書は、発行され次第送付可)</span><span class="notes">&nbsp;※</span></li>
								<li>＊写真2枚(1枚願書貼付)</li>
						</ul>
							<p class="notes02">※‥兼用不可</p>
						</td>
					</tr>
			</table>

			<h3 class="ttl_h3_img"><img src="/common/images/admission/ttl_h3_entry.png"></h3>
			<table class="admission_table_01 entry">
			<colgroup span="1" class="sec_07"></colgroup>
			<colgroup span="2" class="sec_06"></colgroup>
			<colgroup span="1" class="sec_07"></colgroup>
			<colgroup span="1" class="sec_08"></colgroup>
				<tr>
					<th rowspan="2">&nbsp;</th>
					<th rowspan="2">エントリー期間</th>
					<th colspan="2">入学前プログラム</th>
					<th rowspan="2">出願期間</th>
				</tr>
				<tr>
					<th>サロン体験</th>
					<th>特別授業</th>
				</tr>
				<tr>
					<th>1期</th>
					<td>6月7日〜7月13日</td>
					<td>7月22日〜8月1日</td>
					<td>8月2日</td>
					<td>8月2日〜8月9日必着</td>
				</tr>
				<tr>
					<th>2期</th>
					<td>7月14日〜8月7日</td>
					<td>8月19日〜8月29日</td>
					<td>9月6日</td>
					<td>9月6日〜9月19日必着</td>
				</tr>
				<tr>
					<th>3期</th>
					<td>8月18日〜9月30日</td>
					<td>8月19日〜10月10日</td>
					<td>10月11日</td>
					<td>10月11日〜10月31日必着</td>
				</tr>
			</table>
			<p class="saloon">※サロン体験は、平日一時間程度です。サロン体験の日時は、エントリー後、文章にてご連絡いたします。</p>

			<h3><img src="/common/images/admission/ttl_h3_flow.png" alt="AO入試の流れ"></h3>
			<img src="/common/images/admission/ao_img.png" alt="AO入試の流れ">
			<h3 id="si"><img src="/common/images/admission/ttl_h3_recommend01.png" alt="指定校推薦入試"></h3>
		<table class="admission_table_01　recommend">
		<colgroup span="1" class="sec_03"></colgroup>
		<colgroup span="1" class="sec_09"></colgroup>
			<tr>
				<th>入試種別</th>
				<td>専願／指定校推薦入試</td>
			</tr>
			<tr>
				<th>特典</th>
				<td>選考料<span class="bold">18,000円免除</span></td>
			</tr>
			<tr>
				<th>出願資格</th>
				<td>学校長の推薦で、下記3項目すべてに該当する方
					<dl class="rec_dl">
						<dt>(1)</dt>
						<dd>最終学校での学習成績の評定平均値が2.8位上の方。</dd>
						<dt>(2)</dt>
						<dd>最終学校での出席すべき日数の95％以上出席している方。</dd>
						<dt>(3)</dt>
						<dd>下記いずれかに該当する方。</dd>
						<dt>・</dt>
						<dd>高等学校を卒業した方、または平成27年3月に全日制、定時制、単位制のいずれかの制度の高等学校を卒業見込みの方(平成26年4月以降の卒業を含む)。</dd>
						<dt>・</dt>
						<dd>最終学歴が中学校卒業で平成27年3月31日までに、高等学校卒業程度認定試験(大検含む)に合格した方または合格見込みの方。</dd>
					</dl>
					(注)5月に各高等学校にお知らせします。ご不明な点は、本校入試係までお問い合わせください。
				</td>
			</tr>
			<tr>
				<th>願書受付期間</th>
				<td>平成26年10月1日〜15日　※必着</td>
			</tr>
			<tr>
				<th>選考方法</th>
				<td>書類審査</td>
			</tr>
			<tr>
				<th>提出書類と選考料</th>
				<td>
					<ul>
						<li>＊入学願書</li>
						<li>＊指定校推薦書(指定校推薦募集要項参照)</li>
						<li>＊調査書または最終学校の成績証明書<span class="notes03">&nbsp;※</span></li>
						<li>＊最終学校の卒業証明書または卒業見込証明書<span class="notes03">&nbsp;※</span></li>
						<li>＊写真2枚(1枚願書貼付)</li>
						</ul>
						<p class="notes03">※‥兼用可</p>
				</td>
			</tr>
			<tr>
				<th>合格発表</th>
				<td>平成26年10月23日</td>
			</tr>
		</table>

		<h3 id="su"><img src="/common/images/admission/ttl_h3_recommend02.png" alt="推薦入試"></h3>
		<table class="admission_table_01　recommend">
		<colgroup span="1" class="sec_03"></colgroup>
		<colgroup span="1" class="sec_09"></colgroup>
			<tr>
				<th>入試種別</th>
				<td>専願／指定校推薦入試</td>
			</tr>
			<tr>
				<th>出願資格</th>
				<td>担任の先生、部活顧問の先生、進路指導の先生、サロン関係者のいずれかの推薦で、<br>下記3項目すべてに該当する方
					<dl class="rec_dl">
						<dt>(1)</dt>
						<dd>最終学校での学習成績の評定平均値が2.5位上の方。</dd>
						<dt>(2)</dt>
						<dd>最終学校での出席すべき日数の90％以上出席している方。</dd>
						<dt>(3)</dt>
						<dd>下記いずれかに該当する方。</dd>
						<dt>・</dt>
						<dd>高等学校を卒業した方、または平成27年3月に全日制、定時制、単位制のいずれかの制度の高等学校を卒業見込みの方(平成26年4月以降の卒業を含む)。</dd>
						<dt>・</dt>
						<dd>最終学歴が中学校卒業で平成27年3月31日までに、高等学校卒業程度認定試験(大検含む)に合格した方または合格見込みの方。</dd>
					</dl>
				</td>
			</tr>
			<tr>
				<th>願書受付期間</th>
				<td>平成26年10月1日〜15日　※必着</td>
			</tr>
			<tr>
				<th>選考方法</th>
				<td>面接、書類審査</td>
			</tr>
			<tr>
				<th>試験日</th>
				<td>平成26年10月20日</td>
			</tr>
			<tr>
				<th>提出書類と選考料</th>
				<td>
					<ul>
						<li>＊入学願書</li>
						<li>＊推薦書</li>
						<li>＊調査書または最終学校の成績証明書<span class="notes03">&nbsp;※</span></li>
						<li>＊最終学校の卒業証明書または卒業見込証明書<span class="notes03">&nbsp;※</span></li>
						<li>＊写真2枚(1枚願書貼付)</li>
						<li>＊選考料　18,000円</li>
						</ul>
						<p class="notes03">※‥兼用可</p>
				</td>
			</tr>
			<tr>
				<th>合格発表</th>
				<td>平成26年10月23日</td>
			</tr>
		</table>
		<h3 id="ji"><img src="/common/images/admission/ttl_h3_recommend03.png" alt="自己推薦入試"></h3>
		<table class="admission_table_01　recommend">
		<colgroup span="1" class="sec_03"></colgroup>
		<colgroup span="1" class="sec_09"></colgroup>
			<tr>
				<th>入試種別</th>
				<td>専願／指定校推薦入試</td>
			</tr>
			<tr>
				<th>出願資格</th>
				<td>本校オープンキャンパス等のイベントに参加し、理容・美容・エステティック・ネイル・メイクの各業界で活躍したいという意欲が高い方で、下記のいずれかに該当する方。
					<dl class="rec_dl">
						<dt>(1)</dt>
						<dd>高等学校を卒業した方、または平成27年3月に全日制、定時制、単位制のいずれかの制度の高等学校を卒業見込みの方(平成26年4月以降の卒業を含む)。</dd>
						<dt>(2)</dt>
						<dd>最終学歴が中学校卒業で平成27年3月31日までに、高等学校卒業程度認定試験(大検含む)に合格した方または合格見込みの方。</dd>
				</td>
			</tr>
			<tr>
				<th>願書受付期間</th>
				<td>平成26年10月1日〜15日　※必着</td>
			</tr>
			<tr>
				<th>選考方法</th>
				<td>面接、書類審査</td>
			</tr>
			<tr>
				<th>試験日</th>
				<td>平成26年10月20日</td>
			</tr>
			<tr>
				<th>出願書類と選考料</th>
				<td>
					<ul>
						<li>＊入学願書</li>
						<li>＊推薦書</li>
						<li>＊調査書または最終学校の成績証明書<span class="notes">&nbsp;※</span></li>
						<li>＊最終学校の卒業証明書または卒業見込証明書<span class="notes">&nbsp;※</span></li>
						<li>＊写真2枚(1枚願書貼付)</li>
						<li>＊選考料　18,000円</li>
						</ul>
						<p class="notes02">※‥兼用不可</p>
				</td>
			</tr>
			<tr>
				<th>合格発表</th>
				<td>平成26年10月23日</td>
			</tr>
		</table>

		<h3 id="i"><img src="/common/images/admission/ttl_h3_general.png" alt="一般入試"></h3>
		<table class="admission_table_01　recommend">
		<colgroup span="1" class="sec_03"></colgroup>
		<colgroup span="1" class="sec_09"></colgroup>
			<tr>
				<th>入試種別</th>
				<td>併願可／一般入試</td>
			</tr>
			<tr>
				<th>出願資格</th>
				<td>下記のいずれかに該当する方。
					<dl class="rec_dl">
						<dt>(1)</dt>
						<dd>高等学校を卒業した方、または平成27年3月に全日制、定時制、単位制のいずれかの制度の高等学校を卒業見込みの方(平成26年4月以降の卒業を含む)。</dd>
						<dt>(2)</dt>
						<dd>最終学歴が中学校卒業で平成27年3月31日までに、高等学校卒業程度認定試験(大検含む)に合格した方または合格見込みの方。</dd>
				</td>
			</tr>
			<tr>
				<th>願書受付期間／<br>試験日／合格発表</th>
				<td class="pd0">
					<table class="general_table">
					<tr>
						<th>願書受付期間</th>
						<th>試験日</th>
						<th>合格発表</th>
					</tr>
					<tr>
						<td>一次募集：平成26年11月1日〜11月28日必着</td>
						<td>11月17日<br>12月8日</td>
						<td>11月20日<br>12月11日</td>
					</tr>
					<tr>
						<td>二次募集：平成26年12月1日〜</td>
						<td>12月10日以降</td>
						<td rowspan="2">入試日翌日</td>
					</tr>
					<tr>
						<td>三次募集：平成27年2月2日〜</td>
						<td>2月9日以降</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<th>選考方法</th>
				<td>面接、書類審査</td>
			</tr>
			<tr>
				<th>出願書類と選考料</th>
				<td>
					<ul>
						<li>＊入学願書</li>
						<li>＊調査書または最終学校の成績証明書<span class="notes">&nbsp;※</span></li>
						<li>＊最終学校の卒業証明書または卒業見込証明書<span class="notes">&nbsp;※</span></li>
						<li>＊写真2枚(1枚願書貼付)</li>
						<li>＊選考料　18,000円</li>
						</ul>
						<p class="notes02">※‥兼用不可</p>
				</td>
			</tr>
		</table>
		</div><!-- daytime end -->
	</div><!-- content end -->
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");?>
</div>
</body>
</html>