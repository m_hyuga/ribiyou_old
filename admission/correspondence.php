<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>通信課程 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/admission.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="admission";
				require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/admission/img_main.jpg" alt="入学案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/admission/"><img src="/common/images/common/sidenav/admission/nav_admission.png" alt="入学案内" /></a></dt>
					<dd><a href="/admission/daytime.php"><img src="/common/images/common/sidenav/admission/nav_admission_daytime.png" alt="昼間課" /></a></dd>
					<dd class="navigationhover"><a href="/admission/correspondence.php"><img src="/common/images/common/sidenav/admission/nav_admission_correspondence.png" alt="通信課" /></a></dd>
					<dd><a href="/admission/tuition.php"><img src="/common/images/common/sidenav/admission/nav_admission_tuition.png" alt="学費" /></a></dd>
				</dl>
			</div>
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/admission/">入学案内</a></li>
				<li class="pankuzu_next">通信課程</li>
			</ul>
		</div>
		<div id="correspondence" class="admission_content">
			<h2><img src="/common/images/admission/ttl_correspondence.png"></h2>
			<p>働きながら、大学などに在学しながら、理容師、美容師の資格取得を目指す方のためのコースです。</p>
			<table class="admission_table_01">
			<colgroup span="1" class="sec_09"></colgroup>
				<tr>
					<th class="bor_tp">募集定員</th>
					<td>理容科40名 ／ 美容科80名</td>
				</tr>
				<tr>
					<th>出願資格</th>
					<td>通学校・高等学校を卒業した方、または平成27年3月に、中学校以上を卒業見込みの方</td>
				</tr>
				<tr>
					<th>願書受付期間／<br>試験日／合格発表</th>
					<td class="pd0">
						<table class="general_table">
						<colgroup span="1" class="sec_04"></colgroup>
						<colgroup span="1" class="sec_10"></colgroup>
						<colgroup span="2" class="sec_01"></colgroup>
					<tr class="bor_tn">
						<td class="">&nbsp;</td>
						<td>願書受付期間</td>
						<td>試験日</td>
						<td>合格発表</td>
					</tr>
					<tr>
						<td rowspan="2">平成27年<br>4月入学</td>
						<td>一次募集：平成26年12月1日〜12月25日必着</td>
						<td>1月19日</td>
						<td>1月22日</td>
					</tr>
					<tr>
						<td>二次募集：平成27年1月20日〜</td>
						<td>1月28日以降</td>
						<td>入試日翌日</td>
					</tr>
					<tr>
						<td rowspan="2" class="bor_bt0">平成26年<br>10月入学</td>
						<td>一次募集：平成26年6月2日〜8月1日必着</td>
						<td>8月18日</td>
						<td>8月20日</td>
					</tr>
					<tr>
						<td class="bor_bt0">二次募集：平成26年8月18日〜9月3日必着</td>
						<td class="bor_bt0">9月8日</td>
						<td class="bor_bt0">9月9日</td>
					</tr>
				</table>
					</td>
				</tr>
				<tr>
					<th>選考方法</th>
					<td>書類審査・学科試験(国語)・作文</td>
				</tr>
				<tr>
					<th class="bor_bp">出願書類と選考料</th>
					<td>
						<ul>
							<li>＊入学願書</li>
							<li>＊調査書または最終学校の成績証明書<span class="notes">&nbsp;※</span></li>
							<li>＊最終学校の卒業証明書または卒業見込証明書<span class="notes">&nbsp;※</span></li>
							<li>＊写真2枚(1枚願書貼付)</li>
							<li>＊選考料　19,440円</li>
						</ul>
						<p class="notes02">※‥兼用不可</p>
					</td>
				</tr>
			</table>
		</div><!-- correspondence end -->
	</div><!-- content end -->
		<?php require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");?>
</div>
</body>
</html>