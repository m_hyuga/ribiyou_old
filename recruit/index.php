<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>教員募集 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="recruit";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/recruit/img_main.jpg" alt="教員募集" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">教員募集</li>
			</ul>
		</div>
		<div id="mainarea" class="recruit_content">
			<p>富山県理容美容専門学校では、熱意あふれる指導者を募集します。</p>
			<table>
				<tbody>
				<tr>
						<th>募集職種</th>
						<td>
						①美容科専任教員／契約職員<br>
						②理容科専任教員／契約職員<br>
						③エステティック科専任教員／契約職員（日本エステティック協会認定校）<br>
						④ネイル・メイク科専任教員助手／契約職員<br>
						※①②入校後、教育センターの研修を受けていただき、認定試験に合格後、<br>
						　正職員として採用いたします。<br>
						※④入校後、必要資格を取得していただきます。
						</td>
				</tr>
				<tr>
						<th>業務内容</th>
						<td>
						各科教員助手から始めていただきます。
						</td>
				</tr>
				<tr>
						<th>応募資格</th>
						<td>
						①美容師免許<br>
						美容師免許取得後、実務経験3年以上〜5年以下<br>
						②理容師免許<br>
						　理容師免許取得後、実務経験3年以上～5年以下<br>
						③トータルエステティックアドバイザー（TEA）保持者<br>
						④日本メイクアップ技術検定2級以上<br>
						①、②、③、④ともに、教員経験不問（研修いたしますので、ご安心ください）
						</td>
				</tr>
				<tr>
						<th>勤務時間</th>
						<td>
						8:30〜17:15
						</td>
				</tr>
				<tr>
						<th>休日</th>
						<td>
						土・日曜、祝日、有給10日（試用期間6カ月終了後）<br>
						（当校イベント時、休日出勤あり）
						</td>
				</tr>
				<tr>
						<th>給与</th>
						<td>
						16万〜（当校規定による）
						</td>
				</tr>
				<tr>
						<th>待遇</th>
						<td>
						私学共済、各種手当あり。正職員として採用後、昇給、賞与あり。
						</td>
				</tr>
				<tr>
						<th>勤務地</th>
						<td>
						富山県理容美容専門学校（JR富山駅北口徒歩9分）
						</td>
				</tr>
				<tr>
						<th>応募方法</th>
						<td>
						履歴書・職務経歴書をご郵送ください。<br>
						書類選考の上、面接日時についてご連絡いたします。<br>
						採用人数に達し次第、受付を終了させていただきます。<br>
						■郵送先<br>
						〒930-0804 富山県富山市下新町32-26<br>
						学校法人富山県理容美容学校　富山県理容美容専門学校<br>
						担当：山本
						</td>
				</tr>
				</tbody>
				<tfoot>
				<tr>
						<th>お問い合わせ</th>
						<td>
						TEL. 076-432-3037（山本まで）
						</td>
				</tr>
				</tfoot>
			</table>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>