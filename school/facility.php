<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学校案内 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/school.css" />
<link rel="stylesheet" type="text/css" href="/common/css/colorbox.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.colorbox-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".group1").colorbox({rel:'group1'});
});
 </script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="school";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/school/img_main.jpg" alt="学校案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/school/"><img src="/common/images/common/sidenav/school/nav_school.png" alt="学校案内" /></a></dt>
					<dd><a href="/school/history.php"><img src="/common/images/common/sidenav/school/nav_school_history.png" alt="学校の歴史" /></a></dd>
					<dd class="navigationhover"><a href="/school/facility.php"><img src="/common/images/common/sidenav/school/nav_school_facility.png" alt="施設・設備" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/school/">学校案内</a></li>
				<li class="pankuzu_next">施設・設備</li>
			</ul>
		</div>
		<div id="mainarea" class="school_facility_content">
			<h2><img src="/common/images/school/facility/ttl_facility.png" alt="最先端の施設・設備" /></h2>
			<p>
			全国でも屈指のスタイリスト技術と接客術を学べるのが本校の特徴です。<br>
			新校舎では最新鋭の施設・設備で、実践的なサロンワークを身につけていきます。
			</p>
			
			<table>
					<colgroup span="1" class="sec_01"></colgroup>
					<colgroup span="1" class="sec_02"></colgroup>
					<colgroup span="1" class="sec_01"></colgroup>
					<colgroup span="1" class="sec_02"></colgroup>
					<colgroup span="1" class="sec_01"></colgroup>
				<tr>
					<td>
						<a href="/common/images/school/facility/photo_01.jpg" class="group1" title="正面玄関"><img src="/common/images/school/facility/photo_01s.jpg" alt="" class="over" /></a>
						<h3>正面玄関</h3>
						<p>建物に入ってすぐのエントランスは、吹き抜けで開放感たっぷり。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_02.jpg" class="group1" title="ラウンジ"><img src="/common/images/school/facility/photo_02s.jpg" alt="" class="over" /></a>
						<h3>ラウンジ</h3>
						<p>スタイリッシュなインテリアでコーディネートされたラウンジは、明るく開放的です。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_03.jpg" class="group1" title="エステ実習室"><img src="/common/images/school/facility/photo_03s.jpg" alt="" class="over" /></a>
						<h3>エステ実習室</h3>
						<p>低周波やバイブレーションなど、最先端のボディトリートメント機器を備えた本校自慢のエステ実習室です。</p>
					</td>
				
				</tr>
				<tr>
					<td>
						<a href="/common/images/school/facility/photo_04.jpg" class="group1" title="実習室"><img src="/common/images/school/facility/photo_04s.jpg" alt="" class="over" /></a>
						<h3>実習室</h3>
						<p>明るく開放的な実習室では、カット、ワインディング、スタイリングなど様々な技術を学びます。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_05.jpg" class="group1" title="消毒実験室"><img src="/common/images/school/facility/photo_05s.jpg" alt="" class="over" /></a>
						<h3>消毒実験室</h3>
						<p>理容・美容で使用する器具の消毒や、使用方法などを学びます。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_06.jpg" class="group1" title="多目的ホール"><img src="/common/images/school/facility/photo_06s.jpg" alt="" class="over" /></a>
						<h3>多目的ホール</h3>
						<p>学生がランチを楽しむ多目的ホールは、景色も抜群。</p>
					</td>
				
				</tr>
				<tr>
					<td>
						<a href="/common/images/school/facility/photo_07.jpg" class="group1" title="和室実習室"><img src="/common/images/school/facility/photo_07s.jpg" alt="" class="over" /></a>
						<h3>和室実習室</h3>
						<p>浴衣や和服の着付けの実習などを行う、明るく広々した和室です。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_08.jpg" class="group1" title="ジョブサロン"><img src="/common/images/school/facility/photo_08s.jpg" alt="" class="over" /></a>
						<h3>ジョブサロン</h3>
						<p>最新鋭のシャンプーベッドを完備。ヘッドスパ、シェービング、エステ、ネイル、カットなどあらゆる技術が行えるサロン実習室です。</p>
					</td>
					<td>&nbsp;</td>
					<td>
						<a href="/common/images/school/facility/photo_09.jpg" class="group1" title="視聴覚室1・2"><img src="/common/images/school/facility/photo_09s.jpg" alt="" class="over" /></a>
						<h3>視聴覚室1・2</h3>
						<p>カメラやモニターを完備した視聴覚室では、AV機器を使用して講義を行います。</p>
					</td>
				
				</tr>
				<tr>
					<td>
						<a href="/common/images/school/facility/photo_10.jpg" class="group1" title="ネイル実習室"><img src="/common/images/school/facility/photo_10s.jpg" alt="" class="over" /></a>
						<h3>ネイル実習室</h3>
						<p>ネイルの実技を学びます。学生同士がペアになって、練習しあうこともあります。</p>
					</td>
					<td rowspan="3">&nbsp;
					</td>
				
				</tr>
			</table>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>