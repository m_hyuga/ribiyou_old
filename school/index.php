<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学校案内 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/school.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="school";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/school/img_main.jpg" alt="学校案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/school/"><img src="/common/images/common/sidenav/school/nav_school.png" alt="学校案内" /></a></dt>
					<dd><a href="/school/history.php"><img src="/common/images/common/sidenav/school/nav_school_history.png" alt="学校の歴史" /></a></dd>
					<dd><a href="/school/facility.php"><img src="/common/images/common/sidenav/school/nav_school_facility.png" alt="施設・設備" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">学校案内</li>
			</ul>
		</div>
		<div id="mainarea" class="school_content">
			<h2>
			全国トップクラスの最新鋭の施設と設備、<br>
			よりリアルな環境で学ぶから、<br>
			卒業後のサロンワークにもかなり有利。<br>
			いま求められているプロを、最新鋭の環境で育てています。
			</h2>
			<img src="/common/images/school/img_02.jpg" alt=""/>
			<table>
					<colgroup span="1" class="sec_01"></colgroup>
					<colgroup span="1" class="sec_02"></colgroup>
					<colgroup span="1" class="sec_01"></colgroup>
				<tr>
					<td>
					<a href="/school/history.php"><img src="/common/images/school/btn_history.jpg" alt="開校60年以上の歴史" class="over" /></a>
					<p>
					理容・美容の専門技術者の養成機関として、昭和22年に開校して以来、沢山の優秀な人材を輩出し続けている本校。常に時代を先行くトップクラスの教育水準を維持し続けています。
					</p>
					</td>
					<td>&nbsp;</td>
					<td>
					<a href="/school/facility.php"><img src="/common/images/school/btn_facility.jpg" alt="最先端の施設・設備" class="over" /></a>
					<p>
					本校自慢の施設・設備は、北陸トップクラス水準です。サロン仕様の実習室や、最新の機器を導入した実践的な教育環境によって、現場ですぐに活躍できるプロを養成します。
					</p>
					</td>
				</tr>
				<tr>
					<td>
					<a href="/course/"><img src="/common/images/school/btn_course.jpg" alt="学科紹介" class="over" /></a>
					<p>
					理容科・美容科・エステティック科・トータルビューティ科の4つの学科があります。
					</p>
					</td>
					<td>&nbsp;</td>
					<td>
					<a href="/access/"><img src="/common/images/school/btn_access.jpg" alt="アクセス" class="over" /></a>
					<p>
					JR富山駅 北口から徒歩10分。 富山県理容美容専門学校は、通学に便利な抜群のロケーションにあります。
					</p>
					</td>
				</tr>
			</table>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>