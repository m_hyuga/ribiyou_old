<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>学校案内 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/school.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="school";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/school/img_main.jpg" alt="学校案内" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="side_navigation">
				<dl>
					<dt><a href="/school/"><img src="/common/images/common/sidenav/school/nav_school.png" alt="学校案内" /></a></dt>
					<dd class="navigationhover"><a href="/school/history.php"><img src="/common/images/common/sidenav/school/nav_school_history.png" alt="学校の歴史" /></a></dd>
					<dd><a href="/school/facility.php"><img src="/common/images/common/sidenav/school/nav_school_facility.png" alt="施設・設備" /></a></dd>
				</dl>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/school/">学校案内</a></li>
				<li class="pankuzu_next">学校の歴史</li>
			</ul>
		</div>
		<div id="mainarea" class="school_history_content">
			<h2><img src="/common/images/school/history/ttl_history.png" alt="開校60年以上の歴史" /></h2>
			<ul class="history_list">
				<li>
					<dl>
						<dt>昭和22年</dt>
						<dd>理容職業補導所として設立</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和25年3月</dt>
						<dd>理容師養成施設として富山県理容学院が指定される（厚生省衛第80号)</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和25年4月</dt>
						<dd>第1期生45名を迎えて開校（理容部）</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和26年3月</dt>
						<dd>
						私立各種学校の設置認可を受ける（富山県地第427号）<br>
						美容部の設置認可を受ける
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和26年4月</dt>
						<dd>第2期生として、理容部65名、美容部20名を迎える</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和29年4月</dt>
						<dd>通信教育指定校として認可される</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和29年6月</dt>
						<dd>通信課生第1期57名を迎える（美容部）</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和30年7月</dt>
						<dd>学校法人の認可を受け、富山県理容美容学校となる（富山県地第620号）</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和40年8月</dt>
						<dd>新校舎完成</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>昭和51年4月</dt>
						<dd>高等専修学校の認可を受ける（富山県總分159号）</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>平成10年3月</dt>
						<dd>第2校舎完成</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>平成10年4月</dt>
						<dd>法改正により二年制に移行、名称を富山県理容美容専門学校と改める。</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>平成20年4月</dt>
						<dd>第３校舎落成。エステティック科、ネイル・メイク科新設される。</dd>
					</dl>
				</li>
			</ul>
			<ul class="history_photo">
				<li><img src="/common/images/school/history/photo_01.jpg" alt="" /></li>
				<li><img src="/common/images/school/history/photo_02.jpg" alt="" /></li>
				<li><img src="/common/images/school/history/photo_03.jpg" alt="" /></li>
				<li><img src="/common/images/school/history/photo_04.jpg" alt="" /></li>
			</ul>
			<br class="clear">
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>