<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>第5回全国理容美容学生技術大会信越北陸地区予選大会　選手たちの様子です★</h3>
            <time>2013年07月31日</time>
            <p>全国理容美容学生技術大会信越北陸地区予選大会の写真をアップします❤</p>
<p>&nbsp;</p>
<p>県理美の選手たちは、<font style="FONT-SIZE: 1.56em"><strong>大注目</strong></font>されていましたよ！！</p>
<p>&nbsp;</p>
<p>&nbsp;<a href="http://www.toyama-bb.ac.jp/news/img/CIMG0542-1.JPG"><img class="mt-image-none" alt="CIMG0542-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/CIMG0542-1-thumb-448x336-1235.jpg" width="448" height="336" /></a></p>
<p>
<p>&nbsp;</p>
<p>理容部門ワインディングの墓越さんです。</p>
<p>大会会場のおーーーーーきなモニターに映し出されています！</p>
<p>見ていた応援チームも熱くなります。</p>
<p>&nbsp;</p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_1605-1.JPG"><img class="mt-image-none" alt="IMG_1605-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_1605-1-thumb-448x237-1227.jpg" width="448" height="237" /></a></p>
<p></p>
<p></p>
<p>&nbsp;</p>
<p>理容部門ワインディングは選手がひしめき合っております！！</p>
<p>でも<font style="FONT-SIZE: 1.56em"><strong>集中集中！！！</strong></font></p>
<p><font style="FONT-SIZE: 0.99em">普段の練習を思い出して！！！</font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_1732-1.JPG"><img class="mt-image-none" alt="IMG_1732-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_1732-1-thumb-380x336-1229.jpg" width="380" height="336" /></a></span></p>
<p>&nbsp;</p>
<p>美容部門カットです。</p>
<p>このウイッグに今の自分の全力を込めます。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_1722-1.JPG"><img class="mt-image-none" alt="IMG_1722-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_1722-1-thumb-437x336-1233.jpg" width="437" height="336" /></a></span></p>
<p>&nbsp;</p>
<p>朝も昼も夜も、ずーーーーーっとやってきた練習を、本番20分という短時間にいかに&nbsp;発揮できるか！！</p>
<p>自分との戦いでもあるのです！！厳しい～！！</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"></span></p>
<p></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/CIMG0539-1.JPG"></a></span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_1888-1.JPG"><img class="mt-image-none" alt="IMG_1888-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_1888-1-thumb-448x299-1239.jpg" width="448" height="299" /></a></span>&nbsp;</p>
<p>&nbsp;</p>
<p>全校で応援にかけつけました。</p>
<p>理容科美容科の1年生は、来年の自分たちを想像していたかもしれません・・・★☆★☆</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/CIMG0539-1.JPG"><img class="mt-image-none" alt="CIMG0539-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/CIMG0539-1-thumb-448x336-1237.jpg" width="448" height="336" /></a></p>
<p>&nbsp;</p>
<p>真剣な表情★モニターからも競技熱がビシビシ伝わってきます！！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>全国大会は11月17日に兵庫県で開催されます。</p>
<p>それまでに、さらに技術を磨いて、いろんな面でパワーアップして頑張りましょう！！！！！！</p>
<p>&nbsp;</p>
<p>【理容部門　ワインディング】</p>
<p>優秀賞　墓越　和哉（富山第一高等学校　出身）</p>
<p>優秀賞　澤井　彩夏（富山県立伏木高等学校　出身）</p>
<p>&nbsp;</p>
<p>【理容部門　ミディアムカットヘア】</p>
<p>優秀賞　佐生　大（富山県立滑川高等学校　出身）</p>
<p>優秀賞　太田　桂子</p>
<p>敢闘賞　桶谷　怜児（富山県立高岡工芸高等学校　出身）</p>
<p>&nbsp;</p>
<p>【理容部門　チャレンジアートヘア】</p>
<p>敢闘賞　藤井　純（富山県立南砺総合高等学校井波高等学校　出身）</p>
<p>&nbsp;</p>
<p>【美容部門　ワインディング】</p>
<p>優秀賞　鞍田　理佳子（富山県立伏木高等学校　出身）</p>
<p>&nbsp;</p>
<p>【美容部門　カット】</p>
<p>優秀賞　池下　このみ（富山県立志貴野高等学校　出身）</p>
<p>優秀賞　櫻田　龍玄（龍谷富山高等学校　出身）</p>
<p>敢闘賞　長谷川　紘道（富山県立魚津工業高等学校　出身）</p>
<p>&nbsp;</p>
<p>【デザイン画】</p>
<p>敢闘賞　舟根　麻未（富山県立富山北部高等学校　出身）</p>
<p>&nbsp;</p>
<p>優秀賞を獲得した選手が、全国大会へ出場します。</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-27.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>第5回全国理容美容学生技術大会信越北陸地区予選大会　選手たちの様子です★</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>