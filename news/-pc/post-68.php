<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>１０月１１日　　オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
１０月１１日　　オープンキャン…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.10.11</time>
			<h2>１０月１１日　　オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				みなさまこんにちは！<div><br /></div><div><font style="font-size: 1.953125em;"><b>☆Happy Halloween☆</b></font></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07780.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07780.JPG" width="490" height="367" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">今日はみんなで<b><font style="font-size: 1.953125em;">仮装オープンキャンパス♪♪</font></b>　</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">ワイワイにぎやかな一日になりました＼(^o^)／</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">さて、さて、今日のクローズアップは<font style="font-size: 1.25em;"><b>トータルビューティ科</b></font>の<font style="font-size: 1.5625em;"><b>『傷メイク</b></font></span><span style="font-size: 1em;"><font style="font-size: 1.5625em;"><b>』</b></font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07762.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07762.JPG" width="490" height="367" class="mt-image-none" /></span></div><div>ハロウィンに自分でできる『傷メイク』の体験をしました。</div><div><br /></div><div>すごいですね！(＠_＠;)本当の傷があるみたいです。</div><div><br /></div><div>本校のトータルビューティ科では、通常のメイクはもちろん学びますが、</div><div>このような特殊メイクや撮影用メイクなどもできるようになっちゃいます(*^_^*)</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div>そのほかにも各科の素敵な体験をしていただきました。</div><div><br /></div><div><b><font style="font-size: 1.5625em;">＜理容科＞　</font></b><font style="font-size: 1.5625em;">シェービング体験</font></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07774.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07774.JPG" width="490" height="367" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">すべすべワントーン白いお肌に・・・。魔女がシェービングしてます(＠_＠;)</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">＜美容科＞　ヘアチョーク体験</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07768.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07768.JPG" width="490" height="367" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">簡単！誰でもできるヘアチョークで海外セレブ気分♪　悪魔ちゃんがカラフルになって</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">可愛くなりました。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">＜エステティック科＞　　セルライト撃退！体験（脚）</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07756.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07756.JPG" width="490" height="367" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">G5（細かく振動させてセルライトを分解する機械）でスッキリ美脚に☆</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">今日は仮装パーティーということで、みんなとっても楽しそうでした(^^♪</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07739.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07739.JPG" width="490" height="367" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">次の仮装イベントは・・・待ちに待ったクリスマスかな？！</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">どんな仮装をしようか今からワクワクします(*^_^*)</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">次回のオープンキャンパスは</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.953125em;">１１月８日（土）１０：００～１２：３０　</font>（９：３０～受付開始）</span></div><div><br /></div><div><font style="font-size: 1.25em;"><font style="font-size: 0.8em;">体験内容、参加申し込みはホームページをご覧ください</font>→</font><a href="http://www.toyama-bb.ac.jp/"><b><font style="font-size: 1.5625em;">こちらをクリック☆</font></b></a></div><div><br /></div><div><br /></div><div><br /></div><div>また、次回オープンキャンパスまでの間に、進学相談会も行います(^.^)</div><div><b><font style="font-size: 1.5625em;">１０月２５日（土）１０：００～　</font></b>（９：３０～受付開始）</div><div>進学についてのお悩みや、奨学金についてなど相談がある方はぜひ、ご参加ください！</div><div><br /></div><div><br /></div><div>次回も楽しい体験を準備しております(^O^)／</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC07784.JPG" src="http://www.toyama-bb.ac.jp/news/DSC07784.JPG" width="490" height="367" class="mt-image-none" /></span></div><div>僕たち、私たちスタッフが皆さんをお待ちしています！！一緒に楽しい体験をしましょう☆</div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-67.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>