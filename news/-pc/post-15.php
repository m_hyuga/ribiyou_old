<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>サロン室　リニューアル | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
サロン室　リニューアル</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.11.08</time>
			<h2>サロン室　リニューアル</h2>
			<div class="news_area">
				<p>県理美の新しい顔が完成しました！</p>
<p>以前にも掲載しましたが、今回は中の様子を詳しくお伝えします。</p>
<p>&nbsp;</p>
<p>フェイシャルエステの部屋は２つあります。</p>
<p>お客様おひとりおひとりのプライベート空間とするために、完全個室になっています。</p>
<p>どちらもとても落ち着いたデザインになっており、リラックスできるBGM。</p>
<p>ホッとした雰囲気です。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8128-1.JPG"><img class="mt-image-none" alt="IMG_8128-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8128-1-thumb-448x299-729.jpg" width="448" height="299" /></a></span></p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8173-1.JPG"><img class="mt-image-none" alt="IMG_8173-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8173-1-thumb-448x290-731.jpg" width="448" height="290" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>ボディエステの部屋です。痩身とマッサージの機械もあります。</p>
<p>奥にはシャワー室も完備しています。</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8116-1.JPG"><img class="mt-image-none" alt="IMG_8116-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8116-1-thumb-448x299-733.jpg" width="448" height="299" /></a></span></p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8114-1.JPG"><img class="mt-image-none" alt="IMG_8114-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8114-1-thumb-299x448-735.jpg" width="299" height="448" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>パウダールームです。</p>
<p>エステティック施術後、メイクしてお帰りいただけます。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8182-1.JPG"><img class="mt-image-none" alt="IMG_8182-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8182-1-thumb-287x448-737.jpg" width="287" height="448" /></a></span></p>
<p>&nbsp;</p>
<p>平成25年4月から、完全予約制・完全個室・女性専用エステティックサロンとしてオープンです。</p>
<p>営業時間やメニュー、料金につきましては、今後お伝えしてまいります。</p>
<p>どうぞ、お楽しみに。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_8169-1.JPG"><img class="mt-image-none" alt="IMG_8169-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/IMG_8169-1-thumb-448x473-739.jpg" width="448" height="473" /></a></span></p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/25111.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/2012.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>