<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>理容科・美容科　１・２学年　ワクワク交流授業♪ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
理容科・美容科　１・２学年　ワ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.05.02</time>
			<h2>理容科・美容科　１・２学年　ワクワク交流授業♪</h2>
			<div class="news_area">
				先日、理容科・美容科の1学年と2学年の交流授業がありました！<div><br /></div><div>今回は先輩から後輩へワインディング技術を教えるという内容です☆</div><div><br /></div><div>教えてもらう1年生は、入学して初めての先輩との交流にワクワクしながら一生懸命に</div><div><br /></div><div>まだまだ慣れない技術の習得に励んでいました。</div><div><br /></div><div>教える側の２年生もいつもより目をキラキラ輝かせながら後輩に技術を教えており、</div><div><br /></div><div>その姿はとても頼もしく感じました！！</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03544.JPG"><img alt="DSC03544.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03544-thumb-300x225-1733.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03496.JPG"><img alt="DSC03496.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03496-thumb-300x225-1735.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03507.JPG"><img alt="DSC03507.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03507-thumb-300x225-1737.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03472.JPG"><img alt="DSC03472.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03472-thumb-300x225-1741.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>これからもこのような交流授業をおこなって、先輩と後輩が良い関係を築き、たくさんの技術を</div><div><br /></div><div>伝えていって欲しいと思います(*^_^*)</div><div><br /></div><div>なんと、オープンキャンパスでもこのような先輩との交流が楽しめます♪オープンキャンパスで</div><div><br /></div><div>先輩から学校に関するお話を聞いたり、技術を学んでみませんか？！</div><div><br /></div><div><br /></div><div>オープンキャンパスのお申し込みは→<font style="font-size: 1.953125em;"><b><a href="http://www.toyama-bb.ac.jp/opencampus/">こちら☆</a><font style="font-size: 0.5120000000000001em;">をクリック</font></b><a href="http://www.toyama-bb.ac.jp/opencampus/"></a></font></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/2.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-50.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>