<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>平成２５年度特待生決定しました！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
平成２５年度特待生決定しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.01.07</time>
			<h2>平成２５年度特待生決定しました！</h2>
			<div class="news_area">
				<p>平成２５年度特待生が決定しました！今年はなんと！過去最多１６名、学業優秀賞名７名、善行奨励賞９名の生徒が受賞しました！！</p>
<p>学業優秀賞は、特に学業成績が優れた生徒に贈られ、学費２０万円の免除を受けることができます！善行奨励賞は、学業優秀賞に次ぐ成績を収めた生徒で、他の生徒の模範となる生徒に贈られ、学費１０万円の免除を受けることができます！</p>
<p><font style="FONT-SIZE: 1.25em">学業優秀賞の受賞者です！</font></p>
<p><a href="http://www.toyama-bb.ac.jp/news/134.JPG">
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;</span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"></a></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/136.JPG"></a></span><a href=""><font style="FONT-SIZE: 1.25em">
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img class="mt-image-none" alt="1351.JPG" src="http://www.toyama-bb.ac.jp/news/1351.JPG" width="480" height="386" /></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img class="mt-image-none" alt="136.JPGのサムネール画像のサムネール画像" src="" width="1" height="1" /></a><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/136-thumb-639x467-1637.jpg"></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img class="mt-image-none" alt="136.JPGのサムネール画像" src="" width="1" height="1" /></a></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"></span></font></p>
<p>２年理容科　太田　桂子さん</p>
<p>２年理容科　佐生　大さん（富山県立滑川高等学校出身）</p>
<p>２年理容科　古家　汐里さん（富山県立富山西高等学校出身）</p>
<p>２年美容科　森田　佑奈さん（岐阜県立飛騨高山高等学校出身）</p>
<p>１年理容科　清水　理紗子さん（富山第一高等学校出身）</p>
<p>１年美容科　菓子　茉穂さん（富山県立小杉高等学校出身）</p>
<p>１年美容科　中舘　沙也香さん</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">善行奨励賞の受賞者です！</font></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img class="mt-image-none" alt="1361.JPG" src="http://www.toyama-bb.ac.jp/news/1361.JPG" width="479" height="350" /></span></p>
<p>２年美容科　森田　航平さん（岐阜県立飛騨高山高等学校出身）</p>
<p>２年美容科　狩野　千秋さん（富山県立富山西高等学校出身）</p>
<p>１年美容科　山本　琴乃さん（富山県立雄山高等学校出身）</p>
<p>１年美容科　南　星来さん（富山県立富山商業高等学校出身）</p>
<p>１年美容科　橘　桃加さん（富山県立上市高等学校出身）</p>
<p>１年美容科　眞田　裕里さん（富山県立富山商業高等学校出身）</p>
<p>１年美容科　岩瀧　晴香さん（富山県立南砺福光高等学校出身）</p>
<p>１年エステティック科　砂　ひとみさん（富山県立伏木高等学校出身）</p>
<p>１年エステティック科　鍋山　梨菜さん</p>
<p>&nbsp;</p>
<p>本当におめでとうございました！これからの活躍も期待します！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-33.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-35.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>