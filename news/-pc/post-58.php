<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>☆七夕かざり☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
☆七夕かざり☆</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.07.03</time>
			<h2>☆七夕かざり☆</h2>
			<div class="news_area">
				もうすぐ七夕ですね！！<div>今年も正面玄関にみんなで七夕を飾りました。<div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14070113.DSC05083.JPG"><img alt="" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14070113.DSC05083-thumb-300x225-1806.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>それぞれ短冊にいろんな願い事を書いて・・・</div><div>お星さまにお願いッ　(=m=)。０O</div><div><a href="http://www.toyama-bb.ac.jp/news/14070113.DSC05088.JPG" style="font-size: 1em;"><img alt="14070113.DSC05088.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14070113.DSC05088-thumb-300x225-1810.jpg" width="300" height="225" class="mt-image-none" /></a></div><div><br /></div><div>楽しい時間に、ニンマリ大満足な様子♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14070113.DSC05080.JPG"><img alt="14070113.DSC05080.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14070113.DSC05080-thumb-300x225-1808.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>みんなの願いが叶いますように・・・☆</div><div>そして、晴れますように。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC05250.JPG"><img alt="DSC05250.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC05250-thumb-300x400-1812.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div><br /></div><div>生徒達の間では、本校の七夕は願いが叶うという・・・<font style="font-size: 1.953125em;"><b>う・わ・さ！！</b></font></div><div><br /></div><div>そこで・・・・</div><div><br /></div><div><font style="font-size: 1.953125em;">7/13　オープンキャンパス<u>七夕企画です！！</u></font></div><div><br /></div><div>この<font style="font-size: 1.5625em;"><b>願いが叶う七夕に短冊を飾りませんか？！</b></font></div><div><br /></div><div><br /></div><div>ニューヨークからトップスタイリストも来校し、ヘアショー開催と内容が盛りだくさんです。</div><div><br /></div><div>ぜひご参加ください(^○^)一緒に願いを叶えましょう♪</div><div><br /></div><div><br /></div><div>オープンキャンパスへの参加は→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.5625000000000002em;"><b>こちらをクリック☆</b></font></a></div><div><br /></div></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-57.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/cat/post-59.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>