<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>7月7日オープンキャンパス開催しますよー！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
7月7日オープンキャンパス開催…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.07.03</time>
			<h2>7月7日オープンキャンパス開催しますよー！</h2>
			<div class="news_area">
				<p><font style="FONT-SIZE: 1.25em">みなさん、こんにちは～！</font></p>
<p><font style="FONT-SIZE: 1.25em">今日は朝から強風ですね。みんなが家に帰るまで、どうか電車、止まらないで・・・・。</font></p>
<p><font style="FONT-SIZE: 1.25em">&nbsp;</font></p>
<p><font style="FONT-SIZE: 1.25em">7月7日10：00～　県理美オープンキャンパスです。</font></p>
<p><font style="FONT-SIZE: 1.25em">&nbsp;</font></p>
<p><font style="FONT-SIZE: 1.25em"><font style="FONT-SIZE: 1.53em">七夕</font>です。　なんとなく、　<font style="FONT-SIZE: 1.25em"><strong>ウキウキドキドキ❤</strong></font></font></p>
<p><font style="FONT-SIZE: 1.25em">&nbsp;</font></p>
<p><font style="FONT-SIZE: 1.25em">体験は、メンズコーンロウ、カット、エステ（脚ツルツル版）、チーク作成から１つ選んでください。</font></p>
<p><font style="FONT-SIZE: 1.25em">エステとチーク作成は、もうすぐ定員です。お早めにお申込みください。</font></p>
<p><font style="FONT-SIZE: 1.25em">&nbsp;</font></p>
<p><font style="FONT-SIZE: 1.25em">体験後は、<font style="FONT-SIZE: 1.56em"><strong>県理美夏祭り</strong></font>です。</font></p>
<p><font style="FONT-SIZE: 1.25em">理容科は、「<font style="FONT-SIZE: 1em">えりｏｒ眉シェービング</font>」</font></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"><strong><font style="FONT-SIZE: 1.25em">夏女子の皆様</font><font style="FONT-SIZE: 1.25em">！</font></strong>アップスタイルをすっきりキメたいですよね～。</font></p>
<p><font style="FONT-SIZE: 1.25em">な・の・で、うなじをきれ～にシェービングしませんか？</font></p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0011-2.JPG"><img class="mt-image-none" alt="IMG_0011-2.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0011-2-thumb-336x392-1114.jpg" width="336" height="392" /></a></span></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">そして、<strong><font style="FONT-SIZE: 1.25em">夏男子の皆様！</font></strong></font></p>
<p><font style="FONT-SIZE: 1.25em">キリッとした眉は、海でも山でもプールでも部活でも、夏は必須です！</font></p>
<p><font style="FONT-SIZE: 1.25em">この機会にぜひ、モテ眉になっちゃってください！</font></p>
<p><font style="FONT-SIZE: 1.25em">&nbsp;</font></p>
<p><font style="FONT-SIZE: 1.25em">美容科は、いろいろなスタイリングができる体験ブースです。</font></p>
<p><font style="FONT-SIZE: 1.56em"><strong>自分やお友達のヘアアレンジにバンバン使えます。</strong></font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/013-1.JPG"><img class="mt-image-none" alt="013-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/013-1-thumb-448x244-1116.jpg" width="448" height="244" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">エステティック科は、エステ機器の体験です。</font></p>
<p><font style="FONT-SIZE: 1.25em"><strong><font style="FONT-SIZE: 1.25em">「余分な脂肪サヨウナラ～」</font></strong>な～んていう実力派機器をはじめ、いろいろおもしろい機器あるんですよ～。</font></p>
<p><font style="FONT-SIZE: 1.25em">今回は、プチ体験なので、服の上からでも大丈夫です。ご安心を！</font></p>
<p>&nbsp;</p>
<p></p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0646-1.JPG"><img class="mt-image-none" alt="IMG_0646-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0646-1-thumb-310x448-1118.jpg" width="310" height="448" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">ネイルは、&nbsp;自分の<strong><font style="FONT-SIZE: 1.25em">爪をかわいくカスタマイズ❤</font></strong>しちゃいましょう！（シールなので、自分ですぐにはずせます。翌日の学校は、はずして行ってね～。）</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">このほか、県理美校内にひそんでいる<strong><font style="FONT-SIZE: 1.25em">信長様</font></strong>と<strong><font style="FONT-SIZE: 1.25em">チョキ</font><font style="FONT-SIZE: 1.25em">ちゃん</font></strong>を探す企画を同時開催！</font></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">校内のどこかにいます。</font></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">ホントです。</font></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">みたら、たぶん、わかるはず。</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.56em"><strong>「あ、信長・・・!!!」</strong></font></p>
<p><font style="FONT-SIZE: 1.56em"><strong>「きゃー！チョキちゃーん＼(^o^)／」</strong></font></p>
<p><font style="FONT-SIZE: 1.25em">という。</font></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">探してご本人からスタンプをもらってください。</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">そして、そして、この日は、なんといっても<font style="FONT-SIZE: 1.56em"><strong>七夕</strong></font>なんです。（しつこいですか？）</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"></font></p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0551-1.jpg"><font style="FONT-SIZE: 1.25em"><img class="mt-image-none" alt="IMG_0551-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0551-1-thumb-413x336-1120.jpg" width="413" height="336" /></font></a></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">正面玄関に巨大笹を設置します。</font></p>
<p><font style="FONT-SIZE: 1.25em">みなさんの願いをたんざくに書いてください！</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">星に願いを～！！！！！！</font></p>
<p><font style="FONT-SIZE: 1.25em">願いよ、届けーーーーーーっっっっっっ！！！！！！！！！！！&nbsp;</font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">お申込みはこちらから</font></p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0707.html"><font style="FONT-SIZE: 1.25em">http://toyama-bb.ac.jp/opencampus/form/index0707.html</font></a></p>
<p><font style="FONT-SIZE: 1.25em"></font></p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/bbt.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-25.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>