<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>全国大会３位！と優秀賞！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
全国大会３位！と優秀賞！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2011.12.15</time>
			<h2>全国大会３位！と優秀賞！</h2>
			<div class="news_area">
				<p>11月<font style="FONT-SIZE: 1em">20</font>日に愛知県体育館で開催された第3回日本理容美容学生技術大会　ミディアムカット（国家試験課題）において、</p>
<p>長谷川　洋さんが<font style="FONT-SIZE: 1.25em"><strong>銅賞（第3位）</strong></font>を獲得しました！！同じ競技で、前川建斗さんが<font style="FONT-SIZE: 1.25em">優秀賞</font>を獲得しました！</p>
<p>&nbsp;</p>
<p>長谷川さんと前川さんにインタビューしました！</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/CIMG0494.JPG"></a>&nbsp;</p>
<p>長谷川　洋さん（上市高等学校　出身）&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/P1010899.JPG"></a></p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/CIMG0504.JPG"><img class="mt-image-none" alt="CIMG0504.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2011/12/CIMG0504-thumb-200x158-239.jpg" width="200" height="158" /></a> 
<p>&nbsp; 
<p>☆大会に向けての心境はどうでしたか？ 
<p><strong>緊張して緊張して気持ちがいっぱいいっぱいでした。ちなみに競技中は記憶がありません！</strong> 
<p>☆本番での作品の出来はどうでしたか？ 
<p><strong>思っていたよりできましたが、後でよく見ると「まだまだだな～、おれ頑張れ！」な作品でした。</strong> 
<p>☆表彰式で自分の名前が呼ばれた時の気持ちは？ 
<p><strong>とっても嬉しかったです。今まで頑張ったことが報われた気がしました。たくさんの方々にいろんなことを教えていただき感謝しています。ありがとうございました!!</strong> 
<p>&nbsp;</p>
<p>前川健斗さん（桜井高校出身）</p>
<p>&nbsp;</p>
<p>&nbsp;<a href="http://www.toyama-bb.ac.jp/news/img/CIMG0494.JPG"><img class="mt-image-none" alt="CIMG0494.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2011/12/CIMG0494-thumb-200x150-237.jpg" width="200" height="150" /></a></p>
<p>&nbsp;</p>
<p>☆大会に向けての心境はどうでしたか？</p>
<p><strong>北陸三県大会で結果を残せなかったので、すごく焦っていました。競技では、いい作品をつくってやろうぐらいで、あまり考え事はしていません。</strong></p>
<p>☆本番での作品の出来はどうでしたか？</p>
<p><strong>自分の納得いく作品には仕上がりませんでした。</strong></p>
<p>☆表彰式で自分の名前が呼ばれた時の気持ちは？</p>
<p><strong>目標は去年の松井さん越えでしたが、とりあえず入賞できてよかったです。（昨年松井さんは沖縄で行われた全国大会の同じ種目で優秀賞を獲得しています）</strong></p>
<p><strong>大会までぼくを支えてくれた皆さんに本当に感謝しています。ありがとうございます！！</strong></p>
<p><strong></strong>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/CIMG0330.JPG"><img class="mt-image-none" alt="CIMG0330.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2011/12/CIMG0330-thumb-200x150-241.jpg" width="200" height="150" /></a></span></p>
<p>&nbsp;</p>
<p>お疲れさまでした！おめでとう！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/115.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/cat/post-3.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>