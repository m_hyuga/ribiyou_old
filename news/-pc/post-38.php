<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>先輩から後輩へ・・・ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
先輩から後輩へ・・・</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.01.25</time>
			<h2>先輩から後輩へ・・・</h2>
			<div class="news_area">
				みなさまこんにちは！<br />寒い毎日が続いていますね。インフルエンザも流行り始めているようで、体調の管理には十分お気を付けください☆<br /><br />先日オープンキャンパススタッフの新旧バトンタッチが行われました！<br /><br />母校をもっと素晴らしい学校にして欲しい、リーダーシップのとれる人材になって欲しい、など色々な思いを込めて先輩から後輩へ・・・県理美を愛する熱い思いが受け継がれました！<br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01668-thumb-500x666-1654.jpg"><img alt="DSC01668.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01668-thumb-500x666-1654-thumb-200x266-1655.jpg" class="mt-image-none" style="" height="266" width="200" /></a></span><br /><br />新制オープンキャンパススタッフに乞うご期待です☆<br />この新体制、体験せずにはいられない！ぜひ、オープンキャンパスにご参加下さい！！<br /><br />次回、開催のオープンキャンパスは<b><font style="font-size: 1.95312em;">３月１５日</font></b>です☆<br />　<br /><br /><br /><br /><br /><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-36.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-37.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>