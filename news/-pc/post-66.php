<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>外部実践実習に行ってきました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
外部実践実習に行ってきました☆</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.09.03</time>
			<h2>外部実践実習に行ってきました☆</h2>
			<div class="news_area">
				トータルビューティ科が外部実践実習に行ってきました☆<div><br /></div><div>外部実践実習とは・・・？</div><div><br /></div><div>ネイルやメイク、エステティックなど授業で習得した技術を、学外の施設で施術する授業の</div><div><br /></div><div>一環のことです。トータルビューティ科のオリジナルカリキュラムなんですよ。</div><div><br /></div><div><br /></div><div><br /></div><div>今日はネイルをさせていただきました(^○^)</div><div><br /></div><div>爪の形を整えるファイリング・爪の表面をピカピカにつやを出すバッフィング・</div><div><br /></div><div>手や指に潤いを与えるハンドマッサージの技術をさせていただき、とっても良い経験になりました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="1.JPG" src="http://www.toyama-bb.ac.jp/news/1.JPG" width="326" height="245" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">会話も弾んで、とても良いひと時を過ごして頂き、お客様にも好評でした。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">お客様に喜んでいただき、とても嬉しかったようです！！</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="2.JPG" src="http://www.toyama-bb.ac.jp/news/2.JPG" width="245" height="326" class="mt-image-none" /></span></div><div>自らの新たな課題を見つけることもでき、将来の進路へと着実につながる授業でした。</div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-65.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/cat/96ao.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>