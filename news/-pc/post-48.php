<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>☆新入生☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
☆新入生☆</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.04.14</time>
			<h2>☆新入生☆</h2>
			<div class="news_area">
				<div>４月９日に入学式が行われ、新しいスーツに身を包んだ新入生が入学しました☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03008.JPG"><img alt="DSC03008.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03008-thumb-300x225-1705.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>緊張した空気の中お祝いのお言葉をいただき、身が引き締まる思いと</div><div>これから始まる新しい毎日にわくわくしていました(*^_^*)</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03041.JPG"><img alt="" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03041-thumb-300x225-1707.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>６５期生生徒宣誓をした、柿谷奈緒さんです。</div><div><br /></div><div><br /></div><div>そして、入学式の次の日からオリエンテーションが始まりました♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H26%20018.JPG"><img alt="" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/H26 018-thumb-300x225-1709.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>みんなでグループになって自己紹介をしたり・・・</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H26%20049.JPG"><img alt="H26 049.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/H26 049-thumb-300x225-1713.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>エアロビクスをしたり・・・</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H26%20051.JPG"><img alt="H26 051.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/H26 051-thumb-300x225-1715.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>AEDの使い方を勉強したり・・・</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H26%20091.JPG"><img alt="H26 091.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/H26 091-thumb-300x225-1717.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>グループで協力して、薪でご飯を炊いたり、カレーを作ったりしました！</div><div>なかなか火が付かず、カレーが作れない班もありましたがグループの</div><div>枠をこえて助け合い、なんとかお昼ご飯を食べることができました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H26%20106.JPG"><img alt="H26 106.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/H26 106-thumb-300x225-1719.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>３日間で身に付けたことを活かしてこれから起こる、楽しい時・</div><div>嬉しい時・辛い時・逃げ出したい時もみんなで力を合わせて</div><div>乗り越えていって欲しいと思います☆</div><div><br /></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/cat/post-47.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/cat/1-1.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>