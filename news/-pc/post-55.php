<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.22</time>
			<h2>オープンキャンパス開催しました！！</h2>
			<div class="news_area">
				本日、お天気がすぐれない中、たくさんの高校生の皆様にご参加いただきました☆<div><br /></div><div><br /></div><div>皆さんに体験していただいた内容は・・・</div><div><br /></div><div><font style="font-size: 1.25em;"><b>理容科：　パーマのかけ方、覚えてみよう！Let's カーリーhair!!</b></font></div><div><font style="font-size: 1.25em;"><b><br /></b></font></div><div><font style="font-size: 1.25em;"><b>美容科：　髪が泣いている！？紫外線から髪を守ろう！</b></font></div><div><font style="font-size: 1.25em;"><b><br /></b></font></div><div><font style="font-size: 1.25em;"><b>エステティック科：　本格ボディマッサージ体験☆</b></font></div><div><font style="font-size: 1.25em;"><b><br /></b></font></div><div><font style="font-size: 1.25em;"><b>トータルビューティ科：　チョイ足しメイクでとびっきりかわいく変身！</b></font></div><div><font style="font-size: 1em;"><br /></font></div><div>どの体験も非常に楽しんでいただけたようで、体験後の在校生との交流会でも学校生活のことや、将来の夢について熱く語っておられました！！</div><div><br /></div><div>そして・・・次回のオープンキャンパスはかなり<font style="font-size: 1.953125em;">大注目です</font>・・・</div><div><br /></div><div><font style="font-size: 1.953125em;">なんと！次回はこれ！！！！！！</font></div><div><font style="font-size: 1.953125em;"><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04713.JPG"><img alt="DSC04713.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04713-thumb-500x666-1792.jpg" width="500" height="666" class="mt-image-none" /></a></span></font></div><div><br /></div><div>ニューヨークより、先生をお迎えしてセミナーを開催いたします☆</div><div><br /></div><div>めったに見られないチャンスなので、ぜひぜひ！見に来てください！！</div><div><br /></div><div>そして、本校でニューヨークを目指しましょう！！！！</div><div><br /></div><div><br /></div><div><br /></div><div>オープンキャンパスのお申し込みは→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.953125em;"><b>こちらをクリック☆</b></font></a></div><div><br /></div><div><b><br /></b></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-54.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-56.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>