<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>1学年がＳＢＳメイクディレクター２級を受験☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
1学年がＳＢＳメイクディレクタ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.03.12</time>
			<h2>1学年がＳＢＳメイクディレクター２級を受験☆</h2>
			<div class="news_area">
				1学年のみんながＳＢＳメイクディレクター2級を受験しました！！<div><br /></div><div>試験当日までたくさん練習を積み重ね、本番ではみんな一生懸命に</div><div><br /></div><div>取り組んでいました。<br /><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/IMG_4016.jpg"><img alt="IMG_4016.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/IMG_4016-thumb-350x233-1676.jpg" width="350" height="233" class="mt-image-none" /></a></span></div><div>ＳＢＳとは、本来なら美容師免許を取得してから受験し、実技・筆記試験の</div><div><br /></div><div>両方をクリアしないと合格できないハードルの高い資格なのです。</div><div><br /></div><div>合格すれば、就職してからすぐにお客様に技術ができる素敵な資格です。</div><div><br /></div><div>そして！ＳＢＳの資格を本校では在学中に受験することができます！！</div><div><br /></div><div>なぜなら・・・本校が北陸で唯一のＳＢＳ認定校だからです(^O^)</div><div><br /></div><div><br /></div><div>将来美容師を目指すあなた！！</div><div><br /></div><div>めざせ！ＳＢＳディレクター(^o^)丿</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-43.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/o.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>