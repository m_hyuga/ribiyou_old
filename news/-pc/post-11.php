<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>中島友康先生のデモンストレーションを動画で公開！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
中島友康先生のデモンストレーシ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.06.13</time>
			<h2>中島友康先生のデモンストレーションを動画で公開！</h2>
			<div class="news_area">
				<div><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">6月に実施した、</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">中島友康先生のセミナーをダイジェスト版で公開</span><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">！</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">【中島友康先生プロフィール】</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ＰＥＥＫ-Ａ-ＢＯＯに１３年間勤務め、</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">トップスタイリストとな</span><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">った後、ＮＹに渡る。&nbsp;</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">Ｉｓｈｉｓａｌｏｎなどを経て、現在はＮＹ・</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ミートパッキング地</span><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">区のサロンＳａｌｌｙ Ｈｅｒｓｈｂｅｒｇｅｒ Ｄｏｗｎｔｏｗｎ</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">に勤めるかたわら、ＹＵＩＳＡＬＯＮを共同経営、またヘア・</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">アー</span><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ティストとしても活躍。</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">シンガーＪｏａｎＪｅｔｔのロックスタイルは彼の手によるもの。</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">また、Ｅｌｌｅ、Ｖａｎｉｔｙ Ｆａｉｒ、Ｃｏｓｍｏｐｏｌｉｔａｎなどの</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">エディトリアル撮影では、</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">シックでセクシーなスタイリングを作る</span><wbr style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ことで知られる。</span><u style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "></u><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ＺａｃＰｏｓｅｎ、Ｅｓｃａｄａ、</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); " /><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">ＪｉｌｌＳｔｕａｒｔ、ＢＣＢＧなどのランウェイも手掛ける。</span>
</div><div><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); "><br /></span></div><div><br /></div><br />
<iframe width="560" height="315" src="http://www.youtube.com/embed/_8McTLUn4Co" frameborder="0" allowfullscreen=""></iframe>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-10.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/241.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>