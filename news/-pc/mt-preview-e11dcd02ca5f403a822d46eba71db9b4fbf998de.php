<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>県理美の新しい顔！完成!!</h3>
            <time>2014年03月26日</time>
            <div><br /></div>4月、新学期に向けて新入生をお迎えするために！<div><font style="font-size: 1.5625em;"><br /></font></div><div><font style="font-size: 1.5625em;">なんと!!　ヽ(^0^)ﾉ　</font></div><div><br /></div><div>新しい玄関が完成しました!!</div><div>開放感のあるステキな玄関ですよ</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02897.JPG"><img alt="DSC02897.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/DSC02897-thumb-300x225-1697.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>明るい玄関で新入生をお迎えします。</div><div>入学式が楽しみです♥</div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-44.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>県理美の新しい顔！完成!!</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>