<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>２学年　就職ガイダンス　開催！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
２学年　就職ガイダンス　開催！…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.09</time>
			<h2>２学年　就職ガイダンス　開催！！</h2>
			<div class="news_area">
				本日、本校にて２学年対象の就職ガイダンスが開催されました☆<div><br /></div><div>県内組合店さまよりたくさんのご参加があり、生徒は将来のことや自分の未来像を描きながら</div><div><br /></div><div>一生懸命に話を聞いていました。普段は私服登校なので、スーツ姿が新鮮でした♪</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14060910.DSC04290.JPG"><img alt="14060910.DSC04290.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/14060910.DSC04290-thumb-300x225-1785.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14060910.DSC04301.JPG"><img alt="14060910.DSC04301.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/14060910.DSC04301-thumb-300x225-1787.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>本校の就職面接サポートはこういった就職ガイダンスはもちろんですが、それ以外にも</div><div><br /></div><div>何回もの面接練習や履歴書記入指導などとってもたくさんの項目について細かく</div><div><br /></div><div>指導を行います。本校就職率１００％の秘密はここにあり！！</div><div><br /></div><div>たくさんのサポートがあるから、面接でも落ち着いて受け答えができたり、</div><div><br /></div><div>自分の想いをうまく伝えられたり・・・。</div><div><br /></div><div>今年も就職率１００％、必ず達成します！！</div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-52.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-54.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>