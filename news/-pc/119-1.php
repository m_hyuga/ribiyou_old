<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>11月9日オープンキャンパス開催★ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
11月9日オープンキャンパス開…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.11.05</time>
			<h2>11月9日オープンキャンパス開催★</h2>
			<div class="news_area">
				<h3>こんにちは！</h3>
<p>富山県理容美容専門学校です。</p>
<p>&nbsp;</p>
<p>11月9日（土）にオープンキャンパスを開催します。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>今回の体験は、</p>
<p><strong>理容科（バリカンアート）：アートの世界へ！ラインアートでLet's創作！</strong></p>
<p>あなたの自由な発想ですてきなアートを仕上げちゃおう（*^_^*）</p>
<p><br class="empty" nodeindex="3" />&nbsp;</p>
<p><strong>美容科（エクステ）：オシャレカラー☆エクステでプチ変身！<br class="empty" nodeindex="5" /></strong>編み込みで取り付けるから、自分で取れます。週明けの学校も大丈夫だよ(*^^)v</p>
<p>&nbsp;</p>
<p><strong>エステティック科（G5＋マッサージ(脚)）　：つぶして流してスラッとボディ！</strong></p>
<p>気になる脚のセルライト、この際つぶしてサヨナラしましょう！！！！！</p>
<p>&nbsp;</p>
<p><strong>トータルビューティ科（ハンドマッサージ）：癒しのアロマトリートメント</strong></p>
<p>お好きなアロマの香りで、ハンドマッサージ体験！！</p>
<p>&nbsp;</p>
<p>体験後はキャンパスツアー！！</p>
<p>施設で見ておきたいところ、是非見てください！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>お待ちしております❤</p>
<p>&nbsp;</p>
<p>＜オープンキャンパス＞</p>
<p>１１月９日（土）　１０：００～（受付　９：３０～）</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index1109.html">http://toyama-bb.ac.jp/opencampus/form/index1109.html</a></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-32.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-33.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>