<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.05.24</time>
			<h2>オープンキャンパス開催しました！！</h2>
			<div class="news_area">
				本日、１４：００より本校にてオープンキャンパスを行いました♪<div><br /></div><div>ワクワク体験内容は・・・</div><div><br /></div><div><font style="font-size: 1.5625em;">理容科</font>　　　　　　　　　　<font style="font-size: 1.25em;">メイクとエステとシェービングの素敵な関係☆</font></div><div><br /></div><div><font style="font-size: 1.5625em;">美容科</font>　　　　　　　　　　<font style="font-size: 1.25em;">ナニこれ簡単！！いつでもイメチェンヘアチョーク</font></div><div><br /></div><div><font style="font-size: 1.5625em;">エステティック科　</font>　　　<font style="font-size: 1.25em;">つやつやスベスベしっとりハンドケア</font></div><div><br /></div><div><font style="font-size: 1.5625em;">トータルビューティ科</font>　　　<font style="font-size: 1.25em;">キュートな巻き髪スタイルをつくっちゃおう！！</font></div><div><br /></div><div>でした。本日もたくさんの生徒さんが参加してくださいました。</div><div>今日は特別に東京より講師の先生をお迎えして、いろんな夢に向かう</div><div>お話をしていただきました☆</div><div>将来の自分像に重ねたり、憧れのまなざしで真剣に聞いているところです・・・</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03884.JPG"><img alt="DSC03884.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03884-thumb-300x225-1743.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>そしてその後、楽しい体験で在校生との交流を深め・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03895.JPG"><img alt="DSC03895.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03895-thumb-300x225-1745.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>学校のことについて、ワクワクな話をしたり・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03910.JPG"><img alt="DSC03910.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03910-thumb-300x225-1747.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>人をきれいにする仕事・カッコよくする仕事についての疑問や不安な点も少しは</div><div>解消できたかと思います。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03903.JPG"><img alt="DSC03903.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC03903-thumb-300x225-1749.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><font style="font-size: 1.25em;"><b>これからもたくさんの学生さんに本校の良さを知っていただきたい！！！</b></font></div><div>なので、これからもどんどん楽しくワクワクするような体験や、企画、そして・・・</div><div>県理美でしか味わえないような素晴らしいオープンキャンパスを開催していきますので、</div><div>ぜひお友達や、ご家族と一緒に来てくださいね(*^_^*)</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC039151.JPG"><img alt="DSC039151.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/05/DSC039151-thumb-300x138-1753.jpg" width="300" height="138" class="mt-image-none" /></a></span></div><div><br /></div><div><font style="font-size: 0.6400000000000001em;">オープンキャンパスの申し込みは</font><span style="font-size: 1.953125em;">→</span><a href="http://www.toyama-bb.ac.jp/opencampus/" style="font-size: 1.953125em;">こちら☆</a></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-49.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-51.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>