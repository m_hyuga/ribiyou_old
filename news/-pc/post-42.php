<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス追加　　☆お知らせ☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス追加　　☆お…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.03.11</time>
			<h2>オープンキャンパス追加　　☆お知らせ☆</h2>
			<div class="news_area">
				皆さま、こんにちは！<div><br /></div><div>先日お知らせいたしました、オープンキャンパスについて</div><div><br /></div><div>新たに・・・うれしい新着情報です♪</div><div><br /></div><div>今回のオープンキャンパスでは</div><div><br /></div><div>日本政策金融公庫様が学費ローンの相談のため、</div><div><br /></div><div>特別ブースを開設されます。</div><div><br /></div><div>学費ローンについて、よくわからないことはこの機会に</div><div><br /></div><div>スッキリ！問題解決しましょう(*^_^*)</div><div><br /></div><div><br /></div><div><p class="MsoNormal"><span style="font-size:14.0pt;mso-bidi-font-size:11.0pt;
font-family:&quot;ＭＳ 明朝&quot;,&quot;serif&quot;;mso-ascii-font-family:Century;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:&quot;ＭＳ 明朝&quot;;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Century;mso-hansi-theme-font:minor-latin"><b>３／１５　　１０：００～（受付９：３０～）</b></span></p><p class="MsoNormal"><span style="font-size:14.0pt;mso-bidi-font-size:11.0pt;
font-family:&quot;ＭＳ 明朝&quot;,&quot;serif&quot;;mso-ascii-font-family:Century;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:&quot;ＭＳ 明朝&quot;;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Century;mso-hansi-theme-font:minor-latin"><b>　　　　　　　　　　　　　　からスタートです！！</b></span><span lang="EN-US" style="font-size:14.0pt;mso-bidi-font-size:11.0pt"><o:p></o:p></span></p>

<p class="MsoNormal"><span style="font-family:&quot;ＭＳ 明朝&quot;,&quot;serif&quot;;mso-ascii-font-family:
Century;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:&quot;ＭＳ 明朝&quot;;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Century;mso-hansi-theme-font:
minor-latin">まだまだオープンキャンパスへの参加を受け付けています♪</span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span><span style="font-family: 'ＭＳ 明朝', serif; font-size: 1em;">参加申し込みはこちらへ→</span><a href="http://http://toyama-bb.ac.jp/opencampus/form/index0327.html">☆参加申し込み☆</a></p></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-41.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-43.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>