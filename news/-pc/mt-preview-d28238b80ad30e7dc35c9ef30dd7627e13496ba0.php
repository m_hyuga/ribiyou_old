<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>youtube　～あの感動をふたたび～</h3>
            <time>2014年02月18日</time>
            <p><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">皆様　ｺﾝﾆﾁﾜ</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">―((*´</span><span style="FONT-FAMILY: 'ＭＳ ゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'ＭＳ ゴシック'">▽</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">｀</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">o)o</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">゛</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">―♪ <o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><font face="ＭＳ Ｐゴシック"><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">お昼間の太陽が気持ちいいです。</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></font></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">さてさて　</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">2013</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">年</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">11</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">月</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">17</span><font face="ＭＳ Ｐゴシック"><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">日（日）に神戸市で開催された全国理容美容学生技術大会。</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></font></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><font face="ＭＳ Ｐゴシック"><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">優勝『日本一』をはじめ　入賞者の新聞記事やテレビ放送も</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></font></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">まだまだ記憶に新しいですが・・・</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">(</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">思い出したらまだまだ胸が</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt"> </span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">じぃぃぃぃぃぃんとしちゃいます</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">(●´</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">艸｀</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">)</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">ヾ</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"> )<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><font face="ＭＳ Ｐゴシック"><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">激戦を勝ち抜いて全国大会に出場した選手たち</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></font></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><font face="ＭＳ Ｐゴシック"><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">結果が出た選手も、残念ながら出なかった選手も　皆皆　立派でした</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></font></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">ということで　　なんと　</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">DVD</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">を作成しちゃったんです</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">(</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">｡</font></span><span style="FONT-FAMILY: 'Cambria Math', 'serif'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'Cambria Math'" lang="EN-US">≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'ＭＳ ゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'ＭＳ ゴシック'">‿≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">)<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">そして　その</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">DVD</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">が結構な評判でして</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">(</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">｡</font></span><span style="FONT-FAMILY: 'Cambria Math', 'serif'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'Cambria Math'" lang="EN-US">≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'ＭＳ ゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'ＭＳ ゴシック'">‿≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">) (</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">｡</font></span><span style="FONT-FAMILY: 'Cambria Math', 'serif'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'Cambria Math'" lang="EN-US">≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'ＭＳ ゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'ＭＳ ゴシック'">‿≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">)(</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">｡</font></span><span style="FONT-FAMILY: 'Cambria Math', 'serif'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'Cambria Math'" lang="EN-US">≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'ＭＳ ゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-bidi-font-family: 'ＭＳ ゴシック'">‿≖</span><span style="FONT-FAMILY: 'Tahoma', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">ิ</span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">)</span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">すごく感動するし　　　皆さまにも　ぜひ　みてもらいたいなぁ・・・</span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><strong><span style="FONT-FAMILY: 'ＭＳ Ｐゴシック'; COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">ん？</span></strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><strong><span style="FONT-FAMILY: 'ＭＳ Ｐゴシック'; COLOR: #333333; FONT-SIZE: 12.5pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">じゃあ</span></strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 15.5pt" lang="EN-US">youtube</span></strong><strong><span style="FONT-FAMILY: 'ＭＳ Ｐゴシック'; COLOR: #333333; FONT-SIZE: 15.5pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">に</span></strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><strong><span style="FONT-FAMILY: 'ＭＳ Ｐゴシック'; COLOR: #333333; FONT-SIZE: 15.5pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">アップしちゃおう</span></strong><strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 15.5pt" lang="EN-US">♪♪</span></strong><strong><span style="FONT-FAMILY: 'ＭＳ Ｐゴシック'; COLOR: #333333; FONT-SIZE: 15.5pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial">　</span></strong><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">&nbsp;<o:p></o:p></span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">↓↓↓</span><span style="COLOR: #333333; FONT-SIZE: 10pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial"><font face="ＭＳ Ｐゴシック">こちら</font></span><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US">↓↓↓</span></p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><span style="FONT-FAMILY: 'Century', 'serif'; FONT-SIZE: 10.5pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-font-size: 11.0pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'ＭＳ 明朝'; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA" lang="EN-US"></span></span><a href="https://www.youtube.com/watch?v=wcJKfcaqTuc&amp;feature=youtu.be">第5回全国理容美容学生技術大会</a><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><span style="FONT-FAMILY: 'Century', 'serif'; FONT-SIZE: 10.5pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-font-size: 11.0pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'ＭＳ 明朝'; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA" lang="EN-US"><font style="FONT-SIZE: 0.8em"></font></span></span>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="FONT-FAMILY: 'Arial', 'sans-serif'; COLOR: #333333; FONT-SIZE: 10pt" lang="EN-US"><span style="FONT-FAMILY: 'Century', 'serif'; FONT-SIZE: 10.5pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-font-size: 11.0pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'ＭＳ 明朝'; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA" lang="EN-US"><font style="FONT-SIZE: 0.8em">心動かしたいかた</font></span></span></p>
<p>感動したいかた</p>
<p>心あつくなりたいかた</p>
<p>おすすめです</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-40.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-40.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>youtube　～あの感動をふたたび～</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>