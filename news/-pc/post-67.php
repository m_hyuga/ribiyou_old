<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>指定校推薦入試　推薦入試　自己推薦入試　出願スタート | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
指定校推薦入試　推薦入試　自己…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.10.01</time>
			<h2>指定校推薦入試　推薦入試　自己推薦入試　出願スタート</h2>
			<div class="news_area">
				<p>指定校推薦入試　推薦入試　自己推薦入試の出願を受け付けます。</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆指定校推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成26年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　　書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　　平成26年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成26年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　 面接、書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">試験日　　　　　平成26年10月20日</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　 平成26年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆自己推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成26年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　 面接、書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">試験日　　　　　平成26年10月20日</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　 平成26年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">詳しくは</font><a href="http://www.toyama-bb.ac.jp/admission/daytime.php"><font style="FONT-SIZE: 1.25em">募集要項</font></a><font style="FONT-SIZE: 1.25em">をご覧ください。</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">パンフレット・募集要項のご請求は</font><a href="http://www.toyama-bb.ac.jp/inquiry/"><font style="FONT-SIZE: 1.25em">こちら</font></a><font style="FONT-SIZE: 1.25em">から</font></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/920.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-68.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>