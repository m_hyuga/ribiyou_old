<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>通信課程・理容科向けにアレンジした「トータルシェービング」が始まりました。 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
通信課程・理容科向けにアレンジ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.08.22</time>
			<h2>通信課程・理容科向けにアレンジした「トータルシェービング」が始まりました。</h2>
			<div class="news_area">
				<div>全国でも県理美・理容科だけで学べるトータルシェービングが通信課程の理容科でも実</div><div>施されました。シェービング伝道家の吉田昌央先生を迎えての授業は、二日間とはいえ、</div><div>とても充実した内容でした。こんな贅沢なスクーリングがある通信課程は、県理美の理</div><div>容科だけではないでしょうか。</div><div>そのスクーリングの様子をわずかですがご紹介します。</div><div><br /></div><div>吉田昌央先生が「一流巻き」と呼ぶスチームタオルの巻き方を伝授しています。</div><div><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4297-1876.php" onclick="window.open('http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4297-1876.php','popup','width=300,height=225,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false" style="font-size: 1em;"><img src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4297-thumb-72x54-1876.jpg" width="72" height="54" alt="DSCF4297.jpg" class="mt-image-none" /></a></div><div>きめの細かい泡の立て方を学んでいます。これって意外と楽しいんですよ。</div><div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSCF4301.jpg"><img alt="DSCF4301.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4301-thumb-72x54-1879.jpg" width="72" height="54" class="mt-image-none" /></a></span></div><div>さて、シェービングの開始です。レザーの刃は除いてあります。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSCF4312.jpg"><img alt="DSCF4312.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4312-thumb-72x54-1881.jpg" width="72" height="54" class="mt-image-none" /></a></span></div><div>トータルシェービングですから、レディースシェービングに対応したクレンジングも学びます。</div></div><div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSCF4368.jpg"><img alt="DSCF4368.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4368-thumb-72x54-1883.jpg" width="72" height="54" class="mt-image-none" /></a></span></div><div>吉田昌央先生が「一流巻き」と呼ぶスチームタオルの巻き方を伝授しています。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4440-1885.php" onclick="window.open('http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4440-1885.php','popup','width=300,height=225,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4440-thumb-72x54-1885.jpg" width="72" height="54" alt="DSCF4440.jpg" class="mt-image-none" /></a></span></div><div>レザーを扱うのが初めての人もいますが、ていねいな指導で少しずつコツがつかめていきます。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4446-1888.php" onclick="window.open('http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4446-1888.php','popup','width=300,height=225,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSCF4446-thumb-72x54-1888.jpg" width="72" height="54" alt="DSCF4446.jpg" class="mt-image-none" /></a></span></div><div>ほかにもフェイスマッサージやシートパックを使った保湿など、レディースシェービング</div></div><div>に対応した様々な技術を学びました。</div><div>シェービングの技術を習得するにはまだまだ時間が必要ですが、トータルシェービング</div><div>の魅力を一人ひとりが実感できた二日間でした。</div><div>近い将来、ブライダルシェービングやメンズスキンケアシェービングで活躍することを期</div><div>待しています。がんばれ、通信生！</div> 
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-63.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/824.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>