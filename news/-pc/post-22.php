<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>男前講座①＆婚活メイクセミナー② | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
男前講座①＆婚活メイクセミナー…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.01.30</time>
			<h2>男前講座①＆婚活メイクセミナー②</h2>
			<div class="news_area">
				<p>本校理容科非常勤講師である稲垣武臣先生が、南砺市の婚活倶楽部なんとの男性会員の方を対象としたセミナーを行われました。</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0039-1.jpg"><img class="mt-image-none" alt="IMG_0039-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/01/IMG_0039-1-thumb-448x299-857.jpg" width="448" height="299" /></a></p>
<p>
<p>&nbsp;</p>
<p></p>
<p>「男前講座☆婚活力UPセミナー～結婚できる男になるには～」というテーマで、第一印象の大切さ・顔形別の似合う髪型・スキンケア等のお話しをされました。</p>
<p>セミナー出席者の方は、清潔感が一番大切ということや、髪型だけでもずいぶん印象が異なることを感じられたようです。</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0031-1.jpg"><img class="mt-image-none" alt="IMG_0031-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/01/IMG_0031-1-thumb-299x448-859.jpg" width="299" height="448" /></a></p>
<p><br />参加者の方のカットも！！</p>
<p>稲垣先生の男前講座は、1月30日も行われます。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>また、1月23日には、本校教員の澁谷による婚活メイクセミナー2回目が行われました。</p>
<p>&nbsp;</p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0029-1.jpg"><img class="mt-image-none" alt="IMG_0029-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/01/IMG_0029-1-thumb-448x327-861.jpg" width="448" height="327" /></a></p>
<p>&nbsp;</p>
<p>今回は、ポイントメイク編でした。</p>
<p>ナチュラルメイクをベースに、最近の流行も取り入れてお伝えできたかと思います。</p>
<p>ご参加いただき、ありがとうございました。<br /></p>
<p></p>
<p>
<p><font color="#ff0000"></font>&nbsp;</p><font color="#ff0000"></font>
<p></p>
<p><font color="#ff0000">
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0031-1.jpg"></a>&nbsp;</p></font>
<p></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/119.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-23.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>