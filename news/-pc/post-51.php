<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>全国理容美容学生技術大会校内予選大会　開催！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
全国理容美容学生技術大会校内予…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.02</time>
			<h2>全国理容美容学生技術大会校内予選大会　開催！！</h2>
			<div class="news_area">
				全国理容美容学生技術大会の校内予選大会が開催され、２学年が信越北陸大会出場を<div>かけて競い合い、５６名中２３名が代表選手に選ばれました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03964.JPG"><img alt="DSC03964.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03964-thumb-300x225-1755.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>理容　　　ミディアムカット競技</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03972.JPG"><img alt="DSC03972.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03972-thumb-300x225-1757.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><div>理容　　　ワインディング競技</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04087.JPG"><img alt="DSC04087.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04087-thumb-300x225-1761.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>美容　　　ワインディング競技</div><div><br /></div><div><a href="http://www.toyama-bb.ac.jp/news/DSC04089.JPG" style="font-size: 1em;"><img alt="DSC04089.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04089-thumb-300x225-1763.jpg" width="300" height="225" class="mt-image-none" /></a>　　　　　　</div><div>美容　　カット競技</div><div>　　　　　　</div><div>理容科はその他に、チャレンジアートヘア競技、ネイルアート競技、美容科はアップスタイル競技があり</div><div>日頃の成果を発揮していました。１学年も今回の校内予選を見学し、来年自分はどの競技に</div><div>出場しようか自分の未来像と重ね合わせて、目を輝かせて見ていました！</div><div><br /></div></div><div>代表生徒は７月２９日の信越北陸大会で素晴らしい結果を残せるようさらに練習を重ね、ここからは自分との闘いになると思いますが、しっかりと目標を持って自分に負けないように頑張って欲しいものです。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04166.JPG"><img alt="DSC04166.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04166-thumb-300x225-1765.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>歓喜のポーズ☆頑張れ！代表選手！！</div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-50.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/1-2.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>