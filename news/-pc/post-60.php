<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.07.19</time>
			<h2>オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				7月１９日（土）　オープンキャンパスが大盛況に終わりました♪<div><br /></div><div>今日は７月２９日に富山市総合体育館で行われる全国理容美容学生技術大会</div><div><br /></div><div>‐信越北陸地区大会‐に出場する選手が日頃の成果を発表しました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC05970.JPG"><img alt="DSC05970.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC05970-thumb-300x225-1831.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>そしてこの大会にかける意気込みを語っていただきました！</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC05984.JPG"><img alt="DSC05984.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC05984-thumb-300x225-1833.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>大会まであと１０日。ぜひ、頑張ってもらいたいです！！</div><div><br /></div><div><br /></div><div><br /></div><div>また、各科の体験も高校生の皆さんに大好評でした☆</div><div><br /></div><div>理容科のシェービング</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06014.JPG"><img alt="DSC06014.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06014-thumb-300x225-1836.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>美容科の浴衣着付体験</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06018.JPG"><img alt="DSC06018.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06018-thumb-300x225-1838.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>エステティック科のフェイシャルエステ体験</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06002.JPG"><img alt="DSC06002.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06002-thumb-300x225-1840.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>トータルビューティ科のヘアメイク体験</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06009.JPG"><img alt="DSC06009.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06009-thumb-300x225-1842.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>今回は夏休みに使える技術の体験が多く、皆さん喜んでおられました(*^_^*)</div><div><br /></div><div>ぜひ、夏休み中にお家でもチャレンジしていただきたいです。</div><div><br /></div><div><br /></div><div><br /></div><div>そして、次回のオープンキャンパスは</div><div><br /></div><div><font style="font-size: 1.953125em;">８月２日　１３：００～<font style="font-size: 0.5120000000000001em;">（１２：３０受付開始）</font></font></div><div><br /></div><div>詳しくはこちらをご確認ください→<font style="font-size: 1.953125em;"><a href="http://www.toyama-bb.ac.jp/opencampus/">オープンキャンパスHP☆</a></font><a href="http://www.toyama-bb.ac.jp/opencampus/"></a></div><div><br /></div><div><br /></div><div><br /></div><div>進学について相談をしたいけれど、８月２日のオープンキャンパスまで待てない！</div><div><br /></div><div>という方に・・・<font style="font-size: 1.5625em;"><b>進学説明会<font style="font-size: 0.5120000000000001em;">（個別相談）</font></b></font>を開催します！</div><div><br /></div><div><font style="font-size: 1.953125em;">７月２６日（土）　１０：００～<font style="font-size: 0.5120000000000001em;">（９：３０受付開始）</font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;"><br /></font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;">進学について疑問や不安などがありましたら、ぜひご相談ください。</font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;"><br /></font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;"><br /></font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;">オープンキャンパスも進学説明会もお気軽にご参加ください(^○^)</font></font></div><div><font style="font-size: 1.953125em;"><font style="font-size: 0.5120000000000001em;"><br /></font></font></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/cat/post-59.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-61.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>