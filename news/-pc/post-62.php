<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.08.02</time>
			<h2>オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				本日もたくさんの高校生の皆さんが参加してくださいました！<div><br /></div><div>駅から本校まで、この暑い中歩いてくるのは大変なことで・・・</div><div><br /></div><div>来てくださった方には本当に感謝です☆ありがとうございます(*^_^*)</div><div><br /></div><div><br /></div><div>さて、本日の体験は・・・</div><div><br /></div><div>理容科　　ワインディングです☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06440.JPG"><img alt="DSC06440.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06440-thumb-300x225-1854.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>美容科　　エクステンションです☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06438.JPG"><img alt="DSC06438.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06438-thumb-300x225-1856.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>エステティック科　　　角質除去＋バンテージ体験です☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06421.JPG"><img alt="DSC06421.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06421-thumb-300x225-1858.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>トータルビューティ科　　　メイク体験です☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06435.JPG"><img alt="DSC06435.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06435-thumb-300x225-1861.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>夏休みの思い出に楽しい♪楽しい♪♪オープンキャンパスに大満足だったようです。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06437.JPG"><img alt="" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06437-thumb-300x225-1860.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>この夏！さらに楽しい企画が盛りだくさんの富山県理容美容専門学校の</div><div><br /></div><div>オープンキャンパスで忘れられない夏休みにしましょう！！ぜひ来てくださいね！！</div><div><br /></div><div>お待ちしています☆</div><div><br /></div><div><font style="font-size: 1.5625em;"><b>次回、8/6</b></font><b style="font-size: 1.5625em;">のオープンキャンパス体験内容は・・・</b></div><div><br /></div><div>●シェービング体験（理容科）</div><div><br /></div><div>●シャンプー体験（美容科）</div><div><br /></div><div>●足のむくみ取り体験（エステティック科）</div><div><br /></div><div>●ヘアアレンジ体験（トータルビューティ科）</div><div><br /></div><div>参加申し込みはこちら→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.953125em;">クリック☆</font></a></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-61.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-63.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>