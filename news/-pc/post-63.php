<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.08.06</time>
			<h2>オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				8月6日　オープンキャンパスを開催しました(*^_^*)<div><br /></div><div>夏休みまっただ中、たくさんの高校生の皆さんが参加してくださいました♪</div><div><br /></div><div>本日のオープンキャンパスの体験内容は・・・夏だからこそ！！</div><div><br /></div><div><br /></div><div>理容科：シェービングでスベスベ、ワントーン明るい肌に☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06459.JPG"><img alt="DSC06459.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06459-thumb-300x225-1864.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>美容科：ヘアトニックで毛穴すっきり☆シャンプー体験</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06452.JPG"><img alt="DSC06452.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06452-thumb-300x225-1866.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>エステティック科：吸引で足のむくみ撃退！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06461.JPG"><img alt="DSC06461.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06461-thumb-300x225-1870.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>トータルビューティ科：巻き髪ヘアアレンジで夏ヘア体験♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06449.JPG"><img alt="DSC06449.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06449-thumb-300x225-1872.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div><div>夏休みにお家でもできるヘアアレンジテクニックや、夏だからこそ</div><div><br /></div><div>気持ちの良いヘアクレンジング体験、むくみ取り、自分ではなかなかできない</div><div><br /></div><div>シェービングなど・・・今でしょ！！の体験をしていただきました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06466.JPG"><img alt="DSC06466.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06466-thumb-300x225-1874.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>先輩とも仲良くなれて大満足♪とっても良い笑顔(^○^)</div><div><br /></div><div>また次回も高校生の皆さんのたくさんの笑顔が見られるように</div><div><br /></div><div>楽しい企画を準備してお待ちしております！！</div><div><br /></div><div><br /></div><div><br /></div><div>次回オープンキャンパスは・・・</div><div><br /></div><div><font style="font-size: 1.953125em;">8月24日（日）10：00～</font>（9：30～受付）</div><div><br /></div><div><font style="font-size: 1.25em;">理容科　：　<b>シャンプー　</b><font style="font-size: 0.8em;">『夏の毛穴の汚れ、きれいスッキリ流しちゃおう！』</font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">美容科　：　<b>アップスタイル　</b><font style="font-size: 0.8em;">『オシャレヘアこれで君もモテモテに！！』</font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">エステティック科　：　<b>ハンドマッサージ　</b><font style="font-size: 0.8em;">『癒しのフルハンドケア♡』</font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">トータルビューティ科　：　<b>ネイル　</b><font style="font-size: 0.8em;">『デコネイルで楽しんじゃおう☆』</font></font></div><div><br /></div><div>オープンキャンパスの参加申し込みは→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.953125em;">こちらをクリック☆</font></a></div><div><br /></div><div><font style="font-size: 1.5625em;">たくさんのご参加をお待ちしています(^.^)/~~~</font></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-62.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-64.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>