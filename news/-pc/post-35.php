<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>いざ！成人式へ！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
いざ！成人式へ！！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.01.10</time>
			<h2>いざ！成人式へ！！</h2>
			<div class="news_area">
				みなさん、こんにちは！<br />いよいよ今週末はたくさんの地域で成人式が開催されます。街でも晴れ着姿の御嬢様方をたくさん見かけるかもしれませんね。<br /><br />それにしても、なぜだか着物姿って良いですよね。華やかさや、上品さなど洋服にはない魅力があります。そして特に髪型や帯結びなど、後姿が素敵だと思いませんか？！☆<br /><br />そこで！その晴れ着姿をさらに美しくするため、本校理容科の皆さんが今年成人する２学年を対象に、えりあしの産毛を剃る技術<font style="font-size: 1.5625em;">『えりそり』</font>をしてくれました♪♪<br />みんな綺麗になって大喜びでした！<br />新成人のみなさま、素晴らしい成人の日になりますように・・・☆<br /><br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC01530.JPG" src="http://www.toyama-bb.ac.jp/news/DSC01530.JPG" class="mt-image-none" style="" height="336" width="448" /></span><br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC01505.JPG" src="http://www.toyama-bb.ac.jp/news/DSC01505.JPG" class="mt-image-none" style="" height="448" width="336" /></span><br /><br /><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-34.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-36.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>