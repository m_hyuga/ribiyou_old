<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>富山県理容美容専門学校同窓会・学内エステティックサロン[tea forêt]オープニングセレモニー | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
富山県理容美容専門学校同窓会・…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.04.03</time>
			<h2>富山県理容美容専門学校同窓会・学内エステティックサロン[tea forêt]オープニングセレモニー</h2>
			<div class="news_area">
				<p>4月1日、富山県理容美容専門学校同窓会と学内エステティックサロン[tea forêt(ﾃｨｱﾌｫｰﾚ)]オープニングセレモニーを開催しました。</p>
<p>&nbsp;</p>
<p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9613-1.JPG"></a>&nbsp; 
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9613-1.JPG"></a><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9544-1.JPG"><img class="mt-image-none" alt="IMG_9544-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_9544-1-thumb-448x299-899.jpg" width="448" height="299" /></a></p>
<p></p>
<p></p>
<p>ご来賓の方々と本校理事長、校長によるテープカットが行われました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9556-1.JPG"><img class="mt-image-none" alt="IMG_9556-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_9556-1-thumb-448x299-901.jpg" width="448" height="299" /></a></span></p>
<p>引き続き行われた富山県理容美容専門学校同窓会の様子です。</p>
<p>本当にたくさんの方にお越しいただきました。ありがとうございます。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9585-1.JPG"><img class="mt-image-none" alt="IMG_9585-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_9585-1-thumb-448x299-903.jpg" width="448" height="299" /></a></span></p>
<p>&nbsp;</p>
<p>同窓会では、本校卒業生で、非常勤講師でもある中居　由佳先生によるヘアショーが行われました。</p>
<p>中居　由佳先生は全日本美容講師会常任創作委員で、1996年世界理容・美容技術選手権総合優勝していらっしゃる先生なんです！！</p>
<p>とってもステキなヘアショーでした！！</p>
<p>&nbsp;</p>
<p>&nbsp;<a href="http://www.toyama-bb.ac.jp/news/img/IMG_9613-1.JPG"><img class="mt-image-none" alt="IMG_9613-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_9613-1-thumb-448x299-905.jpg" width="448" height="299" /></a></p>
<p>&nbsp;&nbsp;モデルさんです。カワイイ！！！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>エステティックサロン[tea forêt（ﾃｨｱﾌｫｰﾚ）]は、５月６日（月）グランドオープンです。</p>
<p>ご予約は４月１日から受け付けております。</p>
<p></p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_1997-1.JPG"><img style="WIDTH: 197px; HEIGHT: 247px" class="mt-image-none" alt="IMG_1997-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_1997-1-thumb-336x411-907.jpg" width="336" height="411" /></a>&nbsp;　　<a href="http://www.toyama-bb.ac.jp/news/img/IMG_9513-2.JPG"><img style="WIDTH: 298px; HEIGHT: 165px" class="mt-image-none" alt="IMG_9513-2.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_9513-2-thumb-448x288-910.jpg" width="448" height="288" /></a></p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;</span></p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">お待ち申しあげております。</span></p>
<p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;</span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">エステティックサロン[tea forêt（ﾃｨｱﾌｫｰﾚ）]</span></p>
<p></p>
<p></p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">〈営業時間〉　月曜日・金曜日・土曜日　１０:００～１９:００（受付時間～１７:００）</span></p>
<p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">　　　　　　　　 火曜日・木曜日　　　　　　１０:００～２１:００（受付時間～１９:００）</span></p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">〈ご予約〉　　フリーダイヤル　０１２０－０３１－８００</span></p>
<p>
<p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">　　　　　　　<a href="http://www.teaforet.jp">　http://www</a></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.teaforet.jp">.teaforet.jp</a>&nbsp; </span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">オンライン予約できます。</span></p>
<p></p>
<p></p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>&nbsp;</span>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9513-2.JPG"></a></p></span>
<p></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-23.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/421.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>