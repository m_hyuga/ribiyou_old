<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>富山第一高校★おめでとう！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
富山第一高校★おめでとう！！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.01.14</time>
			<h2>富山第一高校★おめでとう！！</h2>
			<div class="news_area">
				<p><font style="FONT-SIZE: 1.95em">　　<strong>富山第一高校の皆さん、</strong></font></p>
<p><font style="FONT-SIZE: 1.95em"><strong>　　　サッカー全国優勝おめでとうございます！！</strong></font></p>
<p>日本一、北陸勢初、すごいですねー。感動しました。</p>
<p>きっとここまでくるには、たくさんの辛い練習や、いろんな挫折を乗り越えてこられたことでしょう。</p>
<p>だからこそ、一番輝いた場所に辿り着けたのでしょうね。</p>
<p>私たちも来月には国家試験を控えています。富一イレブンに負けないくらいの気持ちと</p>
<p>練習量で望みたいと思います！！絶対に全員受かるぞ！！</p>
<p>&nbsp;</p>
<p>ちなみに・・・富山第一高校出身の在校生（↓）も、とっても喜んでいました♪</p>
<p><a href="http://www.toyama-bb.ac.jp/news/DSC01566.JPG"><img style="WIDTH: 624px; HEIGHT: 405px" class="mt-image-none" alt="DSC01566.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01566-thumb-700x525-1646.jpg" height="525" width="700" /></a></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-35.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-38.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>