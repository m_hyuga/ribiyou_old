<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>１２月１５日　オープンキャンパス開催！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
１２月１５日　オープンキャンパ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.12.03</time>
			<h2>１２月１５日　オープンキャンパス開催！</h2>
			<div class="news_area">
				<p><font style="FONT-SIZE: 1.56em">１２月１５日（土）　10:00～12:00</font></p>
<p>&nbsp;</p>
<p>冬のオープンキャンパスを開催します。</p>
<p>体験授業は、シェービング、カット、エステティック、メイクから１つお選びください。</p>
<p>&nbsp;</p>
<p>シェービング：クリスマスだよ！わくわくバルーンパーティー</p>
<p>カット：楽しくおしゃれヘアーを作ろう</p>
<p>エステティック：冬のアロママッサージ</p>
<p>　　　（エステティックは女性に限定させていただきます。ご了承ください。）</p>
<p>メイク：イヤイヤ乾燥肌!!スキンケア～パックでピッカピカ</p>
<p>&nbsp;</p>
<p>上記の体験の後は、自由にまわって自由に体験できる『県理美☆冬祭り』を開催！</p>
<p>エクステ、ハンドスパ、編み込み、メンズセット、ネイルアートを体験できます。</p>
<p>&nbsp;</p>
<p>お待ちしております。</p>
<p>お申込みは<a href="http://www.toyama-bb.ac.jp/opencampus2012/form/index1215.html">こちら</a>から</p>
<p>&nbsp;</p>
<p>お問い合わせ　　０１２０－１９０－１３５</p>
<p>　　　　　　　　　　 <a href="mailto:info@toyama-bb.ac.jp">info@toyama-bb.ac.jp</a></p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_3002-1.JPG"><img class="mt-image-none" alt="IMG_3002-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/12/IMG_3002-1-thumb-448x336-812.jpg" width="448" height="336" /></a></span></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-16.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-20.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>