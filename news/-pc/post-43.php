<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>キラキラ☆卒業生 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
キラキラ☆卒業生</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.03.11</time>
			<h2>キラキラ☆卒業生</h2>
			<div class="news_area">
				平成２６年３月１０日<div><br /></div><div>オークス　カナルパークホテル富山にて卒業式が<div><br /></div><div>行われました。</div><div><br /></div><div>理容科・美容科は2年間、エステティック科・ネイルメイク科は1年間、</div><div><br /></div><div>それぞれの目標に向かって毎日全力で歩んできました☆</div><div><br /></div><div>これからやってくる楽しく・希望に満ち溢れた日々に</div><div><br /></div><div>卒業生の瞳はキラキラと輝いていました(*^_^*)</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H%20052.JPG"><img alt="H 052.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/H 052-thumb-350x262-1666.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div>シャンデリアが輝く会場で、卒業を迎えた後ろ姿も輝いていました☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H%20076.JPG"><img alt="H 076.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/H 076-thumb-350x262-1668.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div>墓越さんの答辞、素晴らしかったです！！感極まり、男泣き。</div><div>会場が感動の渦に巻き込まれました・・・。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H%20088.JPG"><img alt="H 088.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/H 088-thumb-350x262-1670.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div>そして・・・</div><div>今年の<font style="font-size: 1.25em;"><b>優等賞（各科の首席）受賞者</b></font>は</div><div><br /></div><div><font style="font-size: 1.25em;">理容科　　　　　　　　　　　太田　桂子　さん　（右）</font></div><div><font style="font-size: 1.25em;">美容科　　　　　　　　　　　森田　佑奈　さん　（右中）</font></div><div><font style="font-size: 1.25em;">エステティック科　　　　　　砂　ひとみ　さん　（左中）</font></div><div><font style="font-size: 1.25em;">ネイル・メイク科　　　　　　本江　みちる　さん　（左）</font></div><div><br /></div><div><br /></div><div>また、優等賞の中から優れた成績を収めた者に贈られる賞</div><div><font style="font-size: 1.5625em;">学校長賞　　　<font style="font-size: 0.8em;">エステティック科</font>　　　砂　ひとみ　さん</font></div><div><br /></div><div>優等賞の中から最も優れた成績を収めた者に贈られる賞</div><div><font style="font-size: 1.5625em;">理事長賞　　　美容科　　　　　　　森田　佑奈　さん</font></div><div><br /></div><div><br /></div><div>が輝きました☆理事長賞を受賞すると、ティアラが贈呈されます。</div><div><br /></div><div>過去に理事長賞に輝いた先輩方も手にした伝統のティアラの輝きが</div><div><br /></div><div>まぶしいですね。後輩たちの憧れでもあります☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/H%20048.JPG"><img alt="H 048.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/H 048-thumb-300x225-1673.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>卒業生の皆さんの人生が明るく、素晴らしいものになりますように・・・☆</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-42.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/1.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>