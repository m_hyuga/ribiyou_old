<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>レジーナ　フォトコンテスト　準入選 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
レジーナ　フォトコンテスト　準…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.12.03</time>
			<h2>レジーナ　フォトコンテスト　準入選</h2>
			<div class="news_area">
				<p>本校美容科の椎名唯さん（富山北部高校出身）の作品が、レジーナフォトコンテストで準入選に選ばれました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/068-1.jpg"><img class="mt-image-none" alt="068-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/11/068-1-thumb-336x448-800.jpg" width="336" height="448" /></a></span></p>
<p>&nbsp;</p>
<p>＜椎名さんのコメント＞</p>
<p>準入選したと知り、嬉しいと同時に正直驚いています。そして、協力してくれた周りの友達、先生にも感謝の気持ちでいっぱいです。このようなフォトコンテストに応募するのは初めてで、何をどうすればいいのかもわからず、特にスタイル作りで悩みました。</p>
<p>　「四季彩」というテーマをカラーで表現したくて、悩んでいるときに、周りの人たちがアドバイスをくれて、そのおかげでイメージがだんだん広がっていきました。その時に1人1人の異なる発想のおもしろさを実感し、自分以外の人の意見を聞くのも大切なんだと思いました。</p>
<p>　同じフォトコンで一緒に頑張ったメンバーもいたので、楽しく取り組むことができました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/1203.jpg"><img class="mt-image-none" alt="1203.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/12/1203-thumb-336x384-810.jpg" width="336" height="384" /></a></span></p>
<p>&nbsp;</p>
<p>準入選した椎名さんです。</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/2012.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-19.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>