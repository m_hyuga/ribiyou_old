<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>理容師・美容師国家試験☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
理容師・美容師国家試験☆</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.02.04</time>
			<h2>理容師・美容師国家試験☆</h2>
			<div class="news_area">
				みなさまこんにちは！<div>今日からお天気がまた冬に戻りましたね。足元が悪いので十分にお気を付け下さい。</div><div><br /></div><div>昨日は美容科の実技国家試験。本日理容科の実技国家試験が無事終了しました！</div><div>これまで2学年の生徒は『全員で合格！！』を合言葉に、毎日朝から放課後まで1秒も無駄にしないように練習を重ねてきました。</div><div>当日は天候にも恵まれ、落ち着いて受験できました。</div><div>いつもはやんちゃなあの子も、普段は大人しいあの子も、不安でいっぱいだったと思いますが真剣に取り組む姿勢はとってもキラキラと輝いていたと思います。</div><div>本当にみんなよく頑張った！！みんなが１等賞☆</div><div><br /></div><div>さぁ、次は筆記試験に向けてまた頑張ろう！！</div><div><br /></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-38.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-39.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>