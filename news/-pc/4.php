<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>第4回ボランティア活動作文コンクール受賞☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
第4回ボランティア活動作文コン…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.02.05</time>
			<h2>第4回ボランティア活動作文コンクール受賞☆</h2>
			<div class="news_area">
				<font style="font-size: 1.5625em;">本校エステ科の生徒が、</font><div><font style="font-size: 1.953125em;">第4回ボランティア作文コンクールで</font></div><div><font style="font-size: 1.953125em;">受賞致しました！！</font><div><br /></div><div><br /></div><div><font style="font-size: 1.25em;"><b>認定校委員長賞</b>　　　<b>吉﨑　望さん</b>　</font></div><div><font style="font-size: 1.25em;">　　　　　　　　　　　　　「敬老の日ボランティア活動に参加して得たこと感じたこと」</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;"><b>佳作</b>　　　　　　　　　　<b>関原　渚さん</b>　</font></div><div><font style="font-size: 1.25em;">　　　　　　　　　　　　　「今年参加したボランティアで経験したこと」</font></div><div><br /></div><div><br /></div><div><font style="font-size: 1.25em;">今回で第4回となる「ボランティア作文コンクール」は全国から多数の作品が応募され、1番優れた作品には「認定校委員長賞＜1名＞」、また「優秀賞＜3名＞」、「佳作＜6名＞」が選出されます。</font></div><div><font style="font-size: 1.25em;">ボランティア活動に取り組んだ感想や活動、気づきなどが語られており素晴らしい経験が伝わってくる作品でした。</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">それにしても、一人しか選ばれることのない賞を頂けるなんて素晴らしいですね☆おめでとうございました！！</font></div></div><div><font style="font-size: 1.25em;"><br /></font></div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/02/DSC01833-thumb-200x150-1660.jpg"><img alt="DSC01833.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/02/DSC01833-thumb-200x150-1660-thumb-300x225-1661.jpg" width="300" height="225" class="mt-image-none" /></a></span><br /><div>　　　＊右から、吉﨑さん、校長先生、関原さん</div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-39.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-40.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>