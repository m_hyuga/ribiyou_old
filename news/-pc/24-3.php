<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>平成24年度　特待生発表!! | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
平成24年度　特待生発表!!</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.12.28</time>
			<h2>平成24年度　特待生発表!!</h2>
			<div class="news_area">
				<span style="FONT-FAMILY: 'ＭＳ 明朝', 'serif'; FONT-SIZE: 10.5pt; mso-ascii-font-family: Century; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-fareast; mso-hansi-font-family: Century; mso-hansi-theme-font: minor-latin; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA"><font style="FONT-SIZE: 1em" color="#000000"> 
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">平成<span lang="EN-US">24</span>年度の特待生が発表されました。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">学業成績ならびに日々の行いが特に優秀な学生を表彰する制度で、意欲的に学ぼうとする生徒を支援する制度です。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast" lang="EN-US"><o:p>&nbsp;</o:p></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">●学業優秀賞：免除額<span lang="EN-US">20</span>万円<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">特に成績が優秀な生徒に学費の一部を免除する。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast" lang="EN-US"><o:p>&nbsp;</o:p></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">●善行奨励賞：免除額<span lang="EN-US">10</span>万円<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">学業優秀で模範となる善行生徒に学費の一部を免除する。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast" lang="EN-US"><o:p>&nbsp;</o:p></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">今年も、意欲的で一生懸命な学生が選ばれました！<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">多くの生徒がこの賞を目指して、毎日の実習やテスト、学校生活を過ごしてきました。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">発表は、まさに運命の瞬間で、張りつめた空気の中で行われました。<span lang="EN-US"><o:p></o:p></span></span></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">嬉しくて涙目になったり、予想外で驚きをかくせなかったりと、受賞者の様子はそれぞれでしたが、みなとても喜んでいました。</span></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9015-1.jpg"><img class="mt-image-none" alt="IMG_9015-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/12/IMG_9015-1-thumb-448x329-839.jpg" width="448" height="329" /></a></p></span>
<p></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA"></span>&nbsp;</p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">学業優秀賞の<span lang="EN-US">4</span>名です。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">浦﨑　麻里子（美容科<span lang="EN-US">2</span>年）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">太田　桂子（理容科<span lang="EN-US">1</span>年）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">池下　このみ（美容科<span lang="EN-US">1</span>年　富山県立志貴野高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">森田　佑奈（美容科<span lang="EN-US">1</span>年　岐阜県立飛騨高山高等学校　出身）</span></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA"></span>&nbsp;</p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_9029-1.jpg"><img class="mt-image-none" alt="IMG_9029-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/12/IMG_9029-1-thumb-448x310-841.jpg" width="448" height="310" /></a></p></span>
<p></p>
<p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA"></span>&nbsp;</p><span style="FONT-FAMILY: 'ＭＳ ゴシック'; FONT-SIZE: 10.5pt; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast; mso-bidi-font-size: 11.0pt; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA">
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">善行奨励賞の<span lang="EN-US">5</span>名です。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">柴田　奈実（美容科<span lang="EN-US">2</span>年　富山県立上市高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">鈴木　さつき（美容科<span lang="EN-US">2</span>年　富山県立富山北部高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">向井　実香（美容科<span lang="EN-US">2</span>年　富山県立高岡西高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">桶谷　怜児（理容科<span lang="EN-US">1</span>年　富山県立高岡工芸高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">舟根　麻未（美容科<span lang="EN-US">1</span>年　富山県立富山北部高等学校　出身）<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast" lang="EN-US"><o:p>&nbsp;</o:p></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast" lang="EN-US"><o:p>&nbsp;</o:p></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">地道な努力を惜しまなかった人たちです。<span lang="EN-US"><o:p></o:p></span></span></p>
<p style="MARGIN: 0mm 0mm 0pt" class="MsoNormal"><span style="FONT-FAMILY: 'ＭＳ ゴシック'; mso-ascii-theme-font: major-fareast; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: major-fareast">おめでとうございます<span lang="EN-US">!!!<o:p></o:p></span></span></p>
<p></span></span></font></span>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-20.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-21.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>