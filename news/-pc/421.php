<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>4月21日オープンキャンパス開催 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
4月21日オープンキャンパス開…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.04.17</time>
			<h2>4月21日オープンキャンパス開催</h2>
			<div class="news_area">
				<p>4月21日（日）にオープンキャンパスを開催します。</p>
<p>時間は１０：００スタートです。</p>
<p>&nbsp;</p>
<p>カット＆ブローコース、エステ＆ネイルコースのどちらかをお選びください。</p>
<p>（エステ＆ネイルコースは、女性のみとさせていただきます。ご了承ください）</p>
<p>&nbsp;</p>
<p>各体験とも、いろんな企画を準備していますので、ぜひぜひお越しください！！</p>
<p>&nbsp;</p>
<p>お申し込みはこちらから　↓</p>
<p><a href="http://www.toyama-bb.ac.jp/opencampus2012/form/index0421.html">http://www.toyama-bb.ac.jp/opencampus2012/form/index0421.html</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>オープンキャンパスでは、新しいパンフレット・募集要項を新しい学校バッグに入れてお渡しします！！</p>
<p>パンフレットもバッグもかわいく仕上がっております！ぜひ、手にとってみてください！！</p>
<p>&nbsp;</p>
<p>
</p><span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0403-1.jpg"><img class="mt-image-none" alt="IMG_0403-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/04/IMG_0403-1-thumb-448x267-943.jpg" width="448" height="267" /></a></span><p></p>
<p>&nbsp;</p>
<p>お待ちしてま～す♪</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/tea-foret.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-24.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>