<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>理容師・美容師国家試験　実技試験合格１００％！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
理容師・美容師国家試験　実技試…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.03.30</time>
			<h2>理容師・美容師国家試験　実技試験合格１００％！！</h2>
			<div class="news_area">
				<p>３月２９日、先日卒業を迎えた理容科・美容科第６２期生の理容師・美容師国家試験の合格発表が行われました。</p>
<p>&nbsp;</p>
<p>実技試験は、理容科も美容科も<strong><font style="FONT-SIZE: 1.25em">合格率</font><font style="FONT-SIZE: 1.25em">１００％</font></strong>を達成するという素晴らしい結果でした！！</p>
<p>筆記試験では、理容科は<strong><font style="FONT-SIZE: 1.25em">合格率</font><font style="FONT-SIZE: 1.25em">１００％</font></strong>で、実技・筆記合わせての<strong><font style="FONT-SIZE: 1.25em">総合合格率</font><font style="FONT-SIZE: 1.25em">１００％</font></strong>です！美容科も、９８．２％という高い合格率でした！</p>
<p>（全国平均　理容６７％　美容８０．４%）</p>
<p><font style="FONT-SIZE: 1.56em"></font>&nbsp;</p>
<p>みんなとてもよく頑張りました。</p>
<p>自信を持って、理容師・美容師として、がんばっていってください！&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-22.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/tea-foret.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>