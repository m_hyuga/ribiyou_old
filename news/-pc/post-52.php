<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
オープンキャンパス開催しました…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.07</time>
			<h2>オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				こんにちは！！富山県もいよいよ梅雨入りした本日、オープンキャンパスを開催いたしました。<div><br /></div><div>今日のオープンキャンパスにもたくさんの学生さんが参加してくださいました♪いつもたくさんのご参加</div><div><br /></div><div>ありがとうございます！！</div><div><br /></div><div>さて、本日の体験は・・・</div><div><br /></div><div>理容科：トリートメント体験でとぅるん♪とぅるん♪の髪に大満足。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04276.JPG"><img alt="DSC04276.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04276-thumb-300x225-1777.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>美容科：これで私もプロの美容師に一歩近づく！ワインディング（パーマ技術）に挑戦！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04257.JPG"><img alt="DSC04257.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04257-thumb-300x225-1779.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>エステ科：今年の夏は素足に自信あり☆美脚マッサージを体験！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04267.JPG"><img alt="DSC04267.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04267-thumb-300x225-1781.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>トータルビューティ科：夢が膨らむ・・・☆ブライダルネイル体験♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04261.JPG"><img alt="DSC04261.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04261-thumb-300x225-1783.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>今日の体験も大変盛り上がり、大盛況に終わりました☆</div><div>『楽しかった』の一言が本当に嬉しく、高校生の皆さんの笑顔がたくさん見られて</div><div>とっても良かったです！</div><div><br /></div><div>本校のオープンキャンパスでは楽しく体験をしていただくことはもちろんですが、</div><div>理容・美容・エステ・トータルビューティに関するお悩みや不安に関して</div><div>キャンパススタッフや教員がしっかりと相談に乗らせていただきます。</div><div>気軽に相談してくださいね！</div><div><br /></div><div>次回のオープンキャンパスは・・・</div><div><br /></div><div><font style="font-size: 1.953125em;">６月２２日（日）　１０：００～</font>（受付開始：９：３０～）</div><div><br /></div><div>お申し込みは→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.5625em;"><b>こちらをクリック☆</b></font></a></div><div><br /></div><div><br /></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/1-2.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-53.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>