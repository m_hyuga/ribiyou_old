<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>すごい実習室で・・・ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
すごい実習室で・・・</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.09.09</time>
			<h2>すごい実習室で・・・</h2>
			<div class="news_area">
				<p>皆さま、こんにちは。</p>
<p>本日は、久しぶりの快晴です。</p>
<p>&nbsp;</p>
<p>カエルさんも日陰で今日1日をやり過ごそうと息をひそめてますね（笑）</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>さて、先日からのブログでもご紹介していますトータルジョブサロン（仮称）で、</p>
<p>本日は理容科2年生による</p>
<p>シェービング実習</p>
<p>が行われたので、これはチャンス！と様子を見てきましたので、アップしま～す。</p>
<p>&nbsp;</p>
<p>その前に・・・</p>
<p>トータルジョブサロン（まだ仮称ですが・・・）、皆さんご覧になられましたか？</p>
<p>9月に新しくオープンしたんです。</p>
<p>9月のオープンキャンパスにご参加頂いた皆さんには、ご覧頂きまして、<strong><font style="FONT-SIZE: 1em">ナカナカ</font>好評</strong>です❤</p>
<p>&nbsp;</p>
<p>ある女子高生の方は、目をまんまるくして</p>
<p>「す、すごいですね・・・」</p>
<p>と。</p>
<p>&nbsp;</p>
<p>ありがとうございます。確かにスゴイ実習室なんです。</p>
<p>一番新しい種類のシャンプーセットで、前にご紹介しましたが、これ1台でお客様の</p>
<p>ご要望に全てお応えできちゃう優れもの。</p>
<p>&nbsp;</p>
<p>例えば・・・</p>
<p>「あたくしねぇ、今日は、シャンプーブローして頂きたいの。</p>
<p>あっ、それとぉ、明日パーティがあるからネイルもしていただきたいんだけど、時間があまりないのよぉ」</p>
<p>こんな、セレブ<font style="FONT-SIZE: 0.64em">サマ<font style="FONT-SIZE: 1.25em">もご安心</font></font>！</p>
<p>ブローしながら、そのままネイルも出来ちゃうんです。</p>
<p>他にも、エステやシェービング。</p>
<p>&nbsp;</p>
<p>富山県理容美容専門学校が誇る優秀な実習室です。</p>
<p>&nbsp;</p>
<p>まだ、ご覧になっていない方は是非オープンキャンパスにご参加ください。</p>
<p><strong><font style="FONT-SIZE: 1.25em">圧巻</font></strong>です！</p>
<p>&nbsp;</p>
<p>さて、その実習室で本日行った授業の様子をご覧ください。</p>
<p>&nbsp;</p>
<p>【<strong>理容科2年生によるシェービング実習</strong>】</p>
<p>本日は、富山県理容組合の先輩の方々にモデルを依頼して臨みました。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>先輩方のお顔剃りとあって、学生たちは・・・</p>
<p>緊張の面持ち(#^.^#)</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/IMG_2397.JPG"><img class="mt-image-none" alt="IMG_2397.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2397-thumb-700x466-1352.jpg" width="700" height="466" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>理容科主任の中条先生（左）からの直接指導を真剣に聞く学生。</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/IMG_2407.JPG"><img class="mt-image-none" alt="IMG_2407.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2407-thumb-700x466-1354.jpg" width="700" height="466" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>毎どの事ですが、本校の消毒は完璧!!ですっ(^^)v 
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;</span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img style="WIDTH: 552px; HEIGHT: 274px" class="mt-image-none" alt="IMG_2412.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2412-thumb-300x200-1358.jpg" width="300" height="200" /></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">　　　</span></p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/IMG_2413.JPG"></a>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;</span></p>
<p></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">蒸しタオルでお顔を丁寧に拭いてから、乳液でマッサ～ジ♪</span>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">皆さん、気持ち良さそう❤　</span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/IMG_2409.JPG"><img class="mt-image-none" alt="IMG_2409.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2409-thumb-700x466-1363.jpg" width="700" height="466" /></a></span>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">　　　　　　　　　　　　　　　　　　　　　　　　　　　　　<a href="http://www.toyama-bb.ac.jp/news/IMG_2411.JPG"></a></p>
<p></p>
<p></p></span>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/IMG_2417.JPG"><img class="mt-image-none" alt="IMG_2417.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2417-thumb-700x466-1365.jpg" width="700" height="466" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>そして、頭から首筋、肩にかけて丁寧にマッサージをして、ヘアスタイルを整えていきます。</p>
<p>理容のマッサージと美容のマッサージもちょっと違うんですね～。</p>
<p>今度、理容科主任と美容科主任に聞いてみようと思います(^。^)y-.。o○</p>
<p>インタビューした情報もまたお知らせ致しますので、お楽しみに～♪</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/IMG_2416.JPG"><img class="mt-image-none" alt="IMG_2416.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2416-thumb-700x466-1367.jpg" width="700" height="466" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>今日の理容科2年生のシェービング授業は、「研究授業」でした。</p>
<p>県理美は、学生達にとって少しでも分かりやすい授業を目指して教職員も日々努力してます！</p>
<p>そのうち、県理美から「カリスマ教師」がでちゃうかもです❤<font style="FONT-SIZE: 0.64em">ウフフ</font></p>
<p>&nbsp;</p>
<p>本日、ご協力頂きました富山県理容組合の方々、ありがとうございました。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/97.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/921.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>