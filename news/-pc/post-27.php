<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>速報！全国大会に７名選出！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
速報！全国大会に７名選出！！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.07.30</time>
			<h2>速報！全国大会に７名選出！！</h2>
			<div class="news_area">
				<p>７月３０日、石川県で開催された第5回全国理容美容学生技術大会信越北陸地区予選大会において、全国大会出場者として県理美生選手団から７名が選ばれました！！！</p>
<p>&nbsp;</p>
<p>おめでとうございます！！</p>
<p>&nbsp;</p>
<p>日ごろの練習の賜物！</p>
<p>そして、本番で実力を発揮できる心臓の強さ！</p>
<p>&nbsp;</p>
<p>全国大会は11月17日に兵庫県で開催されます。</p>
<p>それまでに、さらに技術を磨いて、いろんな面でパワーアップして頑張りましょう！！！！！！</p>
<p>&nbsp;</p>
<p>【理容部門　ワインディング】</p>
<p>優秀賞　墓越　和哉（富山第一高等学校　出身）</p>
<p>優秀賞　澤井　彩夏（富山県立伏木高等学校　出身）</p>
<p>&nbsp;</p>
<p>【理容部門　ミディアムカットヘア】</p>
<p>優秀賞　佐生　大（富山県立滑川高等学校　出身）</p>
<p>優秀賞　太田　桂子</p>
<p>敢闘賞　桶谷　怜児（富山県立高岡工芸高等学校　出身）</p>
<p>&nbsp;</p>
<p>【理容部門　チャレンジアートヘア】</p>
<p>敢闘賞　藤井　純（富山県立南砺総合高等学校井波高等学校　出身）</p>
<p>&nbsp;</p>
<p>【美容部門　ワインディング】</p>
<p>優秀賞　鞍田　理佳子（富山県立伏木高等学校　出身）</p>
<p>&nbsp;</p>
<p>【美容部門　カット】</p>
<p>優秀賞　池下　このみ（富山県立志貴野高等学校　出身）</p>
<p>優秀賞　櫻田　龍玄（龍谷富山高等学校　出身）</p>
<p>敢闘賞　長谷川　紘道（富山県立魚津工業高等学校　出身）</p>
<p>&nbsp;</p>
<p>【デザイン画】</p>
<p>敢闘賞　舟根　麻未（富山県立富山北部高等学校　出身）</p>
<p>&nbsp;</p>
<p>優秀賞を獲得した選手が、全国大会へ出場します。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-26.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-28.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>