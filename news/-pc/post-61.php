<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>速報！！全国大会出場決定（県内最多）！！！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
速報！！全国大会出場決定（県内…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.07.29</time>
			<h2>速報！！全国大会出場決定（県内最多）！！！！</h2>
			<div class="news_area">
				本日、富山市総合体育館で行われた全国理容美容学生技術大会　信越北陸大会で<div><br /></div><div>本校学生が全国大会出場を決めました！！</div><div><br /></div><div><br /></div><div><font style="font-size: 1.5625em;"><b>＜理容部門＞</b></font></div><div><br /></div><div><font style="font-size: 1.25em;">ワインディング種目　　　　　　　優秀賞　　　　　　　木本　貴子</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;"><div style="font-size: 13px;"><font style="font-size: 1.25em;">チャレンジアートヘア種目　　　優秀賞　　　　　　　鶴見　賢太</font></div><div style="font-size: 13px;"><br /></div><div style="font-size: 13px;"><font style="font-size: 1.25em;">ミディアムカット種目　　　　　　優秀賞　　　　　　　堀田　慎一郎</font></div><div style="font-size: 13px;"><font style="font-size: 1.25em;"><br /></font></div><div style="font-size: 13px;"><font style="font-size: 1.25em;">ネイルアート種目　　　　　　　　優秀賞　　　　　　　鈴木　詩穂子</font></div></font></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06169%20%281%29.JPG"><img alt="DSC06169 (1).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06169 (1)-thumb-300x225-1846.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><div><br /></div></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06210.JPG"><img alt="DSC06210.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06210-thumb-300x225-1848.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><font style="font-size: 1.5625em;"><b>＜美容部門＞</b></font></div><div><br /></div><div><font style="font-size: 1.25em;">ワインディング種目　　　　　　　優秀賞　　　　　　　眞田　裕里</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">カット種目　　　　　　　　　　　　優秀賞　　　　　　　佐藤　友輝</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">　　　　　　　　　　　　　　　　　　優秀賞　　　　　　　南　　星来</font></div><div><font style="font-size: 1.25em;">　　　　　　</font></div><div><font style="font-size: 1.25em;">　　　　　　　　　　　　　　　　　　優秀賞　　　　　　　小林　憲之亮</font></div><div><font style="font-size: 1.25em;"><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06188%20%281%29.JPG"><img alt="DSC06188 (1).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06188 (1)-thumb-300x225-1850.jpg" width="300" height="225" class="mt-image-none" /></a></span></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;"><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06245%20%281%29.JPG"><img alt="DSC06245 (1).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06245 (1)-thumb-300x225-1852.jpg" width="300" height="225" class="mt-image-none" /></a></span></font></div><div><br /></div><div><br /></div><div>以上、合計8名が10月28日に北海道で行われる全国大会に信越北陸地区代表として</div><div><br /></div><div>出場します！！</div><div><br /></div><div>また、惜しくも全国大会出場には至らなかったのですが、見事！入賞した生徒です☆</div><div><br /></div><div><font style="font-size: 1.5625em;"><b>＜理容部門＞</b></font></div><div>　</div><div><font style="font-size: 1.25em;">チャレンジアート競技　　　　　敢闘賞　　　　　清水　理紗子</font></div><div><br /></div><div><font style="font-size: 1.5625em;"><b>＜美容部門＞</b></font></div><div><br /></div><div><font style="font-size: 1.25em;">ワインディング競技　　　　　　敢闘賞　　　　　松本　沙季</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">カット競技　　　　　　　　　　　敢闘賞　　　　　加藤　未貴</font></div><div><br /></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06355%20%281%29.JPG"><img alt="DSC06355 (1).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/DSC06355 (1)-thumb-300x225-1844.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>大会に出場した選手も、大会運営を手伝ってくれた生徒も、一生懸命に応援してくれた生徒も</div><div><br /></div><div>本当によく頑張りました。感謝です。</div><div><br /></div><div>さあ、次は全国に向けて突き進むぞ！！！</div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-60.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-62.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>