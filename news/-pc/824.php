<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>8月24日　　オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
8月24日　　オープンキャンパ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.08.24</time>
			<h2>8月24日　　オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				みなさんこんにちは！<div><br /></div><div>不安定なお天気が続いていますがいかがお過ごしでしょうか？今年の夏は満喫できましたか？！</div><div><br /></div><div>今日も本校には、夏休み最後の思い出作りにたくさんの高校生の皆さんがオープンキャンパスに</div><div>来てくださいました♪♪</div><div><br /></div><div><br /></div><div>本日の体験内容は・・・・</div><div><br /></div><div><font style="font-size: 1.953125em;">＊理<font style="font-size: 1em;">容</font>科＊</font></div><div><br /></div><div>シャンプー体験　　　　夏にたまった毛穴の汚れをスッキリ洗い流しました！！</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06505.JPG"><img alt="DSC06505.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06505-thumb-300x225-1891.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div><div><font style="font-size: 1.953125em;">＊美容科＊</font></div><div><br /></div><div>アップスタイル体験　　　オシャレにアレンジ☆アナと雪の女王アレンジにも挑戦しました！！</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06500.JPG"><img alt="DSC06500.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06500-thumb-300x225-1893.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div><div><font style="font-size: 1.953125em;">＊エステティック科＊</font></div><div><br /></div><div>ハンドマッサージ体験　　角質除去・マッサージ・パックのフルコースでピッカピカ☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06494.JPG"><img alt="DSC06494.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06494-thumb-300x225-1895.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div><div><font style="font-size: 1.953125em;">＊トータルビューティ科＊</font></div><div><br /></div><div>ネイル体験　　　デコネイルでプクプクかわいいネイルアートに挑戦♡</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06506.JPG"><img alt="DSC06506.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06506-thumb-300x225-1897.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>初めて参加された高校生の皆さんも、とっても楽しそうに体験をされていました(^.^)/~~~</div><div><br /></div><div><br /></div><div><br /></div><div>本校のオープンキャンパスでは皆さんがワクワクするような体験を行っています☆</div><div><br /></div><div>たとえば・・・</div><div><br /></div><div>シェービングやカラーチョーク、デコネイルに角質除去など他にはない・他ではできない体験や、</div><div><br /></div><div>普通とはちょっと違う、一味違ったおもしろ体験ができちゃいます♪　</div><div><br /></div><div><br /></div><div>そこで、次回のオープンキャンパスでは・・・・</div><div><br /></div><div><font style="font-size: 0.6400000000000001em;"><font style="font-size: 1.953125em;">＊理容科　―　<b>カット<font style="font-size: 0.6400000000000001em;">（秋ショート◆）</font></b></font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 0.6400000000000001em;"><font style="font-size: 1.953125em;">＊美容科　―　<b>ワインディング<font style="font-size: 0.6400000000000001em;">（みんなで巻き巻き♪）</font></b></font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 0.6400000000000001em;"><font style="font-size: 1.953125em;">＊エステティック科　―　<b>フットケア<font style="font-size: 0.6400000000000001em;">（乾燥なんて怖くない☆）</font></b></font></font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">＊トータルビューティ科　―　<b>ブライダルメイク<font style="font-size: 0.6400000000000001em;">（Cuteな花嫁に・・・♡）</font></b></font></div><div><font style="font-size: 0.6400000000000001em;">　　</font></div><div>このような体験内容でお待ちしています&lt;(_ _)&gt;</div><div><br /></div><div>参加申し込みは→<font style="font-size: 1.953125em;"><a href="http://www.toyama-bb.ac.jp/opencampus/">こちらをクリック☆</a></font><a href="http://www.toyama-bb.ac.jp/opencampus/"></a></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC06502.JPG"><img alt="DSC06502.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/08/DSC06502-thumb-300x225-1899.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><font style="font-size: 1.953125em;">私たちと一緒に、県理美で夢・叶えましょう！！</font></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-64.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-65.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>