<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>新年度オープンキャンパス開催！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
新年度オープンキャンパス開催！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.03.30</time>
			<h2>新年度オープンキャンパス開催！</h2>
			<div class="news_area">
				<p style="border-style: initial; border-color: initial; font-size: 13px; ">4月22日（日）にオープンキャンパスを開催します♪</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">体験、在校生との座談会、個別相談会、キャンパスツアーと、今回も楽しんでためになるオープンキャンパスとなっています！</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">体験は次の通り～</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">カット「初めてのカット体験」</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">ワインディング「パーマをかける基礎を学ぼう」</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">ネイル「セルフケアの基本を身につけよう」</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">エステティック「自分の肌を知ろう」</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">以上から２つを選択してください。</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;</p><p style="border-style: initial; border-color: initial; font-size: 13px; ">&nbsp;お申し込みは終了しました。
</p> 
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/100.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/cat/post-8.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>