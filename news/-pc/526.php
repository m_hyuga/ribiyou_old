<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>5月26日(土)オープンキャンパス開催！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
5月26日(土)オープンキャン…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.05.14</time>
			<h2>5月26日(土)オープンキャンパス開催！</h2>
			<div class="news_area">
				<p>5月26日(土)オープンキャンパス開催いたします☆</p>
<p>今回の体験は～☆☆☆</p>
<p>&nbsp;</p>
<p>シャンプー「じゃぶじゃぶ洗ってスッキリシャンプー体験」</p>
<p>　　　　　　　＊モデルの頭を洗う体験です。</p>
<p>ワインディング「みんなで楽しくパーマを巻いてみよう」</p>
<p>ネイル「ネイルアートを楽しみましょう」</p>
<p>エステティック「エステティッククレンジングを体験しよう」</p>
<p>　　　　　　　＊モデルウィッグを使用します。</p>
<p>　　　　　　　＊エステティック体験は、女性の方のみとさせていただきます。ご了承ください。</p>
<p>
</p><span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2012/05/IMG_3185-1-thumb-448x299-390.jpg"><img class="mt-image-none" alt="IMG_3185-1.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2012/05/IMG_3185-1-thumb-448x299-390-thumb-448x299-391.jpg" width="448" height="299" /></a></span>&nbsp;<p></p>
<p>
</p><span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_3185-1.JPG"></a></span><p></p>
<p>&nbsp;</p>
<p>シャンプーの体験は、こんな感じで行います。</p>
<p>シャンプー台がいっぱい並んでいる実習室で行います。</p>
<p>&nbsp;</p>
<p>&nbsp;お申し込みは終了しました。</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/513.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-9.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>