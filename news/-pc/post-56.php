<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ホイルワークセミナー開催しました！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
ホイルワークセミナー開催しまし…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.23</time>
			<h2>ホイルワークセミナー開催しました！！</h2>
			<div class="news_area">
				名古屋から講師の先生をお招きして、ホイルワークセミナーを開催しました☆<div><br /></div><div>本日の講師はゆう美容室の谷本育美先生です。</div><div><br /></div><div>ホイルワークとは高度なカラー技術で、カラー剤を塗るところと塗らないところを作り、毛流れに動きを</div><div><br /></div><div>出したり立体感を出す技術の一つなのです。このように・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04828.JPG"><img alt="DSC04828.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04828-thumb-300x225-1804.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>ホイルがきれいにはられた姿がまた芸術的なんです！！</div><div><br /></div><div>そのホイルワークの効果的な入れ方や、テクニックについて実際にモデルさんにカラーリングを施し、</div><div><br /></div><div>トップレベルのカラー技術を学びました。普段はなかなか見ることができない素晴らしい技術に</div><div><br /></div><div>学生たちの目も釘づけでした(^○^)</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04752.JPG"><img alt="DSC04752.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04752-thumb-300x225-1794.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04751.JPG"><img alt="DSC04751.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04751-thumb-300x225-1796.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>２人のモデルさんをプロのテクニックで仕上げていただくと</div><div><br /></div><div>ほら、この通り・・・</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04891.JPG"><img alt="DSC04891.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04891-thumb-300x225-1798.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>とってもかわいらしく、きれいに仕上げていただきました☆モデルさんも大満足です♪</div><div><br /></div><div><br /></div><div>そして、本校生徒がアシスタントに挑戦！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04815.JPG"><img alt="DSC04815.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04815-thumb-300x225-1800.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div>ドキドキしながらも頑張ってお手伝いさせていただきました。</div><div><br /></div><div>緊張したけど、とっても楽しかったそうです☆貴重な体験でした。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC04899.JPG"><img alt="DSC04899.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC04899-thumb-300x225-1802.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>本校ではこういったセミナーなどでは、講師の先生のアシスタントとして</div><div>バックヤードでのお仕事やステージ上でのヘルプなど、鑑賞以外にもセミナーに</div><div>参加することができ、プロの技術を間近で見るチャンスや、鑑賞しているだけでは聞けないこと、プロの世界とは・・・</div><div>などいろんなことを学べるチャンスがいっぱいあります☆</div><div>夢を実現する近道はここにある！！！！！</div><div><br /></div><div><br /><div><br /></div></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-55.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-57.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>