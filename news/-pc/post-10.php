<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>６月９日、６月２４日オープンキャンパス受付中 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
６月９日、６月２４日オープンキ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.06.05</time>
			<h2>６月９日、６月２４日オープンキャンパス受付中</h2>
			<div class="news_area">
				<p>オープンキャンパスの参加を受付中です。</p>
<p>お気軽にお申し込みください！</p>
<p>&nbsp;</p>
<p>６月９日　１つお選びください</p>
<p>理容・美容コース</p>
<p>エステ・ネイルコース(女性のみ）</p>
<p>&nbsp;</p>
<p>６月２４日　２つお選びください。</p>
<p>カット</p>
<p>スタイリング</p>
<p>メイク</p>
<p>エステティック（女性のみ）</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>（パソコン）<a href="http://www.toyama-bb.ac.jp/opencampus2012/">http://www.toyama-bb.ac.jp/opencampus2012/</a></p>
<p>ケータイからは、<a href="mailto:info@toyama-bb.ac.jp">ﾒｰﾙ</a>(←ｸﾘｯｸしてください｡ﾒｰﾙﾌｫｰﾑにﾘﾝｸしています｡)からお申込みくださいますよう､お願い申し上げます｡</p>
<p>下記のことを入力してください｡</p>
<p>･お名前</p>
<p>･学校名､学年</p>
<p>･ご住所</p>
<p>･ご連絡先</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/69-2.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-11.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>