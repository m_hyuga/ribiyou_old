<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>1年生　メンズスタイル作品完成！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
1年生　メンズスタイル作品完成…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.06.03</time>
			<h2>1年生　メンズスタイル作品完成！！</h2>
			<div class="news_area">
				この春、入学したばかりの理容科・美容科の１年生がカットの授業でメンズスタイルを完成させました！<div>入学から約２か月・・・ここまで完成度の高い作品を作れる本校の生徒はなんて素晴らしい☆</div><div><br /></div><div>この作品は、形やカットラインなど自分でプランを考え、</div><div>そのプラン通りにカットをしました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03947.JPG"><img alt="DSC03947.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03947-thumb-300x225-1767.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div><div>このように・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03948.JPG"><img alt="DSC03948.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03948-thumb-300x340-1769.jpg" width="300" height="340" class="mt-image-none" /></a></span></div><div>どうやって切って、どうスタイリングするか・・・</div><div>たくさん悩んで、考えて、出来上がったのがこちら！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03949.JPG"><img alt="DSC03949.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03949-thumb-300x400-1771.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div><br /></div><div>思いどおりにできるか不安だったけど・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03951.JPG"><img alt="DSC03951.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03951-thumb-300x400-1773.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div>とっても素晴らしい作品ができました！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03952.JPG"><img alt="DSC03952.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/06/DSC03952-thumb-300x400-1775.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div><br /></div><div>はさみを持つ手もだいぶ慣れ、カットをしている時の姿が</div><div>とってもプロらしくなってきました。まだまだこれからもたくさん</div><div>カットを勉強して素敵な理容師・美容師になって欲しいです(^○^)</div><div>将来が楽しみでなりません・・・☆</div><div><br /></div><div>本校に入学するとカット技術もバッチリ身に付きますよ。</div><div>６月７日のオープンキャンパスでこちらの作品が見学できます。ぜひぜひお越しください！</div><div>お待ちしておりまーす！！</div><div><br /></div><div>オープンキャンパスのお申し込みは→<a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.5625em;"><b>こちらをクリック☆</b></font></a></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-51.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-52.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>