<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>BBTで県理美のキャンパスライフが紹介されます☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
BBTで県理美のキャンパスライ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.06.28</time>
			<h2>BBTで県理美のキャンパスライフが紹介されます☆</h2>
			<div class="news_area">
				<p>&nbsp;</p>
<p>６月２９日（土）、BBT（富山テレビ　８チャンネルです）の番組「体験しよう！キャンパスライフ」で、県理美が紹介されます。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.55em" size="5"><strong>楽しく</strong></font>そして<font style="FONT-SIZE: 1.25em"><font size="5">真剣</font><font style="FONT-SIZE: 0.8em">な</font></font>実習の様子、たーーーーくさん見ていただけると思います！！！</p>
<p>シャンプーとか、シェービングとか、ネイルとか、エステとか、あれとか、これとか、あれとか。</p>
<p>あ！17日に行った校内予選の様子も！！</p>
<p>あ！あ！エステティックサロンティアフォーレの様子も！！！</p>
<p>短い時間ですか、<font style="FONT-SIZE: 1.56em">ギュギュギュッ</font>と県理美つまってますよ～。お腹いっぱいになりますよ～。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>各科の学生たちが、<font style="FONT-SIZE: 1.56em">キラッキラ☆</font>の夢をお話ししています。</p>
<p>ぜひそちらもお楽しみに～～ヾ(＠⌒ー⌒＠)ノ</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><img style="TEXT-ALIGN: center; MARGIN: 0px auto 20px; DISPLAY: block" class="mt-image-center" alt="IMG_3118-1.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2013/06/IMG_3118-1-thumb-448x336-1112.jpg" width="448" height="336" /></span></p>
<p>
<p>とってもとってもとっても緊張したらしいです。
<p>でも、ちゃーんと話せてましたよ～。
<p>&nbsp;
<p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image">&nbsp;<font style="FONT-SIZE: 1.25em">なので皆様、</font></span></p>
<p><font style="FONT-SIZE: 1.25em"></font>
<p><font style="FONT-SIZE: 1.25em"></font>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><font style="FONT-SIZE: 1.25em">ぜひ、ご覧下さい！！！！！！</font></span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><font style="FONT-SIZE: 1.25em"></font>&nbsp;</span></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><font style="FONT-SIZE: 1.56em">♥放送日時♥</font></span></p>
<p>
<p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><font style="FONT-SIZE: 1.56em"><strong>６月２９日（土）１４：５５～</strong></font></span></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><font style="FONT-SIZE: 1.56em">BBT（富山テレビ）　「体験しよう！キャンパスライフ」</font></span></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-24.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/77.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>