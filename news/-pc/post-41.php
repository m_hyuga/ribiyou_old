<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>近日、オープンキャンパス開催！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
近日、オープンキャンパス開催！…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.03.07</time>
			<h2>近日、オープンキャンパス開催！！</h2>
			<div class="news_area">
				<div><br /></div><div>オープンキャンパスを開催致します☆</div><div><br /></div><div><font style="font-size: 1.953125em;">3月15日（土）　　　１０：００～</font>（受付９：２０～）</div><div><br /></div><div><br /></div><div><br /></div><div>今回のオープンキャンパスの見どころは・・・・</div><div><br /></div><div>３／１４（金）　　<font style="font-size: 1.953125em;">ワークショップがついに、完成します☆</font></div><div><font style="font-size: 1.953125em;"><br /></font></div><div><br /></div><div>県理美のワークショップとは：</div><div><font style="font-size: 1.5625em;">生徒のやりたい！なりたい！！叶えたい！！！を実現する</font></div><div><font style="font-size: 1.5625em;">夢のステージ☆　　　　　</font></div><div><span style="font-size: 1.5625em;"><font style="font-size: 0.8em;">そんなサロンが完成です。</font></span></div><div><br /></div><div><br /></div><div>＜その他にも楽しいことがいっぱい♪＞</div><div><br /></div><div>①県理美にしかない、県理美でしか学べない授業を紹介します♪</div><div><br /></div><div>②自慢の和室に、キラキラ輝くブライダルルームがプラス☆</div><div><br /></div><div>③全国トップクラスの講師陣が魅せる、神業テクニック！！</div><div><br /></div><div>④新しく生まれ変わった校舎で、県理美探検隊☆</div><div><br /></div><div><br /></div><div><font style="font-size: 1.25em;">学生スタッフとの楽しいトークタイムで、実際のスクールライフを知る</font></div><div><font style="font-size: 1.25em;">チャンス！！来て、見て、楽しんで！！</font></div><div><font style="font-size: 1.25em;">お友達との参加も大歓迎です♪たくさんの方の参加をお待ちしています！！</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">事前ご予約も、当日参加もお待ちしています！！</font></div><div><font style="font-size: 1.25em;"><br /></font></div><div><font style="font-size: 1.25em;">ご予約はこちらへ→<a href="http://http://toyama-bb.ac.jp/opencampus/form/index0327.html">http://http://toyama-bb.ac.jp/opencampus/form/index0327.html</a></font></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-40.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-42.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>