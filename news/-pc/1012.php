<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>10月12日オープンキャンパス開催★ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
10月12日オープンキャンパス…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.10.07</time>
			<h2>10月12日オープンキャンパス開催★</h2>
			<div class="news_area">
				<p>お待たせしました！</p>
<p>10月のオープンキャンパスは12日（土）10：00～です。</p>
<p>&nbsp;</p>
<p>今回のオープンキャンパステーマはハロウィンです。</p>
<p>&nbsp;</p>
<p>県理美全体がハロウィン仕様になっていますよ～。</p>
<p>教員デモンストレーションも、ハロウィンをテーマに行います☆</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>そして～、今回の体験は～？</p>
<p>&nbsp;</p>
<p>理容科：あなたのパッションを活かして作品を作りましょう！！</p>
<p>美容科：県理美の最新シャンプー台を使ってみよう！！</p>
<p>エステティック科：むくみ、さらにスッキリ！！</p>
<p>トータルビューティ科：真似❤メイク！！あのモデルになっちゃおう！</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_2690-1.jpg"><img class="mt-image-none" alt="IMG_2690-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/10/IMG_2690-1-thumb-448x299-1427.jpg" width="448" height="299" /></a></p>
<p>
<p>&nbsp;</p>
<p></p>
<p>
<p>「最新シャンプー台」は　↑　こちらです。</p>
<p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>お申込みはこちらから～</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index1012.html">http://toyama-bb.ac.jp/opencampus/form/index1012.html</a></p>
<p>&nbsp;</p>
<p>お待ちしております☆★☆★☆</p>
<p></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-30.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-31.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>