<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>9/20  オープンキャンパス開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
9/20  オープンキャンパス…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.09.20</time>
			<h2>9/20  オープンキャンパス開催しました☆</h2>
			<div class="news_area">
				雲ひとつない気持ちの良い秋晴れの中、本日もたくさんの学生さんが<div><br /></div><div>オープンキャンパスにご参加くださいました(*^_^*)</div><div><br /></div><div>本日の体験は</div><div><br /></div><div><br /></div><div>理容科　シャンプー体験、美容科　ワインディング（パーマ巻き）体験、</div><div><br /></div><div>エステティック科　二の腕たるみ解消体験、　トータルビューティ科　ブライダルネイル体験</div><div><br /></div><div>を行いました(^.^)/~~~</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06895.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06895.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">サロンのような落ち着いた雰囲気のジョブサロン（beat）でシャンプー体験♪</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div>学生たちも普段の実習からこのサロンを使用してお互いにシャンプーをしたり、エステやネイルを</div><div><br /></div><div>しあいっこするんですよ～(^^♪　　まるでお店で働いているような気分で実習できます☆</div><div><br /></div><div>この施設があるからこそ、いろんな実習をリアルに変えられる！！そして、就職してからも即戦力とし</div><div><br /></div><div>て必要とされる人材になれるひ・み・つ・なのです＼(^o^)／</div><div><br /></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06901.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06901.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06916.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06916.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06898.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06898.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">今日もたくさんの笑顔が見られてとってもHappy♡でした。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">秋祭り企画のブース体験も大好評でした。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06924.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06924.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06936.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06936.JPG" width="365" height="274" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">また次回も元気いっぱいのスタッフや教員と楽しい体験とトークをお楽しみください(^○^)</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.25em;">～次回開催のオープンキャンパス～</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.25em;"><br /></font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.953125em;">10月11日（土）10:00～</font>（9:30受付）　</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">＊体験内容などはHPでご確認ください。確認は→</span><a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.25em;"><b>こちらをクリック☆</b></font></a></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 0.8em;"><font style="font-size: 1.5625em;">～進学相談会を開催します～</font></font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.953125em;">9月28日（日）　10:00～</font>（9:30受付）</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 0.8em;">＊進路や進学に関する疑問、お悩みがあればぜひご相談ください。</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/11.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-67.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>