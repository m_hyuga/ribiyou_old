<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>６月２３日オープンキャンパス開催します。 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
６月２３日オープンキャンパス開…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.06.17</time>
			<h2>６月２３日オープンキャンパス開催します。</h2>
			<div class="news_area">
				<p>体験は、シャンプー、ワインディング、エステティック、デコネイルから２つお選びください。</p>
<p>&nbsp;</p>
<p>只今、AO入試エントリー受付中です。</p>
<p>オープンキャンパスで、エントリー面談を受けて、エントリーすることができます。</p>
<p>詳しくは、オープンキャンパスでご説明いたします。</p>
<p>&nbsp;</p>
<p>お申し込みはこちらから</p>
<p><a href="http://www.toyama-bb.ac.jp/opencampus/form/index0623.html">http://www.toyama-bb.ac.jp/opencampus/form/index0623.html</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>６月８日のオープンキャンパスには、本当にたくさんのお客様におこしいただきました。</p>
<p>ありがとうございます！！！</p>
<p>体験はお楽しみいただけたでしょうか。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/011.JPG"><img class="mt-image-none" alt="011.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/06/011-thumb-448x336-1027.jpg" width="448" height="336" /></a></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/025.JPG"><img class="mt-image-none" alt="025.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/06/025-thumb-448x294-1029.jpg" width="448" height="294" /></a></span></p>
<p>&nbsp;</p>
<p>カット＆ブローでは、「アイロンで巻き巻きヘア」を追加メニューに加えました。</p>
<p>実は、5月25日オープンキャンパス時に、参加者の方からリクエストをいただいていたんです。</p>
<p>女の子はできるようになりたいですよね。</p>
<p>このように、体験のリクエストにもお応えしていきますので、お気軽におっしゃってください。</p>
<p>&nbsp;</p>
<p>６月２３日のオープンキャンパス　お待ちしております。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/421.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/bbt.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>