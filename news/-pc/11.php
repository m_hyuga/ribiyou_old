<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>通信課程　11期生卒業式☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
通信課程　11期生卒業式☆</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.09.10</time>
			<h2>通信課程　11期生卒業式☆</h2>
			<div class="news_area">
				9月8日に通信課程11期生の卒業式が行われました(*^_^*)<div><br /></div><div>通信課程の学生は3年間にわたり、サロンワークをしながら夏の面接授業とレポート提出をし</div><div><br /></div><div>各試験をクリアして無事、卒業式を迎えました☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="gljjhgah.JPG" src="http://www.toyama-bb.ac.jp/news/gljjhgah.JPG" width="220" height="176" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06637.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06637.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06669.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06669.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">3年間おつかれさまでした&lt;(_ _)&gt;　ご卒業おめでとうございます。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/cat/96ao.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/920.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>