<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>巨大笹　設置しました！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
巨大笹　設置しました！</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2013.07.04</time>
			<h2>巨大笹　設置しました！</h2>
			<div class="news_area">
				<p>皆様、こんにちは。</p>
<p>&nbsp;</p>
<p>正面玄関に<font style="FONT-SIZE: 1.25em"><strong>巨大笹</strong></font>が設置されました。</p>
<p>この感動！をできる限り早くお伝えしたく、ブログを書いている次第です。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0562-1.jpg"><img style="WIDTH: 467px; HEIGHT: 571px" class="mt-image-none" alt="IMG_0562-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0562-1-thumb-299x448-1122.jpg" width="299" height="448" /></a></span></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.95em"><strong>どーん！！</strong></font></p>
<p>&nbsp;</p>
<p>と、県理美のお客様をお迎えいたします。</p>
<p>ご来校いただいた皆様、学内エステティックサロンティアフォーレのお客様もぜひ、たんざくをお書きください。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0550-1.jpg"><img class="mt-image-none" alt="IMG_0550-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0550-1-thumb-339x336-1124.jpg" width="339" height="336" /></a></span></p>
<p>&nbsp;</p>
<p>幸せフワフワ～なお願いも、<font style="FONT-SIZE: 1.56em"><strong>ガチ</strong></font>なお願いも、なんでも<strong><font style="FONT-SIZE: 1.56em">アリ</font></strong>でございます。</p>
<p>お待ちしておりまーす☆</p>
<p>&nbsp;</p>
<p>7月7日オープンキャンパスお申込みはこちらから</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0707.html">http://toyama-bb.ac.jp/opencampus/form/index0707.html</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0562-1.jpg"></a></span></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/77.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-26.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>