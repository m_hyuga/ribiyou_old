<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/css/normalize.css" />
<link rel="stylesheet" href="/css/css/top.css" />
<link rel="stylesheet" href="/css/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="news_pages">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/526.php">5月26日(土)オープンキャンパス開催！</a></h3>
<div class="date">2012年05月14日</div>
<div class="maintxt">

5月26日(土)オープンキャンパス開催いたします☆
今回の体験は～☆☆☆
&nbsp;
シャンプー「じゃぶじゃぶ洗ってスッキリシャンプー体験」
　　　　　　　＊モデルの頭を洗う体験です。
ワインディン
…<a href="http://www.toyama-bb.ac.jp/news/-pc/526.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/513.php">5月13日(日)オープンキャンパス詳細</a></h3>
<div class="date">2012年04月26日</div>
<div class="maintxt">

各体験の詳細が決まりましたのでお伝えします。
&nbsp;
カット　「セニングシザーを使って　夏のさわやかヘアを作ってみよう！！」
ブロー　「楽しくヘアアレンジをしてみよう☆」
メイク　「上品リップの
…<a href="http://www.toyama-bb.ac.jp/news/-pc/513.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/513526.php">5/13(日）5/26（土）オープンキャンパス開催！</a></h3>
<div class="date">2012年04月25日</div>
<div class="maintxt">

5月13日（日）と5月26日（土）にオープンキャンパス開催決定しました！&nbsp;体験、個別相談会、キャンパスツアーと、今回も楽しんでためになるオープンキャンパスとなっています！&nbsp;5月13
…<a href="http://www.toyama-bb.ac.jp/news/-pc/513526.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-7.php">新年度オープンキャンパス開催！</a></h3>
<div class="date">2012年03月30日</div>
<div class="maintxt">

4月22日（日）にオープンキャンパスを開催します♪&nbsp;体験、在校生との座談会、個別相談会、キャンパスツアーと、今回も楽しんでためになるオープンキャンパスとなっています！&nbsp;体験は次の通
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-7.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/100.php">国家試験合格100％</a></h3>
<div class="date">2012年03月30日</div>
<div class="maintxt">

本日発表された第25回理容師・美容師国家試験において、
理容科合格率100％、美容科合格率100％（実技）を達成しました！
おめでとうございます！！
&nbsp;
試験対策にご協力いただいたみなさま、
…<a href="http://www.toyama-bb.ac.jp/news/-pc/100.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			<br>
			<div class="pagenav">
<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_12.php" class="link_page">12</a> ｜ <span class="current_page">13</span> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_14.php" class="link_page">14</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_15.php" class="link_page">15</a> ｜
</div>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>