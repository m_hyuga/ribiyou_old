<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>１０月２６日進学相談会　開催</h3>
            <time>2013年10月23日</time>
            <p>こんにちは！</p>
<p>富山県理容美容専門学校です。</p>
<p>&nbsp;</p>
<p>１０月２６日（土）に進学相談会を開催します。</p>
<p>&nbsp;</p>
<p>11月1日から一般入試の願書受付が始まります。</p>
<p>その直前の進学相談会です。　</p>
<p>&nbsp;</p>
<p>県理美の気になること、なんでも聞いちゃってください！</p>
<p>施設で見ておきたいところ、是非見てください！！</p>
<p>&nbsp;</p>
<p>学校説明、個別相談、キャンパスツアーが可能です。</p>
<p>お待ちしております。</p>
<p>&nbsp;</p>
<p>＜進学相談会＞</p>
<p>１０月２６日（土）　１０：００～（受付　９：３０～）</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index1026.html">http://toyama-bb.ac.jp/opencampus/form/index1026.html</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/1012.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>１０月２６日進学相談会　開催</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>