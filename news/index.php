<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>NEWS | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">NEWS</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content">
			<p>富山県理容美容専門学校の新着情報を次々と発信していきます！</p>
            
<ul>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-68.php">
<dl class="cf"><dd>2014.10.11</dd>
<dd class="newstitle">１０月１１日　　オープンキャンパス開催しました☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-67.php">
<dl class="cf"><dd>2014.10.01</dd>
<dd class="newstitle">指定校推薦入試　推薦入試　自己推薦入試　出願スタート</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/920.php">
<dl class="cf"><dd>2014.09.20</dd>
<dd class="newstitle">9/20  オープンキャンパス開催しました☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/11.php">
<dl class="cf"><dd>2014.09.10</dd>
<dd class="newstitle">通信課程　11期生卒業式☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/cat/96ao.php">
<dl class="cf"><dd>2014.09.06</dd>
<dd class="newstitle">9月6日　オープンキャンパス＆AO入試特別体験授業を開催しました☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-66.php">
<dl class="cf"><dd>2014.09.03</dd>
<dd class="newstitle">外部実践実習に行ってきました☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-65.php">
<dl class="cf"><dd>2014.09.03</dd>
<dd class="newstitle">みなさんに素敵な報告です☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/824.php">
<dl class="cf"><dd>2014.08.24</dd>
<dd class="newstitle">8月24日　　オープンキャンパス開催しました☆</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-64.php">
<dl class="cf"><dd>2014.08.22</dd>
<dd class="newstitle">通信課程・理容科向けにアレンジした「トータルシェービング」が始まりました。</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-63.php">
<dl class="cf"><dd>2014.08.06</dd>
<dd class="newstitle">オープンキャンパス開催しました☆</dd></dl>
</a></li>
<!–pagebute plugin–>
</ul>
<br>
<br>

<span class="current_page">1</span>｜<a href="http://www.toyama-bb.ac.jp/news/index_2.php" class="link_page">2</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_3.php" class="link_page">3</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_4.php" class="link_page">4</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a>

　<a href="http://www.toyama-bb.ac.jp/news/index_2.php" class="link_next">次のページへ&gt;&gt;</a>

<br>
<!–end page bute plugin–>


		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>