<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>竹　設置しました！</h3>
            <time>2013年07月03日</time>
            <p>皆様、こんにちは。こんばんは。</p>
<p>&nbsp;</p>
<p>正面玄関に<font style="FONT-SIZE: 1.25em"><strong>巨大笹</strong></font>が設置されました。</p>
<p>この感動！をできる限り早くお伝えしたく、ブログを書いている次第です。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0562-1.jpg"><img style="WIDTH: 467px; HEIGHT: 571px" class="mt-image-none" alt="IMG_0562-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0562-1-thumb-299x448-1122.jpg" width="299" height="448" /></a></span></p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.95em"><strong>どーん！！</strong></font></p>
<p>&nbsp;</p>
<p>と、県理美のお客様をお迎えいたします。</p>
<p>いらっしゃった皆様、ティアフォーレのお客様もぜひ、たんざくをお書きください。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0550-1.jpg"><img class="mt-image-none" alt="IMG_0550-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0550-1-thumb-339x336-1124.jpg" width="339" height="336" /></a></span></p>
<p>&nbsp;</p>
<p>幸せフワフワ～なお願いも、<font style="FONT-SIZE: 1.56em"><strong>ガチ</strong></font>なお願いも、なんでも<strong><font style="FONT-SIZE: 1.56em">アリ</font></strong>でございます。</p>
<p>お待ちしておりまーす☆</p>
<p>&nbsp;</p>
<p>7月7日オープンキャンパスお申込みはこちらから</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0707.html">http://toyama-bb.ac.jp/opencampus/form/index0707.html</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0562-1.jpg"></a></span></p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/77.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>竹　設置しました！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>