<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>◇◆謹賀新年◇◆</h3>
            <time>2014年01月06日</time>
            <p>新年　あけましておめでとうございます</p>
<p>&nbsp;</p>
<p>本日から　2014年の県理美がスタートしました。</p>
<p>&nbsp;</p>
<p>今年も皆さんの進路サポートをさせて頂くと共に、昨年より充実したたくさんの情報をこちらのブログで発信していきます。</p>
<p>&nbsp;</p>
<p>本年も県理美をよろしくお願い致します</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/119-1.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>◇◆謹賀新年◇◆</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>