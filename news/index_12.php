<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/css/normalize.css" />
<link rel="stylesheet" href="/css/css/top.css" />
<link rel="stylesheet" href="/css/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="news_pages">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-11.php">中島友康先生のデモンストレーションを動画で公開！</a></h3>
<div class="date">2012年06月13日</div>
<div class="maintxt">

6月に実施した、中島友康先生のセミナーをダイジェスト版で公開！【中島友康先生プロフィール】ＰＥＥＫ-Ａ-ＢＯＯに１３年間勤務め、トップスタイリストとなった後、ＮＹに渡る。&nbsp;Ｉｓｈｉｓａｌｏｎ
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-11.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-10.php">６月９日、６月２４日オープンキャンパス受付中</a></h3>
<div class="date">2012年06月05日</div>
<div class="maintxt">

オープンキャンパスの参加を受付中です。
お気軽にお申し込みください！
&nbsp;
６月９日　１つお選びください
理容・美容コース
エステ・ネイルコース(女性のみ）
&nbsp;
６月２４日　２つお選
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-10.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/69-2.php">6月9日オープンキャンパス申込みフォームに関して</a></h3>
<div class="date">2012年05月31日</div>
<div class="maintxt">

6月9日に開催するオープンキャンパスのお申込みフォームで不具合があり、ご迷惑をおかけしております。大変申し訳ございません。
現在、修正作業をすすめております。
修正されますまで、メール（←クリックして
…<a href="http://www.toyama-bb.ac.jp/news/-pc/69-2.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/69.php">6月9日（土）オープンキャンパス開催！</a></h3>
<div class="date">2012年05月23日</div>
<div class="maintxt">

6月9日(土)オープンキャンパス開催いたします☆今回の体験模擬授業は&nbsp;理容科＆美容科エステティック科＆ネイル科
の2択になっています。&nbsp;&nbsp;&nbsp;&nbsp;オープン
…<a href="http://www.toyama-bb.ac.jp/news/-pc/69.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-9.php">チャンピオンのデモンストレーションを動画で公開！</a></h3>
<div class="date">2012年05月18日</div>
<div class="maintxt">

4月に実施した第63回全国理容競技大会のチャンピオンをお迎えしたセミナーのダイジェスト版を動画で公開！富山県理容組合さんのご厚意で理容科・美容科の生徒も見学させていただきました。クラシカルカット、レデ
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-9.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			<br>
			<div class="pagenav">
<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a> ｜ <span class="current_page">12</span> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_13.php" class="link_page">13</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_14.php" class="link_page">14</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_15.php" class="link_page">15</a> ｜
</div>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>