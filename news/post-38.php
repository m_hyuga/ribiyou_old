<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>先輩から後輩へ・・・</h3>
            <time>2014年01月25日</time>
            みなさまこんにちは！<br />寒い毎日が続いていますね。インフルエンザも流行り始めているようで、体調の管理には十分お気を付けください☆<br /><br />先日オープンキャンパススタッフの新旧バトンタッチが行われました！<br /><br />母校をもっと素晴らしい学校にして欲しい、リーダーシップのとれる人材になって欲しい、など色々な思いを込めて先輩から後輩へ・・・県理美を愛する熱い思いが受け継がれました！<br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01668-thumb-500x666-1654.jpg"><img alt="DSC01668.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01668-thumb-500x666-1654-thumb-200x266-1655.jpg" class="mt-image-none" style="" height="266" width="200" /></a></span><br /><br />新制オープンキャンパススタッフに乞うご期待です☆<br />この新体制、体験せずにはいられない！ぜひ、オープンキャンパスにご参加下さい！！<br /><br />次回、開催のオープンキャンパスは<b><font style="font-size: 1.95312em;">３月１５日</font></b>です☆<br />　<br /><br /><br /><br /><br /><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-36.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>先輩から後輩へ・・・</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>