<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>富山第一高校★おめでとう！！</h3>
            <time>2014年01月14日</time>
            <p><font style="FONT-SIZE: 1.95em">　　<strong>富山第一高校の皆さん、</strong></font></p>
<p><font style="FONT-SIZE: 1.95em"><strong>　　　サッカー全国優勝おめでとうございます！！</strong></font></p>
<p>日本一、北陸勢初、すごいですねー。感動しました。</p>
<p>きっとここまでくるには、たくさんの辛い練習や、いろんな挫折を乗り越えてこられたことでしょう。</p>
<p>だからこそ、一番輝いた場所に辿り着けたのでしょうね。</p>
<p>私たちも来月には国家試験を控えています。富一イレブンに負けないくらいの気持ちと</p>
<p>練習量で望みたいと思います！！絶対に全員受かるぞ！！</p>
<p>&nbsp;</p>
<p>ちなみに・・・富山第一高校出身の在校生（↓）も、とっても喜んでいました♪</p>
<p><a href="http://www.toyama-bb.ac.jp/news/DSC01566.JPG"><img style="WIDTH: 624px; HEIGHT: 405px" class="mt-image-none" alt="DSC01566.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/01/DSC01566-thumb-700x525-1646.jpg" height="525" width="700" /></a></p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/post-35.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>富山第一高校★おめでとう！！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>