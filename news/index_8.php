<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>NEWS | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">NEWS</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content">
			<p>富山県理容美容専門学校の新着情報を次々と発信していきます！</p>
            
<ul>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/24-3.php">
<dl class="cf"><dd>2012.12.28</dd>
<dd class="newstitle">平成24年度　特待生発表!!</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-20.php">
<dl class="cf"><dd>2012.12.27</dd>
<dd class="newstitle">エステティック科　センター試験全員合格</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-19.php">
<dl class="cf"><dd>2012.12.03</dd>
<dd class="newstitle">１２月１５日　オープンキャンパス開催！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-16.php">
<dl class="cf"><dd>2012.12.03</dd>
<dd class="newstitle">レジーナ　フォトコンテスト　準入選</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/2012.php">
<dl class="cf"><dd>2012.11.19</dd>
<dd class="newstitle">理容師2012メッセージ全国大会　橋爪恵美教員　優勝！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-15.php">
<dl class="cf"><dd>2012.11.08</dd>
<dd class="newstitle">サロン室　リニューアル</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/25111.php">
<dl class="cf"><dd>2012.10.25</dd>
<dd class="newstitle">平成25年度生　一般入学願書受付　11月1日開始</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/sbs.php">
<dl class="cf"><dd>2012.10.19</dd>
<dd class="newstitle">神田SBSネイル指導講師作品掲載</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-14.php">
<dl class="cf"><dd>2012.10.18</dd>
<dd class="newstitle">サロン室　改装工事完了</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-13.php">
<dl class="cf"><dd>2012.10.09</dd>
<dd class="newstitle">サロン室　改装中</dd></dl>
</a></li>
<!–pagebute plugin–>
</ul>
<br>
<br>
 <a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_before">&lt;&lt前のページへ</a>　

<a href="http://www.toyama-bb.ac.jp/news/index.php" class="link_page">1</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_2.php" class="link_page">2</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_3.php" class="link_page">3</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_4.php" class="link_page">4</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a>｜<span class="current_page">8</span>｜<a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a>

　<a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_next">次のページへ&gt;&gt;</a>

<br>
<!–end page bute plugin–>


		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>