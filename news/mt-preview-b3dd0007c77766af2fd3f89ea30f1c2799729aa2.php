<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>県大会！優勝!!</h3>
            <time>2014年03月26日</time>
            3月24日、第67回富山県理容競技大会が行われました。<div><br /></div><div>理容科の1年生が出場！とても素晴らしい作品でした　o(^o^)o</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/052%20%282%29.JPG"><img alt="052 (2).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/052 (2)-thumb-250x187-1693.jpg" width="250" height="187" class="mt-image-none" /></a></span></div><div><br /></div><div>優勝　木本　貴子</div><div>２位　清水　理紗子</div><div>３位　安部　翔子</div><div>４位　中林　和沙</div><div>５位　堀田　慎一郎</div><div>入賞された皆さん、おめでとうございました!!</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/052%20%281%29.JPG"><img alt="052 (1).JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/052 (1)-thumb-200x150-1695.jpg" width="200" height="150" class="mt-image-none" /></a></span></div><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/o.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>県大会！優勝!!</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>