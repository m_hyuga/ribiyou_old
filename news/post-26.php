<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>チョキちゃん登場！</h3>
            <time>2013年07月05日</time>
            <p><font style="FONT-SIZE: 1.25em">みなさん</font>、こんにちは！</p>
<p>7月7日のオープンキャンパスの特別ゲスト、噂の<font style="FONT-SIZE: 1.25em">「チョキちゃん」</font>が、一足早く、県理美に遊びに来てくれました！</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0610-1.jpg"><img class="mt-image-none" alt="IMG_0610-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0610-1-thumb-448x299-1126.jpg" width="448" height="299" /></a></span></p>
<p>&nbsp;</p>
<p>こちらが、<font style="FONT-SIZE: 1.56em"><strong>チョキちゃん</strong></font>でーす。</p>
<p>&nbsp;</p>
<p>チョキちゃんは、理容店のシンボル・サインポールをイメージした、理容のマスコットなんです。</p>
<p>7月7日は、県理美のどこかにチョキちゃんがいるので、ぜひ探してスタンプもらってくださいね～！</p>
<p>一緒に写真とかですか？</p>
<p>「喜んで！！」（byチョキちゃん）</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0605-1.jpg"><img class="mt-image-none" alt="IMG_0605-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0605-1-thumb-448x299-1128.jpg" width="448" height="299" /></a></span></p>
<p>&nbsp;</p>
<p>チョキちゃんの願い事はなにかな～？</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>7月7日オープンキャンパスのお申込みはこちらから</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0707.html">http://toyama-bb.ac.jp/opencampus/form/index0707.html</a></p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-25.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>チョキちゃん登場！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>