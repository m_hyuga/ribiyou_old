<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ホイルワーク講習　第二富山支部 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
ホイルワーク講習　第二富山支部</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.04.02</time>
			<h2>ホイルワーク講習　第二富山支部</h2>
			<div class="news_area">
				先日、ＪＨＣＡ第二富山支部にて第4回ホイルワーク講習が行われました。<div>ホイルワークとは・・・カラー剤を塗る所、塗らない所を作り髪に立体感や、ツヤを持たせる技術です。<br /><div><br /></div><div>今回は名古屋から講師の<font style="font-size: 1.5625em;">谷本　育美先生</font>をお招きしてＪＨＣＡカラー検定に</div><div>基づく技術の確認とタイムトライアルを行いました！</div><div><br /></div><div>ホイルワークの解説から・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02973.JPG"><img alt="DSC02973.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02973-thumb-300x225-1699.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>実際にホイルワークをしていただき、ポイント解説もしていただきました☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02985.JPG"><img alt="DSC02985.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02985-thumb-300x225-1701.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>そして、実際にホイルワークでタイムトライアル☆タイムに入るため、</div><div>支部員の皆様も真剣な表情です！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02993.JPG"><img alt="DSC02993.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02993-thumb-300x225-1703.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>なんと！本校教員も講習に参加させていただき、これからの授業に向けて腕を磨きました☆</div><div><br /></div><div>本校ではこれからのカラー授業ではホイルワークや幅広いカラー技術や知識が学べる</div><div><br /></div><div>ようになるんです(^o^)丿　楽しみにしていてくださいね♪</div><div><br /></div><div><br /></div></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/100-1.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-48.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>