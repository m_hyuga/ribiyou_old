<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>平成２６年度　第1回目　オープンキャンパスを開催しました！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
平成２６年度　第1回目　オープ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.04.27</time>
			<h2>平成２６年度　第1回目　オープンキャンパスを開催しました！！</h2>
			<div class="news_area">
				みなさん、こんにちは！！<div><br /></div><div>本日、今年度初めてのオープンキャンパスを開催しました。</div><div><br /></div><div>たくさんの学生さんが参加してくださり、ワイワイ楽しくいろんな体験を行いました☆</div><div><br /></div><div>まずは、学校の授業や施設などの説明を聞いて・・・</div><div><br /></div><div>楽しいキャンパスライフにわくわく、胸を躍らせて</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03443.JPG"><img alt="DSC03443.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03443-thumb-300x225-1721.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>その後、いざ！各科のプロへの第1歩体験をしていただきました☆</div><div><br /></div><div>理容科は『カット』でちょきちょき♪プロ体験</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03448.JPG"><img alt="DSC03448.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03448-thumb-300x225-1723.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>美容科はふわふわカールで春の『ヘアアレンジ』体験♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03447.JPG"><img alt="DSC03447.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03447-thumb-300x400-1725.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div><br /></div><div>トータルビューティ科はこの春完成したぴかぴかのワークショップ"bloom"で『メイク』体験♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03458.JPG"><img alt="DSC03458.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03458-thumb-300x400-1727.jpg" width="300" height="400" class="mt-image-none" /></a></span></div><div><br /></div><div>エステティック科は夏まで待てない！『美脚エステ』体験♪</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC03457.JPG"><img alt="DSC03457.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC03457-thumb-300x225-1729.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>それぞれの科で特色のある体験をして、学生の皆さんは将来の自分と重ね合わせて</div><div><br /></div><div>わくわく♡ドキドキ大満足でした！体験後はもやもや不安や問題を解消するため、個別相談を</div><div><br /></div><div>おこないました。小さな疑問や不安も本校教員に気軽にご相談ください！私たちが必ず解決</div><div><br /></div><div>いたします(*^_^*)</div><div><br /></div><div>まだオープンキャンパスに参加されていない方、夢への第1歩を県理美でスタートしましょう！</div><div><br /></div><div>５月のオープンキャンパス（5月24日開催）でお待ちしております！！</div><div><font style="font-size: 1.5625em;"><br /></font></div><div><font style="font-size: 1.5625em;"><a href="http://www.toyama-bb.ac.jp/opencampus/" style="font-weight: bold;">お申し込みはこちら☆</a><b>　　</b><font style="font-size: 0.5120000000000001em;">お早目のご予約を・・・。</font></font></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-48.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/2.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>