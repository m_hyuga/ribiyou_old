<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>9月6日　オープンキャンパス＆AO入試特別体験授業を開催しました☆ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
9月6日　オープンキャンパス＆…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.09.06</time>
			<h2>9月6日　オープンキャンパス＆AO入試特別体験授業を開催しました☆</h2>
			<div class="news_area">
				皆さんこんにちは！暑さも和らぎ、少しずつ秋らしくなってきましたね。<div><br /></div><div>本日もオープンキャンパス、AO入試特別体験授業にはたくさんの方に参加頂きました♪</div><div><br /></div><div>まずは、オープンキャンパスの報告から・・・</div><div><br /></div><div>今回の体験内容は</div><div><br /></div><div><font style="font-size: 1.25em;">理容科　　カット（秋のショートスタイル）に挑戦(^O^)／</font></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06550.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06550.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">頑張って作った作品に大満足でパチリ☆彡</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06570.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06570.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.25em;">美容科　　　ワインディング(いろんな太さのパーマ巻き）に挑戦(^O^)</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06558.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06558.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">先輩にやさしく教えてもらい、楽しかった様子でパチリ☆彡</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06567.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06567.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.25em;">エステティック科　　　フットケア(角質除去＆マッサージ）を体験(^O^)／</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06538.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06538.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">乾燥知らずのすべすべフットケアで癒されて頂きました。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.25em;">トータルビューティ科　　　メイク(ブライダルメイク）を体験(^O^)／</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06543.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06543.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">CUTEな花嫁になれました♡パチリ☆彡</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06548.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06548.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">毎回たくさん参加していただき、とても嬉しいです☆</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">またワクワク楽しく、笑顔になれる体験を準備してお待ちしております(^.^)/~~~</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">そして、本日はオープンキャンパス終了後、AO入試特別授業体験も開催しました。</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">AO入試</span>特別授業体験とは、入学してからこれからどのような環境や雰囲気で</div><div>授業を受けるのかを知っていただく授業のことです。</div><div>本日受けていただいた学生さんも将来の自分の姿を想像して、真剣な表情で取り組んで</div><div>おられました。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06573.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06573.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06579.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06579.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06589.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06589.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC06598.JPG" src="http://www.toyama-bb.ac.jp/news/DSC06598.JPG" width="259" height="194" class="mt-image-none" /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">春から一緒にプロを目指して学べるのが楽しみです♪♪</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">まだまだこれからも、オープンキャンパスを開催いたします☆</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;">次回の開催は・・・</span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.5625em;"><b>9月20日　10:00～12:30<font style="font-size: 0.64em;">（受付9:30～）</font></b></font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.5625em;"><b><font style="font-size: 0.64em;"><br /></font></b></font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><font style="font-size: 1.953125em;">秋祭り開催＼(^o^)／</font></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><font style="font-size: 1.5625em;"><span class="mt-enclosure mt-enclosure-image" style="display: inline;">詳しくはこちらで確認！！→</span><a href="http://www.toyama-bb.ac.jp/opencampus/"><font style="font-size: 1.25em;"><b>今すぐクリック☆</b></font></a></font></div><div><br /></div><div><br /></div><div>＊＊＊オープンキャンパス以外にも、進学相談会も開催しています＊＊＊</div><div><br /></div><div>9月13日（土）・28日（日）　10:00～（受付9:30）</div><div><br /></div><div>AOエントリーの受付も行っていますのでぜひいらしてくださいね(^^♪</div><div><br /></div><div>お待ちしております♡</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><br /></span></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-66.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/11.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>