<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>6月9日（土）ｵｰﾌﾟﾝｷｬﾝﾊﾟｽ開催！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
6月9日（土）ｵｰﾌﾟﾝｷｬﾝ…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2012.05.23</time>
			<h2>6月9日（土）ｵｰﾌﾟﾝｷｬﾝﾊﾟｽ開催！</h2>
			<div class="news_area">
				<p style="font-size: small; font-family: 'MS PGothic'; ">6月9日（土）にｵｰﾌﾟﾝｷｬﾝﾊﾟｽを開催します♪</p><p style="font-size: small; font-family: 'MS PGothic'; ">&nbsp;</p><p style="font-size: small; font-family: 'MS PGothic'; ">体験､個別相談会､ｷｬﾝﾊﾟｽﾂｱｰと､今回も楽しんでためになるｵｰﾌﾟﾝｷｬﾝﾊﾟｽとなっています!</p><p style="font-size: small; font-family: 'MS PGothic'; ">&nbsp;</p><p style="font-size: small; font-family: 'MS PGothic'; ">今回の模擬授業体験は</p><p style="font-size: small; font-family: 'MS PGothic'; "><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 14px; background-color: rgba(255, 255, 255, 0.917969); ">理容科＆美容科　ﾈｲﾙ＆ｴｽﾃﾃｨｯｸ</span></p><p style="font-size: small; font-family: 'MS PGothic'; "><br /></p><p style="font-size: small; font-family: 'MS PGothic'; ">上記の1種類から選んでください！</p><p style="font-size: 13px; "></p><p style="font-size: small; font-family: 'MS PGothic'; ">&nbsp;</p><p style="font-size: small; font-family: 'MS PGothic'; ">&nbsp;</p><p style="font-size: small; font-family: 'MS PGothic'; ">オープンキャンパス終了しました！</p> 
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/69.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/69-2.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>