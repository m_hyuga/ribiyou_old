<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ニューヨークから☆中島先生来校！！ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
ニューヨークから☆中島先生来校…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.07.14</time>
			<h2>ニューヨークから☆中島先生来校！！</h2>
			<div class="news_area">
				<span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071410.DSC05767.JPG"><img alt="14071410.DSC05767.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071410.DSC05767-thumb-350x262-1814.jpg" width="350" height="262" class="mt-image-none" /></a></span><div><br /></div><div>ニューヨークで活躍中の中島友康先生が本校生徒のためにカットセミナーを</div><div><br /></div><div>開催してくださいました！！</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071410.DSC05761.JPG"><img alt="14071410.DSC05761.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071410.DSC05761-thumb-350x262-1816.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div>世界に通用するカット技術を見たり、海外でのお仕事についてたくさん語っていただきました。</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071411.DSC05778.JPG"><img alt="14071411.DSC05778.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071411.DSC05778-thumb-350x262-1818.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div>中でも、進化し続けることの大切さや努力することの意味を教えていただき、</div><div>クリエイティブなお仕事と熱い想いに生徒達は心大きく揺さぶられる感じがあったと思います。</div><div>素晴らしいショーを見せていただき、たくさん勉強することができました！</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div><div>また、今日は２年生の希望者がセミナースタッフとしてがんばってくれました。このように・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071409.DSC05727.JPG"><img alt="14071409.DSC05727.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071409.DSC05727-thumb-350x262-1820.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071409.DSC05747-thumb-250x187-1826.jpg"><img alt="14071409.DSC05747.JPGのサムネール画像" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071409.DSC05747-thumb-250x187-1826-thumb-350x261-1827.jpg" width="350" height="261" class="mt-image-none" /></a></span></div><div>スタッフになると、仕込みからショーまですべての裏側まで見ることができ、さらにたくさん</div><div>勉強ができます！！本校生徒ならだれでもチャンスがあるんですよ☆</div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071412.DSC05826.JPG"><img alt="14071412.DSC05826.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071412.DSC05826-thumb-350x262-1828.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div>中島先生、アシスタント、モデルの皆さんのおかげで素晴らしいショーになりました！</div><div>来年もまた・・・☆</div><div><br /></div><div><br /></div><div>そしてこの秋、中島先生監修のもとニューヨーク研修が始まります・・・</div><div><br /></div><div><font style="font-size: 1.25em;">見事！ニューヨークへの切符を手にした生徒の発表がありました☆</font></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/14071412.DSC05822.JPG"><img alt="14071412.DSC05822.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/07/14071412.DSC05822-thumb-350x262-1821.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div>橘　桃加さん、山本　琴乃さん、岩﨑　瑞華さん、中舘　沙也香さん、</div><div><br /></div><div>菓子　茉穂さん、岩瀧　晴香さんの６名が受賞しました！！</div><div><br /></div><div>ニューヨークでたくさんのことを体験してきて欲しいです☆</div><div><br /></div><div><br /></div><div><br /></div><div><br /></div>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-58.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-60.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>