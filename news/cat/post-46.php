<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>2014年4月27日 オープンキャンパス追加のお知らせ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
2014年4月27日 オープン…</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2014.03.28</time>
			<h2>2014年4月27日 オープンキャンパス追加のお知らせ</h2>
			<div class="news_area">
				







<p class="p1">県理美の特色や実習・授業がよくわかる！</p><p class="p1">美容の仕事、理容の仕事、エステやネイル・メイクの仕事に</p><p class="p1">









</p><p class="p1">少しでも興味があったら、お気軽にご参加ください。</p><p class="p1"><b><br /></b></p><p class="p1"><b>スタートの春！<br />
新しい県理美☆魅せちゃいます！<br /><br />■学校説明<br />■個別相談会<br />■模擬授業体験<br />理容：スーパーカット＆ブロー！ プロの入口体験しよう!!（カット）<br />美容：春のキュートなおでかけスタイル作っちゃおう♥ （スタイリング）<br />エステ：夏までにスッキリ美脚！！！<br />トータルビューティ：ナチュラルかわいい春メイクで大変身！ （メイク）<br />※体験内容は1つになります。<br />■キャンパスツアー<br /><br />受付場所：富山県理容美容専門学校 正面玄関<br />持ち物：上履き<br />受付時間：9：30<br />開始時間：10：00<br />終了時間：12：30<br /><br />お申し込みは<a href="http://www.toyama-bb.ac.jp/opencampus/form/index0427.html">こちら</a>から<br /><br /><br /><br /></b></p><p class="p1"><b><br /></b></p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-45.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/100-1.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>