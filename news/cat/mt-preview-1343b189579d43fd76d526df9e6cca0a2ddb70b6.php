<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/css/normalize.css" />
	<link rel="stylesheet" href="/css/css/top.css" />
	<link rel="stylesheet" href="/css/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="news_pages">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>ホイルワーク講習　第二富山支部</h3>
            <time>2014年04月02日</time>
            先日、ＪＨＣＡ第二富山支部にて第4回ホイルワーク講習が行われました。<div>ホイルワークとは・・・カラー剤を塗る所、塗らない所を作り髪に立体感や、ツヤを持たせる技術です。<br /><div><br /></div><div>今回は名古屋から講師の<font style="font-size: 1.5625em;">谷本　育美先生</font>をお招きしてＪＨＣＡカラー検定に</div><div>基づく技術の確認とタイムトライアルを行いました！</div><div><br /></div><div>ホイルワークの解説から・・・</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02973.JPG"><img alt="DSC02973.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02973-thumb-300x225-1699.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>実際にホイルワークをしていただき、ポイント解説もしていただきました☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02985.JPG"><img alt="DSC02985.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02985-thumb-300x225-1701.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>そして、実際にホイルワークでタイムトライアル☆タイムに入るため、</div><div>支部員の皆様も真剣な表情です！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02993.JPG"><img alt="DSC02993.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/04/DSC02993-thumb-300x225-1703.jpg" width="300" height="225" class="mt-image-none" /></a></span></div><div><br /></div><div>なんと！本校教員も講習に参加させていただき、これからの授業に向けて腕を磨きました☆</div><div><br /></div><div>本校ではこれからのカラー授業ではホイルワークや幅広いカラー技術や知識が学べる</div><div><br /></div><div>ようになるんです(^o^)丿　楽しみにしていてくださいね♪</div><div><br /></div><div><br /></div></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/100-1.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>ホイルワーク講習　第二富山支部</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>