<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>平成２３年度特待生　決定 | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next"><a href="/news/">NEWS</a></li>
				<li class="pankuzu_next">
平成２３年度特待生　決定</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content_page">
			<time>2011.12.26</time>
			<h2>平成２３年度特待生　決定</h2>
			<div class="news_area">
				<p>平成２３年度の特待生が決定いたしました。</p>
<p>本校では、学業成績ならびに日々の行いが特に優秀な学生を表彰する特待生制度を設け、意欲的に学ぼうとする学生を支援しています。 特待生に選ばれることは学生として栄誉なことです。 努力次第で誰にでもチャンスがあります。</p>
<p>●学業優秀賞...特に成績が優秀な生徒に学費の一部を免除する。<br class="firstChild lastChild empty" nodeindex="1" />●善行奨励賞...学業優秀で模範となる善行生徒に学費の一部を免除する。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_5976.JPG"><img class="mt-image-none" alt="IMG_5976.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2011/12/IMG_5976-thumb-200x114-257.jpg" width="200" height="114" /></a></span></p>
<p>&nbsp;</p>
<p>【学業優秀賞】</p>
<p>網谷美奈子（理容科２年）</p>
<p>板屋愛理（美容科２年）</p>
<p>浦﨑麻里子（美容科１年）</p>
<p>阿部芹香（美容科１年）</p>
<p>【善行奨励賞】</p>
<p>廣瀬正春（理容科２年）</p>
<p>板坂眞登香（理容科２年）</p>
<p>前川健斗（理容科２年）</p>
<p>跡治夏妃（美容科２年）</p>
<p>柴田奈実(美容科１年）</p>
<p>中川ちあき（美容科１年）</p>
<p>鈴木さつき（美容科１年）</p>
<p>向井実香（美容科１年）</p>
<p>山林優加（エステティック科）</p>
<p>中嶋　舞（ネイル･メイク科）</p>
<p>菅山萌希（ネイル・メイク科）</p>
<p>&nbsp;</p>
			</div>
			<div class="content-nav"><br><br><br>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-2.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/-pc/post-4.php">次の記事へ</a>
		
			</div>	
			<br>
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>