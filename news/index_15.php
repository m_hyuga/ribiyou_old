<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/css/normalize.css" />
<link rel="stylesheet" href="/css/css/top.css" />
<link rel="stylesheet" href="/css/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="news_pages">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-4.php">年末年始休業のお知らせ</a></h3>
<div class="date">2011年12月28日</div>
<div class="maintxt">

12月29日～1月3日は、事務窓口・電話受付を休止させていただきます。
この間にいただきました資料請求・メールでのお問い合わせにつきましては、1月4日以降に対応させていただきますので、何卒ご了承くださ
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-4.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-2.php">全国大会３位！と優秀賞！</a></h3>
<div class="date">2011年12月15日</div>
<div class="maintxt">

11月20日に愛知県体育館で開催された第3回日本理容美容学生技術大会　ミディアムカット（国家試験課題）において、
長谷川　洋さんが銅賞（第3位）を獲得しました！！同じ競技で、前川建斗さんが優秀賞を獲得
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-2.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/115.php">オープンキャンパス12月10日開催！</a></h3>
<div class="date">2011年11月30日</div>
<div class="maintxt">

今回は模擬授業体験です。
希望のコース（１つ）をたっぷりじっくり体験できます。
・理容科
　いろんなハサミを使ってカットしてみよう
・美容科
　基礎から学ぼうスタイリング
・エステティック科
　冬のセ
…<a href="http://www.toyama-bb.ac.jp/news/-pc/115.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/910.php">9月10日　オープンキャンパス開催</a></h3>
<div class="date">2011年09月06日</div>
<div class="maintxt">

体験は次の中から２つをお選びいただけます。
・ヘッドスパモデル体験「シャンプーでリラクゼーション」
・カット（理容）「はさみを使って体験してみよう」
・化粧品作成体験「作って体験！爪がツヤツヤ☆パラフ
…<a href="http://www.toyama-bb.ac.jp/news/-pc/910.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-1.php">第３回全国理容美容学生技術大会　出場者（信越北陸地区代表）決定！</a></h3>
<div class="date">2011年08月09日</div>
<div class="maintxt">

平成23年8月9日（火）、福井県にて第３回全国理容美容学生技術大会信越北陸地区予選大会が行われました。本校からは理容科5名、美容科20名の代表選手が出場し、全力を尽くしました。優秀賞の４名は、信越北陸
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-1.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			<br>
			<div class="pagenav">
<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_12.php" class="link_page">12</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_13.php" class="link_page">13</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_14.php" class="link_page">14</a> ｜ <span class="current_page">15</span> ｜
</div>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>