<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>いざ！成人式へ！！</h3>
            <time>2014年01月10日</time>
            みなさん、こんにちは！<br />いよいよ今週末はたくさんの地域で成人式が開催されます。街でも晴れ着姿の御嬢様方をたくさん見かけるかもしれませんね。<br /><br />それにしても、なぜだか着物姿って良いですよね。華やかさや、上品さなど洋服にはない魅力があります。そして特に髪型や帯結びなど、後姿が素敵だと思いませんか？！☆<br /><br />そこで！その晴れ着姿をさらに美しくするため、本校理容科の皆さんが今年成人する２学年を対象に、えりあしの産毛を剃る技術<font style="font-size: 1.5625em;">『えりそり』</font>をしてくれました♪♪<br />みんな綺麗になって大喜びでした！<br />新成人のみなさま、素晴らしい成人の日になりますように・・・☆<br /><br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC01530.JPG" src="http://www.toyama-bb.ac.jp/news/DSC01530.JPG" class="mt-image-none" style="" height="336" width="448" /></span><br /><br /><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><img alt="DSC01505.JPG" src="http://www.toyama-bb.ac.jp/news/DSC01505.JPG" class="mt-image-none" style="" height="448" width="336" /></span><br /><br /><div><br /></div><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-34.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/post-36.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>いざ！成人式へ！！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>