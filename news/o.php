<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>がんばりました!!ワインディングマラソン</h3>
            <time>2014年03月19日</time>
            3月18日（火）、熱い☆熱い戦いが繰り広げられました・・・<div><br /></div><div>美容科1年生によるワインディングマラソン！！！</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC_0062.JPG"><img alt="DSC_0062.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/DSC_0062-thumb-350x262-1683.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div>なんと！12頭分も巻いて＆外して、巻いて＆外して・・・の繰り返し</div><div>自分との戦いなのです（ﾟдﾟlll）</div><div><br /></div><div>これで1頭分です。たくさん巻いてあるでしょう。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/IMG_0884.JPG"><img alt="IMG_0884.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/IMG_0884-thumb-200x300-1681.jpg" width="200" height="300" class="mt-image-none" /></a></span></div><div><br /></div><div>でも、全員、やりきりました！！この達成感は忘れられない経験になったことでしょう♥</div><div><br /></div><div>上位入賞の皆さん！おめでとうございました!!</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/news/DSC02808.JPG"><img alt="DSC02808.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2014/03/DSC02808-thumb-350x262-1685.jpg" width="350" height="262" class="mt-image-none" /></a></span></div><div><br /></div><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/1.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>がんばりました!!ワインディングマラソン</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>