<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>NEWS | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">NEWS</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content">
			<p>富山県理容美容専門学校の新着情報を次々と発信していきます！</p>
            
<ul>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/218.php">
<dl class="cf"><dd>2012.02.10</dd>
<dd class="newstitle">入試相談会2月18日開催</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/24-1.php">
<dl class="cf"><dd>2012.01.10</dd>
<dd class="newstitle">平成24年度生　一般入試願書受付中です。</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-4.php">
<dl class="cf"><dd>2011.12.28</dd>
<dd class="newstitle">年末年始休業のお知らせ</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/cat/post-3.php">
<dl class="cf"><dd>2011.12.26</dd>
<dd class="newstitle">平成２３年度特待生　決定</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-2.php">
<dl class="cf"><dd>2011.12.15</dd>
<dd class="newstitle">全国大会３位！と優秀賞！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/115.php">
<dl class="cf"><dd>2011.11.30</dd>
<dd class="newstitle">オープンキャンパス12月10日開催！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/910.php">
<dl class="cf"><dd>2011.09.06</dd>
<dd class="newstitle">9月10日　オープンキャンパス開催</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-1.php">
<dl class="cf"><dd>2011.08.09</dd>
<dd class="newstitle">第３回全国理容美容学生技術大会　出場者（信越北陸地区代表）決定！</dd></dl>
</a></li>
<!–pagebute plugin–>
</ul>
<br>
<br>
 <a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_before">&lt;&lt前のページへ</a>　

<a href="http://www.toyama-bb.ac.jp/news/index.php" class="link_page">1</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_2.php" class="link_page">2</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_3.php" class="link_page">3</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_4.php" class="link_page">4</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a>｜<span class="current_page">11</span>

<br>
<!–end page bute plugin–>


		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>