<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>9月29日進学相談会　AO入試エントリー　ラストチャンス！</h3>
            <time>2013年09月21日</time>
            <p>9月29日に進学相談会を開催します。</p>
<p>&nbsp;</p>
<p>AO入試エントリーの　<font style="FONT-SIZE: 1.56em"><strong>ラストチャンス</strong></font>です。</p>
<p>AO入試を考えているアナタ！！</p>
<p>ぜひ！！お越しください。</p>
<p>&nbsp;</p>
<p>AO入試エントリー面談の他、個別相談、学校見学もすることができます。</p>
<p>お気軽にお問い合わせください。</p>
<p>お申込みはこちらから</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0929.html">http://toyama-bb.ac.jp/opencampus/form/index0929.html</a></p>
<p>&nbsp;</p>
<p>お待ちしております☆☆☆</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/921.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>9月29日進学相談会　AO入試エントリー　ラストチャンス！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>