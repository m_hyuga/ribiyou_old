<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>11月9日オープンキャンパス開催★</h3>
            <time>2013年11月05日</time>
            <h3>こんにちは！</h3>
<p>富山県理容美容専門学校です。</p>
<p>&nbsp;</p>
<p>11月9日（土）にオープンキャンパスを開催します。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>今回の体験は、</p>
<p><strong>理容科（バリカンアート）：アートの世界へ！ラインアートでLet's創作！</strong></p>
<p>あなたの自由な発想ですてきなアートを仕上げちゃおう（*^_^*）<br class="empty" nodeindex="3" /></p>
<p><strong>美容科（エクステ）：オシャレカラー☆エクステでプチ変身！<br class="empty" nodeindex="5" /></strong>編み込みで取り付けるから、自分で取れます。週明けの学校も大丈夫だよ(*^^)v</p>
<p>&nbsp;</p>
<p><strong>エステティック科（G5＋マッサージ(脚)）　：つぶして流してスラッとボディ！</strong></p>
<p>気になる脚のセルライト、この際つぶしてサヨナラしましょう！！！！！</p>
<p>&nbsp;</p>
<p><strong>トータルビューティ科（ハンドマッサージ）：癒しのアロマトリートメント</strong></p>
<p>お好きなアロマの香りで、ハンドマッサージ体験！！</p>
<p>&nbsp;</p>
<p>体験後はキャンパスツアー！！</p>
<p>施設で見ておきたいところ、是非見てください！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>お待ちしております❤</p>
<p>&nbsp;</p>
<p>＜オープンキャンパス＞</p>
<p>１１月９日（土）　１０：００～（受付　９：３０～）</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index1109.html">http://toyama-bb.ac.jp/opencampus/form/index1109.html</a></p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-32.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>11月9日オープンキャンパス開催★</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>