<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>7月7日オープンキャンパス開催しますよー！</h3>
            <time>2013年07月03日</time>
            <p>みなさん、こんにちは～！</p>
<p>今日は朝から強風ですね！みんなが家に帰るまで、どうか電車、とまらないで・・・・。</p>
<p>&nbsp;</p>
<p>7月7日10：00～　県理美オープンキャンパスでう。</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.53em">七夕</font>です。　なんとなく、　<font style="FONT-SIZE: 1.25em"><strong>ウキウキドキドキ❤</strong></font></p>
<p>&nbsp;</p>
<p>体験は、メンズコーンロウ、カット、エステ（脚ツルツル版）、チーク作成から１つ選んでください。</p>
<p>エステとチーク作成じゃ、もうすぐ定員です。お早めにお申込みください。</p>
<p>&nbsp;</p>
<p>体験後は、<font style="FONT-SIZE: 1.56em"><strong>県理美夏祭り</strong></font>です。</p>
<p>理容科は、「えりｏｒ眉シェービング」</p>
<p>夏女子の皆様！アップスタイルをすっきりキメたいですよね～。</p>
<p>な・の・で、うなじキレ～にシェービングしませんか？</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0011-2.JPG"><img class="mt-image-none" alt="IMG_0011-2.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0011-2-thumb-336x392-1114.jpg" width="336" height="392" /></a></span></p>
<p>&nbsp;</p>
<p>そして、夏男子の皆様！</p>
<p>キリッとした眉は、海でも山でもプールでも部活でも、夏は必須です！</p>
<p>この機会にぜひ、モテ眉になっちゃってください！</p>
<p>&nbsp;</p>
<p>美容科は、いろいろなスタイリングができる体験ブースです。</p>
<p>自分やお友達のヘアアレンジにバンバン使えます。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/013-1.JPG"><img class="mt-image-none" alt="013-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/013-1-thumb-448x244-1116.jpg" width="448" height="244" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>エステティック科は、エステ機器の体験です。</p>
<p>「余分な脂肪サヨナラ～」な～んていう実力派機器をはじめ、いろいろおもしろい機器あるんですよ～。</p>
<p>今回は、プチ体験なので、服の上からでも大丈夫です。ご安心を！</p>
<p>&nbsp;</p></p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_0646-1.JPG"><img class="mt-image-none" alt="IMG_0646-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/07/IMG_0646-1-thumb-310x448-1118.jpg" width="310" height="448" /></a></p>
<p>&nbsp;</p>
<p>ネイルは、&nbsp;</p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/bbt.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>7月7日オープンキャンパス開催しますよー！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>