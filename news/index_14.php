<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/css/normalize.css" />
<link rel="stylesheet" href="/css/css/top.css" />
<link rel="stylesheet" href="/css/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="news_pages">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-6.php">新パンフレット完成！</a></h3>
<div class="date">2012年03月26日</div>
<div class="maintxt">

平成24年度のパンフレットが完成しました！
&nbsp;


&nbsp;
中身は・・・・・・・・・・・開けてみてのお楽しみです!!
&nbsp;
ガイダンスや資料請求で「できたらお渡しします」とご案
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-6.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/post-5.php">春のオープンキャンパス開催！体験２つ選んでください。</a></h3>
<div class="date">2012年03月02日</div>
<div class="maintxt">

3月27日（火）にオープンキャンパスを開催します♪
&nbsp;
体験、在校生との座談会、個別相談会、キャンパスツアーと、今回も楽しんでためになるオープンキャンパスとなっています！
&nbsp;
体験
…<a href="http://www.toyama-bb.ac.jp/news/-pc/post-5.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/cat/24-2.php">平成24年度生　一般入試願書受付中です。</a></h3>
<div class="date">2012年02月13日</div>
<div class="maintxt">

一般入試（第３次募集）の願書受付中です。
願書提出にあたり、ご質問がございましたら、ご遠慮なく当校までお問合せ下さい。
日程は下記の通りです。
&nbsp;
願書受付　平成24年2月13日～2月24日
…<a href="http://www.toyama-bb.ac.jp/news/cat/24-2.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/218.php">入試相談会2月18日開催</a></h3>
<div class="date">2012年02月10日</div>
<div class="maintxt">

入試相談会を下記の日程で行います。
4月からの進路がまだ決まっていないという方、理容・美容が気になる、ネイル･メイク･エステがやっぱり心のどこかにひっかかっているという方、お待ちしています！
&nbs
…<a href="http://www.toyama-bb.ac.jp/news/-pc/218.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			
			<div class="section1">
<h3><a href="http://www.toyama-bb.ac.jp/news/-pc/24-1.php">平成24年度生　一般入試願書受付中です。</a></h3>
<div class="date">2012年01月10日</div>
<div class="maintxt">

一般入試（第2次募集）の願書受付中です。
願書提出にあたり、ご質問がございましたら、ご遠慮なく当校までお問合せ下さい。
日程は下記の通りです。
&nbsp;
願書受付　平成24年１月10日～１月27日
…<a href="http://www.toyama-bb.ac.jp/news/-pc/24-1.php">続きはこちら</a>			
</div>
<!--/#section1--></div>
			
			<br>
			<div class="pagenav">
<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_10.php" class="link_page">10</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_12.php" class="link_page">12</a> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_13.php" class="link_page">13</a> ｜ <span class="current_page">14</span> ｜ <a href="http://www.toyama-bb.ac.jp/news/index_15.php" class="link_page">15</a> ｜
</div>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>