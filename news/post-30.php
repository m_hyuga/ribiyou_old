<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>指定校推薦入試　推薦入試　自己推薦入試　出願スタート</h3>
            <time>2013年10月02日</time>
            <p><font style="FONT-SIZE: 1.25em">指定校推薦入試、推薦入試、自己推薦入試の出願を受け付けています。</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆指定校推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成25年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　　書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　　平成25年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成25年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　 面接、書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">試験日　　　　　平成25年10月21日</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　 平成25年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">◆自己推薦入試</font></p>
<p><font style="FONT-SIZE: 1.25em">願書受付期間　平成25年10月1日～15日　※必着</font></p>
<p><font style="FONT-SIZE: 1.25em">選考方法　　　 面接、書類審査</font></p>
<p><font style="FONT-SIZE: 1.25em">試験日　　　　　平成25年10月21日</font></p>
<p><font style="FONT-SIZE: 1.25em">合格発表　　　 平成25年10月23日</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">詳しくは</font><a href="http://www.toyama-bb.ac.jp/admission/daytime.php"><font style="FONT-SIZE: 1.25em">募集要項</font></a><font style="FONT-SIZE: 1.25em">をご覧ください。</font></p>
<p><font style="FONT-SIZE: 1.25em"></font>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em">パンフレット・募集要項のご請求は</font><a href="http://www.toyama-bb.ac.jp/inquiry/"><font style="FONT-SIZE: 1.25em">こちら</font></a><font style="FONT-SIZE: 1.25em">から</font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/929ao.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>指定校推薦入試　推薦入試　自己推薦入試　出願スタート</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>