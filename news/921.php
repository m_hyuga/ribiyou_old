<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>9月21日オープンキャンパス開催のお知らせ☆</h3>
            <time>2013年09月18日</time>
            <p>こんにちは！</p>
<p>富山県理容美容専門学校です。</p>
<p>&nbsp;</p>
<p>空気もちょっぴり涼しくなって、秋の気配が感じられる今日この頃です。</p>
<p>県理美オープンキャンパスは9月も絶好調で開催いたします！！</p>
<p>&nbsp;</p>
<p>9月21日の体験は・・・</p>
<p>理容科：シェービング</p>
<p>ついについにトータルジョブサロン（仮称）での体験です❤</p>
<p>&nbsp;</p>
<p>美容科：ヘアチョーク</p>
<p>レディーガガも使ったというヘアチョークを使ってみよー！！</p>
<p>&nbsp;</p>
<p>エステティック科：サクション</p>
<p>スペシャル機器で、あなたのむくみ、スッキリしちゃいましょう！</p>
<p>&nbsp;</p>
<p>トータルビューティ科：ジェルネイル</p>
<p>サロンで大流行のジェルネイル！つやっつやを体験してみて！！</p>
<p>&nbsp;</p>
<p>恒例のデモンストレーションは、ついに技術者で生徒が登場！！</p>
<p>テーマは「秋のカジュアルスタイル」です。</p>
<p>どんどんマネして、お出かけしたくなっちゃいますよ～(^o^)</p>
<p>女の子２人と男の子１人の作品が仕上がります。</p>
<p>今日もせっせと準備しているところを見かけました。</p>
<p>お楽しみに！！！</p>
<p>&nbsp;</p>
<p>９月２１日もAO入試のエントリーのチャンスです。</p>
<p>エントリー面談を受けることが可能です。</p>
<p>&nbsp;</p>
<p>９月２１日オープンキャンパスお申込みはこちらから</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index0921.html">http://toyama-bb.ac.jp/opencampus/form/index0921.html</a></p>
<p>&nbsp;</p>
<p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_2041-1.JPG"><img class="mt-image-none" alt="IMG_2041-1.JPG" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/09/IMG_2041-1-thumb-550x376-1373.jpg" width="550" height="376" /></a></p>
<p><strong><font style="FONT-SIZE: 1.56em">お待ちしてま～す☆&nbsp;</font></strong></p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-29.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>9月21日オープンキャンパス開催のお知らせ☆</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>