<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/news/images/title01.png" alt="お知らせ" /></h2>
            <h3>10月12日オープンキャンパス開催★</h3>
            <time>2013年10月07日</time>
            <p>お待たせしました！</p>
<p>10月のオープンキャンパスは12日（土）10：00～です。</p>
<p>&nbsp;</p>
<p>今回のオープンキャンパステーマはハロウィンです。</p>
<p>&nbsp;</p>
<p>県理美全体がハロウィン仕様になっていますよ～。</p>
<p>教員デモンストレーションも、ハロウィンをテーマに行います☆</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>そして～、今回の体験は～？</p>
<p>&nbsp;</p>
<p>理容科：あなたのパッションを活かして作品を作りましょう！！</p>
<p>美容科：県理美の最新シャンプー台を使ってみよう！！</p>
<p>エステティック科：むくみ、さらにスッキリ！！</p>
<p>トータルビューティ科：真似❤メイク！！あのモデルになっちゃおう！</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/news/img/IMG_2690-1.jpg"><img class="mt-image-none" alt="IMG_2690-1.jpg" src="http://www.toyama-bb.ac.jp/news/assets_c/2013/10/IMG_2690-1-thumb-448x299-1427.jpg" width="448" height="299" /></a></p>
<p>
<p>&nbsp;</p>
<p></p>
<p>
<p>美容科の「最新シャンプー台」は　↑　こちらです。</p>
<p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>お申込みはこちらから～</p>
<p><a href="http://toyama-bb.ac.jp/opencampus/form/index1012.html">http://toyama-bb.ac.jp/opencampus/form/index1012.html</a></p>
<p>&nbsp;</p>
<p>お待ちしております☆★☆★☆</p>
<p></p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/news/-pc/post-30.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/news/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>お知らせ</li>
				<li>10月12日オープンキャンパス開催★</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>