<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>NEWS | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".news_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="news";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/news/img_main.jpg" alt="NEWS" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">NEWS</li>
			</ul>
		</div>
		<div id="mainarea" class="news_content">
			<p>富山県理容美容専門学校の新着情報を次々と発信していきます！</p>
            
<ul>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/526.php">
<dl class="cf"><dd>2012.05.14</dd>
<dd class="newstitle">5月26日(土)オープンキャンパス開催！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/513.php">
<dl class="cf"><dd>2012.04.26</dd>
<dd class="newstitle">5月13日(日)オープンキャンパス詳細</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/cat/513526-1.php">
<dl class="cf"><dd>2012.04.25</dd>
<dd class="newstitle">5/13（日）5/26（土）ｵｰﾌﾟﾝｷｬﾝﾊﾟｽ開催</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/513526.php">
<dl class="cf"><dd>2012.04.25</dd>
<dd class="newstitle">5/13(日）5/26（土）オープンキャンパス開催！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/cat/post-8.php">
<dl class="cf"><dd>2012.04.13</dd>
<dd class="newstitle">新年度ｵｰﾌﾟﾝｷｬﾝﾊﾟｽ開催!</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-7.php">
<dl class="cf"><dd>2012.03.30</dd>
<dd class="newstitle">新年度オープンキャンパス開催！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/100.php">
<dl class="cf"><dd>2012.03.30</dd>
<dd class="newstitle">国家試験合格100％</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-6.php">
<dl class="cf"><dd>2012.03.26</dd>
<dd class="newstitle">新パンフレット完成！</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/-pc/post-5.php">
<dl class="cf"><dd>2012.03.02</dd>
<dd class="newstitle">春のオープンキャンパス開催！体験２つ選んでください。</dd></dl>
</a></li>
<!–pagebute plugin–><!–end pagebute plugin–>

<li class="news"><a href="http://www.toyama-bb.ac.jp/news/cat/24-2.php">
<dl class="cf"><dd>2012.02.13</dd>
<dd class="newstitle">平成24年度生　一般入試願書受付中です。</dd></dl>
</a></li>
<!–pagebute plugin–>
</ul>
<br>
<br>
 <a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_before">&lt;&lt前のページへ</a>　

<a href="http://www.toyama-bb.ac.jp/news/index.php" class="link_page">1</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_2.php" class="link_page">2</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_3.php" class="link_page">3</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_4.php" class="link_page">4</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_5.php" class="link_page">5</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_6.php" class="link_page">6</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_7.php" class="link_page">7</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_8.php" class="link_page">8</a>｜<a href="http://www.toyama-bb.ac.jp/news/index_9.php" class="link_page">9</a>｜<span class="current_page">10</span>｜<a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_page">11</a>

　<a href="http://www.toyama-bb.ac.jp/news/index_11.php" class="link_next">次のページへ&gt;&gt;</a>

<br>
<!–end page bute plugin–>


		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>