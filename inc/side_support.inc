<dl id="subNavi" class="cf">
	<dt><a href="/support/"><img src="/images/support/subnavi_support.png" alt="学費サポート" /></a></dt>
	<dd <?php if($pagename=='system'){ echo 'class="currentDir"'; }?>>
		<a href="/support/system.php"><img src="/images/support/subnavi_support01.png" alt="学費支援制度" /></a>
	</dd>
	<dd <?php if($pagename=='exemption'){ echo 'class="currentDir"'; }?>>
		<a href="/support/exemption.php"><img src="/images/support/subnavi_support02.png" alt="授業料免除制度" /></a>
	</dd>
	<dd <?php if($pagename=='loan'){ echo 'class="currentDir"'; }?>>
		<a href="/support/loan.php"><img src="/images/support/subnavi_support03.png" alt="教育ローン" /></a>
	</dd>
	<dd <?php if($pagename=='winner'){ echo 'class="currentDir"'; }?>>
		<a href="/support/winner.php"><img src="/images/support/subnavi_support04.png" alt="特待生受賞者" /></a>
	</dd>
</dl>