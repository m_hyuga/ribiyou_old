
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40732470-1', 'toyama-bb.ac.jp');
  ga('send', 'pageview');

</script>
<header>
	<hgroup class="cf">
		<h1 id="siteTitle"><a href="/"><img src="/images/top/sitetitle.png" alt="学校法人富山県理容美容学校 富山県理容美容専門学校 TOYAMA Barber &amp; Beauty School" /></a></h1>
		<ul id="headNavi">
			<li><img src="/images/top/freedial.png" alt="0120-190-135 入学案内専用ダイヤル" /></li>
			<li><a href="/inquiry/" id="contact" target="_blank"><img src="/images/top/contact.png" alt="資料のご請求・お問合わせ" /></a></li>
		</ul>
	</hgroup>
	<nav>
		<ul>
			<li>
				<a href="/school/"><img src="/images/top/gnav1.png" alt="学校案内" /></a>
			</li>
			<li><a href="/course/"><img src="/images/top/gnav2.png" alt="学科紹介" /></a></li>
			<li><a href="/campuslife/"><img src="/images/top/gnav3.png" alt="キャンパスライフ" /></a></li>
			<li><a href="/employment/"><img src="/images/top/gnav4.png" alt="就職支援" /></a></li>
			<li><a href="/admission/"><img src="/images/top/gnav5.png" alt="入学案内" /></a></li>
			<li><a href="/access/"><img src="/images/top/gnav6.png" alt="アクセス" /></a></li>
		</ul>
	</nav>
</header>
