<dl id="subNavi" class="cf">
	<dt><a href="/course/index.php"><img src="../images/course/subnavi_course.png" alt="学科紹介" /></a></dt>
	<dd <?php if($pagename=='barber'){ echo 'class="currentDir"'; }?>>
		<a href="/course/barber.php"><img src="../images/course/subnavi_course01.png" alt="理容科" /></a>
		<?php if($pagename=='barber'){ echo
		'<ul>
			<li><a href="#curriculum">カリキュラム</a></li>
			<li><a href="#qual">資格</a></li>
			<li><a href="#senior">先輩の声</a></li>
		</ul>';
		} ?>
	</dd>
	<dd <?php if($pagename=='beauty'){ echo 'class="currentDir"'; }?>>
		<a href="/course/beauty.php"><img src="../images/course/subnavi_course02.png" alt="美容科" /></a>
		<?php if($pagename=='beauty'){ echo
		'<ul>
			<li><a href="#curriculum">カリキュラム</a></li>
			<li><a href="#qual">資格</a></li>
			<li><a href="#senior">先輩の声</a></li>
		</ul>';
		} ?>
	</dd>
	<dd <?php if($pagename=='esthetic'){ echo 'class="currentDir"'; }?>>
		<a href="/course/esthetic.php"><img src="../images/course/subnavi_course03.png" alt="エステティック科" /></a>
		<?php if($pagename=='esthetic'){ echo
		'<ul>
			<li><a href="#curriculum">カリキュラム</a></li>
			<li><a href="#qual">資格</a></li>
			<li><a href="#senior">先輩の声</a></li>
		</ul>';
		} ?>
	</dd>
	<dd <?php if($pagename=='total'){ echo 'class="currentDir"'; }?>>
		<a href="/course/total.php"><img src="../images/course/subnavi_course04.png" alt="トータルビューティ科" /></a>
		<?php if($pagename=='total'){ echo
		'<ul>
			<li><a href="#curriculum">カリキュラム</a></li>
			<li><a href="#qual">資格</a></li>
			<li><a href="#senior">先輩の声</a></li>
		</ul>';
		} ?>
	</dd>
	<dd <?php if($pagename=='correspondence'){ echo 'class="currentDir"'; }?>>
		<a href="/course/correspondence.php"><img src="../images/course/subnavi_course05.png" alt="通信課程" /></a>
	</dd>
</dl>