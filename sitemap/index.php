<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>サイトマップ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="sitemap";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/sitemap/img_main.jpg" alt="サイトマップ" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">サイトマップ</li>
			</ul>
		</div>
		<div id="mainarea" class="sitemap_content">
		
			<div class="sitemap_left">
				<ul>
					<li class="sitemap_cap"><a href="/school/">学校案内</a></li>
					<li><a href="/school/history.php">学校の歴史</a></li>
					<li><a href="/school/facility.php">施設・設備</a></li>
				</ul>
				
				<hr>
				<ul>
					<li class="sitemap_cap"><a href="/course/">学校紹介</a></li>
					<li><a href="/course/barber.php">理容科</a></li>
					<li><a href="/course/beauty.php">美容科</a></li>
					<li><a href="/course/esthetic.php">エステティック科</a></li>
					<li><a href="/course/total.php">トータルビューティー科</a></li>
					<li><a href="/course/correspondence.php">通信課程</a></li>
				</ul>
				
				<hr>
				<ul>
					<li class="sitemap_cap"><a href="/admission/">入学案内</a></li>
					<li><a href="/admission/daytime.php">昼間課</a></li>
					<li><a href="/admission/correspondence.php">通信課程</a></li>
					<li><a href="/admission/tuition.php">学費</a></li>
				</ul>
				
				<hr>
				<ul>
					<li class="sitemap_cap"><a href="/support/">学費サポート</a></li>
					<li><a href="/support/system.php">学費支援制度</a></li>
					<li><a href="/support/exemption.php">授業料免除制度</a></li>
					<li><a href="/support/loan.php">教育ローン</a></li>
					<li><a href="/support/winner.php">特待生受賞者</a></li>
					<li><a href="/support/hojo.php">家賃補助制度</a></li>
				</ul>
			</div>

			<div class="sitemap_right">
				<ul>
					<li class="sitemap_cap"><a href="/campuslife/">キャンパスライフ</a></li>
					<li class="sitemap_cap"><a href="/opencampus/">オープンキャンパス</a></li>
					<li class="sitemap_cap"><a href="/employment/">就職支援</a></li>
					<li class="sitemap_cap"><a href="/access/">アクセス</a></li>
					<li class="sitemap_cap"><a href="/mailmagazine/">メールマガジン</a></li>
					<li class="sitemap_cap"><a href="/inquiry/">お問い合せ・資料請求</a></li>
					<li class="sitemap_cap"><a href="/sitemap/">サイトマップ</a></li>
				</ul>
				
				<hr>
				<ul>
					<li><a href="/placement/">採用担当の方へ</a></li>
					<li><a href="/parents/">保護者の方へ</a></li>
					<li><a href="/student/">在校生の方へ</a></li>
					<!--<li><a href="/recruit/">教員募集</a></li>-->
				</ul>
			</div>
			
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>