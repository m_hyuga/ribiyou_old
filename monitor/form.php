<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>お申し込み | エステティック無料モニター大募集!! | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/inquiry.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" href="/inquiry/mfp.statics/mailformpro.css" type="text/css" />
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/monitor/img_main.jpg" alt="エステティック 無料モニター大募集！！" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
                <li class="pankuzu_next"><a href="/monitor/">エステティック無料モニター大募集!!</a></li>
				<li class="pankuzu_next">お申し込み</li>
			</ul>
		</div>
		<div id="mainarea" class="inquiry_content">
		
			<p>エステティック無料モニターへのお申し込みはこちらのフォームより受け付けております。</p>
			<p><span>＊</span>が付いている項目は必ずご記入ください。</p>
			
			<form id="mailformpro" action="/inquiry/mailformpro/mailformpro_monitor.cgi" method="POST">
			<table>
			<tr>
				<th>お名前<span>＊</span></th>
				<td><input type="text" name="お名前" class="size_m" required="required" /></td>
			</tr>
			
			<tr>
				<th>年齢<span>＊</span></th>
				<td><input type="text" name="年齢" class="size_s" required="required" /> 歳</td>
			</tr>
			
			<tr>
				<th>郵便番号<span>＊</span></th>
				<td><input type="text" name="郵便番号" onKeyUp="AjaxZip3.zip2addr('郵便番号','','都道府県','住所');" class="size_s" required="required" /></td>
			</tr>
			<tr>
				<th>都道府県<span>＊</span></th>
				<td>
				<select name="都道府県" required>
				<option value="北海道">北海道</option>
				<option value="青森県">青森県</option>
				<option value="岩手県">岩手県</option>
				<option value="宮城県">宮城県</option>
				<option value="秋田県">秋田県</option>
				<option value="山形県">山形県</option>
				<option value="福島県">福島県</option>
				<option value="茨城県">茨城県</option>
				<option value="栃木県">栃木県</option>
				<option value="群馬県">群馬県</option>
				<option value="埼玉県">埼玉県</option>
				<option value="千葉県">千葉県</option>
				<option value="東京都">東京都</option>
				<option value="神奈川県">神奈川県</option>
				<option value="新潟県">新潟県</option>
				<option value="富山県" selected="selected">富山県</option>
				<option value="石川県">石川県</option>
				<option value="福井県">福井県</option>
				<option value="山梨県">山梨県</option>
				<option value="長野県">長野県</option>
				<option value="岐阜県">岐阜県</option>
				<option value="静岡県">静岡県</option>
				<option value="愛知県">愛知県</option>
				<option value="三重県">三重県</option>
				<option value="滋賀県">滋賀県</option>
				<option value="京都府">京都府</option>
				<option value="大阪府">大阪府</option>
				<option value="兵庫県">兵庫県</option>
				<option value="奈良県">奈良県</option>
				<option value="和歌山県">和歌山県</option>
				<option value="鳥取県">鳥取県</option>
				<option value="島根県">島根県</option>
				<option value="岡山県">岡山県</option>
				<option value="広島県">広島県</option>
				<option value="山口県">山口県</option>
				<option value="徳島県">徳島県</option>
				<option value="香川県">香川県</option>
				<option value="愛媛県">愛媛県</option>
				<option value="高知県">高知県</option>
				<option value="福岡県">福岡県</option>
				<option value="佐賀県">佐賀県</option>
				<option value="長崎県">長崎県</option>
				<option value="熊本県">熊本県</option>
				<option value="大分県">大分県</option>
				<option value="宮崎県">宮崎県</option>
				<option value="鹿児島県">鹿児島県</option>
				<option value="沖縄県">沖縄県</option>
				</select>
				</td>
			</tr>
			<tr>
				<th>住所<span>＊</span></th>
				<td><input type="text" name="住所" class="size_l" required="required" /></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><input type="text" name="電話番号" class="size_m" /></td>
			</tr>
			<tr>
				<th>携帯電話</th>
				<td><input type="text" name="携帯電話" class="size_m" /></td>
			</tr>
			<tr>
				<th>メールアドレス<span>＊</span></th>
				<td><input type="email" name="email" data-type="email" data-parent="mailfield" class="size_m" required="required" /></td>
			</tr>
			
			
			<tr> 
				<th>ご質問など</th>
				<td>
				<textarea name="ご質問など"></textarea>
				</td>
			</tr>
			</table>
			<p class="btn_inquiry">
			<input type="submit" alt="確認画面へ" width="154" height="52"  class="over" />
			</p>
			</form>
			<script type="text/javascript" id="mfpjs"  src="/inquiry/mailformpro/mailformpro_monitor.cgi" charset="UTF-8"></script>
			
			</div>
		</div><!-- /display_none end -->
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>