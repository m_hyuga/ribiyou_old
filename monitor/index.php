<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>エステティック無料モニター大募集!! | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<link rel="stylesheet" type="text/css" href="/common/css/colorbox.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.colorbox-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".opencampus_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/monitor/img_main.jpg" alt="エステティック 無料モニター大募集！！" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">エステティック無料モニター大募集!!</li>
			</ul>
		</div>
		<div id="mainarea" class="monitor_content">
		<img src="/common/images/monitor/ttl_img.jpg" alt="エステティック 無料モニター大募集！！" />
		<p class="btn_area"><a href="/course/esthetic.php#monitor_jump" ><img src="/common/images/monitor/btn_shosai.png" alt="サロン実習の詳細はこちら" class="over" /></a></p>
		<h2><img src="/common/images/monitor/ttl_merit.png" alt="メリット" /></h2>
		<p>学内エステティックサロンtea foret で人気のフェイシャルエステティックコース（スタンダードコース）と<br>
			同内容の施術サービスを無料でご提供。 </p>
		<p class="monitor_pbox">※専科学生は、本校エステティック科1年を修了し、<br>
			認定エステティシャンの資格を持った学生ですので安心して施術をお受けいただけます。 <br>
			また、施術時はエステティック科教員又は、サロンエステティシャンが立ち合わせていただきます。 </p>

		<h2><img src="/common/images/monitor/ttl_joken.png" alt="お申込み条件" /></h2>
		<p>エステティックモニターへのご応募は、以下の項目をご理解いただける方に限らせていただきます。<br>
			ご応募いただく前に、必ずご確認くださいますようお願いします。 </p>
		<ul class="monitor_list">
			<li><span>女性の方限定です。</span></li>
			<li><span>専科実習生のサロン実習(実務実習)へご理解・ご協力をいただける方に限ります。 </span></li>
			<li><span>tea foret を初めてご利用いただく方に限ります。</span></li>
			<li><span>平日（月～金曜日、祝日除く）午前10時から午後3時頃まで、tea foretへご来店いただける方に限ります。</span></li>
			<li><span>ご来店日時はご相談とさせていただきます。当日の空き状況等でお客様のご希望に添えない場合がございますので予めご了承ください。</span></li>
			<li><span>無料モニターは予告なく終了する場合がございます。</span></li>
		</ul>
		
		<div class="monitor_box">
			<h3><img src="/common/images/monitor/ttl_moshikomi.jpg" alt="お問い合わせ・モニターへの申し込み方法" /></h3>
			<p>
			エステティック無料モニター希望の方は以下のメールフォームから<br>
			<strong>お名前・住所・電話番号・メールアドレス</strong>をご明記の上、<br>
			お申込みください。<br>
			折り返し、運営側より返信いたします。
			</p>
			<img src="/common/images/monitor/img_arrow.png" alt="" /><br>
			<a href="form.php"><img src="/common/images/monitor/btn_toform.png" alt="無料モニター申し込みフォーム" class="over" /></a>

		</div>

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>