<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>平成26年度　受賞者表彰</h3>
            <time>2014年12月26日</time>
            <p>今年度、大会等で入賞した生徒たちがお披露目されました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00385-1.JPG"><img class="mt-image-none" alt="DSC00385-1.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00385-1-thumb-300x224-2043.jpg" width="300" height="224" /></a></span></p>
<p>&nbsp;</p>
<p>【第6回全国理容美容学生技術大会】</p>
<p>理容　ネイル部門　優秀賞　　鈴木　詩穂子</p>
<p>美容　カット部門　優秀賞　　　小林　憲之亮</p>
<p>&nbsp;</p>
<p>【理容北陸三県大会】</p>
<p>ワインディング部門　　　　3位　　上田　遼平</p>
<p>　　　　　　　　　　　　　　　　敢闘賞　　清水　俊輔</p>
<p>&nbsp;</p>
<p>ミディアムカット部門　　　優勝　 中林　和沙</p>
<p>　　　　　　　　　　　　　　　　3位　　鶴見　賢太</p>
<p>　　　　　　　　　　　　　　　　敢闘賞　安部　翔子</p>
<p>&nbsp;</p>
<p>【第11回レジーナフォトコンペティション】</p>
<p>準入選　　中林　彩香</p>
<p>敢闘賞　　森沢　翔</p>
<p>&nbsp;</p>
<p>おめでとうございます！！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-158.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>平成26年度　受賞者表彰</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>