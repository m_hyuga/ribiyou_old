<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>北陸ヘアーフェスティバル優勝!!</h3>
            <time>2014年12月01日</time>
            12月1日（月）ボルファート富山（富山市）にて北陸ヘアーフェスティバルが開催されました。<div>富山・石川・福井からプロの理容師と理容師を目指す学生が集まり、各競技が行われました。</div><div>本校からは理容科の1年生がワインディング競技に出場!!</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09920-1983.php" onclick="window.open('http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09920-1983.php','popup','width=490,height=367,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09920-thumb-350x262-1983.jpg" width="350" height="262" alt="DSC09920.jpg" class="mt-image-center" style="text-align: left; display: block; margin: 0px auto 20px;" /></a></span></div><div>理容科の2年生はミディアムカット競技に出場しました☆</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09950-1986.php" onclick="window.open('http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09950-1986.php','popup','width=392,height=294,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09950-thumb-300x225-1986.jpg" width="300" height="225" alt="DSC09950.jpg" class="mt-image-center" style="text-align: center; display: block; margin: 0 auto 20px;" /></a></span></div><div><font style="font-size: 1.953125em;">なんと!!!5名も入賞(^O^)</font></div><div><font style="font-size: 1.5625em;">ミディアムカット競技1位　中林和沙さん</font></div><div><font style="font-size: 1.5625em;">ミディアムカット競技3位　鶴見賢太さん</font></div><div><font style="font-size: 1em;"><span style="font-size: 1.5625em;">ミディアムカット競技5位　安部翔子さん</span></font></div><div><font style="font-size: 1.5625em;">ワインディング競技3位　上田遼平さん</font></div><div><font style="font-size: 1.5625em;">ワインディング競技4位　清水俊輔さん</font></div><div><br /></div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09959-1989.php" onclick="window.open('http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09959-1989.php','popup','width=490,height=367,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09959-thumb-350x262-1989.jpg" width="350" height="262" alt="DSC09959.jpg" class="mt-image-center" style="text-align: center; display: block; margin: 0 auto 20px;" /></a></span></div><div>美容科の2年生がデモンストレーションとしてホイルワークを披露しました。</div><div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09919-1992.php" onclick="window.open('http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09919-1992.php','popup','width=471,height=317,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09919-thumb-350x235-1992.jpg" width="350" height="235" alt="DSC09919.jpg" class="mt-image-center" style="text-align: center; display: block; margin: 0 auto 20px;" /></a></span></div><div>ホイルワークとはヘアスタイルを立体的に魅力的に見せるカラーリングのテクニックです☆</div><div>「学生がここまでできるとは！」と会場でも大注目でした</div><div><br /></div><div><br /></div><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-155.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-157.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>北陸ヘアーフェスティバル優勝!!</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>