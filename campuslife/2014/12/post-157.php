<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>レイヤーブロー実習</h3>
            <time>2014年12月25日</time>
            <p>理容科・美容科2年生がレイヤーブローの実習をおこないました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC06956.JPG"><img class="mt-image-none" alt="DSC06956.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC06956-thumb-300x224-2009.jpg" width="300" height="224" /></a></span></p>
<p>まずは、美容科国家試験課題でもあるレイヤーカットです。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC09231.JPG"><img class="mt-image-none" alt="DSC09231.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09231-thumb-300x224-2011.jpg" width="300" height="224" /></a></span></p>
<p>レイヤーカットをしたウイッグにパーマをかけました。</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC09852.JPG"></a></span>
<p><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC09853.JPG"><img class="mt-image-none" alt="DSC09853.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09853-thumb-300x225-2025.jpg" width="300" height="225" /></a></p>
<p>&nbsp;</p>
<p></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC09859.JPG"><img class="mt-image-none" alt="DSC09859.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09859-thumb-250x187-2027.jpg" width="250" height="187" /></a></span>　　　　 
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC09854.JPG"><img class="mt-image-none" alt="DSC09854.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC09854-thumb-250x187-2029.jpg" width="250" height="187" /></a></span>　　　<a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC05356.JPG"></a></p>
<p>パーマをかけたら、次はホイルワークでカラーリングを施します。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00330.JPG"></a>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00325.JPG"><img class="mt-image-none" alt="DSC00325.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00325-thumb-250x187-2031.jpg" width="250" height="187" /></a></span>　　　<a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00332.JPG"><img class="mt-image-none" alt="DSC00332.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00332-thumb-250x187-2019.jpg" width="250" height="187" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00331.JPG"><img class="mt-image-none" alt="DSC00331.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00331-thumb-150x200-2033.jpg" width="150" height="200" /></a></span>　　　ブローで仕上げです。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00445.JPG"></a></span>
<p><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00450.JPG"><img style="WIDTH: 252px; HEIGHT: 350px" class="mt-image-none" alt="DSC00450.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00450-thumb-300x414-2035.jpg" width="300" height="414" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00460.JPG"><img class="mt-image-none" alt="DSC00460.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/DSC00460-thumb-300x224-2037.jpg" width="300" height="224" /></a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://www.toyama-bb.ac.jp/campuslife/img/DSC00444.JPG"></a></p>
<p>
<p>&nbsp;</p>
<p>完成です。</p>
<p></p>
<p>１つのウイッグにいろいろな技術がぎゅーーーーっと詰まっています。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-156.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-158.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>レイヤーブロー実習</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>