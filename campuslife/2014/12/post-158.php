<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>特待生決定！</h3>
            <time>2014年12月26日</time>
            <p>平成26年度特待生が発表されました！</p>
<p>&nbsp;</p>
<p>学業成績ならびに秘儀の行いが特に優秀な生徒を表彰する制度です。意欲的に学ぼうとする生徒を支援しています。努力次第で誰にでもチャンスがあります。</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"><strong>＜学業優秀賞＞</strong></font></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/ga.JPG"><font style="FONT-SIZE: 1.25em"><strong><img class="mt-image-none" alt="ga.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/ga-thumb-300x225-2039.jpg" width="300" height="225" /></strong></font></a></span></p>
<p>学業優秀賞は、特に成績が優秀な生徒に　学費の一部を免除します。</p>
<p>&nbsp;</p>
<p>駒方　文香（富山県立高岡西高等学校　出身）</p>
<p>黒部　てのり（富山国際大学附属高等学校　出身）</p>
<p>林　奈々花（富山県立伏木高等学校　出身）</p>
<p>下林　幸恵（富山県立富山北部高等学校　出身）</p>
<p>五十嵐　美穂（中越高等学校（新潟県）　出身）</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><font style="FONT-SIZE: 1.25em"><strong>＜善行奨励賞＞</strong></font></p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/zenkou.JPG"><img class="mt-image-none" alt="zenkou.JPG" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/12/zenkou-thumb-300x225-2041.jpg" width="300" height="225" /></a></span></p>
<p>善行奨励賞は、学業優秀で模範となる善行生徒に学費の一部を免除する。</p>
<p>&nbsp;</p>
<p>猛尾　茜（富山県立富山いずみ高等学校　出身）</p>
<p>上里　ともよ（富山県立桜井高等学校　出身）</p>
<p>京谷　友里加（富山県立小杉高等学校　出身）</p>
<p>上森　なつ美（富山県立氷見高等学校　出身）</p>
<p>有坂　南珠（富山県立桜井高等学校　出身）</p>
<p>野村　唯（富山県立水橋高等学校　出身）</p>
<p>窪田　弥々（富山県立泊高等学校　出身）</p>
<p>辻谷　優玖（富山県立志貴野高等学校　出身）</p>
<p>坂高　優希（富山県立伏木高等学校　出身）</p>
<p>恩田　千春（富山県立滑川高等学校　出身）</p>
<p>眞田　裕里（富山県立富山商業高等学校　出身）</p>
<p>鈴木　詩穂子（富山県立滑川高等学校　出身）</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-157.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/26.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>特待生決定！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>