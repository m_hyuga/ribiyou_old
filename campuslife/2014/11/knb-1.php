<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>いっちゃんKNB出演しました！</h3>
            <time>2014年11月24日</time>
            11月24日のいっちゃんKNBに本校の鈴木詩穂子さん、木本貴子さん、牧野志保さん、舟邊美都さんが出演しました～(^o^)<div><span class="mt-enclosure mt-enclosure-image" style="display: inline;"><a href="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/11/IMG_20141124_153250-1978.php" onclick="window.open('http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/11/IMG_20141124_153250-1978.php','popup','width=4128,height=2336,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/11/IMG_20141124_153250-thumb-300x169-1978.jpg" width="300" height="169" alt="IMG_20141124_153250.jpg" class="mt-image-center" style="text-align: center; display: block; margin: 0 auto 20px;" /></a></span><div>全国理容美容学生技術大会の入賞と12月14日に行われるガールズアップフェスタの告知におじゃましました！ガールズアップフェスタでは、本校の学生がネイルのブースを担当します(*^_^*)ぜひお立ち寄りください！</div></div><div><br /></div>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no5.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/2014/12/post-155.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>いっちゃんKNB出演しました！</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>