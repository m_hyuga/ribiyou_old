<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/page.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<!-- InstanceEndEditable -->
<link href="http://www.toyama-bb.ac.jp/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="http://www.toyama-bb.ac.jp/css/common.css" rel="stylesheet" type="text/css" media="all">
<!-- InstanceBeginEditable name="page_css" -->
<link href="http://www.toyama-bb.ac.jp/css/page02.css" rel="stylesheet" type="text/css" media="all">
<!-- InstanceEndEditable -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("jquery", "1.2.6");</script>
<script src="http://hokuseisns.sakura.ne.jp/toyama-bb/js/smartRollover.js"></script>
<!--[if IE 6]>
<script src="http://www.toyama-bb.ac.jp/js/DD_belatedPNG.js"></script>
<script>
DD_belatedPNG.fix('img, .png_bg');
</script>
<![endif]-->
<script type="text/javascript">
$(document).ready(function(){
	$(function() { 
		$('.hover_img')
		.hover(
			function() {$(this).stop(true, false).animate({	opacity: .5	}, { duration: 100 }) }, 
			function() {$(this).stop(true, false).animate({	opacity: 1 }, {	duration: 100 }) }
		);
	});
});
</script>




<link rel="prev" href="http://www.toyama-bb.ac.jp/campuslife/2014/01/" title="2014&amp;#24180;1&amp;#26376;" />
<link rel="next" href="http://www.toyama-bb.ac.jp/campuslife/2014/11/" title="2014&amp;#24180;11&amp;#26376;" />

<link href="http://www.toyama-bb.ac.jp/css/page01.css" rel="stylesheet" type="text/css" media="all">
<title>2014&#24180;10&#26376;一覧｜イベントレポート｜キャンパスライフ｜富山県理容美容専門学校｜富山の理容・美容・エステティック・ネイル・メイクの専門学校</title>

</head>

<a name="top"></a>
<div id="Wrapper">
	<div id="Inner">
		<div id="Header" class="clearfix">
			<h1><a href="http://www.toyama-bb.ac.jp"><img src="http://www.toyama-bb.ac.jp/images/common/header/logo.jpg" alt="富山県理容美容専門学校" width="215" height="42"></a></h1>
			<div class="right">
				<ul>
                                <li><img src="http://www.toyama-bb.ac.jp/images/common/header/tel.jpg" alt="0120-190-135" width="307" height="25" class="MR15"></li>
					<li><a href="http://www.toyama-bb.ac.jp/inquiry/material.html"><img src="http://www.toyama-bb.ac.jp/images/common/header/img01_off.jpg" alt="資料請求" width="127" height="25" class="MR5"></a></li>
					<li><img src="http://www.toyama-bb.ac.jp/images/common/header/img02_off.jpg" alt="お問い合わせ" width="127" height="25"></li>
					<br clear="all">
				</ul>
			</div>
		</div>
		<div id="Nav">
			<ul>
                        <li><a href="http://www.toyama-bb.ac.jp/course/barber.html"><img src="http://www.toyama-bb.ac.jp/images/common/nav/nav01_off.jpg" width="176" height="50"></a></li>
                        <li><a href="http://www.toyama-bb.ac.jp/course/beauty.html"><img src="http://www.toyama-bb.ac.jp/images/common/nav/nav02_off.jpg" width="175" height="50"></a></li>
                        <li><a href="http://www.toyama-bb.ac.jp/course/esthetic.html"><img src="http://www.toyama-bb.ac.jp/images/common/nav/nav03_off.jpg" width="175" height="50"></a></li>
                        <li><a href="http://www.toyama-bb.ac.jp/course/nail.html"><img src="http://www.toyama-bb.ac.jp/images/common/nav/nav04_off.jpg" width="174" height="50"></a></li>
                        <li><a href="http://www.toyama-bb.ac.jp/admission/guidelines01.html"><img src="http://www.toyama-bb.ac.jp/images/common/nav/nav05_off.jpg" alt="募集要項" width="175" height="50"></a></li>
				<br clear="all">
			</ul>
		</div>
<div id="Contents">
			<div id="Crumbs"> <a href="http://www.toyama-bb.ac.jp/index.html">HOME</a>　＞　キャンパスライフ イベントレポート</div>
<div id="SideSection"><img src="http://www.toyama-bb.ac.jp/images/common/side/header.jpg" width="200" height="21">
<div class="SideNav">
<ul>
<li><a href="http://www.toyama-bb.ac.jp/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav01.jpg" alt="home" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/school/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav02.jpg" alt="学校案内" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/course/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav03.jpg" alt="学科紹介" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/campuslife/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav04.jpg" alt="キャンパスライフ" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/support/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav05.jpg" alt="就職支援" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/admission/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav06.jpg" alt="入学案内" width="180" height="40"></a></li>
<li><a href="http://www.toyama-bb.ac.jp/access/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/nav07.jpg" alt="アクセス" width="180" height="40"></a></li>
</ul>
</div>
<img src="http://www.toyama-bb.ac.jp/images/common/side/footer.jpg" width="200" height="20"> <a href="http://www.toyama-bb.ac.jp/mb/index.php"><img src="http://www.toyama-bb.ac.jp/images/common/side/bnr_mobile.jpg" alt="モバイルサイト" width="200" height="83" class="MT5"></a> <a href="http://www.toyama-bb.ac.jp/inquiry/mailmagazine.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/bnr_mail.jpg" alt="メールマガジン" width="200" height="83" class="MT5"></a> <a href="http://www.toyama-bb.ac.jp/recruit/index.html"><img src="http://www.toyama-bb.ac.jp/images/common/side/bnr_kyouin.jpg" alt="教員募集" width="200" height="58" class="MT5"></a> <a href="http://www.toyama-bb.ac.jp/recruit/index.html#esthetician"><img src="http://www.toyama-bb.ac.jp/images/common/side/bnr_esthe.jpg" alt="エステティシャン募集" width="200" height="58" class="MT5"></a></div>

<div id="Section">
				<div id="PageArea">
					<div class="Contents">
						<div class="ContentsWrapper"> 
							<h3 class="MB20"><img src="http://www.toyama-bb.ac.jp/images/campuslife/title01.jpg" alt="キャンパスライフ" width="700" height="41"></h3>
							<div id="main_area_01">


<div class="section1">






<select onChange="nav(this)">
<option>月を選択http://www.toyama-bb.ac.jp.</option>

<option value="http://www.toyama-bb.ac.jp/campuslife/2014/11/">2014&#24180;11&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2014/10/">2014&#24180;10&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2014/01/">2014&#24180;1&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/12/">2013&#24180;12&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/11/">2013&#24180;11&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/10/">2013&#24180;10&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/09/">2013&#24180;9&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/08/">2013&#24180;8&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/07/">2013&#24180;7&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/06/">2013&#24180;6&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/05/">2013&#24180;5&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/04/">2013&#24180;4&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/03/">2013&#24180;3&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/02/">2013&#24180;2&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2013/01/">2013&#24180;1&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/12/">2012&#24180;12&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/11/">2012&#24180;11&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/10/">2012&#24180;10&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/09/">2012&#24180;9&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/08/">2012&#24180;8&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/07/">2012&#24180;7&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/06/">2012&#24180;6&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/05/">2012&#24180;5&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/04/">2012&#24180;4&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/03/">2012&#24180;3&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/02/">2012&#24180;2&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2012/01/">2012&#24180;1&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/12/">2011&#24180;12&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/11/">2011&#24180;11&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/10/">2011&#24180;10&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/09/">2011&#24180;9&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/08/">2011&#24180;8&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/07/">2011&#24180;7&#26376;</option>



<option value="http://www.toyama-bb.ac.jp/campuslife/2011/06/">2011&#24180;6&#26376;</option>

</select>

<script type="text/javascript" charset="utf-8">
/*<![CDATA[ */
function nav(sel) {
 if (sel.selectedIndex == -1) return;
 var opt = sel.options[sel.selectedIndex];
 if (opt && opt.value)
location.href = opt.value;
}
/* ]]> */
</script>




<div class="pagenav">
<span class="current_page">1</span> ｜
</div>



<ul>
<li class="campuslife_ivent_title"><a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no5.php">ニューヨーク研修★速報No.5</a>　2014.10.03</li>
<li class="campuslife_ivent_content">本日はニューヨーク観光Day！！
&nbsp;
まずは、MoMA美術館（ニューヨーク近代美術館）で現代アートに触れました。
ステキすぎます！！
http://youtu.be/7D_oekpilBQ
<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no5.php">続きはこちら</a></li>
</ul><br />



<ul>
<li class="campuslife_ivent_title"><a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/post-154.php">ニューヨーク研修★速報No.4</a>　2014.10.03</li>
<li class="campuslife_ivent_content">この日はサロンで研修させていただきました！
生徒たちは、「本っっ当に楽しかった！！！」とみんな大興奮だったようです。
&nbsp;
サロンでの研修１　⇒　http://youtu.be/QgBj_HW<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/post-154.php">続きはこちら</a></li>
</ul><br />



<ul>
<li class="campuslife_ivent_title"><a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no3.php">ニューヨーク研修★速報No.3</a>　2014.10.02</li>
<li class="campuslife_ivent_content">カットのレクチャーを受けました❗今年の7月に、本校にて講習していただいた中島友康先生(ニューヨーク在住)が今回のニューヨーク研修をバックアップしてくださっていますが、このカット講習でも身振り手振りを交<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no3.php">続きはこちら</a></li>
</ul><br />



<ul>
<li class="campuslife_ivent_title"><a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no2.php">ニューヨーク研修★速報No.2</a>　2014.10.02</li>
<li class="campuslife_ivent_content">今日は、なんと!!!ニューヨークのサロンで実務体験!サロンの様子はこちらから→サロンの様子★ドキドキしますが、こんなチャンス、なかなか体験できません❗がんばってきます!!!<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/no2.php">続きはこちら</a></li>
</ul><br />



<ul>
<li class="campuslife_ivent_title"><a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/post-153.php">ニューヨーク研修始まりました!!</a>　2014.10.01</li>
<li class="campuslife_ivent_content">今年度、選ばれた6名のNY研修生がついに!!ニューヨーク到着－!!!さて、今日はサロンで勉強会です。研修の様子はコチラから→http://youtu.be/IzGmkgp0gXgデモンストレーションを<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/post-153.php">続きはこちら</a></li>
</ul><br />



</table>
								<!--/#section1-->
							</div>
							</div>
					</div>
				</div>
				<div class="PageTop"> <a href="#top"><img src="http://www.toyama-bb.ac.jp/images/common/parts/pagetop.jpg" width="107" height="23" class="hover_img"></a> </div>
				<br clear="all">
			</div>
			<br clear="all">
		</div>


<div id="Footer">
			<div id="FooterNav"> <img src="http://www.toyama-bb.ac.jp/images/common/footer/header.jpg" width="960" height="31">
				<div class="FooterNav clearfix">
					<div class="Fnav">
						<ul>
							<li><a href="http://www.toyama-bb.ac.jp/index.html">トップページ</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/inquiry/material.html">資料請求</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/inquiry/index.html">お問い合わせ</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/access/index.html">アクセス</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/sitemap/index.html">サイトマップ</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/opencampus2012/index.html">オープンキャンパス</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/student/index.html">在校生の皆さまへ</a></li>
						</ul>
					</div>
					<div class="Fnav">
						<ul>
							<li><a href="http://www.toyama-bb.ac.jp/school/index.html">学校案内</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/school/index.html">特徴ダイジェスト</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/school/history.html">開校60年以上の歴史</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/school/facilities.html">最先端の施設・設備</a></li>
						</ul>
					</div>
					<div class="Fnav">
						<ul>
                                                <li><a href="http://www.toyama-bb.ac.jp/course/index.html">学科紹介</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/course/barber.html">理容科</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/course/beauty.html">美容科</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/course/esthetic.html">エステティック科</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/course/nail.html">ネイル・メイク科</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/course/other.html">通信課程について</a></li>
						</ul>
					</div>
					<div class="Fnav">
						<ul>
							<li><a href="http://www.toyama-bb.ac.jp/campuslife/index.html">キャンパスライフ</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/campuslife/index.html">年間行事</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/campuslife/report/index.html">イベントレポート</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/support/index.html">就職支援</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/support/qualification.html">資格に強い</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/support/recruitment.html">就職に強い</a></li>
						</ul>
					</div>
					<div class="Fnav">
						<ul>
							<li><a href="http://www.toyama-bb.ac.jp/admission/index.html">入学案内</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/index.html">入学までの流れ</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/guidelines01.html">募集事項</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/guidelines02.html">入試要項</a></li>
							<li><a href="http://hokuseisns.sakura.ne.jp/toyama-bb/admission/tuition.html">学費一覧</a></li>
						</ul>
					</div>
					<div class="Fnav">
						<ul>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/index.html">学費サポート</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/index.html">学費支援制度</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/original.html">ｵﾘｼﾞﾅﾙ学費ｻﾎﾟｰﾄ制度</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/lawn.html">教育ローン一覧</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/exemption.html">授業料免除制度</a></li>
							<li><a href="http://www.toyama-bb.ac.jp/admission/support/comment.html">特待生コメント</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="Address">
				<h1><img src="http://www.toyama-bb.ac.jp/images/common/footer/logo.jpg" width="178" height="32"></h1>
				〒930-0804　富山県富山市下新町32-26　tel.076-432-3037　fax.076-432-3046<br>
				<span class="F10">CopyrightⓒTOYAMA Barber & Beauty School all rights reserved.</span> </div>
		</div>
	</div>
</div>

</body>
</html>
