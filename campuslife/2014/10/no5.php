<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>富山県理容美容専門学校</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/normalize.css" />
	<link rel="stylesheet" href="/top.css" />
	<link rel="stylesheet" href="/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body id="pagetop">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/header.inc"); ?>
	<div id="mainContent" class="cf">
		<article class="">
			<section class="cf">
			<h2><img src="/images/campuslife/title02.png" alt="イベントレポート" /></h2>
            <h3>ニューヨーク研修★速報No.5</h3>
            <time>2014年10月03日</time>
            <p>本日はニューヨーク観光Day！！</p>
<p>&nbsp;</p>
<p>まずは、MoMA美術館（ニューヨーク近代美術館）で現代アートに触れました。</p>
<p>ステキすぎます！！</p>
<p><a href="http://youtu.be/7D_oekpilBQ" target="_blank">http://youtu.be/7D_oekpilBQ</a><br /></p>
<p>&nbsp;</p>
<p>刺激されたニューヨーク研修生も、「アートになってみました！」</p>
<p><a href="http://youtu.be/qg380CeS6I4" target="_blank">http://youtu.be/qg380CeS6I4</a><br /></p>
<p>&nbsp;</p>
<p>ハイラインというかつて運搬用に使用されていた高架橋を改修して作られた空中公園を散策♪</p>
<p><a href="http://youtu.be/HnHwFOdmA2Q" target="_blank">http://youtu.be/HnHwFOdmA2Q</a><br /></p>
<p>&nbsp;</p>
<p>このあと、ギャラリー見学をして、中島先生のサロンも見学させていただきました。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>そして夜は</p>
<p>&nbsp;</p>
<p>
<span style="DISPLAY: inline" class="mt-enclosure mt-enclosure-image"><a href="http://www.toyama-bb.ac.jp/campuslife/img/IMG_4288-1.jpg"><img class="mt-image-none" alt="IMG_4288-1.jpg" src="http://www.toyama-bb.ac.jp/campuslife/assets_c/2014/10/IMG_4288-1-thumb-300x400-1936.jpg" width="300" height="400" /></a></span></p>
<p>&nbsp;</p>
<p>アラジンのミュージカルを見ましたよ～(*^^)v</p>
<p>&nbsp;</p>
<p>本当に、ステキで活動的な街です！</p>
<p>&nbsp;</p>
            <p>
			<a href="http://www.toyama-bb.ac.jp/campuslife/2014/10/post-154.php">前の記事へ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/">トップ</a>　｜　<a href="http://www.toyama-bb.ac.jp/campuslife/2014/11/knb-1.php">次の記事へ</a>
			
			</p>
            </section>
		</article>
		<aside>
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/sidebnr.inc"); ?>
		</aside>
	</div>
	<footer>
		<nav class="cf">
			<ul>
				<li><a href="/"><img src="/images/top/icon_home.png" alt="HOME" /></a></li>
				<li>イベントレポート</li>
				<li>ニューヨーク研修★速報No.5</li>
			</ul>
			<p><a href="#pagetop">Pagetop</a></p>
		</nav>
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/inc/footer.inc"); ?>
	</footer>
</body>
</html>