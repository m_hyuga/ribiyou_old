<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>トータルシェービング | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<link rel="stylesheet" type="text/css" href="/common/css/colorbox.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.colorbox-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".opencampus_sec_02 dd a").colorbox({inline:true, width:"480px"});
});
 </script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop" class="bg_fff">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/shaving/img_main.jpg" alt="トータルシェービング" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="mainarea" class="shaving_content">
			<p class="center"><img src="/common/images/shaving/ico_hart01.png" /></p>
			
			<section class="sec_01">
				<h2><img src="/common/images/shaving/ttl_01.png" /></h2>
				<img src="/common/images/shaving/img_01_01.jpg" />
				<p>
				剃るというイメージはもう過去のもの。<br>
				県理美・理容科で学ぶのは、素肌を美しく輝かせる魔法のようなスキンケアシェービングです。<br>
				話題のブライダルシェービングにも完全対応。その高度な技術は、どこまでも優しく、そして繊細。<br>         
				お肌のカウンセリングやスキンケアの技術と知識もしっかり身につきます。
				</p>
				<p class="center"><img src="/common/images/shaving/img_01_02.jpg" /></p>
			</section>
			<p class="center"><img src="/common/images/shaving/ico_hart02.png" /></p>
			
			
			
			
			<section class="sec_02">
				<h2><img src="/common/images/shaving/ttl_02.png" /></h2>
				<img src="/common/images/shaving/img_02_01.jpg" />
				<img src="/common/images/shaving/img_02_02.jpg" />
				<img src="/common/images/shaving/img_02_03.jpg" />
				<img src="/common/images/shaving/img_02_04.jpg" />
				<p>
				襟足(えりあし)、首筋、耳、鼻、瞼(まぶた)、顎(あご)、そして、眉。<br>  
				特に眉をデザインするにはシェービングが理想的。抜くときの「痛い」という感覚は、シェービング<br>  
				にはまったく無縁です。痛みを伴わずに眉に絶妙のカーブと陰影をデザインできます。<br>  
				また、背中や腕、指先まで輝かせるのがトータルシェービング。ブライダルシェービングに求められ<br>  
				る技術のすべてが身につきます。
				</p>
			</section>
			<p class="center"><img src="/common/images/shaving/ico_hart03.png" /></p>
			
			
			
			
			<section class="sec_03">
				<h2><img src="/common/images/shaving/ttl_03.png" /></h2>
				<img src="/common/images/shaving/img_03_01.jpg" />
				<img src="/common/images/shaving/img_03_02.jpg" />
				<img src="/common/images/shaving/img_03_03.jpg" />
				<p>
				TとSは、Total Shaving、Top Specialist、Toyama Styleの頭文字です。<br> 
				トータルシェービングをマスターして、目指すはスペシャリストの頂点。このトータルシェービング<br> 
				を学べるのは全国でも富山県理容美容専門学校だけ。つまりトヤマスタイル、というわけです。<br> 
				TとSには県理美・理容科のシェービングへの熱いハートがこもっています。
				</p>
			</section>
			<p class="center"><img src="/common/images/shaving/ico_hart04.png" /></p>
			
			
			
			
			<section class="sec_04">
				<h2><img src="/common/images/shaving/ttl_04.png" /></h2>
				<p>
				県理美・理容科のトータルシェービ<br> 
				ングは、メイクを美しく際立たせま<br> 
				す。顔ならワントーン、背中なら<br> 
				ツートン明るくなります。<br> 
				メイクのノリもスムーズになり、<br> 
				肌がいっそう輝くようになるのが<br> 
				シェービングのマジック。<br> 
				トータルシェービングの実力、ま<br> 
				ずはメイクで試してみてください。
				</p>
				<ul>
					<li><img src="/common/images/shaving/img_04_01.jpg" /></li>
					<li><img src="/common/images/shaving/img_04_02.jpg" /></li>
				</ul>
				<h3><img src="/common/images/shaving/txt_04.png" /></h3>
			</section>
			<p class="center"><img src="/common/images/shaving/ico_hart05.png" /></p>
			
			
			
			
			<section class="sec_05">
				<h2><img src="/common/images/shaving/ttl_05.png" /></h2>
				<p>
				県理美・理容科のトータルシェービ<br> 
				ングは、メンズの素肌をどこまで<br> 
				も清潔に、限りなくクリアに仕上<br> 
				げます。<br> 
				これまでの爽快感だけに終わら<br> 
				ない、男の素肌を磨くトータル<br> 
				シェービング、もう「ヒゲ剃り」<br> 
				なんて言わせません。<br> 
				クリアに磨け！メンズの素肌。
				</p>
				<ul>
					<li><img src="/common/images/shaving/img_05_01.jpg" /></li>
					<li><img src="/common/images/shaving/img_05_02.jpg" /></li>
				</ul>
				<h3><img src="/common/images/shaving/txt_05.png" /></h3>
			</section>


			
			<section class="sec_06">
				<p class="center"><img src="/common/images/shaving/ico_hart06.png" /></p>
				<br><br>
				<h2><img src="/common/images/shaving/ttl_06.png" /></h2>
				<img src="/common/images/shaving/img_06_01.jpg" />
				<p>
				シェービングは、従来の男性のヒゲを剃るイメージから、女性の素肌を美しく仕上げるレディースシェービング、そしてウエディングシーンに欠かせないブライダルシェービングまで、透明感のある輝く素肌を演出する技術としてすっかり定着しました。<br>
				県理美で学べる「トータルシェービング」は、シェービングとスキンケアを融合したちょっと贅沢な内容です。授業は学生が毎回ペアを組んで行うので、学生は実習を重ねるごとに肌がきれいになっていくうれしい特典付き！<br>
				こうしたサロンと同じ実践的な授業ができるのも、県理美には「ジョブサロン・beat」という素晴らしい教室があるからなんです。楽しく、そして真剣に取り組む学生の表情を見てくださいね。
				</p>
				<table>
					<tr>
						<td>
						<img src="/common/images/shaving/img_06_02.jpg" />
						<p>何をしていると思いますか？　爪の汚れをチェッ<br>クしています。細部まで実践そのものの授業です。</p>
						</td>
						<td>
						<img src="/common/images/shaving/img_06_03.jpg" />
						<p>授業前のチームミーティング。楽しそうですね。<br>理容科はアットホームな雰囲気がGOOD！</p>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<img src="/common/images/shaving/img_06_04.jpg" />
						<p>トータルシェービングの授業では男女の差なくクレンジングやマッサージをしっかりと学びます。全国の専門学校の男子学生で一番スキンケアに精通しているのは県理美・理容科の男子でしょう。</p>
						</td>
					</tr>
				</table>
				
				<p class="center"><img src="/common/images/shaving/ico_hart06.png" /></p>
				<p class="center">「面白い、夢中になれる！」これこそ、トータルシェービングの一番の魅力です。</p>
				<table>
				<tr>
				<td>
				<img src="/common/images/shaving/img_06_05.jpg" />
				<img src="/common/images/shaving/img_06_06.jpg" />
				<p>
				ひとたびシェービングが始まるとこの表情です。真剣そのもの。ちなみに2 年生は「ここにも、そこにも、すみずみにまで心と技が届きます」ができるようになります。トータルシェービングの実力です。
				</p>
				</td>
				</tr>
				</table>
				<p class="center"><img src="/common/images/shaving/ico_hart06.png" /></p>
				<br>
				<img src="/common/images/shaving/img_06_07.jpg" />
				<img src="/common/images/shaving/img_06_08.jpg" />
				<img src="/common/images/shaving/img_06_09.jpg" />
				<p>
				トータルシェービングの授業はスタートしてまだ半年ほどですが、学生は日に日に実力がアップしています。これからはウエディングを彩るブライダルシェービングを中心に、高度なスキンケア技術も学んでいくので、みんな期待に胸をふくらませているところ。<br>
				ところで、県理美・理容科の魅力はシェービングだけではないんですよ。昨年、理美容の学生選手権・ミディアムカット部門で日本一なった学生も県理美の理容科生。その学生を育てた先生がカットの授業を担当しているからヘアの技術も県理美の理容科はピカイチなんです。<br>
				技術指導、設備、そしてアットホームな雰囲気と、３拍子そろった県理美の理容科です。
				</p>
				
				<br><br><br>
				<p class="center"><img src="/common/images/shaving/img_06_10.png" /></p>
			</section>

			
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>