<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>アクセス | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="access";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/access/img_main.jpg" alt="アクセス" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href="/"><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">アクセス</li>
			</ul>
		</div>
		<div id="mainarea" class="access_content">
			<iframe width="685" height="375" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.jp/maps?hl=ja&amp;ie=UTF8&amp;q=%E5%AF%8C%E5%B1%B1%E7%9C%8C%E7%90%86%E5%AE%B9%E7%BE%8E%E5%AE%B9%E5%B0%82%E9%96%80%E5%AD%A6%E6%A0%A1&amp;fb=1&amp;gl=jp&amp;hq=%E5%AF%8C%E5%B1%B1%E7%9C%8C%E7%90%86%E5%AE%B9%E7%BE%8E%E5%AE%B9%E5%B0%82%E9%96%80%E5%AD%A6%E6%A0%A1&amp;hnear=%E5%AF%8C%E5%B1%B1%E7%9C%8C%E5%AF%8C%E5%B1%B1%E5%B8%82&amp;cid=0,0,14230240596193584921&amp;brcurrent=3,0x5ff7f20918176af5:0x6d5abf97791f1de3,0&amp;source=embed&amp;t=m&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
			<table>
				<tbody>
				<tr>
						<th>学校名</th>
						<td>
						学校法人 富山県理容美容学校<br>
						<span class="access_big">富山県理容美容専門学校</span>
						</td>
				</tr>
				<tr>
						<th>所在地</th>
						<td>〒930-0804 富山県富山市下新町32-26</td>
				</tr>
				<tr>
						<th>電話番号</th>
						<td>076-432-3037<span class="access_ico">0120-190-135</span></td>
				</tr>
				<tr>
						<th>ファックス番号</th>
						<td>076-432-3046</td>
				</tr>
				<tr>
						<th>メールアドレス</th>
						<td>info@toyama-bb.ac.jp<br>
						<a href="/inquiry/" class="to_inquiry">お問い合わせフォームはこちら</a>
						</td>
				</tr>
				</tbody>
				<tfoot>
				<tr>
						<th>携帯サイトURL</th>
						<td>
						<img src="/common/images/access/img_qr.jpg" /><br>
						<a href="http://www.toyama-bb.ac.jp/mb/index.html" target="_blank">http://www.toyama-bb.ac.jp/mb/index.html</a></td>
				</tr>
				</tfoot>
			</table>
			<img src="/common/images/access/photo.jpg" alt="" />
		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>