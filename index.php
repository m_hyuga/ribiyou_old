<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/index.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/sp/common/js/jquery.xdomainajax.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>
<link rel="stylesheet" type="text/css" href="/common/js/jquery.bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type='text/javascript' src='http://www.google.com/jsapi'></script>
<script type="text/javascript">
	var xmlUrl = "http://feedblog.ameba.jp/rss/ameblo/fjp24622niftycom/rss20.xml";  //feedのURL
	var setNum = 5; //表示件数
	var setID = "feed"; //表示させる箇所のID
	google.load("feeds", "1");
	function initialize() {
			var html = '';
			var feed = new google.feeds.Feed(xmlUrl);
			feed.setNumEntries(setNum);
			feed.load(function(result) {
					if (!result.error){
							var container = document.getElementById(setID);
							for (var i = 0; i < result.feed.entries.length; i++) {
									var entry = result.feed.entries[i];
									var title = entry.title;    //記事タイトル取得
									var link = entry.link;      //記事のリンクを取得
									//日付を取得し年月日を整形
									var publishedDate = entry.publishedDate;
									var pubDD = new Date(publishedDate);
									yy = pubDD.getYear();if (yy < 2000) { yy += 1900; }
									mm = pubDD.getMonth() + 1;if(mm < 10) { mm = "0" + mm; }
									dd = pubDD.getDate();if(dd < 10) { dd = "0" + dd; }
									var pubDate = yy +'.'+ mm +'.'+ dd ;
									//カテゴリ要素がある場合は取得
									for (var j = 0; j < entry.categories.length; j++) {
											var categorie = entry.categories[j];
									}
									//表示する部分を整形
									html += '<li><dl class="cf"><dd>' + pubDate + '</dd><dd class="newstitle"><a href="' + link + '" target="_blank">' + title +'</a></dd></dl></li>';
							}
							container.innerHTML = html;
					}
			});
	}
	google.setOnLoadCallback(initialize);
</script>
</head>
<body id="pagetop">
	<?php $pageID="top";
	if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");
	?>
<div id="main">
	<div id="slide">
		<section>
			<ul class="bxslider">
				<li class="slide01"><p><a href="/opencampus/"><img src="/common/images/top/slide01.jpg" alt="" /></a></p></li>
				<li class="slide02"><p><a href="/course/"><img src="/common/images/top/slide02.png" alt="" /></a></p></li>
				<li class="slide03"><p><a href="/inquiry/"><img src="/common/images/top/slide03.png" alt="" /></a></p></li>
			</ul>
			<div class="pager-slider">
				<p class="prev-slider"></p>
				<p class="next-slider"></p>
			</div>
		</section>
	</div>
	<div id="banner">
		<section>
			<ul class="cf">
				<li><a href="/course/barber.php"><img src="/common/images/top/bnr_ribiyo_off.png" alt="理容科" /></a></li>
				<li><a href="/course/beauty.php"><img src="/common/images/top/bnr_biyoka_off.png" alt="美容科" /></a></li>
				<li><a href="/course/esthetic.php"><img src="/common/images/top/bnr_esthe_off.png" alt="エステティック科" /></a></li>
				<li><a href="/course/total.php"><img src="/common/images/top/bnr_totalbearuty_off.png" alt="トータルビューティー科" /></a></li>
			</ul>
		</section>
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
			<div class="movie_area">
				<section>
					<h2><img src="/common/images/common/sidenav/ttl_movie.png" alt="動画コンテンツ" /></h2>
					<p class="movie_btn"><a href="https://www.youtube.com/user/toyamakenribi" target="_blank"><img src="/common/images/common/sidenav/btn_movie.png" alt="一覧" class="over" /></a></p>
					<p class="movie_images"><a href="https://www.youtube.com/user/toyamakenribi" target="_blank"><img src="/common/images/common/sidenav/pic_movie.jpg" alt="" class="over" /></a></p>
				</section>
			</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="mainarea" class="top_content">
			<ul class="banner_area cf">
				<li><a href="/monitor/"><img src="/common/images/top/bnr_monitor.jpg" alt="エステティック無料モニター大募集!!" class="over" /></a></li>
				<li><a href="/support/hojo.php"><img src="/common/images/top/bnr_hojo.jpg" alt="家賃補助制度スタート！" class="over" /></a></li>
				<li><a href="/opencampus/"><img src="/common/images/top/bnr_opencampus.png" alt="オープンキャンパス" class="over" /></a></li>
				<li class="left"><a href="https://www.youtube.com/watch?v=wcJKfcaqTuc" target="_blank"><img src="/common/images/top/bnr_taikai.jpg" alt="全国理容美容学生技術大会 優勝&入賞！！" class="over" /></a></li>
				<li class="right"><a href="http://www.toyama-bb.ac.jp/news/-pc/100-1.php"><img src="/common/images/top/bnr_zeningokaku.jpg" alt="国家試験全員合格!!" class="over" /></a></li>
				<li class="left"><a href="/parents/"><img src="/common/images/top/bnr_parent.png" alt="保護者の方へ" class="over" /></a></li>
				<li class="right"><a href="/placement/"><img src="/common/images/top/bnr_saiyotanto.png" alt="採用担当の方へ" class="over" /></a></li>
			</ul>
			<div class="news_area">
				<h2><a href="/news/"><img src="/common/images/top/btn_news.png" alt="一覧へ" /></a></h2>
					<MTEntries include_blogs="2" category="お知らせPC版" lastn="10">
					<MTSetVarBlock name="date"><$MTEntryDate format="%Y%m%d%I%M%S"$></MTSetVarBlock>
					<MTSetVarBlock name="entry{$date}">
					<li class="news"><a href="<$MTEntryPermalink$>">
					<dl class="cf"><dd><$MTEntryDate format="%Y.%m.%d"$></dd>
					<dd class="newstitle"><$MTEntryTitle$></dd></dl>
					</a></li>
					</MTSetVarBlock>
					</MTEntries>
                                
					<MTEntries include_blogs="4" category="イベントレポートPC版" lastn="10">
					<MTSetVarBlock name="date"><$MTEntryDate format="%Y%m%d%I%M%S"$></MTSetVarBlock>
					<MTSetVarBlock name="entry{$date}">
					<li class="event"><a href="<$MTEntryPermalink$>">
					<dl class="cf"><dd><$MTEntryDate format="%Y.%m.%d"$></dd>
					<dd class="newstitle"><$MTEntryTitle$></dd></dl>
					</a></li>
					</MTSetVarBlock>
					</MTEntries>
                                
					<ul>
					<MTLoop name="entry" sort_by="key reverse">
					<MTIf name="__counter__" le="7">
					<$MTVar name="__value__"$>
					</MTIf>
					</MTLoop>
					</ul>

			</div><!--.news_area-->
				<div class="bottommap_area">
			<img src="/common/images/top/bnr_movie.jpg" usemap="#map_1" border="0">
			<map name="map_1">
			　<area shape="rect" coords="120,65,190,90" href="http://school.js88.com/success/movie/js_movie.asp?c_id=1&m_id=0101" alt="美容師" target="_blank">
			　<area shape="rect" coords="200,65,272,90" href="http://school.js88.com/success/movie/js_movie.asp?c_id=1&m_id=0102" alt="理容師" target="_blank">
			　<area shape="rect" coords="277,65,453,90" href="http://school.js88.com/success/movie/js_movie.asp?c_id=1&m_id=0103" alt="ヘアメイクアーティスト" target="_blank">
			　<area shape="rect" coords="460,65,610,90" href="http://school.js88.com/success/movie/js_movie.asp?c_id=1&m_id=0105" alt="ネイルアーティスト" target="_blank">
			</map>
			</div>
		</div><!--#mainarea-->
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>