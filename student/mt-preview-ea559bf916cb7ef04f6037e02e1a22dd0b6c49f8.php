<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>在校生の方へ | 富山県理容美容専門学校</title>
<meta name="description" content="理容、美容、エステティック、ネイル、メイクの真のプロを目指す富山県理容美容専門学校">
<meta name="keywords" content="富山,理容,美容,専門学校,エステティック,ネイル,メイク">
<link rel="stylesheet" type="text/css" href="/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="/common/css/menu.css" />
<link rel="stylesheet" type="text/css" href="/common/css/other.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/rollover2.js"></script>

</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body id="pagetop">
		<?php $pageID="opencampus";
				if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/nav.inc");?>
<div id="main">
	<div id="titlebanner">
	<img src="/common/images/student/img_main.jpg" alt="在校生の方へ" />
	</div>
	<div id="content" class="cf">
		<div id="sidenavi">
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/sidebar.inc");
		?>
		</div>
		<div id="pankuzu_area">
			<ul>
				<li class="pankuzu_home"><a href=""><img src="/common/images/common/ico_home.png" alt="HOME" class="over" /></a></li>
				<li class="pankuzu_next">在校生の方へ</li>
			</ul>
		</div>
		<div id="mainarea" class="student_content">
				<p>
				各種書類のダウンロードと学校からのお知らせをご案内しています。<br>
				緊急時などもこちらに掲載していきますので、ブックマーク等お願いします。
				</p>
				
				<h2><img src="/common/images/student/ttl_news.png" alt="お知らせ" /></h2>
				<ul class="news_list">
					
					<div class="section1">
<h4>お知らせ</h4>
<div class="newstxt">
<p><div>現在、お知らせはありません。</div> 	
</p>
</div>
<!--//section1END--></div>
					
				</ul>
				
				<h2><img src="/common/images/student/ttl_download.png" alt="書類ダウンロード" /></h2>
				
				
				<ul class="down_list">
				
				<li><span>●</span>登校許可証明書<a href="http://www.toyama-bb.ac.jp/student/download/post.php" class="btn_down over">ダウンロード</a></li>
				
				
				
				<li><span>●</span>在学証明書・卒業見込証明書 交付願<a href="http://www.toyama-bb.ac.jp/student/download/post3.php" class="btn_down over">ダウンロード</a></li>
				
				</ul>
				
				
		</div><!-- /display_none end -->

		</div>
	</div>
		<?php 
		if (strstr($_SERVER['SERVER_NAME'], 'mdm')){
			$_SERVER['DOCUMENT_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], "", $_SERVER['SCRIPT_FILENAME']);}
		require_once($_SERVER['DOCUMENT_ROOT']."/common/inc/footer.inc");
		?>
</div>
</body>
</html>